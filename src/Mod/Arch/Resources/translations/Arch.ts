<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>App::Property</name>
    <message>
        <location filename="../../ArchAxis.py" line="129"/>
        <source>The intervals between axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="136"/>
        <source>The angles of each axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="143"/>
        <source>The label of each axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="150"/>
        <source>An optional custom bubble number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="157"/>
        <source>The length of the axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="172"/>
        <source>If not zero, the axes are not represented as one full line but as two lines of the given length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="271"/>
        <source>The size of the axis bubbles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="279"/>
        <source>The numbering style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="298"/>
        <source>The type of line to draw this axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="310"/>
        <source>Where to add bubbles to this axis: Start, end, both or none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="327"/>
        <source>The line width to draw this axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="335"/>
        <source>The color of this axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="343"/>
        <source>The number of the first axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="351"/>
        <source>The font to use for texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="359"/>
        <source>The font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="367"/>
        <source>If true, show the labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="376"/>
        <source>A transformation to apply to each label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="226"/>
        <source>The base object this component is built upon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="235"/>
        <source>The object this component is cloning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="244"/>
        <location filename="../../ArchSite.py" line="725"/>
        <source>Other shapes that are appended to this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="253"/>
        <location filename="../../ArchSite.py" line="734"/>
        <source>Other shapes that are subtracted from this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="262"/>
        <location filename="../../ArchBuildingPart.py" line="388"/>
        <source>An optional description for this component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="271"/>
        <location filename="../../ArchBuildingPart.py" line="397"/>
        <source>An optional tag for this component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="281"/>
        <source>An optional standard (OmniClass, etc...) code for this component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="288"/>
        <source>A material for this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="304"/>
        <source>Specifies if moving this object moves its base instead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="317"/>
        <source>Specifies if this object must move together when its host is moved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="329"/>
        <source>The area of all vertical faces of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="340"/>
        <location filename="../../ArchSite.py" line="744"/>
        <source>The area of the projection of this object onto the XY plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="350"/>
        <source>The perimeter length of the horizontal area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="361"/>
        <source>An optional higher-resolution mesh or shape for this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="371"/>
        <source>An optional axis or axis system on which this object should be duplicated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1414"/>
        <source>Use the material color as this object&apos;s shape color, if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="66"/>
        <source>A single section of the fence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="74"/>
        <source>A single fence post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="82"/>
        <source>The Path the fence should follow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="92"/>
        <source>The number of sections the fence is built of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="103"/>
        <source>The number of posts used to build the fence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="312"/>
        <source>When true, the fence will be colored like the original post and section.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="629"/>
        <source>The base terrain of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="639"/>
        <source>The street and house number of this site, with postal box or apartment number if needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="648"/>
        <source>The postal or zip code of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="655"/>
        <source>The city of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="664"/>
        <source>The region, province or county of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="671"/>
        <source>The country of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="678"/>
        <location filename="../../ArchSite.py" line="685"/>
        <source>The latitude of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="695"/>
        <source>Angle between the true North and the North direction in this document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="707"/>
        <source>The elevation of level 0 of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="716"/>
        <source>A url that shows this site in a mapping website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="753"/>
        <source>The perimeter length of this terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="762"/>
        <source>The volume of earth to be added to this terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="772"/>
        <source>The volume of earth to be removed from this terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="782"/>
        <source>An extrusion vector to use when performing boolean operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="792"/>
        <source>Remove splitters from the resulting shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="802"/>
        <source>An optional offset between the model (0,0,0) origin and the point indicated by the geocoordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchIFC.py" line="83"/>
        <location filename="../../ArchSite.py" line="811"/>
        <source>The type of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="822"/>
        <source>The time zone where this site is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="832"/>
        <source>An optional EPW File for the location of this site. Refer to the Site documentation to know how to obtain one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1045"/>
        <source>Show wind rose diagram or not. Uses solar diagram scale. Needs Ladybug module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1052"/>
        <source>Show solar diagram or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1059"/>
        <source>The scale of the solar diagram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1067"/>
        <source>The position of the solar diagram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1074"/>
        <source>The color of the solar diagram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1085"/>
        <source>When set to &apos;True North&apos; the whole geometry will be rotated to match the true north of this site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1094"/>
        <source>Show compass or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1103"/>
        <source>The rotation of the Compass relative to the Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1113"/>
        <source>The position of the Compass relative to the Site placement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="1123"/>
        <source>Update the Declination value based on the compass rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="216"/>
        <source>The diameter of the bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="226"/>
        <source>The distance between the border of the beam and the first bar (concrete cover).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="236"/>
        <source>The distance between the border of the beam and the last bar (concrete cover).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="243"/>
        <source>The amount of bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="250"/>
        <source>The spacing between the bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="261"/>
        <source>The total distance to span the rebars over. Keep 0 to automatically use the host shape size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="271"/>
        <source>The direction to use to spread the bars. Keep (0,0,0) for automatic direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="281"/>
        <source>The fillet to apply to the angle of the base profile. This value is multiplied by the bar diameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="288"/>
        <source>List of placement of all the bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="297"/>
        <source>The structure object that hosts this rebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="304"/>
        <source>The custom spacing of rebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="311"/>
        <source>Length of a single rebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="319"/>
        <source>Total length of all rebars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="327"/>
        <source>The rebar mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="714"/>
        <source>Shape of rebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="128"/>
        <source>The axes this system is made of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="135"/>
        <source>The placement of this axis system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="338"/>
        <location filename="../../ArchMaterial.py" line="924"/>
        <source>A description for this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="347"/>
        <location filename="../../ArchEquipment.py" line="394"/>
        <source>A standard code (MasterFormat, OmniClass,...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="357"/>
        <source>A URL where to find information about this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="366"/>
        <source>The transparency value of this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="373"/>
        <source>The color of this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="382"/>
        <source>The color of this material when cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="930"/>
        <source>The list of layer names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="936"/>
        <source>The list of layer materials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="942"/>
        <source>The list of layer thicknesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="303"/>
        <source>The length of these stairs, if no baseline is defined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="310"/>
        <source>The width of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="317"/>
        <source>The total height of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="327"/>
        <source>The alignment of these stairs on their baseline, if applicable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="340"/>
        <source>The width of a Landing (Second edge and after - First edge follows Width property)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="352"/>
        <source>The number of risers in these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="361"/>
        <source>The depth of the treads of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="371"/>
        <source>The height of the risers of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="379"/>
        <source>The size of the nosing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="386"/>
        <source>The thickness of the treads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="396"/>
        <source>The Blondel ratio indicates comfortable stairs and should be between 62 and 64cm or 24.5 and 25.5in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="405"/>
        <source>The thickness of the risers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="415"/>
        <source>The depth of the landing of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="426"/>
        <source>The depth of the treads of these stairs - Enforced regardless of Length or edge&apos;s Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="436"/>
        <source>The height of the risers of these stairs - Enforced regardless of Height or edge&apos;s Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="446"/>
        <source>The direction of flight after landing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="466"/>
        <source>The &apos;absolute&apos; top level of a flight of stairs leads to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="474"/>
        <location filename="../../ArchStairs.py" line="482"/>
        <source>The &apos;left outline&apos; of stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="540"/>
        <source>The &apos;left outline&apos; of all segments of stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="550"/>
        <source>The &apos;right outline&apos; of all segments of stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="603"/>
        <source>The type of landings of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="613"/>
        <source>The type of winders in these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="623"/>
        <source>The type of structure of these stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="634"/>
        <source>The thickness of the massive structure or of the stringers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="641"/>
        <source>The width of the stringers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="651"/>
        <source>The offset between the border of the stairs and the structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="661"/>
        <location filename="../../ArchStairs.py" line="1694"/>
        <source>The overlap of the stringers above the bottom of the treads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="670"/>
        <source>The thickness of the lower floor slab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="679"/>
        <source>The thickness of the upper floor slab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="689"/>
        <source>The type of connection between the lower slab and the start of the stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="704"/>
        <source>The type of connection between the end of the stairs and the upper floor slab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="100"/>
        <source>The base file this component is built upon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="109"/>
        <source>The part to use from the base file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="119"/>
        <source>The way the referenced objects are included in the current document. &apos;Normal&apos; includes the shape, &apos;Transient&apos; discards the shape when the object is switched off (smaller filesize), &apos;Lightweight&apos; does not import the shape but only the OpenInventor representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="136"/>
        <source>Fuse objects of same material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="444"/>
        <source>The latest time stamp of the linked file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="455"/>
        <source>If true, the colors from the linked file will be kept updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1106"/>
        <location filename="../../ArchFloor.py" line="244"/>
        <source>The placement of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1113"/>
        <location filename="../../ArchBuildingPart.py" line="404"/>
        <source>The shape of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1123"/>
        <source>The objects that must be considered by this section plane. Empty means the whole document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1133"/>
        <source>If false, non-solids will be cut too, with possible wrong results.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1144"/>
        <source>If True, resulting views will be clipped to the section plane area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1154"/>
        <source>If true, the color of the objects material will be used to fill cut areas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1165"/>
        <source>Geometry further than this value will be cut off. Keep zero for unlimited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1248"/>
        <source>The display length of this section plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1261"/>
        <source>The display height of this section plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1274"/>
        <source>The size of the arrows of this section plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1282"/>
        <source>The transparency of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1290"/>
        <location filename="../../ArchBuildingPart.py" line="660"/>
        <source>The line width of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1298"/>
        <location filename="../../ArchSectionPlane.py" line="1313"/>
        <source>Show the cut in the 3D view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1305"/>
        <source>The color of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1323"/>
        <source>The distance between the cut plane and the actual view cut (keep this a very small value but not zero)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1331"/>
        <source>Show the label in the 3D view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1338"/>
        <location filename="../../ArchSpace.py" line="657"/>
        <source>The name of the font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1346"/>
        <location filename="../../ArchSpace.py" line="673"/>
        <source>The size of the text font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1666"/>
        <location filename="../../ArchPanel.py" line="1086"/>
        <location filename="../../ArchPanel.py" line="1252"/>
        <source>The linked object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1673"/>
        <source>The rendering mode to use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1682"/>
        <source>If cut geometry is shown or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1689"/>
        <source>If cut geometry is filled or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1698"/>
        <location filename="../../ArchPanel.py" line="1095"/>
        <source>The line width of the rendered objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1708"/>
        <source>The size of the texts inside this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1719"/>
        <source>If checked, source objects are displayed regardless of being visible in the 3D model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1728"/>
        <source>The line color of the projected objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1737"/>
        <source>The color of the cut faces (if turned on)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="303"/>
        <source>The list of angles of the roof segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="313"/>
        <source>The list of horizontal length projections of the roof segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="323"/>
        <source>The list of IDs of the relative profiles of the roof segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="332"/>
        <source>The list of thicknesses of the roof segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="341"/>
        <source>The list of overhangs of the roof segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="351"/>
        <source>The list of calculated heights of the roof segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="361"/>
        <source>The face number of the base object used to build the roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="371"/>
        <source>The total length of the ridges and hips of the roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="381"/>
        <source>The total length of the borders of the roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="392"/>
        <source>Specifies if the direction of the roof should be flipped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="740"/>
        <source>The objects that host this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="747"/>
        <source>The components of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="758"/>
        <source>The depth of the hole that this window makes in its host object. If 0, the value will be calculated automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="768"/>
        <source>An optional object that defines a volume to be subtracted from hosts of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="775"/>
        <source>The width of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="782"/>
        <source>The height of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="791"/>
        <source>The normal direction of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="800"/>
        <source>The preset number this window is based on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="808"/>
        <source>The frame size of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="815"/>
        <source>The offset size of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="822"/>
        <source>The area of this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="829"/>
        <source>The width of louvre elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="836"/>
        <source>The space between louvre elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="845"/>
        <source>Opens the subcomponents that have a hinge defined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="855"/>
        <source>The number of the wire that defines the hole. If 0, the value will be calculated automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="864"/>
        <source>Shows plan opening symbols if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="873"/>
        <source>Show elevation opening symbols if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="1855"/>
        <source>The number of the wire that defines the hole. A value of 0 means automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="376"/>
        <source>The model description of this equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="385"/>
        <source>The URL of the product page of this equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="403"/>
        <source>Additional snap points for this equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="413"/>
        <source>The electric power needed by this equipment in Watts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchIFC.py" line="75"/>
        <source>IFC data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchIFC.py" line="92"/>
        <source>IFC properties of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchIFC.py" line="318"/>
        <location filename="../../ArchIFC.py" line="332"/>
        <source>Description of IFC attributes are not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuilding.py" line="287"/>
        <location filename="../../ArchBuildingPart.py" line="232"/>
        <location filename="../../ArchBuildingPart.py" line="261"/>
        <source>The type of this building</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="131"/>
        <source>The profile used to build this frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="141"/>
        <source>Specifies if the profile must be aligned with the extrusion wires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="152"/>
        <source>An offset vector between the base sketch and the frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="161"/>
        <source>Crossing point of the path on the profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="171"/>
        <source>An optional additional placement to add to the profile before extruding it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="181"/>
        <source>The rotation of the profile around its extrusion axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="188"/>
        <source>The type of edges to consider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="204"/>
        <source>If true, geometry is fused, otherwise a compound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFloor.py" line="227"/>
        <location filename="../../ArchBuildingPart.py" line="350"/>
        <source>The height of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFloor.py" line="236"/>
        <location filename="../../ArchBuildingPart.py" line="379"/>
        <source>The computed floor area of this floor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="70"/>
        <source>The length of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="77"/>
        <source>The width of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="84"/>
        <source>The height of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="971"/>
        <location filename="../../ArchPrecast.py" line="93"/>
        <source>The structural nodes of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="128"/>
        <location filename="../../ArchPrecast.py" line="346"/>
        <location filename="../../ArchPrecast.py" line="528"/>
        <source>The size of the chamfer of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="135"/>
        <source>The dent length of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="142"/>
        <location filename="../../ArchPrecast.py" line="542"/>
        <source>The dent height of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="149"/>
        <location filename="../../ArchPrecast.py" line="385"/>
        <source>The dents of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="270"/>
        <source>The chamfer length of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="277"/>
        <source>The base length of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="353"/>
        <source>The groove depth of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="360"/>
        <source>The groove height of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="369"/>
        <source>The spacing between the grooves of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="378"/>
        <source>The number of grooves of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="535"/>
        <source>The dent width of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="654"/>
        <source>The type of this slab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="664"/>
        <source>The size of the base of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="673"/>
        <source>The number of holes in this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="682"/>
        <source>The major radius of the holes of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="691"/>
        <source>The minor radius of the holes of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="700"/>
        <source>The spacing between the holes of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="813"/>
        <source>The length of the down floor of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="822"/>
        <source>The number of risers in this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="829"/>
        <source>The riser height of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="836"/>
        <source>The tread depth of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="199"/>
        <source>An optional host object for this curtain wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="209"/>
        <source>The height of the curtain wall, if based on an edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="217"/>
        <source>The number of vertical mullions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="228"/>
        <source>If the profile of the vertical mullions get aligned with the surface or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="238"/>
        <source>The number of vertical sections of this curtain wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="253"/>
        <source>The height of the vertical mullions profile, if no profile is used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="264"/>
        <source>The width of the vertical mullions profile, if no profile is used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="275"/>
        <source>A profile for vertical mullions (disables vertical mullion size)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="282"/>
        <source>The number of horizontal mullions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="293"/>
        <source>If the profile of the horizontal mullions gets aligned with the surface or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="303"/>
        <source>The number of horizontal sections of this curtain wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="318"/>
        <source>The height of the horizontal mullions profile, if no profile is used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="329"/>
        <source>The width of the horizontal mullions profile, if no profile is used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="340"/>
        <source>A profile for horizontal mullions (disables horizontal mullion size)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="347"/>
        <source>The number of diagonal mullions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="358"/>
        <source>The size of the diagonal mullions, if any, if no profile is used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="369"/>
        <source>A profile for diagonal mullions, if any (disables horizontal mullion size)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="376"/>
        <source>The number of panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="384"/>
        <source>The thickness of the panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="394"/>
        <source>Swaps horizontal and vertical lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="404"/>
        <source>Perform subtractions between components so none overlap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="413"/>
        <source>Centers the profile over the edges or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="424"/>
        <source>The vertical direction reference to be used by this object to deduce vertical/horizontal directions. Keep it close to the actual vertical direction of your curtain wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="284"/>
        <source>Outside Diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="290"/>
        <source>Wall thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="319"/>
        <location filename="../../ArchProfile.py" line="397"/>
        <location filename="../../ArchProfile.py" line="431"/>
        <location filename="../../ArchProfile.py" line="495"/>
        <source>Width of the beam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="325"/>
        <location filename="../../ArchProfile.py" line="403"/>
        <location filename="../../ArchProfile.py" line="437"/>
        <location filename="../../ArchProfile.py" line="501"/>
        <source>Height of the beam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="331"/>
        <source>Thickness of the web</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="337"/>
        <source>Thickness of the flanges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="443"/>
        <source>Thickness of the sides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="507"/>
        <source>Thickness of the webs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="513"/>
        <source>Thickness of the flange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="225"/>
        <source>The diameter of this pipe, if not based on a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="234"/>
        <source>The length of this pipe, if not based on an edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="243"/>
        <source>An optional closed profile to base this pipe on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="250"/>
        <source>Offset from the start point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="257"/>
        <source>Offset from the end point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="267"/>
        <source>The wall thickness of this pipe, if not based on a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="470"/>
        <source>The curvature radius of this connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="479"/>
        <source>The pipes linked by this connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="486"/>
        <source>The type of this connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="885"/>
        <source>The length of this wall. Not used if this wall is based on an underlying object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="895"/>
        <source>The width of this wall. Not used if this wall is based on a face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="907"/>
        <source>This overrides Width attribute to set width of each segment of wall.  Ignored if Base object provides Widths information, with getWidths() method.  (The 1st value override &apos;Width&apos; attribute for 1st segment of wall; if a value is zero, 1st value of &apos;OverrideWidth&apos; will be followed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="918"/>
        <source>This overrides Align attribute to set Align of each segment of wall.  Ignored if Base object provides Aligns information, with getAligns() method.  (The 1st value override &apos;Align&apos; attribute for 1st segment of wall; if a value is not &apos;Left, Right, Center&apos;, 1st value of &apos;OverrideAlign&apos; will be followed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="929"/>
        <source>The height of this wall. Keep 0 for automatic. Not used if this wall is based on a solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="939"/>
        <source>The area of this wall as a simple Height * Length calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="950"/>
        <source>The alignment of this wall on its base object, if applicable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="962"/>
        <location filename="../../ArchPanel.py" line="626"/>
        <location filename="../../ArchWall.py" line="961"/>
        <source>The normal extrusion direction of this object (keep (0,0,0) for automatic normal)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="971"/>
        <source>The face number of the base object used to build this wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="981"/>
        <source>The offset between this wall and its baseline (only for left and right alignments)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="997"/>
        <source>Enable this to make the wall generate blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1004"/>
        <source>The length of each block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1011"/>
        <source>The height of each block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1020"/>
        <source>The horizontal offset of the first line of blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1030"/>
        <source>The horizontal offset of the second line of blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1039"/>
        <source>The size of the joints between each block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1046"/>
        <source>The number of entire blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1054"/>
        <source>The number of broken blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="174"/>
        <source>The angle of the truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="182"/>
        <source>The slant type of this truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="192"/>
        <source>The normal direction of this truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="202"/>
        <source>The height of the truss at the start position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="212"/>
        <source>The height of the truss at the end position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="222"/>
        <source>An optional start offset for the top strut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="231"/>
        <source>An optional end offset for the top strut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="241"/>
        <source>The height of the main top and bottom elements of the truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="252"/>
        <source>The width of the main top and bottom elements of the truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="262"/>
        <source>The type of the middle element of the truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="270"/>
        <source>The direction of the rods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="278"/>
        <source>The diameter or side of the rods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="286"/>
        <source>The number of rod sections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="296"/>
        <source>If the truss has a rod at its endpoint or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="303"/>
        <source>How to draw the rods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="105"/>
        <source>The description column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="112"/>
        <source>The values column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="119"/>
        <source>The units column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="126"/>
        <source>The objects column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="133"/>
        <source>The filter column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="143"/>
        <source>If True, a spreadsheet containing the results is recreated when needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="152"/>
        <source>The spreadsheet to print the results to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="162"/>
        <source>If True, additional lines with each individual object are added to the results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="298"/>
        <source>The objects that make the boundaries of this space object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="307"/>
        <source>The computed floor area of this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="316"/>
        <source>The finishing of the floor of this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="325"/>
        <source>The finishing of the walls of this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="334"/>
        <source>The finishing of the ceiling of this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="344"/>
        <source>Objects that are included inside this space, such as furniture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="351"/>
        <source>The type of this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="359"/>
        <source>The thickness of the floor finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="369"/>
        <source>The number of people who typically occupy this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="379"/>
        <source>The electric power needed to light this space in Watts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="389"/>
        <source>The electric power needed by the equipment of this space in Watts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="399"/>
        <source>If True, Equipment Power will be automatically filled by the equipment included in this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="408"/>
        <source>The type of air conditioning of this space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="418"/>
        <source>Specifies if this space is internal or external</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="649"/>
        <source>The text to show. Use $area, $label, $tag, $floor, $walls, $ceiling to insert the respective data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="665"/>
        <source>The color of the area text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="683"/>
        <source>The size of the first line of text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="693"/>
        <source>The space between the lines of text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="704"/>
        <source>The position of the text. Leave (0,0,0) for automatic position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="711"/>
        <source>The justification of the text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="723"/>
        <source>The number of decimals to use for calculated texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="731"/>
        <source>Show the unit suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="932"/>
        <location filename="../../ArchPanel.py" line="506"/>
        <source>The length of this element, if not based on a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="942"/>
        <location filename="../../ArchPanel.py" line="516"/>
        <source>The width of this element, if not based on a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="525"/>
        <source>The thickness or extrusion depth of this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="532"/>
        <source>The number of sheets to use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="542"/>
        <source>The offset between this panel and its baseline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="551"/>
        <source>The length of waves for corrugated elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="560"/>
        <source>The height of waves for corrugated elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="570"/>
        <source>The horizontal offset of waves for corrugated elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="579"/>
        <source>The direction of waves for corrugated elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="588"/>
        <source>The type of waves for corrugated elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="598"/>
        <source>If the wave also affects the bottom side or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="605"/>
        <source>The area of this panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1001"/>
        <location filename="../../ArchPanel.py" line="615"/>
        <source>The facemaker type to use to build the profile of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1103"/>
        <source>The color of the panel outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1111"/>
        <location filename="../../ArchPanel.py" line="1270"/>
        <location filename="../../ArchPanel.py" line="1627"/>
        <source>The size of the tag text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1118"/>
        <source>The color of the tag text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1126"/>
        <source>The X offset of the tag text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1134"/>
        <source>The Y offset of the tag text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1142"/>
        <location filename="../../ArchPanel.py" line="1295"/>
        <location filename="../../ArchPanel.py" line="1652"/>
        <source>The font of the tag text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1262"/>
        <source>The text to display. Can be %tag%, %label% or %description% to display the panel tag or label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1281"/>
        <location filename="../../ArchPanel.py" line="1638"/>
        <source>The position of the tag text. Keep (0,0,0) for center position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1288"/>
        <location filename="../../ArchPanel.py" line="1645"/>
        <source>The rotation of the tag text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1308"/>
        <location filename="../../ArchPanel.py" line="1689"/>
        <source>If True, the object is rendered as a face, if possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1318"/>
        <source>The allowed angles this object can be rotated to when placed on sheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1329"/>
        <source>An offset value to move the cut plane from the center point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1516"/>
        <location filename="../../ArchPanel.py" line="1873"/>
        <source>A margin inside the boundary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1525"/>
        <location filename="../../ArchPanel.py" line="1882"/>
        <source>Turns the display of the margin on/off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1613"/>
        <source>The linked Panel cuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1620"/>
        <source>The tag text to display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1656"/>
        <source>The font file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1662"/>
        <source>The width of the sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1670"/>
        <source>The height of the sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1678"/>
        <source>The fill ratio of this sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1699"/>
        <source>Specifies an angle for the wood grain (Clockwise, 0 is North)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1708"/>
        <source>Specifies the scale applied to each panel view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1718"/>
        <source>A list of possible rotations for the nester</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="1892"/>
        <source>Turns the display of the wood grain texture on/off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="360"/>
        <source>If true, the height value propagates to contained objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="370"/>
        <source>The level of the (0,0,0) point of this level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="414"/>
        <source>This property stores an inventor representation for this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="425"/>
        <source>If true, only solids will be collected by this object when referenced from other files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="436"/>
        <source>A MaterialName:SolidIndexesList map that relates material names with solid indexes to be used when referencing this object from other files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="670"/>
        <source>An optional unit to express levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="679"/>
        <source>A transformation to apply to the level mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="689"/>
        <source>If true, show the level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="699"/>
        <source>If true, show the unit on the level tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="709"/>
        <source>If true, display offset will affect the origin mark too</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="718"/>
        <source>If true, the object&apos;s label is displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="726"/>
        <source>The font to be used for texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="734"/>
        <source>The font size of texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="742"/>
        <source>The individual face colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="754"/>
        <source>If true, when activated, the working plane will automatically adapt to this level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="765"/>
        <source>If set to True, the working plane will be kept on Auto mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="774"/>
        <source>Camera position data associated with this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="785"/>
        <source>If set, the view stored in this object will be restored on double-click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="795"/>
        <source>If True, double-clicking this object in the tree activates it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="807"/>
        <source>If this is enabled, the inventor representation of this object will be saved in the FreeCAD file, allowing to reference it in other files in lightweight mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="817"/>
        <source>A slot to save the inventor representation of this object, if enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="830"/>
        <source>If true, show the objects contained in this Building Part will adopt these line, color and transparency settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="837"/>
        <source>The line width of child objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="845"/>
        <source>The line color of child objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="861"/>
        <source>The shape color of child objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="877"/>
        <source>The transparency of child objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="886"/>
        <source>Cut the view above this level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="896"/>
        <source>The distance between the level plane and the cut line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="906"/>
        <source>Turn cutting on when activating this level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="918"/>
        <source>The capture box for newly created objects expressed as [XMin,YMin,ZMin,XMax,YMax,ZMax]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="925"/>
        <source>Turns auto group box on/off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="934"/>
        <source>Automatically set size from contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="943"/>
        <source>A margin to use when autosize is turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="842"/>
        <location filename="../../ArchStructure.py" line="1844"/>
        <source>An optional extrusion path for this element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="851"/>
        <source>The computed length of the extrusion path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="862"/>
        <source>Start offset distance along the extrusion path (positive: extend, negative: trim)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="872"/>
        <source>End offset distance along the extrusion path (positive: extend, negative: trim)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="882"/>
        <source>Automatically align the Base of the Structure perpendicular to the Tool axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="892"/>
        <source>X offset between the Base origin and the Tool axis (only used if BasePerpendicularToTool is True)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="902"/>
        <source>Y offset between the Base origin and the Tool axis (only used if BasePerpendicularToTool is True)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="912"/>
        <source>Mirror the Base along its Y axis (only used if BasePerpendicularToTool is True)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="922"/>
        <source>Base rotation around the Tool axis (only used if BasePerpendicularToTool is True)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="952"/>
        <source>The height or extrusion depth of this element. Keep 0 for automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="981"/>
        <source>A description of the standard profile this element is based upon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="991"/>
        <source>Offset distance between the centerline and the nodes line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1371"/>
        <source>If the nodes are visible or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1378"/>
        <source>The width of the nodes line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1385"/>
        <source>The size of the node points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1393"/>
        <source>The color of the nodes line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1401"/>
        <source>The type of structural node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1893"/>
        <source>Axes systems this structure is built on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1902"/>
        <source>The element numbers to exclude when this structure is based on axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1910"/>
        <source>If true the element are aligned with axes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch</name>
    <message>
        <location filename="../../ArchAxis.py" line="101"/>
        <source>Create Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="383"/>
        <location filename="../../ArchAxis.py" line="998"/>
        <location filename="../../ArchComponent.py" line="2290"/>
        <source>Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1993"/>
        <location filename="../../ArchWindow.py" line="2129"/>
        <location filename="../../ArchAxisSystem.py" line="384"/>
        <location filename="../../ArchSpace.py" line="1033"/>
        <location filename="../../ArchAxis.py" line="999"/>
        <location filename="../../ArchComponent.py" line="2273"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2130"/>
        <location filename="../../ArchAxisSystem.py" line="385"/>
        <location filename="../../ArchSpace.py" line="1025"/>
        <location filename="../../ArchAxis.py" line="1000"/>
        <location filename="../../ArchComponent.py" line="2274"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="1005"/>
        <source>Distances (mm) and angles (deg) between axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="1008"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="1009"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="1010"/>
        <source>Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="1011"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1111"/>
        <source>Error computing the shape of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1140"/>
        <source>has no solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1152"/>
        <source>has an invalid shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1156"/>
        <source>has a null shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1798"/>
        <source>Toggle subcomponents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="1928"/>
        <source>Closing Sketch edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="268"/>
        <location filename="../../ArchRoof.py" line="276"/>
        <location filename="../../ArchComponent.py" line="1954"/>
        <source>Please select a base object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2272"/>
        <source>Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2277"/>
        <source>Components of this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2280"/>
        <source>Base component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2283"/>
        <source>Additions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2286"/>
        <source>Subtractions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2289"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2141"/>
        <location filename="../../ArchComponent.py" line="2293"/>
        <source>Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2296"/>
        <source>Fixtures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2297"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2298"/>
        <source>Hosts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2301"/>
        <source>Edit IFC properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2304"/>
        <source>Edit standard code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2366"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2146"/>
        <location filename="../../ArchComponent.py" line="2367"/>
        <location filename="../../ArchCommands.py" line="1995"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="715"/>
        <location filename="../../ArchSchedule.py" line="741"/>
        <location filename="../../ArchComponent.py" line="2368"/>
        <location filename="../../ArchCommands.py" line="1996"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2375"/>
        <source>Add property...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2379"/>
        <source>Add property set...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2380"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2547"/>
        <source>New property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchComponent.py" line="2584"/>
        <source>New property set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindowPresets.py" line="637"/>
        <source>Door</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="563"/>
        <source>Please either select only Building objects or nothing at all!

Site is not allowed to accept any other object besides Building.

Other objects will be removed from the selection.

Note: You can change that in the preferences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="571"/>
        <source>There is no valid object in the selection.

Site creation aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="579"/>
        <source>Create Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importWebGL.py" line="917"/>
        <location filename="../../importOBJ.py" line="348"/>
        <location filename="../../importOBJ.py" line="393"/>
        <location filename="../../importJSON.py" line="63"/>
        <source>Successfully written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importOBJ.py" line="97"/>
        <location filename="../../importOBJ.py" line="118"/>
        <source>Found a shape containing curves, triangulating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importOBJ.py" line="491"/>
        <source>Successfully imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="464"/>
        <source>Invalid cutplane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="766"/>
        <source>is not closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="768"/>
        <source>is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="770"/>
        <source>doesn&apos;t contain any solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="778"/>
        <source>contains a non-closed solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="785"/>
        <source>contains faces that are not part of any solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1180"/>
        <source>Survey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1183"/>
        <source>Set description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1184"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1187"/>
        <source>Copy Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1188"/>
        <source>Copy Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1189"/>
        <source>Export CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="714"/>
        <location filename="../../ArchSchedule.py" line="739"/>
        <location filename="../../ArchCommands.py" line="1192"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="568"/>
        <location filename="../../ArchPrecast.py" line="1743"/>
        <location filename="../../ArchWall.py" line="580"/>
        <location filename="../../ArchCommands.py" line="1193"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1194"/>
        <source>Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1255"/>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="656"/>
        <location filename="../../ArchCommands.py" line="1300"/>
        <source>Export CSV File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1351"/>
        <source>Object doesn&apos;t have settable IFC Attributes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1359"/>
        <source>Disabling Brep force flag of object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1367"/>
        <location filename="../../ArchCommands.py" line="1375"/>
        <source>Enabling Brep force flag of object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1601"/>
        <source>Add space boundary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1609"/>
        <source>Grouping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1651"/>
        <source>Remove space boundary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1659"/>
        <source>Ungrouping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1703"/>
        <source>Split Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1748"/>
        <source>Mesh to Shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1856"/>
        <source>All good! No problems found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1929"/>
        <location filename="../../ArchCommands.py" line="1966"/>
        <source>Create Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="552"/>
        <location filename="../../ArchProfile.py" line="157"/>
        <location filename="../../ArchCommands.py" line="1993"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1994"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="716"/>
        <location filename="../../ArchSchedule.py" line="743"/>
        <location filename="../../ArchCommands.py" line="1997"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="2009"/>
        <source>The object doesn&apos;t have an IfcProperties attribute. Cancel spreadsheet creation for object:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="2040"/>
        <source>Create IFC properties spreadsheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="45"/>
        <location filename="../../InitGui.py" line="253"/>
        <location filename="../../InitGui.py" line="256"/>
        <source>Arch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProject.py" line="137"/>
        <source>Create Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="138"/>
        <location filename="../../ArchRebar.py" line="174"/>
        <source>Create Rebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="188"/>
        <source>Please select a base face on a structural object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="90"/>
        <source>Only axes must be selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="97"/>
        <source>Create Axis System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="103"/>
        <source>Please select at least one axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="388"/>
        <source>Axis system components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importGBXML.py" line="49"/>
        <location filename="../../importGBXML.py" line="56"/>
        <source>This exporter can currently only export one site object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importGBXML.py" line="113"/>
        <source>Error: Space &apos;%s&apos; has no Zone. Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="184"/>
        <source>Cutting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="226"/>
        <source>Cut Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="229"/>
        <source>Cut Plane options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="232"/>
        <source>Which side to cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="235"/>
        <source>Behind</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="236"/>
        <source>Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="133"/>
        <source>Create material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="176"/>
        <source>Create multi-material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2145"/>
        <location filename="../../ArchMaterial.py" line="1054"/>
        <location filename="../../ArchMaterial.py" line="1097"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="1055"/>
        <location filename="../../ArchMaterial.py" line="1098"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2148"/>
        <location filename="../../ArchMaterial.py" line="1056"/>
        <location filename="../../ArchMaterial.py" line="1099"/>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="1138"/>
        <source>New layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="1177"/>
        <source>Total thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="1191"/>
        <source>depends on the object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStairs.py" line="248"/>
        <source>Create Stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="864"/>
        <source>Create external reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importDAE.py" line="63"/>
        <source>pycollada not found, collada support is disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../import3DS.py" line="86"/>
        <location filename="../../importIFClegacy.py" line="1057"/>
        <location filename="../../importSH3D.py" line="73"/>
        <location filename="../../importDAE.py" line="140"/>
        <source>Error: Couldn&apos;t determine character encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importDAE.py" line="401"/>
        <source>file %s successfully created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="111"/>
        <location filename="../../ArchPanel.py" line="119"/>
        <location filename="../../ArchPanel.py" line="131"/>
        <source>View of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1081"/>
        <source>Create Section Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1992"/>
        <source>Section plane settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1998"/>
        <source>Remove highlighted objects from the list above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2001"/>
        <source>Add selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2008"/>
        <source>Add selected object(s) to the scope of this section plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2013"/>
        <source>Objects seen by this section plane:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2016"/>
        <source>Section plane placement:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2019"/>
        <source>Rotate X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2024"/>
        <source>Rotates the plane along the X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2027"/>
        <source>Rotate Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2032"/>
        <source>Rotates the plane along the Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2035"/>
        <source>Rotate Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2040"/>
        <source>Rotates the plane along the Z axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2041"/>
        <source>Resize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2046"/>
        <source>Resizes the plane to fit the objects in the list above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2049"/>
        <location filename="../../ArchWall.py" line="605"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="2054"/>
        <source>Centers the plane on the objects in the list above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="242"/>
        <location filename="../../ArchRoof.py" line="260"/>
        <source>Create Roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="273"/>
        <location filename="../../ArchRoof.py" line="985"/>
        <source>Unable to create a roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1179"/>
        <source>Roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1186"/>
        <source>Parameters of the roof profiles :
* Angle : slope in degrees relative to the horizontal.
* Run : horizontal distance between the wall and the ridge.
* Thickness : thickness of the roof.
* Overhang : horizontal distance between the eave and the wall.
* Height : height of the ridge above the base (calculated automatically).
* IdRel : Id of the relative profile used for automatic calculations.
---
If Angle = 0 and Run = 0 then the profile is identical to the relative profile.
If Angle = 0 then the angle is calculated so that the height is the same as the relative profile.
If Run = 0 then the run is calculated so that the height is the same as the relative profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1189"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1190"/>
        <source>Angle (deg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1191"/>
        <source>Run (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1192"/>
        <source>IdRel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1193"/>
        <source>Thickness (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1194"/>
        <source>Overhang (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="1195"/>
        <source>Height (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="97"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="236"/>
        <location filename="../../ArchWindow.py" line="267"/>
        <location filename="../../ArchWindow.py" line="319"/>
        <source>Create Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="287"/>
        <source>Choose a face on an existing object or select a preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="363"/>
        <source>Window not based on sketch. Window not aligned or resized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="382"/>
        <source>No Width and/or Height constraint in window sketch. Window not resized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="385"/>
        <source>No window found. Cannot continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="456"/>
        <source>Window options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="460"/>
        <source>Auto include in host object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="468"/>
        <source>Sill height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="512"/>
        <location filename="../../ArchStructure.py" line="559"/>
        <location filename="../../ArchProfile.py" line="164"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="1609"/>
        <source>This window has no defined opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="1919"/>
        <location filename="../../ArchWindow.py" line="1970"/>
        <location filename="../../ArchWindow.py" line="2170"/>
        <source>Get selected edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2076"/>
        <source>Unable to create component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2117"/>
        <source>Window elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2118"/>
        <source>Hole wire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2125"/>
        <source>The number of the wire that defines a hole in the host object. A value of zero will automatically adopt the largest wire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2128"/>
        <source>Pick selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2131"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2134"/>
        <source>Create/update component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2135"/>
        <source>Base 2D object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2138"/>
        <location filename="../../ArchWindow.py" line="2147"/>
        <source>Wires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2144"/>
        <source>Create new component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2149"/>
        <location filename="../../ArchPrecast.py" line="1749"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2150"/>
        <source>Hinge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2151"/>
        <source>Opening mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2152"/>
        <location filename="../../ArchWindow.py" line="2160"/>
        <source>+ default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2159"/>
        <source>If this is checked, the default Frame value of this window will be added to the value entered here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2167"/>
        <source>If this is checked, the default Offset value of this window will be added to the value entered here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2175"/>
        <source>Press to retrieve the selected edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2178"/>
        <source>Invert opening direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="2181"/>
        <source>Invert hinge position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="197"/>
        <source>You must select a base shape object and optionally a mesh object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="218"/>
        <source>Create Equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="265"/>
        <source>You must select exactly one base object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="271"/>
        <source>The selected object must be a mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="277"/>
        <source>This mesh has more than 1000 facets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="282"/>
        <source>This operation can take a long time. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="296"/>
        <source>The mesh has more than 500 facets. This will take a couple of minutes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="298"/>
        <source>Create 3 views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="75"/>
        <source>Shapes elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="76"/>
        <source>Choose which field provides shapes elevations:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="119"/>
        <source>No shape found in this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="150"/>
        <source>Shapefile module not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="154"/>
        <source>The shapefile python library was not found on your system. Would you like to download it now from &lt;a href=&quot;https://github.com/GeospatialPython/pyshp&quot;&gt;https://github.com/GeospatialPython/pyshp&lt;/a&gt;? It will be placed in your macros folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="163"/>
        <source>Error: Unable to download from:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="184"/>
        <source>Could not download shapefile module. Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="190"/>
        <source>Shapefile module not downloaded. Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="195"/>
        <source>Shapefile module not found. Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importSHP.py" line="202"/>
        <source>The shapefile library can be downloaded from the following URL and installed in your macros folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuilding.py" line="242"/>
        <source>You can put anything but Site and Building objects in a Building object.

Building object is not allowed to accept Site and Building objects.

Site and Building objects will be removed from the selection.

You can change that in the preferences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuilding.py" line="252"/>
        <source>There is no valid object in the selection.

Building creation aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuilding.py" line="260"/>
        <source>Create Building</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="51"/>
        <source>Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="97"/>
        <source>Create Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="327"/>
        <source>Crossing point not found in profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="81"/>
        <source>Create Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="231"/>
        <source>Auto height is larger than height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="242"/>
        <source>Total row size is larger than height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="266"/>
        <source>Auto width is larger than width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="277"/>
        <source>Total column size is larger than width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="535"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="536"/>
        <source>Total width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="537"/>
        <source>Total height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="538"/>
        <source>Add row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="539"/>
        <source>Del row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="542"/>
        <source>Add col</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="545"/>
        <source>Del col</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="548"/>
        <source>Create span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="551"/>
        <source>Remove span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="554"/>
        <source>Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="558"/>
        <source>Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFloor.py" line="162"/>
        <source>You can put anything but the following objects: Site, Building, and Floor - in a Floor object.

Floor object is not allowed to accept Site, Building, or Floor objects.

Site, Building, and Floor objects will be removed from the selection.

You can change that in the preferences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFloor.py" line="172"/>
        <source>There is no valid object in the selection.

Floor creation aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFloor.py" line="180"/>
        <source>Create Floor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1268"/>
        <source>Precast elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1269"/>
        <source>Slab type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1270"/>
        <source>Chamfer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1271"/>
        <source>Dent length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1272"/>
        <source>Dent width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1273"/>
        <source>Dent height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1274"/>
        <source>Slab base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1275"/>
        <source>Number of holes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1276"/>
        <source>Major diameter of holes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1277"/>
        <source>Minor diameter of holes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1278"/>
        <source>Spacing between holes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1279"/>
        <source>Number of grooves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1280"/>
        <source>Depth of grooves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1281"/>
        <source>Height of grooves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1282"/>
        <source>Spacing between grooves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1283"/>
        <source>Number of risers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1284"/>
        <source>Length of down floor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1285"/>
        <source>Height of risers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1286"/>
        <source>Depth of treads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1739"/>
        <source>Precast options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1740"/>
        <source>Dents list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1741"/>
        <source>Add dent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1742"/>
        <source>Remove dent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="582"/>
        <location filename="../../ArchPrecast.py" line="1744"/>
        <location filename="../../ArchWall.py" line="586"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="591"/>
        <location filename="../../ArchPrecast.py" line="1745"/>
        <location filename="../../ArchWall.py" line="594"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1746"/>
        <source>Slant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1747"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPrecast.py" line="1748"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="85"/>
        <source>Curtain Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="120"/>
        <location filename="../../ArchTruss.py" line="104"/>
        <source>Please select only one base object or none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="127"/>
        <location filename="../../ArchCurtainWall.py" line="160"/>
        <source>Create Curtain Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="144"/>
        <source>Create profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="153"/>
        <source>Profile settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="211"/>
        <source>Create Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="698"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="129"/>
        <location filename="../../ArchPipe.py" line="140"/>
        <source>Create Pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="176"/>
        <source>Please select exactly 2 or 3 Pipe objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="183"/>
        <source>Please select only Pipe objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="188"/>
        <source>Create Connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="284"/>
        <source>Unable to build the base path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="306"/>
        <source>Unable to build the profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="341"/>
        <source>Unable to build the pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="363"/>
        <source>The base object is not a Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="368"/>
        <source>Too many wires in the base shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="373"/>
        <source>The base wire is closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="396"/>
        <source>The profile is not a 2D Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="401"/>
        <source>The profile is not closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="508"/>
        <source>Only the 3 first wires will be connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="532"/>
        <location filename="../../ArchPipe.py" line="587"/>
        <source>Common vertex not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="557"/>
        <source>Pipes are already aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="611"/>
        <source>At least 2 pipes must align</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="117"/>
        <source>Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="127"/>
        <source>Walls can only be based on Part or Mesh objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="335"/>
        <location filename="../../ArchWall.py" line="425"/>
        <location filename="../../ArchWall.py" line="737"/>
        <source>Create Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="379"/>
        <source>First point of wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="421"/>
        <location filename="../../ArchWall.py" line="413"/>
        <source>Next point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="556"/>
        <source>Wall options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="560"/>
        <source>Wall Presets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="566"/>
        <source>This list shows all the MultiMaterials objects of this document. Create some to define wall types.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="602"/>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="606"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="607"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="611"/>
        <location filename="../../ArchWall.py" line="614"/>
        <source>Con&amp;tinue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="625"/>
        <source>Use sketches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="812"/>
        <source>Merge Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="822"/>
        <source>The selected wall contains no subwall to merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="827"/>
        <location filename="../../ArchWall.py" line="833"/>
        <source>Please select only wall objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="835"/>
        <source>Merge Walls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1257"/>
        <source>Cannot compute blocks for wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1100"/>
        <location filename="../../ArchWall.py" line="1275"/>
        <source>This mesh is an invalid solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="1391"/>
        <source>Error: Unable to modify the base object of this wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="69"/>
        <source>Truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="109"/>
        <location filename="../../ArchTruss.py" line="140"/>
        <source>Create Truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="359"/>
        <source>Unable to retrieve value from object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="616"/>
        <source>Import CSV File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="686"/>
        <source>Unable to recognize that file type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="808"/>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="249"/>
        <source>Create Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="1010"/>
        <source>Set text position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="1018"/>
        <source>Space boundaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importIFClegacy.py" line="156"/>
        <source>Couldn&apos;t locate IfcOpenShell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importIFClegacy.py" line="507"/>
        <source>IfcOpenShell not found or disabled, falling back on internal parser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importIFClegacy.py" line="516"/>
        <source>IFC Schema not found, IFC import disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importIFClegacy.py" line="1206"/>
        <source>Error: IfcOpenShell is not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../importIFClegacy.py" line="1217"/>
        <source>Error: your IfcOpenShell version is too old</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="110"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="186"/>
        <location filename="../../ArchPanel.py" line="224"/>
        <source>Create Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="432"/>
        <source>Create Panel Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="466"/>
        <source>Create Panel Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1154"/>
        <location filename="../../ArchPanel.py" line="736"/>
        <source>Facemaker returned an error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="957"/>
        <source>Error computing shape of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1966"/>
        <location filename="../../ArchPanel.py" line="990"/>
        <source>Couldn&apos;t compute a shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="2001"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="2007"/>
        <source>Edit views positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="2116"/>
        <source>This object has no face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="199"/>
        <source>BuildingPart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="320"/>
        <source>Create BuildingPart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="79"/>
        <source>Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="127"/>
        <location filename="../../ArchStructure.py" line="541"/>
        <source>Beam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="130"/>
        <location filename="../../ArchStructure.py" line="542"/>
        <source>Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="220"/>
        <source>Create Structures From Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="261"/>
        <source>Please select the base object first and then the edges to use as extrusion paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="292"/>
        <source>Create Structural System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="314"/>
        <source>Please select at least an axis object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="367"/>
        <location filename="../../ArchStructure.py" line="427"/>
        <source>Create Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="396"/>
        <source>First point of the beam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="398"/>
        <source>Base point of column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="536"/>
        <source>Structure options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="540"/>
        <source>Drawing mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="605"/>
        <source>Switch L/H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="607"/>
        <source>Switch L/W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1064"/>
        <source>Error: The base shape couldn&apos;t be extruded along this tool object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1539"/>
        <source>Node Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1546"/>
        <source>Reset nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1557"/>
        <source>Edit nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1567"/>
        <source>Extend nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1574"/>
        <source>Extends the nodes of this element to reach the nodes of another element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1584"/>
        <source>Connect nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1591"/>
        <source>Connects nodes of this element with the nodes of another element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1601"/>
        <source>Toggle all nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1606"/>
        <source>Toggles all structural nodes of the document on/off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1615"/>
        <source>Extrusion Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1622"/>
        <location filename="../../ArchStructure.py" line="1855"/>
        <source>Select tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1629"/>
        <source>Select object or edges to be used as a Tool (extrusion path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1656"/>
        <location filename="../../ArchStructure.py" line="1719"/>
        <source>Choose another Structure object:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1662"/>
        <location filename="../../ArchStructure.py" line="1725"/>
        <source>The chosen object is not a Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1668"/>
        <location filename="../../ArchStructure.py" line="1731"/>
        <source>The chosen object has no structural nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1676"/>
        <location filename="../../ArchStructure.py" line="1739"/>
        <source>One of these objects has more than 2 nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1694"/>
        <location filename="../../ArchStructure.py" line="1757"/>
        <source>Unable to find a suitable intersection point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1700"/>
        <source>Intersection found.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1762"/>
        <source>Intersection found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="1817"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchMaterial</name>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="14"/>
        <source>Arch material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="26"/>
        <source>Choose a preset card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="30"/>
        <source>Choose preset...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="44"/>
        <source>Copy values from an existing material in the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="48"/>
        <source>Copy existing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="58"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="65"/>
        <source>The name/label of this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="94"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="101"/>
        <source>An optional description for this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="112"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="125"/>
        <source>The color of this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="139"/>
        <source>Section Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="163"/>
        <source>Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="170"/>
        <source>A transparency value for this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="184"/>
        <source>Standard code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="191"/>
        <source>A standard (MasterFormat, Omniclass...) code for this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="204"/>
        <source>Opens a browser dialog to choose a class from a BIM standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="223"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="236"/>
        <source>A URL describing this material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="249"/>
        <source>Opens the URL in a browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMaterial.ui" line="268"/>
        <source>Father</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_3Views</name>
    <message>
        <location filename="../../ArchEquipment.py" line="248"/>
        <source>3 views from mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="252"/>
        <source>Creates 3 views (top, front, side) from a mesh-based object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Add</name>
    <message>
        <location filename="../../ArchCommands.py" line="1587"/>
        <source>Add component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1590"/>
        <source>Adds the selected components to the active object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Axis</name>
    <message>
        <location filename="../../ArchAxis.py" line="94"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxis.py" line="96"/>
        <source>Creates a set of axes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_AxisSystem</name>
    <message>
        <location filename="../../ArchAxisSystem.py" line="76"/>
        <source>Axis System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchAxisSystem.py" line="80"/>
        <source>Creates an axis system from a set of axes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_AxisTools</name>
    <message>
        <location filename="../../ArchAxis.py" line="1027"/>
        <location filename="../../ArchAxis.py" line="1028"/>
        <source>Axis tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Building</name>
    <message>
        <location filename="../../ArchBuilding.py" line="207"/>
        <source>Building</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuilding.py" line="211"/>
        <source>Creates a building object including selected objects.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_BuildingPart</name>
    <message>
        <location filename="../../ArchBuildingPart.py" line="301"/>
        <source>BuildingPart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="306"/>
        <source>Creates a BuildingPart object including selected objects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Check</name>
    <message>
        <location filename="../../ArchCommands.py" line="1843"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1846"/>
        <source>Checks the selected objects for problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_CloneComponent</name>
    <message>
        <location filename="../../ArchCommands.py" line="1950"/>
        <source>Clone component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1955"/>
        <source>Clones an object as an undefined architectural component</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_CloseHoles</name>
    <message>
        <location filename="../../ArchCommands.py" line="1821"/>
        <source>Close holes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1824"/>
        <source>Closes holes in open shapes, turning them solids</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Component</name>
    <message>
        <location filename="../../ArchCommands.py" line="1914"/>
        <source>Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1918"/>
        <source>Creates an undefined architectural component</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_CurtainWall</name>
    <message>
        <location filename="../../ArchCurtainWall.py" line="103"/>
        <source>Curtain Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCurtainWall.py" line="108"/>
        <source>Creates a curtain wall object from selected line or from scratch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_CutLine</name>
    <message>
        <location filename="../../ArchCutPlane.py" line="86"/>
        <source>Cut with line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="89"/>
        <source>Cut an object with a line</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_CutPlane</name>
    <message>
        <location filename="../../ArchCutPlane.py" line="117"/>
        <source>Cut with plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCutPlane.py" line="120"/>
        <source>Cut an object with a plane</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Equipment</name>
    <message>
        <location filename="../../ArchEquipment.py" line="176"/>
        <source>Equipment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchEquipment.py" line="181"/>
        <source>Creates an equipment object from a selected object (Part or Mesh)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Fence</name>
    <message>
        <location filename="../../ArchFence.py" line="436"/>
        <source>Fence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFence.py" line="440"/>
        <source>Creates a fence object from a selected section, post and path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Floor</name>
    <message>
        <location filename="../../ArchFloor.py" line="115"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFloor.py" line="120"/>
        <source>Creates a Building Part object that represents a level, including selected objects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Frame</name>
    <message>
        <location filename="../../ArchFrame.py" line="81"/>
        <source>Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchFrame.py" line="86"/>
        <source>Creates a frame object from a planar 2D object (the extrusion path(s)) and a profile. Make sure objects are selected in that order.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Grid</name>
    <message>
        <location filename="../../ArchGrid.py" line="72"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="76"/>
        <source>Creates a customizable grid object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="109"/>
        <source>The number of rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="116"/>
        <source>The number of columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="123"/>
        <source>The sizes for rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="130"/>
        <source>The sizes of columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="139"/>
        <source>The span ranges of cells that are merged together</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="148"/>
        <source>The type of 3D points produced by this grid object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="162"/>
        <source>The total width of this grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="169"/>
        <source>The total height of this grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="179"/>
        <source>Creates automatic column divisions (set to 0 to disable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="188"/>
        <source>Creates automatic row divisions (set to 0 to disable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="198"/>
        <source>When in edge midpoint mode, if this grid must reorient its children along edge normals or not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchGrid.py" line="205"/>
        <source>The indices of faces to hide</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_IfcSpreadsheet</name>
    <message>
        <location filename="../../ArchCommands.py" line="2025"/>
        <source>Create IFC spreadsheet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="2030"/>
        <source>Creates a spreadsheet to store IFC properties of an object.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Material</name>
    <message>
        <location filename="../../ArchMaterial.py" line="122"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="127"/>
        <source>Creates or edits the material definition of a selected object.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_MaterialTools</name>
    <message>
        <location filename="../../ArchMaterial.py" line="1247"/>
        <location filename="../../ArchMaterial.py" line="1248"/>
        <source>Material tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_MergeWalls</name>
    <message>
        <location filename="../../ArchWall.py" line="776"/>
        <source>Merge Walls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="779"/>
        <source>Merges the selected walls, if possible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_MeshToShape</name>
    <message>
        <location filename="../../ArchCommands.py" line="1721"/>
        <source>Mesh to Shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1724"/>
        <source>Turns selected meshes into Part Shape objects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_MultiMaterial</name>
    <message>
        <location filename="../../ArchMaterial.py" line="164"/>
        <source>Multi-Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchMaterial.py" line="168"/>
        <source>Creates or edits multi-materials</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Nest</name>
    <message>
        <location filename="../../ArchPanel.py" line="2028"/>
        <source>Nest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="2032"/>
        <source>Nests a series of selected shapes in a container</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Panel</name>
    <message>
        <location filename="../../ArchPanel.py" line="159"/>
        <source>Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="164"/>
        <source>Creates a panel object from scratch or from a selected object (sketch, wire, face or solid)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_PanelTools</name>
    <message>
        <location filename="../../ArchPanel.py" line="2216"/>
        <location filename="../../ArchPanel.py" line="2217"/>
        <source>Panel tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Panel_Cut</name>
    <message>
        <location filename="../../ArchPanel.py" line="417"/>
        <source>Panel Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="421"/>
        <source>Creates 2D views of selected panels</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Panel_Sheet</name>
    <message>
        <location filename="../../ArchPanel.py" line="452"/>
        <source>Panel Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPanel.py" line="456"/>
        <source>Creates a 2D sheet which can contain panel cuts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Pipe</name>
    <message>
        <location filename="../../ArchPipe.py" line="109"/>
        <source>Pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="113"/>
        <source>Creates a pipe object from a given Wire or Line</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_PipeConnector</name>
    <message>
        <location filename="../../ArchPipe.py" line="157"/>
        <source>Connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchPipe.py" line="162"/>
        <source>Creates a connector between 2 or 3 selected pipes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_PipeTools</name>
    <message>
        <location filename="../../ArchPipe.py" line="668"/>
        <location filename="../../ArchPipe.py" line="669"/>
        <source>Pipe tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Profile</name>
    <message>
        <location filename="../../ArchProfile.py" line="124"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProfile.py" line="126"/>
        <source>Creates a profile object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Project</name>
    <message>
        <location filename="../../ArchProject.py" line="104"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchProject.py" line="109"/>
        <source>Creates a project entity aggregating the selected sites.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Rebar</name>
    <message>
        <location filename="../../ArchRebar.py" line="112"/>
        <source>Custom Rebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRebar.py" line="117"/>
        <source>Creates a Reinforcement bar from the selected face of solid object and/or a sketch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_RebarTools</name>
    <message>
        <location filename="../../InitGui.py" line="155"/>
        <source>Rebar tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="160"/>
        <source>Create various types of rebars, including U-shaped, L-shaped, and stirrup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Reference</name>
    <message>
        <location filename="../../ArchReference.py" line="848"/>
        <source>External reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchReference.py" line="852"/>
        <source>Creates an external reference object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Remove</name>
    <message>
        <location filename="../../ArchCommands.py" line="1636"/>
        <source>Remove component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1640"/>
        <source>Remove the selected components from their parents, or create a hole in a component</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_RemoveShape</name>
    <message>
        <location filename="../../ArchCommands.py" line="1801"/>
        <source>Remove Shape from Arch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1804"/>
        <source>Removes cubic shapes from Arch components</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Roof</name>
    <message>
        <location filename="../../ArchRoof.py" line="221"/>
        <source>Roof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchRoof.py" line="225"/>
        <source>Creates a roof object from the selected wire.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Schedule</name>
    <message>
        <location filename="../../ArchSchedule.py" line="65"/>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSchedule.py" line="68"/>
        <source>Creates a schedule to collect data from the model</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_SectionPlane</name>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1059"/>
        <source>Section Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSectionPlane.py" line="1063"/>
        <source>Creates a section plane object, including the selected objects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_SelectNonSolidMeshes</name>
    <message>
        <location filename="../../ArchCommands.py" line="1764"/>
        <source>Select non-manifold meshes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1768"/>
        <source>Selects all non-manifold meshes from the document or from the selected groups</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Site</name>
    <message>
        <location filename="../../ArchSite.py" line="527"/>
        <source>Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSite.py" line="531"/>
        <source>Creates a site object including selected objects.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Space</name>
    <message>
        <location filename="../../ArchStairs.py" line="238"/>
        <source>Creates a stairs object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="236"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchSpace.py" line="240"/>
        <source>Creates a space object from selected boundary objects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_SplitMesh</name>
    <message>
        <location filename="../../ArchCommands.py" line="1691"/>
        <source>Split Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1694"/>
        <source>Splits selected meshes into independent components</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Stairs</name>
    <message>
        <location filename="../../ArchStairs.py" line="236"/>
        <source>Stairs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_StructuralSystem</name>
    <message>
        <location filename="../../ArchStructure.py" line="274"/>
        <source>Structural System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="278"/>
        <source>Create a structural system object from a selected structure and axis</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Structure</name>
    <message>
        <location filename="../../ArchStructure.py" line="330"/>
        <source>Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="335"/>
        <source>Creates a structure object from scratch or from a selected object (sketch, wire, face or solid)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_StructureTools</name>
    <message>
        <location filename="../../ArchStructure.py" line="2035"/>
        <location filename="../../ArchStructure.py" line="2036"/>
        <source>Structure tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_StructuresFromSelection</name>
    <message>
        <location filename="../../ArchStructure.py" line="205"/>
        <source>Multiple Structures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchStructure.py" line="209"/>
        <source>Create multiple Arch Structure objects from a selected base, using each selected edge as an extrusion path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Survey</name>
    <message>
        <location filename="../../ArchCommands.py" line="1873"/>
        <source>Survey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1874"/>
        <source>Starts survey</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_ToggleIfcBrepFlag</name>
    <message>
        <location filename="../../ArchCommands.py" line="1893"/>
        <source>Toggle IFC Brep flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="1897"/>
        <source>Force an object to be exported as Brep or not</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_ToggleSubs</name>
    <message>
        <location filename="../../ArchCommands.py" line="2063"/>
        <source>Toggle subcomponents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchCommands.py" line="2066"/>
        <source>Shows or hides the subcomponents of this object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Truss</name>
    <message>
        <location filename="../../ArchTruss.py" line="87"/>
        <source>Truss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchTruss.py" line="92"/>
        <source>Creates a truss object from selected line or from scratch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Wall</name>
    <message>
        <location filename="../../ArchWall.py" line="291"/>
        <source>Wall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWall.py" line="296"/>
        <source>Creates a wall object from scratch or from a selected object (wire, face or solid)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Arch_Window</name>
    <message>
        <location filename="../../ArchWindow.py" line="179"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchWindow.py" line="184"/>
        <source>Creates a window object from a selected object (wire, rectangle or sketch)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BimServer</name>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="14"/>
        <source>BimServer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="20"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="28"/>
        <source>The name of the BimServer you are currently connecting to. Change settings in Arch Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="31"/>
        <source>Bim Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="38"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="55"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="79"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="90"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="97"/>
        <source>The list of projects present on the Bim Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="109"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="115"/>
        <source>Available revisions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="144"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="156"/>
        <location filename="../ui/BimServerTaskPanel.ui" line="205"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="164"/>
        <source>Root object:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/BimServerTaskPanel.ui" line="178"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="14"/>
        <source>Schedule definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="22"/>
        <source>Schedule name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="29"/>
        <source>Unnamed schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="60"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="63"/>
        <source>A description for this operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="68"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="71"/>
        <source>The property to retrieve from each object.
Can be &quot;Count&quot; to count the objects, or property names
like &quot;Length&quot; or &quot;Shape.Volume&quot; to retrieve
a certain property.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="79"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="82"/>
        <source>An optional unit to express the resulting value. Ex: m^3 (you can also write m³ or m3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="87"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="90"/>
        <source>An optional semicolon (;) separated list of object names
(internal names, not labels), to be considered by this operation.
If the list contains groups, children will be added.
Leave blank to use all objects from the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="98"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;An optional semicolon (;) separated list of property:value filters. Prepend ! to a property name to invert the effect of the filer (exclude objects that match the filter). Objects whose property contains the value will be matched. Examples of valid filters (everything is case-insensitive):&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Name:Wall&lt;/span&gt; - Will only consider objects with &amp;quot;wall&amp;quot; in their name (internal name)&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Name:Wall&lt;/span&gt; - Will only consider objects which DON&apos;T have &amp;quot;wall&amp;quot; in their name (internal name)&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Description:Win&lt;/span&gt; - Will only consider objects with &amp;quot;win&amp;quot; in their description&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Label:Win&lt;/span&gt; - Will only consider objects which DO NOT have &amp;quot;win&amp;quot; in their label&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IfcType:Wall&lt;/span&gt; - Will only consider objects which Ifc Type is &amp;quot;Wall&amp;quot;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Tag:Wall&lt;/span&gt; - Will only consider objects which tag is NOT &amp;quot;Wall&amp;quot;&lt;/p&gt;&lt;p&gt;If you leave this field empty, no filtering is applied&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="111"/>
        <source>If this is enabled, an associated spreadsheet containing the results will be maintained together with this schedule object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="114"/>
        <source>Associate spreadsheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="121"/>
        <source>If this is turned on, additional lines will be filled with each object considered. If not, only the totals.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="124"/>
        <source>Detailed results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="135"/>
        <source>Adds a line below the selected line/cell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="138"/>
        <source>Add row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="150"/>
        <source>Deletes the selected line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="153"/>
        <source>Del row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="165"/>
        <source>Clears the whole list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="168"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="180"/>
        <source>Put selected objects into the &quot;Objects&quot; column of the selected row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="183"/>
        <source>Add selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="190"/>
        <source>Imports the contents of a CSV file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="193"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="205"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This exports the results to a CSV or Markdown file. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note for CSV export:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;In Libreoffice, you can keep this CSV file linked by right-clicking the Sheets tab bar -&amp;gt; New sheet -&amp;gt; From file -&amp;gt; Link (Note: as of LibreOffice v6.x the correct path now is: Sheet -&amp;gt; Insert Sheet... -&amp;gt; From file -&amp;gt; Browse...)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchSchedule.ui" line="208"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogBimServerLogin.ui" line="14"/>
        <source>BimServer Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogBimServerLogin.ui" line="22"/>
        <source>BimServer URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogBimServerLogin.ui" line="36"/>
        <source>Login (email):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogBimServerLogin.ui" line="50"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogBimServerLogin.ui" line="62"/>
        <source>Keep me logged in across FreeCAD sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogDisplayText.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="14"/>
        <source>IFC properties editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="22"/>
        <source>IFC UUID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="29"/>
        <source>Leave this empty to generate one at export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="38"/>
        <source>List of IFC properties for this object. Double-click to edit, drag and drop to reorganize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="63"/>
        <source>Delete selected property/set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="76"/>
        <source>Force exporting geometry as BREP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/DialogIfcProperties.ui" line="83"/>
        <source>Force export full FreeCAD parametric data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Draft</name>
    <message>
        <location filename="../../InitGui.py" line="262"/>
        <location filename="../../InitGui.py" line="265"/>
        <location filename="../../InitGui.py" line="268"/>
        <location filename="../../InitGui.py" line="272"/>
        <source>Draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="312"/>
        <location filename="../../InitGui.py" line="315"/>
        <location filename="../../InitGui.py" line="318"/>
        <source>Import-Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ArchBuildingPart.py" line="1377"/>
        <source>Writing camera position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="14"/>
        <source>Git</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="20"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="48"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="55"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="64"/>
        <source>List of files to be committed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="93"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="100"/>
        <source>Select all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="112"/>
        <location filename="../ui/GitTaskPanel.ui" line="147"/>
        <source>Commit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="120"/>
        <source>Commit message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="159"/>
        <source>Remote repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="183"/>
        <source>Pull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/GitTaskPanel.ui" line="190"/>
        <source>Push</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="14"/>
        <source>Multimaterial definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="21"/>
        <source>Copy existing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="29"/>
        <source>Edit definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="37"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="49"/>
        <source>Composition:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="59"/>
        <source>Total thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="68"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="75"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="82"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="89"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchMultiMaterial.ui" line="96"/>
        <source>Invert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="14"/>
        <source>Nesting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="20"/>
        <source>Container</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="36"/>
        <source>Pick selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="46"/>
        <source>Shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="57"/>
        <source>Add selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="64"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="76"/>
        <source>Nesting parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="82"/>
        <source>Tolerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="89"/>
        <source>Closer than this, two points are considered equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="102"/>
        <source>Arcs subdivisions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="109"/>
        <source>The number of segments to divide non-linear edges into, for calculations. If curved shapes overlap, try raising this value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="125"/>
        <source>Rotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="132"/>
        <source>A comma-separated list of angles to try and rotate the shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="135"/>
        <source>0,90,180,270</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="145"/>
        <source>Nesting operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="154"/>
        <source>pass %p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="163"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="170"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/ArchNest.ui" line="177"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Gui::Dialog::DlgSettingsArch</name>
    <message>
        <location filename="../ui/preferences-arch.ui" line="14"/>
        <source>General settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="26"/>
        <source>Object creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="32"/>
        <source>Auto-join walls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="48"/>
        <source>If this is checked, when 2 similar walls are being connected, their underlying sketches will be joined into one, and the two walls will become one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="51"/>
        <source>Join walls base sketches when possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="64"/>
        <source>Two possible strategies to avoid circular dependencies: Create one more object (unchecked) or remove external geometry of base sketch (checked)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="67"/>
        <source>Remove external geometry of base sketches when needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="80"/>
        <source>If this is checked, when an object becomes Subtraction or Addition of an Arch object, it will receive the Draft Construction color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="83"/>
        <source>Apply Draft construction style to subcomponents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="114"/>
        <source>Do not compute areas for object with more than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="121"/>
        <source> faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="157"/>
        <source>Interval between file checks for references</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="164"/>
        <source> seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="188"/>
        <source>By default, new objects will have their &quot;Move with host&quot; property set to False, which means they won&apos;t move when their host object is moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="191"/>
        <source>Set &quot;Move with host&quot; property to True by default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="207"/>
        <source>Set &quot;Move base&quot; property to True by default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="223"/>
        <source>If this is checked, when an Arch object has a material, the object will take the color of the material. This can be overridden for each object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="226"/>
        <source>Use material color as shape color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="260"/>
        <source>IFC version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="280"/>
        <source>The IFC version will change which attributes and products are supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="290"/>
        <source>IFC4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="295"/>
        <source>IFC2X3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="308"/>
        <source>Mesh to Shape Conversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="316"/>
        <source>If this is checked, conversion is faster but the result might still contain triangulated faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="319"/>
        <source>Fast conversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="335"/>
        <source>Tolerance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="345"/>
        <source>Tolerance value to use when checking if 2 adjacent faces as planar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="368"/>
        <source>If this is checked, flat groups of faces will be force-flattened, resulting in possible gaps and non-solid results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="371"/>
        <source>Force flat faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="388"/>
        <source>If this is checked, holes in faces will be performed by subtraction rather than using wires orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="391"/>
        <source>Cut method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="412"/>
        <source>2D rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="420"/>
        <source>Show debug information during 2D rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="423"/>
        <source>Show renderer debug messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="456"/>
        <source>Cut areas line thickness ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="476"/>
        <source>Specifies how many times the viewed line thickness must be applied to cut lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="512"/>
        <source>Symbol line thickness ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="565"/>
        <source>Hidden geometry pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="585"/>
        <source>This is the SVG stroke-dasharray property to apply
to projections of hidden objects.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="589"/>
        <source>30, 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="625"/>
        <source>Pattern scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="645"/>
        <source>Scaling factor for patterns used by object that have
a Footprint display mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="673"/>
        <source>Bim server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="681"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="688"/>
        <source>The URL of a bim server instance (www.bimserver.org) to connect to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="691"/>
        <source>http://localhost:8082</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="708"/>
        <source>If this is selected, the &quot;Open BimServer in browser&quot;
button will open the Bim Server interface in an external browser
instead of the FreeCAD web workbench</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="713"/>
        <source>Open in external browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="731"/>
        <source>Survey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="739"/>
        <source>If this is checked, the text that gets placed in the clipboard will include the unit. Otherwise, it will be a simple number expressed in internal units (millimeters)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-arch.ui" line="742"/>
        <source>Include unit when sending measurements to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="14"/>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="26"/>
        <source>Walls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="34"/>
        <location filename="../ui/preferences-archdefaults.ui" line="211"/>
        <location filename="../ui/preferences-archdefaults.ui" line="452"/>
        <location filename="../ui/preferences-archdefaults.ui" line="702"/>
        <location filename="../ui/preferences-archdefaults.ui" line="859"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="41"/>
        <location filename="../ui/preferences-archdefaults.ui" line="80"/>
        <location filename="../ui/preferences-archdefaults.ui" line="179"/>
        <location filename="../ui/preferences-archdefaults.ui" line="218"/>
        <location filename="../ui/preferences-archdefaults.ui" line="257"/>
        <location filename="../ui/preferences-archdefaults.ui" line="340"/>
        <location filename="../ui/preferences-archdefaults.ui" line="379"/>
        <location filename="../ui/preferences-archdefaults.ui" line="462"/>
        <location filename="../ui/preferences-archdefaults.ui" line="504"/>
        <location filename="../ui/preferences-archdefaults.ui" line="546"/>
        <location filename="../ui/preferences-archdefaults.ui" line="670"/>
        <location filename="../ui/preferences-archdefaults.ui" line="709"/>
        <location filename="../ui/preferences-archdefaults.ui" line="748"/>
        <source> mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="73"/>
        <location filename="../ui/preferences-archdefaults.ui" line="250"/>
        <location filename="../ui/preferences-archdefaults.ui" line="494"/>
        <location filename="../ui/preferences-archdefaults.ui" line="741"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="103"/>
        <source>Use sketches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="132"/>
        <location filename="../ui/preferences-archdefaults.ui" line="293"/>
        <location filename="../ui/preferences-archdefaults.ui" line="415"/>
        <location filename="../ui/preferences-archdefaults.ui" line="941"/>
        <location filename="../ui/preferences-archdefaults.ui" line="1044"/>
        <source>Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="139"/>
        <source>This is the default color for new Wall objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="164"/>
        <source>Structures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="172"/>
        <location filename="../ui/preferences-archdefaults.ui" line="663"/>
        <location filename="../ui/preferences-archdefaults.ui" line="820"/>
        <source>Length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="300"/>
        <source>This is the default color for new Structure objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="325"/>
        <source>Rebars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="333"/>
        <source>Diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="372"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="444"/>
        <source>Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="459"/>
        <source>The default width for new windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="501"/>
        <source>The default height for new windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="536"/>
        <source>Thickness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="543"/>
        <source>The default thickness for new windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="582"/>
        <location filename="../ui/preferences-archdefaults.ui" line="1079"/>
        <source>Transparency:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="602"/>
        <source>Frame color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="626"/>
        <source>Glass color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="655"/>
        <source>Stairs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="784"/>
        <source>Number of steps:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="812"/>
        <source>Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="827"/>
        <location filename="../ui/preferences-archdefaults.ui" line="866"/>
        <location filename="../ui/preferences-archdefaults.ui" line="905"/>
        <location filename="../ui/preferences-archdefaults.ui" line="985"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="898"/>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="970"/>
        <source>Pipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="978"/>
        <source>Diameter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1025"/>
        <source>Helpers (grids, axes, etc...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1071"/>
        <source>Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1118"/>
        <source>Line style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1135"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1140"/>
        <source>Dashed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1145"/>
        <source>Dotted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1150"/>
        <source>Dashdot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-archdefaults.ui" line="1158"/>
        <source>Line color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="14"/>
        <source>IFC import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="41"/>
        <source>Show this dialog when importing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="54"/>
        <source>Shows verbose debug messages during import and export
of IFC files in the Report view panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="58"/>
        <source>Show debug messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="71"/>
        <source>Clones are used when objects have shared geometry
One object is the base object, the others are clones.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="75"/>
        <source>Create clones when objects have shared geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="99"/>
        <location filename="../ui/preferences-ifc.ui" line="119"/>
        <source>EXPERIMENTAL
The number of cores to use in multicore mode.
Keep 0 to disable multicore mode.
The maximum value should be your number of cores minus 1,
for example, 3 if you have a 4-core CPU.

Set it to 1 to use multicore mode in single-core mode; this is safer
if you start getting crashes when you set multiple cores.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="129"/>
        <source>Number of cores to use (experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="157"/>
        <source>Import options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="165"/>
        <source>Import arch IFC objects as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="172"/>
        <location filename="../ui/preferences-ifc.ui" line="221"/>
        <source>Specifies what kind of objects will be created in FreeCAD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="182"/>
        <source>Parametric Arch objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="187"/>
        <location filename="../ui/preferences-ifc.ui" line="231"/>
        <source>Non-parametric Arch objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="192"/>
        <location filename="../ui/preferences-ifc.ui" line="236"/>
        <source>Simple Part shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="197"/>
        <source>One compound per floor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="202"/>
        <source>Do not import Arch objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="214"/>
        <source>Import struct IFC objects as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="241"/>
        <source>One compound for all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="246"/>
        <source>Do not import structural objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="258"/>
        <source>Root element:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="265"/>
        <source>Only subtypes of the specified element will be imported.
Keep the element IfcProduct to import all building elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="284"/>
        <source>Openings will be imported as subtractions, otherwise wall shapes
will already have their openings subtracted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="288"/>
        <source>Separate openings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="301"/>
        <source>The importer will try to detect extrusions.
Note that this might slow things down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="305"/>
        <source>Detect extrusions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="318"/>
        <source>Split walls made of multiple layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="321"/>
        <source>Split multilayer walls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="334"/>
        <source>Object names will be prefixed with the IFC ID number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="337"/>
        <source>Prefix names with ID number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="350"/>
        <source>If several materials with the same name and color are found in the IFC file,
they will be treated as one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="354"/>
        <source>Merge materials with same name and same color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="367"/>
        <source>Each object will have their IFC properties stored in a spreadsheet object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="370"/>
        <source>Import IFC properties in spreadsheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="383"/>
        <source>IFC files can contain unclean or non-solid geometry. If this option is checked, all the geometry is imported, regardless of their validity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="386"/>
        <source>Allow invalid shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="401"/>
        <source>Exclude list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="408"/>
        <source>Comma-separated list of IFC entities to be excluded from imports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="429"/>
        <source>Fit view during import on the imported objects.
This will slow down the import, but one can watch the import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="433"/>
        <source>Fit view while importing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="446"/>
        <source>Creates a full parametric model on import using stored
FreeCAD object properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="450"/>
        <source>Import full FreeCAD parametric definitions if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="463"/>
        <source>If this option is checked, the default &apos;Project&apos;, &apos;Site&apos;, &apos;Building&apos;, and &apos;Storeys&apos;
objects that are usually found in an IFC file are not imported, and all objects
are placed in a &apos;Group&apos; instead.
&apos;Buildings&apos; and &apos;Storeys&apos; are still imported if there is more than one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc.ui" line="469"/>
        <source>Replace &apos;Project&apos;, &apos;Site&apos;, &apos;Building&apos;, and &apos;Storey&apos; with &apos;Group&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="14"/>
        <source>DAE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="57"/>
        <location filename="../ui/preferences-dae.ui" line="26"/>
        <source>Export options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="34"/>
        <source>Scaling factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="41"/>
        <source>All dimensions in the file will be scaled with this factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="67"/>
        <source>Mesher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="74"/>
        <source>Meshing program that should be used.
If using Netgen, make sure that it is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="85"/>
        <source>Builtin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="90"/>
        <source>Mefisto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="95"/>
        <source>Netgen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="108"/>
        <source>Builtin and mefisto mesher options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="116"/>
        <source>Tessellation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="123"/>
        <source>Tessellation value to use with the Builtin and the Mefisto meshing program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="147"/>
        <source>Netgen mesher options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="155"/>
        <source>Grading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="162"/>
        <source>Grading value to use for meshing using Netgen.
This value describes how fast the mesh size decreases.
The gradient of the local mesh size h(x) is bound by |Δh(x)| ≤ 1/value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="187"/>
        <source>Segments per edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="194"/>
        <source>Maximum number of segments per edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="217"/>
        <source>Segments per radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="224"/>
        <source>Number of segments per radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="247"/>
        <source>Allow a second order mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="250"/>
        <source>Second order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="267"/>
        <source>Allows optimization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="270"/>
        <source>Optimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="290"/>
        <source>Allow quadrilateral faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-dae.ui" line="293"/>
        <source>Allow quads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="14"/>
        <source>IFC export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="35"/>
        <location filename="../ui/preferences-ifc.ui" line="35"/>
        <source>General options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="41"/>
        <source>Show this dialog when exporting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="65"/>
        <location filename="../ui/preferences-ifc-export.ui" line="78"/>
        <source>The type of objects that you wish to export:
- Standard model: solid objects.
- Structural analysis: wireframe model for structural calculations.
- Standard + structural: both types of models.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="71"/>
        <source>Export type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="91"/>
        <source>Standard model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="96"/>
        <source>Structural analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="101"/>
        <source>Standard + structural</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="111"/>
        <source>Some IFC viewers don&apos;t like objects exported as extrusions.
Use this to force all objects to be exported as BREP geometry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="115"/>
        <source>Force export as Brep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="128"/>
        <source>Use triangulation options set in the DAE options page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="131"/>
        <source>Use DAE triangulation options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="144"/>
        <source>Curved shapes that cannot be represented as curves in IFC
are decomposed into flat facets.
If this is checked, an additional calculation is done to join coplanar facets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="149"/>
        <source>Join coplanar facets when triangulating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="162"/>
        <source>When exporting objects without unique ID (UID), the generated UID
will be stored inside the FreeCAD object for reuse next time that object
is exported. This leads to smaller differences between file versions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="167"/>
        <source>Store IFC unique ID in FreeCAD objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="183"/>
        <source>IFCOpenShell is a library that allows to import IFC files.
Its serializer functionality allows to give it an OCC shape and it will
produce adequate IFC geometry: NURBS, faceted, or anything else.
Note: The serializer is still an experimental feature!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="189"/>
        <source>Use IfcOpenShell serializer if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="202"/>
        <source>2D objects will be exported as IfcAnnotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="205"/>
        <source>Export 2D objects as IfcAnnotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="221"/>
        <source>All FreeCAD object properties will be stored inside the exported objects,
allowing to recreate a full parametric model on reimport.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="225"/>
        <source>Export full FreeCAD parametric model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="238"/>
        <source>When possible, similar entities will be used only once in the file if possible.
This can reduce the file size a lot, but will make it less easily readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="242"/>
        <source>Reuse similar entities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="258"/>
        <source>When possible, IFC objects that are extruded rectangles will be
exported as IfcRectangleProfileDef.
However, some other applications might have problems importing that entity.
If this is your case, you can disable this and then all profiles will be exported as IfcArbitraryClosedProfileDef.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="264"/>
        <source>Disable IfcRectangleProfileDef</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="277"/>
        <source>Some IFC types such as IfcWall or IfcBeam have special standard versions
like IfcWallStandardCase or IfcBeamStandardCase.
If this option is turned on, FreeCAD will automatically export such objects
as standard cases when the necessary conditions are met.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="283"/>
        <source>Auto-detect and export as standard cases when applicable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="296"/>
        <source>If no site is found in the FreeCAD document, a default one will be added.
A site is not mandatory but a common practice is to have at least one in the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="300"/>
        <source>Add default site if one is not found in the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="313"/>
        <source>If no building is found in the FreeCAD document, a default one will be added.
Warning: The IFC standard asks for at least one building in each file. By turning this option off, you will produce a non-standard IFC file.
However, at FreeCAD, we believe having a building should not be mandatory, and this option is there to have a chance to demonstrate our point of view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="318"/>
        <source>Add default building if one is not found in the document (no standard)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="334"/>
        <source>If no building storey is found in the FreeCAD document, a default one will be added.
A building storey is not mandatory but a common practice to have at least one in the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="338"/>
        <source>Add default building storey if one is not found in the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="353"/>
        <location filename="../ui/preferences-ifc-export.ui" line="368"/>
        <source>The units you want your IFC file to be exported to.

Note that IFC files are ALWAYS written in metric units; imperial units
are only a conversion factor applied on top of them.
However, some BIM applications will use this factor to choose which
unit to work with when opening the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="361"/>
        <source>IFC file units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="383"/>
        <source>Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/preferences-ifc-export.ui" line="388"/>
        <source>Imperial</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Workbench</name>
    <message>
        <location filename="../../InitGui.py" line="75"/>
        <source>Structure tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="91"/>
        <source>Axis tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="99"/>
        <source>Panel tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="109"/>
        <source>Material tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="115"/>
        <source>Pipe tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="168"/>
        <source>Rebar tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="184"/>
        <source>Arch tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="188"/>
        <source>Draft creation tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="193"/>
        <source>Draft annotation tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="198"/>
        <source>Draft modification tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="206"/>
        <location filename="../../InitGui.py" line="211"/>
        <source>&amp;Arch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="207"/>
        <location filename="../../InitGui.py" line="240"/>
        <source>Utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="215"/>
        <location filename="../../InitGui.py" line="223"/>
        <location filename="../../InitGui.py" line="231"/>
        <location filename="../../InitGui.py" line="239"/>
        <source>&amp;Draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="216"/>
        <source>Creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="224"/>
        <source>Annotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InitGui.py" line="232"/>
        <source>Modification</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
