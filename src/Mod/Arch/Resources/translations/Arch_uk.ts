<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk" sourcelanguage="en">
  <context>
    <name>App::Property</name>
    <message>
      <location filename="../../ArchAxis.py" line="129"/>
      <source>The intervals between axes</source>
      <translation>Відстані між осями</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="136"/>
      <source>The angles of each axis</source>
      <translation>Кути кожної осі</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="143"/>
      <source>The label of each axis</source>
      <translation>Позначки кожної осі</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="150"/>
      <source>An optional custom bubble number</source>
      <translation>Опція необхідної кількості бульбашок</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="157"/>
      <source>The length of the axes</source>
      <translation>Довжина осей</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="172"/>
      <source>If not zero, the axes are not represented as one full line but as two lines of the given length</source>
      <translation>Якщо не нуль, осі будуть позначені не в вигляді одного повного рядка, а у вигляді двох рядків заданої довжини</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="271"/>
      <source>The size of the axis bubbles</source>
      <translation>Крок міток осей</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="279"/>
      <source>The numbering style</source>
      <translation>Стиль нумерації</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="298"/>
      <source>The type of line to draw this axis</source>
      <translation>Тип лінії, по якій треба накреслити дану вісь</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="310"/>
      <source>Where to add bubbles to this axis: Start, end, both or none</source>
      <translation>Куди додати бульбашки до цієї осі: початок, кінець, обоє або жодної</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="327"/>
      <source>The line width to draw this axis</source>
      <translation>Ширина лінії, щоб намалювати цю вісь</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="335"/>
      <source>The color of this axis</source>
      <translation>Колір цієї осі</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="343"/>
      <source>The number of the first axis</source>
      <translation>Номер першої осі</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="351"/>
      <source>The font to use for texts</source>
      <translation>Шрифт для тексту</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="359"/>
      <source>The font size</source>
      <translation>Розмір шрифту</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="367"/>
      <source>If true, show the labels</source>
      <translation>Якщо істина, відображати мітки</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="376"/>
      <source>A transformation to apply to each label</source>
      <translation>Перетворення для кожної позначки</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="226"/>
      <source>The base object this component is built upon</source>
      <translation>Базовий обʼєкт для будування цього компоненту</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="235"/>
      <source>The object this component is cloning</source>
      <translation>Обʼєкт цього компоненту на клонуванні</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="244"/>
      <location filename="../../ArchSite.py" line="725"/>
      <source>Other shapes that are appended to this object</source>
      <translation>Інші фігури, приєднані до цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="253"/>
      <location filename="../../ArchSite.py" line="734"/>
      <source>Other shapes that are subtracted from this object</source>
      <translation>Інші фігури, які віднімаються від цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="262"/>
      <location filename="../../ArchBuildingPart.py" line="388"/>
      <source>An optional description for this component</source>
      <translation>Необовʼязковий опис для цього компонента</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="271"/>
      <location filename="../../ArchBuildingPart.py" line="397"/>
      <source>An optional tag for this component</source>
      <translation>Необовʼязковий тег для цього компонента</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="281"/>
      <source>An optional standard (OmniClass, etc...) code for this component</source>
      <translation>Опція стандартний код (OmniClass тощо ...) для цього компонента</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="288"/>
      <source>A material for this object</source>
      <translation>Матеріал для цього обʼєкту</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="304"/>
      <source>Specifies if moving this object moves its base instead</source>
      <translation>Вказує, якщо переміщення цього об’єкта замість нього переміщує його основу</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="317"/>
      <source>Specifies if this object must move together when its host is moved</source>
      <translation>Вказує, чи повинен цей об'єкт рухатися разом із господарем</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="329"/>
      <source>The area of all vertical faces of this object</source>
      <translation>Площа всіх вертикальних граней цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="340"/>
      <location filename="../../ArchSite.py" line="744"/>
      <source>The area of the projection of this object onto the XY plane</source>
      <translation>Площа проекції цього об'єкта на площину XY</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="350"/>
      <source>The perimeter length of the horizontal area</source>
      <translation>Довжина периметра горизонтальної площини</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="361"/>
      <source>An optional higher-resolution mesh or shape for this object</source>
      <translation>Необов'язкова сітка з більшою роздільною здатністю або фігура для цього об'єкта</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="371"/>
      <source>An optional axis or axis system on which this object should be duplicated</source>
      <translation>Додаткові осі, або ж осі, вздовж яких має дублюватись об'єкт</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1414"/>
      <source>Use the material color as this object&apos;s shape color, if available</source>
      <translation type="unfinished">Use the material color as this object&apos;s shape color, if available</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="66"/>
      <source>A single section of the fence</source>
      <translation>Єдина секція огорожі</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="74"/>
      <source>A single fence post</source>
      <translation>Єдиний стовп огорожі</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="82"/>
      <source>The Path the fence should follow</source>
      <translation>Траєкторія огорожі якою вона слідує</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="92"/>
      <source>The number of sections the fence is built of</source>
      <translation>Кількість секцій, з яких побудований паркан</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="103"/>
      <source>The number of posts used to build the fence</source>
      <translation>Кількість стовпів, використаних для будівництва огорожі</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="312"/>
      <source>When true, the fence will be colored like the original post and section.</source>
      <translation>Якщо так, огорожа буде забарвлена ​​як оригінальний стовп та секція.</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="629"/>
      <source>The base terrain of this site</source>
      <translation>Базовий рельєф цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="639"/>
      <source>The street and house number of this site, with postal box or apartment number if needed</source>
      <translation>Вулиця та номер будинку цієї ділянки, за необхідності номер поштової скриньки або квартири</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="648"/>
      <source>The postal or zip code of this site</source>
      <translation>Поштовий індекс цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="655"/>
      <source>The city of this site</source>
      <translation>Місто цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="664"/>
      <source>The region, province or county of this site</source>
      <translation>Регіон або провінція цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="671"/>
      <source>The country of this site</source>
      <translation>Країна цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="678"/>
      <location filename="../../ArchSite.py" line="685"/>
      <source>The latitude of this site</source>
      <translation>Висота цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="695"/>
      <source>Angle between the true North and the North direction in this document</source>
      <translation>Кут між справжньою Північчю та Північним напрямком у цьому документі</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="707"/>
      <source>The elevation of level 0 of this site</source>
      <translation>Висота рівня 0 цієї ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="716"/>
      <source>A url that shows this site in a mapping website</source>
      <translation>Посилання, що показує цю ділянку на картографічному веб-сайті</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="753"/>
      <source>The perimeter length of this terrain</source>
      <translation>Довжина периметра цієї місцевості</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="762"/>
      <source>The volume of earth to be added to this terrain</source>
      <translation>Обсяг землі що буде додано до цієї місцевості</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="772"/>
      <source>The volume of earth to be removed from this terrain</source>
      <translation>Обсяг землі що буде видалений з цієї місцевості</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="782"/>
      <source>An extrusion vector to use when performing boolean operations</source>
      <translation>Вектор видавлювання, що використовуватиметься під час виконання логічних операцій</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="792"/>
      <source>Remove splitters from the resulting shape</source>
      <translation>Видалити розділювачі з результуючої форми</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="802"/>
      <source>An optional offset between the model (0,0,0) origin and the point indicated by the geocoordinates</source>
      <translation>Необовʼязковий зсув між початком координат моделі (0,0,0) та точкою, що зазначена геокоординатами</translation>
    </message>
    <message>
      <location filename="../../ArchIFC.py" line="83"/>
      <location filename="../../ArchSite.py" line="811"/>
      <source>The type of this object</source>
      <translation>Тип цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="822"/>
      <source>The time zone where this site is located</source>
      <translation>Часовий пояс, де розташований цей сайт</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="832"/>
      <source>An optional EPW File for the location of this site. Refer to the Site documentation to know how to obtain one</source>
      <translation>Інший EPW файл для розташування цього сайту. Зверніться до документації сайту, щоб дізнатися як отримати один</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1045"/>
      <source>Show wind rose diagram or not. Uses solar diagram scale. Needs Ladybug module</source>
      <translation>Показувати розу вітрів в чи ні. Використовує шкалу сонячної діаграми. Потрібен модуль сонечка</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1052"/>
      <source>Show solar diagram or not</source>
      <translation>Відображення сонячної діаграми чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1059"/>
      <source>The scale of the solar diagram</source>
      <translation>Масштаб сонячної діаграми</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1067"/>
      <source>The position of the solar diagram</source>
      <translation>Позиція сонячної діаграми</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1074"/>
      <source>The color of the solar diagram</source>
      <translation>Колір сонячної діаграми</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1085"/>
      <source>When set to &apos;True North&apos; the whole geometry will be rotated to match the true north of this site</source>
      <translation type="unfinished">When set to &apos;True North&apos; the whole geometry will be rotated to match the true north of this site</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1094"/>
      <source>Show compass or not</source>
      <translation>Відображати компас чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1103"/>
      <source>The rotation of the Compass relative to the Site</source>
      <translation>Обертання компаса, відносно ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1113"/>
      <source>The position of the Compass relative to the Site placement</source>
      <translation>Позиція компаса, відносно розміщення ділянки</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="1123"/>
      <source>Update the Declination value based on the compass rotation</source>
      <translation>Оновити значення Відхилення, засноване на обертанні компаса</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="216"/>
      <source>The diameter of the bar</source>
      <translation>Діаметр стрижня</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="226"/>
      <source>The distance between the border of the beam and the first bar (concrete cover).</source>
      <translation>Відстань між межею балки та першим стрижнем (бетонне покриття).</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="236"/>
      <source>The distance between the border of the beam and the last bar (concrete cover).</source>
      <translation>Відстань між межею балки та останнім стрижнем (бетонне покриття).</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="243"/>
      <source>The amount of bars</source>
      <translation>Кількість стрижнів</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="250"/>
      <source>The spacing between the bars</source>
      <translation>Відстань між стрижнями</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="261"/>
      <source>The total distance to span the rebars over. Keep 0 to automatically use the host shape size.</source>
      <translation>Загальна відстань до арматурних стержнів. Зберігайте 0, щоб автоматично використовувати розмір форми хоста.</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="271"/>
      <source>The direction to use to spread the bars. Keep (0,0,0) for automatic direction.</source>
      <translation>Напрямок поширення стрижнів. Залишити (0,0,0) для автоматичного напрямку.</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="281"/>
      <source>The fillet to apply to the angle of the base profile. This value is multiplied by the bar diameter.</source>
      <translation>Заокруглення наносити на кут базового профілю. Це значення множиться на діаметр бруска.</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="288"/>
      <source>List of placement of all the bars</source>
      <translation>Список розміщення всіх балок</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="297"/>
      <source>The structure object that hosts this rebar</source>
      <translation>Обʼєкт структури, що містить цю арматуру</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="304"/>
      <source>The custom spacing of rebar</source>
      <translation>Користувацький крок армування</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="311"/>
      <source>Length of a single rebar</source>
      <translation>Довжина одного стрижня</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="319"/>
      <source>Total length of all rebars</source>
      <translation>Загальна довжина всіх стрижнів</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="327"/>
      <source>The rebar mark</source>
      <translation>Марка арматури</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="714"/>
      <source>Shape of rebar</source>
      <translation>Форма арматури</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="128"/>
      <source>The axes this system is made of</source>
      <translation>Осі, що утворюють систему</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="135"/>
      <source>The placement of this axis system</source>
      <translation>Розташування цієї системи осей</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="338"/>
      <location filename="../../ArchMaterial.py" line="924"/>
      <source>A description for this material</source>
      <translation>Опис цього матеріалу</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="347"/>
      <location filename="../../ArchEquipment.py" line="394"/>
      <source>A standard code (MasterFormat, OmniClass,...)</source>
      <translation>Стандарт (MasterFormat, OmniClass,...)</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="357"/>
      <source>A URL where to find information about this material</source>
      <translation>Посилання, де знаходити інформацію про цей матеріал</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="366"/>
      <source>The transparency value of this material</source>
      <translation>Значення прозорості цього матеріалу</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="373"/>
      <source>The color of this material</source>
      <translation>Колір цього матеріалу</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="382"/>
      <source>The color of this material when cut</source>
      <translation>Колір цього матеріалу при різанні</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="930"/>
      <source>The list of layer names</source>
      <translation>Перелік імен шарів</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="936"/>
      <source>The list of layer materials</source>
      <translation>Перелік матеріалів шарів</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="942"/>
      <source>The list of layer thicknesses</source>
      <translation>Перелік товщин шарів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="303"/>
      <source>The length of these stairs, if no baseline is defined</source>
      <translation>Довжина сходів, якщо вони визначаються не базовою лінією</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="310"/>
      <source>The width of these stairs</source>
      <translation>Ширина цих сходів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="317"/>
      <source>The total height of these stairs</source>
      <translation>Загальна висота цих схоів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="327"/>
      <source>The alignment of these stairs on their baseline, if applicable</source>
      <translation>Вирівнювання цих сходів по основі, якщо можливо</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="340"/>
      <source>The width of a Landing (Second edge and after - First edge follows Width property)</source>
      <translation>Ширина посадки (другий край і після - перший край відповідає властивості Width)</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="352"/>
      <source>The number of risers in these stairs</source>
      <translation>Кількість підсходинків в цих сходах</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="361"/>
      <source>The depth of the treads of these stairs</source>
      <translation>Глибина східців цих сходів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="371"/>
      <source>The height of the risers of these stairs</source>
      <translation>Висота підсходинків цих сходів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="379"/>
      <source>The size of the nosing</source>
      <translation>Розмір виступу східця</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="386"/>
      <source>The thickness of the treads</source>
      <translation>Товщина східців</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="396"/>
      <source>The Blondel ratio indicates comfortable stairs and should be between 62 and 64cm or 24.5 and 25.5in</source>
      <translation>Співвідношення блонделя вказує на зручні сходи і має бути від 62 до 64cm або 24,5 і 25.5in</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="405"/>
      <source>The thickness of the risers</source>
      <translation>Товщина стояків</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="415"/>
      <source>The depth of the landing of these stairs</source>
      <translation>Глибина східців цих сходів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="426"/>
      <source>The depth of the treads of these stairs - Enforced regardless of Length or edge&apos;s Length</source>
      <translation type="unfinished">The depth of the treads of these stairs - Enforced regardless of Length or edge&apos;s Length</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="436"/>
      <source>The height of the risers of these stairs - Enforced regardless of Height or edge&apos;s Height</source>
      <translation type="unfinished">The height of the risers of these stairs - Enforced regardless of Height or edge&apos;s Height</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="446"/>
      <source>The direction of flight after landing</source>
      <translation>Напрямок польоту після приземлення</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="466"/>
      <source>The &apos;absolute&apos; top level of a flight of stairs leads to</source>
      <translation type="unfinished">The &apos;absolute&apos; top level of a flight of stairs leads to</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="474"/>
      <location filename="../../ArchStairs.py" line="482"/>
      <source>The &apos;left outline&apos; of stairs</source>
      <translation type="unfinished">The &apos;left outline&apos; of stairs</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="540"/>
      <source>The &apos;left outline&apos; of all segments of stairs</source>
      <translation type="unfinished">The &apos;left outline&apos; of all segments of stairs</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="550"/>
      <source>The &apos;right outline&apos; of all segments of stairs</source>
      <translation type="unfinished">The &apos;right outline&apos; of all segments of stairs</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="603"/>
      <source>The type of landings of these stairs</source>
      <translation>Тип сходових майданчиків</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="613"/>
      <source>The type of winders in these stairs</source>
      <translation>Тип забіжних ступенів в цих сходах</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="623"/>
      <source>The type of structure of these stairs</source>
      <translation>Тип структури цих сходів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="634"/>
      <source>The thickness of the massive structure or of the stringers</source>
      <translation>Товщина масивної конструкції або тетивів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="641"/>
      <source>The width of the stringers</source>
      <translation>Ширина тетиву</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="651"/>
      <source>The offset between the border of the stairs and the structure</source>
      <translation>Зсув між меж сходів і структурою</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="661"/>
      <location filename="../../ArchStairs.py" line="1694"/>
      <source>The overlap of the stringers above the bottom of the treads</source>
      <translation>Перекриття шнурів над нижної частиною проступей з'єднань</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="670"/>
      <source>The thickness of the lower floor slab</source>
      <translation>Товщина покриття підлоги</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="679"/>
      <source>The thickness of the upper floor slab</source>
      <translation>Товщина покриття підлоги</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="689"/>
      <source>The type of connection between the lower slab and the start of the stairs</source>
      <translation>Тип зв’язку між нижньою плитою та початком сходів</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="704"/>
      <source>The type of connection between the end of the stairs and the upper floor slab</source>
      <translation>Тип зв'язку між кінцем сходів і верхньою підлогою плити</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="100"/>
      <source>The base file this component is built upon</source>
      <translation>Базовий файл, на якому створюється цей компонент</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="109"/>
      <source>The part to use from the base file</source>
      <translation>Частина для використання з базового файлу</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="119"/>
      <source>The way the referenced objects are included in the current document. &apos;Normal&apos; includes the shape, &apos;Transient&apos; discards the shape when the object is switched off (smaller filesize), &apos;Lightweight&apos; does not import the shape but only the OpenInventor representation</source>
      <translation type="unfinished">The way the referenced objects are included in the current document. &apos;Normal&apos; includes the shape, &apos;Transient&apos; discards the shape when the object is switched off (smaller filesize), &apos;Lightweight&apos; does not import the shape but only the OpenInventor representation</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="136"/>
      <source>Fuse objects of same material</source>
      <translation>Злити обʼєкти одного матеріалу</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="444"/>
      <source>The latest time stamp of the linked file</source>
      <translation>Остання часова мітка звʼязаного файлу</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="455"/>
      <source>If true, the colors from the linked file will be kept updated</source>
      <translation>Якщо увімкнено, кольори з зв'язаного файлу будуть оновлюватися</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1106"/>
      <location filename="../../ArchFloor.py" line="244"/>
      <source>The placement of this object</source>
      <translation>Розміщення цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1113"/>
      <location filename="../../ArchBuildingPart.py" line="404"/>
      <source>The shape of this object</source>
      <translation>Форма цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1123"/>
      <source>The objects that must be considered by this section plane. Empty means the whole document.</source>
      <translation>Об'єкти, які необхідно враховувати за допомогою цієї площини перерізу. Порожній означає весь документ.</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1133"/>
      <source>If false, non-solids will be cut too, with possible wrong results.</source>
      <translation>Якщо хибне, несуцільні також будуть вирізані, з можливими хибними результатами.</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1144"/>
      <source>If True, resulting views will be clipped to the section plane area.</source>
      <translation>Якщо значення "Істина", отримані види будуть відсічені до площини перерізу.</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1154"/>
      <source>If true, the color of the objects material will be used to fill cut areas.</source>
      <translation>Якщо увімкнено, колір матеріалу, який буде використовуватися для заповнення розрізаних областей.</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1165"/>
      <source>Geometry further than this value will be cut off. Keep zero for unlimited.</source>
      <translation>Геометрія, що перевищує це значення, буде відрізана. Зберігайте нуль для безмежного.</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1248"/>
      <source>The display length of this section plane</source>
      <translation>Довжина відображення цієї площини перерізу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1261"/>
      <source>The display height of this section plane</source>
      <translation>Висота відображення цієї площини перерізу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1274"/>
      <source>The size of the arrows of this section plane</source>
      <translation>Розмір стрілок цієї площини перерізу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1282"/>
      <source>The transparency of this object</source>
      <translation>Прозорість цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1290"/>
      <location filename="../../ArchBuildingPart.py" line="660"/>
      <source>The line width of this object</source>
      <translation>Товщина лінії цього обʼєкту</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1298"/>
      <location filename="../../ArchSectionPlane.py" line="1313"/>
      <source>Show the cut in the 3D view</source>
      <translation>Показати розріз в 3D перегляді</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1305"/>
      <source>The color of this object</source>
      <translation>Колір цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1323"/>
      <source>The distance between the cut plane and the actual view cut (keep this a very small value but not zero)</source>
      <translation>Відстань між розрізом площини та фактичним вирізом (має дуже маленьке значення, але не нуль)</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1331"/>
      <source>Show the label in the 3D view</source>
      <translation>Показати позначки в 3D-вигляді</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1338"/>
      <location filename="../../ArchSpace.py" line="657"/>
      <source>The name of the font</source>
      <translation>Назва шрифту</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1346"/>
      <location filename="../../ArchSpace.py" line="673"/>
      <source>The size of the text font</source>
      <translation>Розмір шрифту тексту</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1666"/>
      <location filename="../../ArchPanel.py" line="1086"/>
      <location filename="../../ArchPanel.py" line="1252"/>
      <source>The linked object</source>
      <translation>Повʼязаний обʼєкт</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1673"/>
      <source>The rendering mode to use</source>
      <translation>Використовувати режим рендерингу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1682"/>
      <source>If cut geometry is shown or not</source>
      <translation>Показувати чи ні обрізану геометрію</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1689"/>
      <source>If cut geometry is filled or not</source>
      <translation>Якщо розріз геометрії заповнен чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1698"/>
      <location filename="../../ArchPanel.py" line="1095"/>
      <source>The line width of the rendered objects</source>
      <translation>Товщина лінії відтворюваних обʼєктів</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1708"/>
      <source>The size of the texts inside this object</source>
      <translation>Розмір тексту всередині цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1719"/>
      <source>If checked, source objects are displayed regardless of being visible in the 3D model</source>
      <translation>Якщо відмічено, то вихідні об'єкти відображаються, незалежно від того, чи вони видимі в 3D моделі</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1728"/>
      <source>The line color of the projected objects</source>
      <translation>Колір ліній проектованих обʼєктів</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1737"/>
      <source>The color of the cut faces (if turned on)</source>
      <translation>Колір обрізаних граней (якщо увімкнено)</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="303"/>
      <source>The list of angles of the roof segments</source>
      <translation>Список кутів сегментів даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="313"/>
      <source>The list of horizontal length projections of the roof segments</source>
      <translation>Перелік горизонтальних довжин проекцій сегментів даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="323"/>
      <source>The list of IDs of the relative profiles of the roof segments</source>
      <translation>Список ID відносних профілів сегментів даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="332"/>
      <source>The list of thicknesses of the roof segments</source>
      <translation>Список товщин сегментів даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="341"/>
      <source>The list of overhangs of the roof segments</source>
      <translation>Список перекриття сегментів даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="351"/>
      <source>The list of calculated heights of the roof segments</source>
      <translation>Список обчислених висот сегментів даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="361"/>
      <source>The face number of the base object used to build the roof</source>
      <translation>Кількість поверхонь базового об'єкта, що використовуються в конструкції цього даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="371"/>
      <source>The total length of the ridges and hips of the roof</source>
      <translation>Загальна довжина гребня та ребер даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="381"/>
      <source>The total length of the borders of the roof</source>
      <translation>Загальна довжина меж даху</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="392"/>
      <source>Specifies if the direction of the roof should be flipped</source>
      <translation>Визначає, чи напрям покрівлі треба повернути</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="740"/>
      <source>The objects that host this window</source>
      <translation>Обʼєкти, що розміщують це вікно</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="747"/>
      <source>The components of this window</source>
      <translation>Компоненти цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="758"/>
      <source>The depth of the hole that this window makes in its host object. If 0, the value will be calculated automatically.</source>
      <translation>Глибина отвору, яке це вікно робить у своєму хост-об’єкті. Якщо 0, значення обчислюється автоматично.</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="768"/>
      <source>An optional object that defines a volume to be subtracted from hosts of this window</source>
      <translation>Необов’язковий об’єкт, який визначає том, який слід відняти від хостів цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="775"/>
      <source>The width of this window</source>
      <translation>Ширина цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="782"/>
      <source>The height of this window</source>
      <translation>Висота цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="791"/>
      <source>The normal direction of this window</source>
      <translation>Напрямок нормалі цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="800"/>
      <source>The preset number this window is based on</source>
      <translation>Початковий номер цього вікна базується на</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="808"/>
      <source>The frame size of this window</source>
      <translation>Розмір цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="815"/>
      <source>The offset size of this window</source>
      <translation>Величина зсуву цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="822"/>
      <source>The area of this window</source>
      <translation>Область цього вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="829"/>
      <source>The width of louvre elements</source>
      <translation>Ширина елементів жалюзі</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="836"/>
      <source>The space between louvre elements</source>
      <translation>Відстань між елементами жалюзі</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="845"/>
      <source>Opens the subcomponents that have a hinge defined</source>
      <translation>Відкриває підкомпоненти, в яких визначений шарнір</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="855"/>
      <source>The number of the wire that defines the hole. If 0, the value will be calculated automatically</source>
      <translation>Кількість каркасів, що визначає отвір. Якщо 0, значення обчислюється автоматично</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="864"/>
      <source>Shows plan opening symbols if available</source>
      <translation>Показує план відкриття символів, якщо такі є</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="873"/>
      <source>Show elevation opening symbols if available</source>
      <translation>Показати висоту відкриття символів, якщо такі є</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="1855"/>
      <source>The number of the wire that defines the hole. A value of 0 means automatic</source>
      <translation>Число дротів, що визначає отвір. Значення 0 означає автоматичне значення</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="376"/>
      <source>The model description of this equipment</source>
      <translation>Опис моделі даного обладнання</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="385"/>
      <source>The URL of the product page of this equipment</source>
      <translation>URL-адреса сторінки товару даного обладнання</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="403"/>
      <source>Additional snap points for this equipment</source>
      <translation>Додаткові точки привʼязки для даного обладнання</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="413"/>
      <source>The electric power needed by this equipment in Watts</source>
      <translation>Електрична потужність, необхідна для цього обладнання у Ватах</translation>
    </message>
    <message>
      <location filename="../../ArchIFC.py" line="75"/>
      <source>IFC data</source>
      <translation>Дані IFC</translation>
    </message>
    <message>
      <location filename="../../ArchIFC.py" line="92"/>
      <source>IFC properties of this object</source>
      <translation>IFC властивості  цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchIFC.py" line="318"/>
      <location filename="../../ArchIFC.py" line="332"/>
      <source>Description of IFC attributes are not yet implemented</source>
      <translation>Опис атрибутів IFC ще не реалізований</translation>
    </message>
    <message>
      <location filename="../../ArchBuilding.py" line="287"/>
      <location filename="../../ArchBuildingPart.py" line="232"/>
      <location filename="../../ArchBuildingPart.py" line="261"/>
      <source>The type of this building</source>
      <translation>Тип цієї будівлі</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="131"/>
      <source>The profile used to build this frame</source>
      <translation>Профіль використовується для створення цього кадру</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="141"/>
      <source>Specifies if the profile must be aligned with the extrusion wires</source>
      <translation>Вказує, чи повинен профіль бути вирівняний з екструзійними дротами</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="152"/>
      <source>An offset vector between the base sketch and the frame</source>
      <translation>Вектор зсуву між базовим ескізом і рамкою</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="161"/>
      <source>Crossing point of the path on the profile.</source>
      <translation>Точка перетину шляху на профілі.</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="171"/>
      <source>An optional additional placement to add to the profile before extruding it</source>
      <translation>Необов'язкове додаткове розміщення для додавання до профілю перед його видавлюванням</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="181"/>
      <source>The rotation of the profile around its extrusion axis</source>
      <translation>Обертання профілю навколо його осі екструзії</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="188"/>
      <source>The type of edges to consider</source>
      <translation>Тип країв для розглядання</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="204"/>
      <source>If true, geometry is fused, otherwise a compound</source>
      <translation>Якщо увімкнено, геометрія зливається в іншому випадку - сполучається</translation>
    </message>
    <message>
      <location filename="../../ArchFloor.py" line="227"/>
      <location filename="../../ArchBuildingPart.py" line="350"/>
      <source>The height of this object</source>
      <translation>Висота цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchFloor.py" line="236"/>
      <location filename="../../ArchBuildingPart.py" line="379"/>
      <source>The computed floor area of this floor</source>
      <translation>Обчислена площа цього поверху</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="70"/>
      <source>The length of this element</source>
      <translation>Довжина цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="77"/>
      <source>The width of this element</source>
      <translation>Ширина елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="84"/>
      <source>The height of this element</source>
      <translation>Висота елементу</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="971"/>
      <location filename="../../ArchPrecast.py" line="93"/>
      <source>The structural nodes of this element</source>
      <translation>Структурні вузли цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="128"/>
      <location filename="../../ArchPrecast.py" line="346"/>
      <location filename="../../ArchPrecast.py" line="528"/>
      <source>The size of the chamfer of this element</source>
      <translation>Розмір фаски елементу</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="135"/>
      <source>The dent length of this element</source>
      <translation>Довжина отвору цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="142"/>
      <location filename="../../ArchPrecast.py" line="542"/>
      <source>The dent height of this element</source>
      <translation>Висота отвору цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="149"/>
      <location filename="../../ArchPrecast.py" line="385"/>
      <source>The dents of this element</source>
      <translation>Отвори цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="270"/>
      <source>The chamfer length of this element</source>
      <translation>Довжина фаски цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="277"/>
      <source>The base length of this element</source>
      <translation>Базова довжина цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="353"/>
      <source>The groove depth of this element</source>
      <translation>Глибина пазу цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="360"/>
      <source>The groove height of this element</source>
      <translation>Висота пазу цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="369"/>
      <source>The spacing between the grooves of this element</source>
      <translation>Відстань між пазами цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="378"/>
      <source>The number of grooves of this element</source>
      <translation>Кількість пазів цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="535"/>
      <source>The dent width of this element</source>
      <translation>Ширина отвору цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="654"/>
      <source>The type of this slab</source>
      <translation>Тип цієї плити</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="664"/>
      <source>The size of the base of this element</source>
      <translation>Розмір основи цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="673"/>
      <source>The number of holes in this element</source>
      <translation>Кількість отворів у цьому елементі</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="682"/>
      <source>The major radius of the holes of this element</source>
      <translation>Основний радіус отворів цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="691"/>
      <source>The minor radius of the holes of this element</source>
      <translation>Незначний радіус отворів цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="700"/>
      <source>The spacing between the holes of this element</source>
      <translation>Відстань між отворами елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="813"/>
      <source>The length of the down floor of this element</source>
      <translation>Довжина нижньої частини елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="822"/>
      <source>The number of risers in this element</source>
      <translation>Кількість підсходинків в цьому елементі</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="829"/>
      <source>The riser height of this element</source>
      <translation>Висота підсходинка цього елементу</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="836"/>
      <source>The tread depth of this element</source>
      <translation>Глибина протектору цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="199"/>
      <source>An optional host object for this curtain wall</source>
      <translation>Опція форми об’єкту для цієї навісної стіни</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="209"/>
      <source>The height of the curtain wall, if based on an edge</source>
      <translation>Висота навісної стіни якщо базуватись на ребрі</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="217"/>
      <source>The number of vertical mullions</source>
      <translation>Кількість вертикальних опор</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="228"/>
      <source>If the profile of the vertical mullions get aligned with the surface or not</source>
      <translation>Якщо профіль вертикальних опор вирівняється з поверхнею чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="238"/>
      <source>The number of vertical sections of this curtain wall</source>
      <translation>Кількість вертикальних частин цієї завіси</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="253"/>
      <source>The height of the vertical mullions profile, if no profile is used</source>
      <translation>Розмір діагональних опор, якщо такі є, якщо не використовується профіль</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="264"/>
      <source>The width of the vertical mullions profile, if no profile is used</source>
      <translation>Ширина профілю вертикальних опор, якщо не використовується профіль</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="275"/>
      <source>A profile for vertical mullions (disables vertical mullion size)</source>
      <translation>Профіль для вертикальних опор (вимикає розмір вертикальних опор)</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="282"/>
      <source>The number of horizontal mullions</source>
      <translation>Кількість горизонтальних опор</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="293"/>
      <source>If the profile of the horizontal mullions gets aligned with the surface or not</source>
      <translation>Якщо профіль вертикальних опор вирівняється з поверхнею чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="303"/>
      <source>The number of horizontal sections of this curtain wall</source>
      <translation>Кількість вертикальних частин цієї завіси</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="318"/>
      <source>The height of the horizontal mullions profile, if no profile is used</source>
      <translation>Розмір діагональних опор, якщо такі є, якщо не використовується профіль</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="329"/>
      <source>The width of the horizontal mullions profile, if no profile is used</source>
      <translation>Ширина профілю вертикальних опор, якщо не використовується профіль</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="340"/>
      <source>A profile for horizontal mullions (disables horizontal mullion size)</source>
      <translation>Профіль для горизонтальних опор (вимикає розмір горизонтальних опор)</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="347"/>
      <source>The number of diagonal mullions</source>
      <translation>Кількість діагональних опор</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="358"/>
      <source>The size of the diagonal mullions, if any, if no profile is used</source>
      <translation>Розмір діагональних опор, якщо такі є, якщо не використовується профіль</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="369"/>
      <source>A profile for diagonal mullions, if any (disables horizontal mullion size)</source>
      <translation>Профіль для діагональних опор, якщо такі є (вимикає розмір горизонтальних опор)</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="376"/>
      <source>The number of panels</source>
      <translation>Кількість панелей</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="384"/>
      <source>The thickness of the panels</source>
      <translation>Товщина панелей</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="394"/>
      <source>Swaps horizontal and vertical lines</source>
      <translation>Міняє місцями горизонтальні та вертикальні лінії</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="404"/>
      <source>Perform subtractions between components so none overlap</source>
      <translation>Виконує віднімання між компонентами, щоб жодне не перетиналося</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="413"/>
      <source>Centers the profile over the edges or not</source>
      <translation>Центрувати профіль по краях чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="424"/>
      <source>The vertical direction reference to be used by this object to deduce vertical/horizontal directions. Keep it close to the actual vertical direction of your curtain wall</source>
      <translation>Посилання на вертикальний напрямок, яке використовується цим об’єктом для виведення вертикального / горизонтального напрямків. Тримайте його близько до фактичного вертикального напрямку вашого покриття стіни</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="284"/>
      <source>Outside Diameter</source>
      <translation>Зовнішній діаметр</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="290"/>
      <source>Wall thickness</source>
      <translation>Товщина стінки</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="319"/>
      <location filename="../../ArchProfile.py" line="397"/>
      <location filename="../../ArchProfile.py" line="431"/>
      <location filename="../../ArchProfile.py" line="495"/>
      <source>Width of the beam</source>
      <translation>Ширина променю</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="325"/>
      <location filename="../../ArchProfile.py" line="403"/>
      <location filename="../../ArchProfile.py" line="437"/>
      <location filename="../../ArchProfile.py" line="501"/>
      <source>Height of the beam</source>
      <translation>Висота променю</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="331"/>
      <source>Thickness of the web</source>
      <translation>Товщина стрижня</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="337"/>
      <source>Thickness of the flanges</source>
      <translation>Товщина фланців</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="443"/>
      <source>Thickness of the sides</source>
      <translation>Товщина сторін</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="507"/>
      <source>Thickness of the webs</source>
      <translation>Товщина стрижнів</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="513"/>
      <source>Thickness of the flange</source>
      <translation>Товщина фланця</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="225"/>
      <source>The diameter of this pipe, if not based on a profile</source>
      <translation>Діаметр цієї труби, якщо вона не базується на профілі</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="234"/>
      <source>The length of this pipe, if not based on an edge</source>
      <translation>Довжина цієї труби, якщо вона не базується на ребрі</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="243"/>
      <source>An optional closed profile to base this pipe on</source>
      <translation>Коефіцієнт заповнення цього листа</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="250"/>
      <source>Offset from the start point</source>
      <translation>Зсув від початкової точки</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="257"/>
      <source>Offset from the end point</source>
      <translation>Зсув від кінцевої точки</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="267"/>
      <source>The wall thickness of this pipe, if not based on a profile</source>
      <translation>Товщина стіни цієї труби, якщо вона не базується на профілі</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="470"/>
      <source>The curvature radius of this connector</source>
      <translation>Радіус викривлення зʼєднувача</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="479"/>
      <source>The pipes linked by this connector</source>
      <translation>Труби, повʼязані з цим зʼєднувачем</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="486"/>
      <source>The type of this connector</source>
      <translation>Тип зʼєднувача</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="885"/>
      <source>The length of this wall. Not used if this wall is based on an underlying object</source>
      <translation>Довжина цієї стіни. Не використовується, якщо стіна базується на об'єкті, який розташований під нею</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="895"/>
      <source>The width of this wall. Not used if this wall is based on a face</source>
      <translation>Ширина цієї стіни. Не використовується, якщо стіна розташована на поверхні</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="907"/>
      <source>This overrides Width attribute to set width of each segment of wall.  Ignored if Base object provides Widths information, with getWidths() method.  (The 1st value override &apos;Width&apos; attribute for 1st segment of wall; if a value is zero, 1st value of &apos;OverrideWidth&apos; will be followed)</source>
      <translation type="unfinished">This overrides Width attribute to set width of each segment of wall.  Ignored if Base object provides Widths information, with getWidths() method.  (The 1st value override &apos;Width&apos; attribute for 1st segment of wall; if a value is zero, 1st value of &apos;OverrideWidth&apos; will be followed)</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="918"/>
      <source>This overrides Align attribute to set Align of each segment of wall.  Ignored if Base object provides Aligns information, with getAligns() method.  (The 1st value override &apos;Align&apos; attribute for 1st segment of wall; if a value is not &apos;Left, Right, Center&apos;, 1st value of &apos;OverrideAlign&apos; will be followed)</source>
      <translation type="unfinished">This overrides Align attribute to set Align of each segment of wall.  Ignored if Base object provides Aligns information, with getAligns() method.  (The 1st value override &apos;Align&apos; attribute for 1st segment of wall; if a value is not &apos;Left, Right, Center&apos;, 1st value of &apos;OverrideAlign&apos; will be followed)</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="929"/>
      <source>The height of this wall. Keep 0 for automatic. Not used if this wall is based on a solid</source>
      <translation>Висота стіни. Задайте 0 для автоматичного визначення. Не використовується, якщо ця стіна базується на твердому тілі</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="939"/>
      <source>The area of this wall as a simple Height * Length calculation</source>
      <translation>Площа цієї стіни як просте обчислення висоти * довжини</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="950"/>
      <source>The alignment of this wall on its base object, if applicable</source>
      <translation>Вирівнювання стіни на базовому об'єкті, якщо можливо</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="962"/>
      <location filename="../../ArchPanel.py" line="626"/>
      <location filename="../../ArchWall.py" line="961"/>
      <source>The normal extrusion direction of this object (keep (0,0,0) for automatic normal)</source>
      <translation>Нормальний напрямок видавлювання для цього об'єкта (залишити (0,0,0) для завдання автоматичної нормалі)</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="971"/>
      <source>The face number of the base object used to build this wall</source>
      <translation>Номер поверхні базового об'єкту, що використовується для створення цієї стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="981"/>
      <source>The offset between this wall and its baseline (only for left and right alignments)</source>
      <translation>Зсув стіни від базової лінії (тільки для лівого і правого вирівнювання)</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="997"/>
      <source>Enable this to make the wall generate blocks</source>
      <translation>Увімкніть це, щоб стіна генерувала блоки</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1004"/>
      <source>The length of each block</source>
      <translation>Довжина кожного блоку</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1011"/>
      <source>The height of each block</source>
      <translation>Висота кожного блоку</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1020"/>
      <source>The horizontal offset of the first line of blocks</source>
      <translation>Горизонтальний зсув першого рядка блоків</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1030"/>
      <source>The horizontal offset of the second line of blocks</source>
      <translation>Горизонтальний зсув другого рядка блоків</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1039"/>
      <source>The size of the joints between each block</source>
      <translation>Розмір зʼєднання між кожним блоком</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1046"/>
      <source>The number of entire blocks</source>
      <translation>Кількість всіх блоків</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1054"/>
      <source>The number of broken blocks</source>
      <translation>Кількість зламаних блоків</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="174"/>
      <source>The angle of the truss</source>
      <translation>Кут ферми покрівлі</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="182"/>
      <source>The slant type of this truss</source>
      <translation>Косий тип цієї ферми покрівлі</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="192"/>
      <source>The normal direction of this truss</source>
      <translation>Напрямок нормалі цієї ферми покрівлі</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="202"/>
      <source>The height of the truss at the start position</source>
      <translation>Висота ферми покрівлі у початковому положенні</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="212"/>
      <source>The height of the truss at the end position</source>
      <translation>Висота ферми покрівлі у кінцевому положенні</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="222"/>
      <source>An optional start offset for the top strut</source>
      <translation>Необовʼязковий початковий зсув для верхньої стійки</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="231"/>
      <source>An optional end offset for the top strut</source>
      <translation>Необовʼязковий кінцевий зсув для верхньої стійки</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="241"/>
      <source>The height of the main top and bottom elements of the truss</source>
      <translation>Висота основних і нижніх елементів ферм покрівлі</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="252"/>
      <source>The width of the main top and bottom elements of the truss</source>
      <translation>Висота основних і нижніх елементів ферм покрівлі</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="262"/>
      <source>The type of the middle element of the truss</source>
      <translation>Тип середнього елемента ферми покрівлі</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="270"/>
      <source>The direction of the rods</source>
      <translation>Напрямок стрижнів</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="278"/>
      <source>The diameter or side of the rods</source>
      <translation>Діаметр або сторона стрижнів</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="286"/>
      <source>The number of rod sections</source>
      <translation>Кількість секцій стрижня</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="296"/>
      <source>If the truss has a rod at its endpoint or not</source>
      <translation>Якщо ферма покрівлі має стрижень у кінцевій точці чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="303"/>
      <source>How to draw the rods</source>
      <translation>Як намалювати стрижні</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="105"/>
      <source>The description column</source>
      <translation>Стовпець опису</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="112"/>
      <source>The values column</source>
      <translation>Значення значень</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="119"/>
      <source>The units column</source>
      <translation>Одиниці стовпця</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="126"/>
      <source>The objects column</source>
      <translation>Обʼєкти стовпця</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="133"/>
      <source>The filter column</source>
      <translation>Фільтр стовпця</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="143"/>
      <source>If True, a spreadsheet containing the results is recreated when needed</source>
      <translation>Якщо значення True, електронна таблиця, що містить результати, за потреби відтворюється</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="152"/>
      <source>The spreadsheet to print the results to</source>
      <translation>Таблиця результатів</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="162"/>
      <source>If True, additional lines with each individual object are added to the results</source>
      <translation>Якщо  True, до результатів додаються додаткові лінії з кожним окремим об'єктом</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="298"/>
      <source>The objects that make the boundaries of this space object</source>
      <translation>Об'єкти, що утворюють кордон заданого просторового об'єкта</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="307"/>
      <source>The computed floor area of this space</source>
      <translation>Обчислена площа цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="316"/>
      <source>The finishing of the floor of this space</source>
      <translation>Оздоблення підлоги цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="325"/>
      <source>The finishing of the walls of this space</source>
      <translation>Оздоблення стін цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="334"/>
      <source>The finishing of the ceiling of this space</source>
      <translation>Оздоблення стелі цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="344"/>
      <source>Objects that are included inside this space, such as furniture</source>
      <translation>Об'єкти, які знаходяться всередині цього простору, такі як меблі</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="351"/>
      <source>The type of this space</source>
      <translation>Тип цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="359"/>
      <source>The thickness of the floor finish</source>
      <translation>Товщина покриття підлоги</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="369"/>
      <source>The number of people who typically occupy this space</source>
      <translation>Кількість людей, які зазвичай займають цей простір</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="379"/>
      <source>The electric power needed to light this space in Watts</source>
      <translation>Електроенергія у ватах, необхідна для освітлення цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="389"/>
      <source>The electric power needed by the equipment of this space in Watts</source>
      <translation>Електроенергія у ватах, необхідна для обладнання цього простору</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="399"/>
      <source>If True, Equipment Power will be automatically filled by the equipment included in this space</source>
      <translation>Якщо значення True, потужність обладнання буде автоматично заповнене обладнанням, що входить у цей простір</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="408"/>
      <source>The type of air conditioning of this space</source>
      <translation>Тип кондиціонування повітря в цьому просторі</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="418"/>
      <source>Specifies if this space is internal or external</source>
      <translation>Вказує, чи є ця область внутрішньою або зовнішньою</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="649"/>
      <source>The text to show. Use $area, $label, $tag, $floor, $walls, $ceiling to insert the respective data</source>
      <translation>Текст для відображення. Використовуйте $area, $label, $tag, $floor, $walls, $ceiling, щоб вставити відповідні дані</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="665"/>
      <source>The color of the area text</source>
      <translation>Колір області тексту</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="683"/>
      <source>The size of the first line of text</source>
      <translation>Розмір першого рядка тексту</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="693"/>
      <source>The space between the lines of text</source>
      <translation>Відстань між рядками тексту</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="704"/>
      <source>The position of the text. Leave (0,0,0) for automatic position</source>
      <translation>Розташування тексту. Залишити (0,0,0) для автоматичного розташування</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="711"/>
      <source>The justification of the text</source>
      <translation>Вирівнювання тексту</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="723"/>
      <source>The number of decimals to use for calculated texts</source>
      <translation>Кількість десяткових знаків для використання розрахункових текстів</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="731"/>
      <source>Show the unit suffix</source>
      <translation>Показати суфікс одиниці</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="932"/>
      <location filename="../../ArchPanel.py" line="506"/>
      <source>The length of this element, if not based on a profile</source>
      <translation>Довжина елемента, якщо вона не визначена в профілі</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="942"/>
      <location filename="../../ArchPanel.py" line="516"/>
      <source>The width of this element, if not based on a profile</source>
      <translation>Ширина елемента, якщо вона не визначена в профілі</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="525"/>
      <source>The thickness or extrusion depth of this element</source>
      <translation>Товщина або глибина видавлювання цього елемента</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="532"/>
      <source>The number of sheets to use</source>
      <translation>Кількість аркушів, для використання</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="542"/>
      <source>The offset between this panel and its baseline</source>
      <translation>Відстань між цією панеллю та її базовою лінією</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="551"/>
      <source>The length of waves for corrugated elements</source>
      <translation>Довжина хвиль для гофрованих елементів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="560"/>
      <source>The height of waves for corrugated elements</source>
      <translation>Висота хвиль для гофрованих елементів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="570"/>
      <source>The horizontal offset of waves for corrugated elements</source>
      <translation>Висота хвиль для гофрованих елементів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="579"/>
      <source>The direction of waves for corrugated elements</source>
      <translation>Напрямок хвиль для гофрованих елементів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="588"/>
      <source>The type of waves for corrugated elements</source>
      <translation>Тип хвиль для гофрованих елементів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="598"/>
      <source>If the wave also affects the bottom side or not</source>
      <translation>Якщо хвиля також впливає на нижню сторону або ні</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="605"/>
      <source>The area of this panel</source>
      <translation>Область цієї панелі</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1001"/>
      <location filename="../../ArchPanel.py" line="615"/>
      <source>The facemaker type to use to build the profile of this object</source>
      <translation>Тип генератора поверхні, який слід використовувати для створення профілю цього об'єкта</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1103"/>
      <source>The color of the panel outline</source>
      <translation>Колір панелі дерева</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1111"/>
      <location filename="../../ArchPanel.py" line="1270"/>
      <location filename="../../ArchPanel.py" line="1627"/>
      <source>The size of the tag text</source>
      <translation>Розмір тексту тега</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1118"/>
      <source>The color of the tag text</source>
      <translation>Колір тексту тега</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1126"/>
      <source>The X offset of the tag text</source>
      <translation>X координата тексту тега</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1134"/>
      <source>The Y offset of the tag text</source>
      <translation>Y координата тексту тега</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1142"/>
      <location filename="../../ArchPanel.py" line="1295"/>
      <location filename="../../ArchPanel.py" line="1652"/>
      <source>The font of the tag text</source>
      <translation>Шрифт тексту тега</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1262"/>
      <source>The text to display. Can be %tag%, %label% or %description% to display the panel tag or label</source>
      <translation>Текст для відображення. Повинен бути %tag%, %label% або %description% для відображення панелі тегу або мітки</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1281"/>
      <location filename="../../ArchPanel.py" line="1638"/>
      <source>The position of the tag text. Keep (0,0,0) for center position</source>
      <translation>Розташування тексту тега. Залиште (0,0,0) для центрального положення</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1288"/>
      <location filename="../../ArchPanel.py" line="1645"/>
      <source>The rotation of the tag text</source>
      <translation>Обертання тексту тега</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1308"/>
      <location filename="../../ArchPanel.py" line="1689"/>
      <source>If True, the object is rendered as a face, if possible.</source>
      <translation>Якщо увімкнено, об'єкт представляється як поверхня, якщо це можливо.</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1318"/>
      <source>The allowed angles this object can be rotated to when placed on sheets</source>
      <translation>Дозволені кути повороту цього об’єкта при розміщенні на листі</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1329"/>
      <source>An offset value to move the cut plane from the center point</source>
      <translation>Значення зсуву для переміщення площини зрізу з центральної точки</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1516"/>
      <location filename="../../ArchPanel.py" line="1873"/>
      <source>A margin inside the boundary</source>
      <translation>Поле всередині границі</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1525"/>
      <location filename="../../ArchPanel.py" line="1882"/>
      <source>Turns the display of the margin on/off</source>
      <translation>Ввімкнення/вимкнення відображення поля</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1613"/>
      <source>The linked Panel cuts</source>
      <translation>Повʼязана панель розрізів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1620"/>
      <source>The tag text to display</source>
      <translation>Текст тега для відображення</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1656"/>
      <source>The font file</source>
      <translation>Файл шрифту</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1662"/>
      <source>The width of the sheet</source>
      <translation>Ширина аркуша</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1670"/>
      <source>The height of the sheet</source>
      <translation>Висота аркуша</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1678"/>
      <source>The fill ratio of this sheet</source>
      <translation>Коефіцієнт заповнення цього листа</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1699"/>
      <source>Specifies an angle for the wood grain (Clockwise, 0 is North)</source>
      <translation>Визначає напрямок волокон деревини (за годинниковою стрілкою, 0 - Північ)</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1708"/>
      <source>Specifies the scale applied to each panel view.</source>
      <translation>Точно визначенний масштаб використовується до кожної візуалізації панелі</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1718"/>
      <source>A list of possible rotations for the nester</source>
      <translation>Список можливих обертацій для ручки</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="1892"/>
      <source>Turns the display of the wood grain texture on/off</source>
      <translation>Вмикає / вимикає відображення зернистої текстури дерева</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="360"/>
      <source>If true, the height value propagates to contained objects</source>
      <translation>Якщо значення true, значення висоти поширюється на вміщені об'єкти</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="370"/>
      <source>The level of the (0,0,0) point of this level</source>
      <translation>Рівень (0,0,0) цього рівня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="414"/>
      <source>This property stores an inventor representation for this object</source>
      <translation>Цій властивості зберігається подання винахідника для цього об’єкта</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="425"/>
      <source>If true, only solids will be collected by this object when referenced from other files</source>
      <translation>Якщо увімкнено, будуть збиратися лише суцільні об’єкти, коли він посилається на інші файли</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="436"/>
      <source>A MaterialName:SolidIndexesList map that relates material names with solid indexes to be used when referencing this object from other files</source>
      <translation>Назва MaterialName: Карта SolidIndexesList що пов'язує імена матеріалів з суцільними індексами для посилань на цей об'єкт з інших файлів</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="670"/>
      <source>An optional unit to express levels</source>
      <translation>Опція одиниці рівнів</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="679"/>
      <source>A transformation to apply to the level mark</source>
      <translation>Трансформація, яка застосовується до позначки рівня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="689"/>
      <source>If true, show the level</source>
      <translation>Якщо увімкнено, показувати рівень</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="699"/>
      <source>If true, show the unit on the level tag</source>
      <translation>Якщо увімкнено, показувати одиниці вимірювання на відмітці рівня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="709"/>
      <source>If true, display offset will affect the origin mark too</source>
      <translation>Якщо ввімкнено, то зсув зображення вплине на початок координат також</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="718"/>
      <source>If true, the object&apos;s label is displayed</source>
      <translation type="unfinished">If true, the object&apos;s label is displayed</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="726"/>
      <source>The font to be used for texts</source>
      <translation>Шрифт для тексту</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="734"/>
      <source>The font size of texts</source>
      <translation>Розмір шрифту тексту</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="742"/>
      <source>The individual face colors</source>
      <translation>Індивідуальний колір поверхні</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="754"/>
      <source>If true, when activated, the working plane will automatically adapt to this level</source>
      <translation>Якщо правильно, при активації, робоча площина автоматично адаптується до цього рівня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="765"/>
      <source>If set to True, the working plane will be kept on Auto mode</source>
      <translation>Якщо встановлено значення True, робоча площина буде тримати в автоматичному режимі</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="774"/>
      <source>Camera position data associated with this object</source>
      <translation>Положення камери повʼязані з цим обʼєктом</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="785"/>
      <source>If set, the view stored in this object will be restored on double-click</source>
      <translation>Якщо вказано, вид збережений в цьому об'єкті буде відновлений подвійним натисканням</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="795"/>
      <source>If True, double-clicking this object in the tree activates it</source>
      <translation>Якщо Правда, двічі клацніть на цей об'єкт в дереві активує його</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="807"/>
      <source>If this is enabled, the inventor representation of this object will be saved in the FreeCAD file, allowing to reference it in other files in lightweight mode.</source>
      <translation>Якщо це увімкнено, то винахідник буде зберегти представництво цього об'єкта у файлі FreeCAD, дозволяє посилатись на нього в інших файлах в режимі легкої ваги.</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="817"/>
      <source>A slot to save the inventor representation of this object, if enabled</source>
      <translation>Слот, щоб зберегти винахідник представлення цього об'єкта, якщо це увімкнено</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="830"/>
      <source>If true, show the objects contained in this Building Part will adopt these line, color and transparency settings</source>
      <translation>Якщо так, показує об’єкти, що містяться в цій Building Part, приймуть ці параметри лінії, кольору та прозорості</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="837"/>
      <source>The line width of child objects</source>
      <translation>Ширина лінії дочірніх обʼєктів</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="845"/>
      <source>The line color of child objects</source>
      <translation>Колір лінії дочірнього обʼєкту</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="861"/>
      <source>The shape color of child objects</source>
      <translation>Колір фігури дочірніх обʼєктів</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="877"/>
      <source>The transparency of child objects</source>
      <translation>Прозорість дочірних обʼєктів</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="886"/>
      <source>Cut the view above this level</source>
      <translation>Вирізати вигляд цього рівня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="896"/>
      <source>The distance between the level plane and the cut line</source>
      <translation>Відстань між рівною площиною і лінії обрізки</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="906"/>
      <source>Turn cutting on when activating this level</source>
      <translation>Ввімкнути вирізання при активації цього рівня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="918"/>
      <source>The capture box for newly created objects expressed as [XMin,YMin,ZMin,XMax,YMax,ZMax]</source>
      <translation>Коробка зйомки для новостворених об'єктів, виражених як [XMin,YMin,ZMin,XMax,YMax,ZMax]</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="925"/>
      <source>Turns auto group box on/off</source>
      <translation>Автоматичне ввімкнення/вимкнення вікна групи</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="934"/>
      <source>Automatically set size from contents</source>
      <translation>Автоматично встановлювати розмір з вмісту</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="943"/>
      <source>A margin to use when autosize is turned on</source>
      <translation>Поле яке буде використовувати, коли ввімкнено автоматичне розмір</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="842"/>
      <location filename="../../ArchStructure.py" line="1844"/>
      <source>An optional extrusion path for this element</source>
      <translation>Опціональний шлях витиснення для цього елементу</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="851"/>
      <source>The computed length of the extrusion path</source>
      <translation>Розрахувати довжина шляху екструзії</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="862"/>
      <source>Start offset distance along the extrusion path (positive: extend, negative: trim)</source>
      <translation>Відстань початкового зсуву вздовж траєкторії видавлювання (позитивне: збільшення, негативне: обрізка)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="872"/>
      <source>End offset distance along the extrusion path (positive: extend, negative: trim)</source>
      <translation>Кінцева відстань зсуву вздовж шляху екструзії (позитивна: розширюється, негативна: обрізати)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="882"/>
      <source>Automatically align the Base of the Structure perpendicular to the Tool axis</source>
      <translation>Автоматично вирівняти основу конструкції перпендикулярно осі інструменту</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="892"/>
      <source>X offset between the Base origin and the Tool axis (only used if BasePerpendicularToTool is True)</source>
      <translation>Зсув X між початковою точкою та віссю інструменту (використовується тільки якщо BasePerpendicularToTool дорівнює True)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="902"/>
      <source>Y offset between the Base origin and the Tool axis (only used if BasePerpendicularToTool is True)</source>
      <translation>Зсув Y між початковою точкою та віссю інструменту (використовується тільки якщо BasePerpendicularToTool дорівнює True)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="912"/>
      <source>Mirror the Base along its Y axis (only used if BasePerpendicularToTool is True)</source>
      <translation>Віддзеркалити базу вздовж осі Y (використовується тільки якщо База перпендикулярна інструменту обрано)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="922"/>
      <source>Base rotation around the Tool axis (only used if BasePerpendicularToTool is True)</source>
      <translation>База повертається навколо вісі інструменту (використовується, лише якщо База перпендикулярно інструменту обрано)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="952"/>
      <source>The height or extrusion depth of this element. Keep 0 for automatic</source>
      <translation>Висота або глибина видавлювання елемента. Задайте 0 для автоматичного визначення</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="981"/>
      <source>A description of the standard profile this element is based upon</source>
      <translation>Опис стандартного профілю на якому ґрунтується цей елемент</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="991"/>
      <source>Offset distance between the centerline and the nodes line</source>
      <translation>Відстань зсуву між центральною лінією і вузлами</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1371"/>
      <source>If the nodes are visible or not</source>
      <translation>Вузли видимі чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1378"/>
      <source>The width of the nodes line</source>
      <translation>Ширина лінії вузлів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1385"/>
      <source>The size of the node points</source>
      <translation>Розмір точок вузла</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1393"/>
      <source>The color of the nodes line</source>
      <translation>Колір лінії вузлів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1401"/>
      <source>The type of structural node</source>
      <translation>Тип структури вузла</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1893"/>
      <source>Axes systems this structure is built on</source>
      <translation>Система координат, на основі якої побудовано конструкцію</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1902"/>
      <source>The element numbers to exclude when this structure is based on axes</source>
      <translation>Номери виключених елементів при побудові структури на осях</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1910"/>
      <source>If true the element are aligned with axes</source>
      <translation>Якщо відмічено, елемент вирівнюється по осях</translation>
    </message>
  </context>
  <context>
    <name>Arch</name>
    <message>
      <location filename="../../ArchAxis.py" line="101"/>
      <source>Create Axis</source>
      <translation>Створити вісь</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="383"/>
      <location filename="../../ArchAxis.py" line="998"/>
      <location filename="../../ArchComponent.py" line="2290"/>
      <source>Axes</source>
      <translation>Вісі</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1993"/>
      <location filename="../../ArchWindow.py" line="2129"/>
      <location filename="../../ArchAxisSystem.py" line="384"/>
      <location filename="../../ArchSpace.py" line="1033"/>
      <location filename="../../ArchAxis.py" line="999"/>
      <location filename="../../ArchComponent.py" line="2273"/>
      <source>Remove</source>
      <translation>Видалити</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2130"/>
      <location filename="../../ArchAxisSystem.py" line="385"/>
      <location filename="../../ArchSpace.py" line="1025"/>
      <location filename="../../ArchAxis.py" line="1000"/>
      <location filename="../../ArchComponent.py" line="2274"/>
      <source>Add</source>
      <translation>Додати</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="1005"/>
      <source>Distances (mm) and angles (deg) between axes</source>
      <translation>Відстані (мм) і кути (град) між осями</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="1008"/>
      <source>Axis</source>
      <translation>Вісь</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="1009"/>
      <source>Distance</source>
      <translation>Відстань</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="1010"/>
      <source>Angle</source>
      <translation>Кут</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="1011"/>
      <source>Label</source>
      <translation>Позначка</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1111"/>
      <source>Error computing the shape of this object</source>
      <translation>Помилка обробки форма цього обʼєкту</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1140"/>
      <source>has no solid</source>
      <translation>не має суцільного тіла</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1152"/>
      <source>has an invalid shape</source>
      <translation>має неприпустиму форму</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1156"/>
      <source>has a null shape</source>
      <translation>має нульову форму</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1798"/>
      <source>Toggle subcomponents</source>
      <translation>Перемкнути підкомпоненти</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="1928"/>
      <source>Closing Sketch edit</source>
      <translation>Закрити редагування ескізу</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="268"/>
      <location filename="../../ArchRoof.py" line="276"/>
      <location filename="../../ArchComponent.py" line="1954"/>
      <source>Please select a base object</source>
      <translation>Будь ласка, виберіть базовий обʼєкт</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2272"/>
      <source>Component</source>
      <translation>Компонент</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2277"/>
      <source>Components of this object</source>
      <translation>Компоненти цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2280"/>
      <source>Base component</source>
      <translation>Основний компонент</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2283"/>
      <source>Additions</source>
      <translation>Додавання</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2286"/>
      <source>Subtractions</source>
      <translation>Віднімання</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2289"/>
      <source>Objects</source>
      <translation>Обʼєкти</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2141"/>
      <location filename="../../ArchComponent.py" line="2293"/>
      <source>Components</source>
      <translation>Компоненти</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2296"/>
      <source>Fixtures</source>
      <translation>Арматура</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2297"/>
      <source>Group</source>
      <translation>Група</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2298"/>
      <source>Hosts</source>
      <translation>Хост</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2301"/>
      <source>Edit IFC properties</source>
      <translation>Редагувати властивості IFC</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2304"/>
      <source>Edit standard code</source>
      <translation>Редагувати стандартний код</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2366"/>
      <source>Property</source>
      <translation>Властивість</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2146"/>
      <location filename="../../ArchComponent.py" line="2367"/>
      <location filename="../../ArchCommands.py" line="1995"/>
      <source>Type</source>
      <translation>Тип</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="715"/>
      <location filename="../../ArchSchedule.py" line="741"/>
      <location filename="../../ArchComponent.py" line="2368"/>
      <location filename="../../ArchCommands.py" line="1996"/>
      <source>Value</source>
      <translation>Значення</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2375"/>
      <source>Add property...</source>
      <translation>Додати властивість...</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2379"/>
      <source>Add property set...</source>
      <translation>Додати набір властивостей...</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2380"/>
      <source>New...</source>
      <translation>Новий...</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2547"/>
      <source>New property</source>
      <translation>Нова властивість</translation>
    </message>
    <message>
      <location filename="../../ArchComponent.py" line="2584"/>
      <source>New property set</source>
      <translation>Новий набір властивостей</translation>
    </message>
    <message>
      <location filename="../../ArchWindowPresets.py" line="637"/>
      <source>Door</source>
      <translation>Двері</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="563"/>
      <source>Please either select only Building objects or nothing at all!

Site is not allowed to accept any other object besides Building.

Other objects will be removed from the selection.

Note: You can change that in the preferences.</source>
      <translation>Будь ласка, оберіть тільки будівельні об'єкти, або нічого!

Ділянка не може приймати будь-який інший об'єкт, окрім будівництва.

Інші об'єкти будуть вилучені з виділення.

Примітка: ви можете змінити це у налаштуваннях.</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="571"/>
      <source>There is no valid object in the selection.

Site creation aborted.</source>
      <translation>У вибраному об’єкті немає дійсного об'єкта.

Створення ділянки перервано.</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="579"/>
      <source>Create Site</source>
      <translation>Створити ділянку</translation>
    </message>
    <message>
      <location filename="../../importWebGL.py" line="917"/>
      <location filename="../../importOBJ.py" line="348"/>
      <location filename="../../importOBJ.py" line="393"/>
      <location filename="../../importJSON.py" line="63"/>
      <source>Successfully written</source>
      <translation>Успішно записано </translation>
    </message>
    <message>
      <location filename="../../importOBJ.py" line="97"/>
      <location filename="../../importOBJ.py" line="118"/>
      <source>Found a shape containing curves, triangulating</source>
      <translation>Знайдено форму, яка містить криві, тріангуляція</translation>
    </message>
    <message>
      <location filename="../../importOBJ.py" line="491"/>
      <source>Successfully imported</source>
      <translation>Імпорт завершено успішно</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="464"/>
      <source>Invalid cutplane</source>
      <translation>Неправильна сітка перетину</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="766"/>
      <source>is not closed</source>
      <translation>не замкнений</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="768"/>
      <source>is not valid</source>
      <translation>неприпустимо</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="770"/>
      <source>doesn&apos;t contain any solid</source>
      <translation type="unfinished">doesn&apos;t contain any solid</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="778"/>
      <source>contains a non-closed solid</source>
      <translation>містить незамкнене суцільне тіло</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="785"/>
      <source>contains faces that are not part of any solid</source>
      <translation>містить поверхні, які не належать жодному суцільному тілу</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1180"/>
      <source>Survey</source>
      <translation>Перегляд</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1183"/>
      <source>Set description</source>
      <translation>Додати опис</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1184"/>
      <source>Clear</source>
      <translation>Очистити</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1187"/>
      <source>Copy Length</source>
      <translation>Копіювати довжину</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1188"/>
      <source>Copy Area</source>
      <translation>Копіювати область</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1189"/>
      <source>Export CSV</source>
      <translation>Експорт CSV</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="714"/>
      <location filename="../../ArchSchedule.py" line="739"/>
      <location filename="../../ArchCommands.py" line="1192"/>
      <source>Description</source>
      <translation>Опис</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="568"/>
      <location filename="../../ArchPrecast.py" line="1743"/>
      <location filename="../../ArchWall.py" line="580"/>
      <location filename="../../ArchCommands.py" line="1193"/>
      <source>Length</source>
      <translation>Довжина</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1194"/>
      <source>Area</source>
      <translation>Площа</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1255"/>
      <source>Total</source>
      <translation>Всього</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="656"/>
      <location filename="../../ArchCommands.py" line="1300"/>
      <source>Export CSV File</source>
      <translation>Експортувати .CSV файл</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1351"/>
      <source>Object doesn&apos;t have settable IFC Attributes</source>
      <translation type="unfinished">Object doesn&apos;t have settable IFC Attributes</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1359"/>
      <source>Disabling Brep force flag of object</source>
      <translation>Вимкнення прапорцю Brep-форсування</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1367"/>
      <location filename="../../ArchCommands.py" line="1375"/>
      <source>Enabling Brep force flag of object</source>
      <translation>Ввімкнення прапорцю Brep-форсування</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1601"/>
      <source>Add space boundary</source>
      <translation>Додати відступ</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1609"/>
      <source>Grouping</source>
      <translation>Групування</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1651"/>
      <source>Remove space boundary</source>
      <translation>Прибрати відступ</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1659"/>
      <source>Ungrouping</source>
      <translation>Розгрупування</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1703"/>
      <source>Split Mesh</source>
      <translation>Розділити меш</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1748"/>
      <source>Mesh to Shape</source>
      <translation>Меш у форму</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1856"/>
      <source>All good! No problems found</source>
      <translation>Все добре! Проблем не виявлено</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1929"/>
      <location filename="../../ArchCommands.py" line="1966"/>
      <source>Create Component</source>
      <translation>Створити компонент</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="552"/>
      <location filename="../../ArchProfile.py" line="157"/>
      <location filename="../../ArchCommands.py" line="1993"/>
      <source>Category</source>
      <translation>Категорія</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1994"/>
      <source>Key</source>
      <translation>Ключ</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="716"/>
      <location filename="../../ArchSchedule.py" line="743"/>
      <location filename="../../ArchCommands.py" line="1997"/>
      <source>Unit</source>
      <translation>Одиниця</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="2009"/>
      <source>The object doesn&apos;t have an IfcProperties attribute. Cancel spreadsheet creation for object:</source>
      <translation type="unfinished">The object doesn&apos;t have an IfcProperties attribute. Cancel spreadsheet creation for object:</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="2040"/>
      <source>Create IFC properties spreadsheet</source>
      <translation>Створити таблицю властивостей IFC</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="45"/>
      <location filename="../../InitGui.py" line="253"/>
      <location filename="../../InitGui.py" line="256"/>
      <source>Arch</source>
      <translation>Арка</translation>
    </message>
    <message>
      <location filename="../../ArchProject.py" line="137"/>
      <source>Create Project</source>
      <translation>Створити проект</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="138"/>
      <location filename="../../ArchRebar.py" line="174"/>
      <source>Create Rebar</source>
      <translation>Створити арматуру</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="188"/>
      <source>Please select a base face on a structural object</source>
      <translation>Будь ласка, виберіть базову грань на структурному обʼєкті</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="90"/>
      <source>Only axes must be selected</source>
      <translation>Потрібно вибрати тільки осі</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="97"/>
      <source>Create Axis System</source>
      <translation>Створити систему осей</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="103"/>
      <source>Please select at least one axis</source>
      <translation>Виберіть принаймні одну вісь</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="388"/>
      <source>Axis system components</source>
      <translation>Компоненти системи осей</translation>
    </message>
    <message>
      <location filename="../../importGBXML.py" line="49"/>
      <location filename="../../importGBXML.py" line="56"/>
      <source>This exporter can currently only export one site object</source>
      <translation>Цей експортер наразі може експортувати лише один обʼєкт частини</translation>
    </message>
    <message>
      <location filename="../../importGBXML.py" line="113"/>
      <source>Error: Space &apos;%s&apos; has no Zone. Aborting.</source>
      <translation type="unfinished">Error: Space &apos;%s&apos; has no Zone. Aborting.</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="184"/>
      <source>Cutting</source>
      <translation>Перерізання</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="226"/>
      <source>Cut Plane</source>
      <translation>Площина розрізу</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="229"/>
      <source>Cut Plane options</source>
      <translation>Опції площини розрізу</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="232"/>
      <source>Which side to cut</source>
      <translation>Яку сторону вирізати</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="235"/>
      <source>Behind</source>
      <translation>Позаду</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="236"/>
      <source>Front</source>
      <translation>Фронт</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="133"/>
      <source>Create material</source>
      <translation>Створити матеріал</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="176"/>
      <source>Create multi-material</source>
      <translation>Створити мульти-матеріал</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2145"/>
      <location filename="../../ArchMaterial.py" line="1054"/>
      <location filename="../../ArchMaterial.py" line="1097"/>
      <source>Name</source>
      <translation>Назва</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="1055"/>
      <location filename="../../ArchMaterial.py" line="1098"/>
      <source>Material</source>
      <translation>Матеріал</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2148"/>
      <location filename="../../ArchMaterial.py" line="1056"/>
      <location filename="../../ArchMaterial.py" line="1099"/>
      <source>Thickness</source>
      <translation>Порожнина</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="1138"/>
      <source>New layer</source>
      <translation>Новий шар</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="1177"/>
      <source>Total thickness</source>
      <translation>Загальна товщина</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="1191"/>
      <source>depends on the object</source>
      <translation>залежить від об’єкта</translation>
    </message>
    <message>
      <location filename="../../ArchStairs.py" line="248"/>
      <source>Create Stairs</source>
      <translation>Створити сходи</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="864"/>
      <source>Create external reference</source>
      <translation>Створити зовнішнє посилання</translation>
    </message>
    <message>
      <location filename="../../importDAE.py" line="63"/>
      <source>pycollada not found, collada support is disabled.</source>
      <translation>pycollada не знайдена, підтримка collada відключена.</translation>
    </message>
    <message>
      <location filename="../../import3DS.py" line="86"/>
      <location filename="../../importIFClegacy.py" line="1057"/>
      <location filename="../../importSH3D.py" line="73"/>
      <location filename="../../importDAE.py" line="140"/>
      <source>Error: Couldn&apos;t determine character encoding</source>
      <translation type="unfinished">Error: Couldn&apos;t determine character encoding</translation>
    </message>
    <message>
      <location filename="../../importDAE.py" line="401"/>
      <source>file %s successfully created.</source>
      <translation>файл %s створений успішно.</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="111"/>
      <location filename="../../ArchPanel.py" line="119"/>
      <location filename="../../ArchPanel.py" line="131"/>
      <source>View of</source>
      <translation>Вид</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1081"/>
      <source>Create Section Plane</source>
      <translation>Створити площину розрізу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1992"/>
      <source>Section plane settings</source>
      <translation>Налаштування площини перетину</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1998"/>
      <source>Remove highlighted objects from the list above</source>
      <translation>Видаліть виділені обʼєкти зі списку вище</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2001"/>
      <source>Add selected</source>
      <translation>Додати вибрані</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2008"/>
      <source>Add selected object(s) to the scope of this section plane</source>
      <translation>Додати вибраний об'єкт(и) до діапазону цієї площини перерізу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2013"/>
      <source>Objects seen by this section plane:</source>
      <translation>Видимі обʼєкти на площині перетину:</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2016"/>
      <source>Section plane placement:</source>
      <translation>Розміщення площини перетину:</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2019"/>
      <source>Rotate X</source>
      <translation>Повернути по осі Х</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2024"/>
      <source>Rotates the plane along the X axis</source>
      <translation>Обертає площину вздовж осі X</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2027"/>
      <source>Rotate Y</source>
      <translation>Повернути по осі Y</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2032"/>
      <source>Rotates the plane along the Y axis</source>
      <translation>Обертає площину вздовж осі Y</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2035"/>
      <source>Rotate Z</source>
      <translation>Повернути по осі Z</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2040"/>
      <source>Rotates the plane along the Z axis</source>
      <translation>Обертає площину вздовж осі Z</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2041"/>
      <source>Resize</source>
      <translation>Змінити розмір</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2046"/>
      <source>Resizes the plane to fit the objects in the list above</source>
      <translation>Змінює розмір площини відповідно до об’єктів у списку вище</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2049"/>
      <location filename="../../ArchWall.py" line="605"/>
      <source>Center</source>
      <translation>Центр</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="2054"/>
      <source>Centers the plane on the objects in the list above</source>
      <translation>Центрує площину на об’єктах у списку вище</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="242"/>
      <location filename="../../ArchRoof.py" line="260"/>
      <source>Create Roof</source>
      <translation>Створити покрівлю</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="273"/>
      <location filename="../../ArchRoof.py" line="985"/>
      <source>Unable to create a roof</source>
      <translation>Не вдалося створити покрівлю</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1179"/>
      <source>Roof</source>
      <translation>Покрівля</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1186"/>
      <source>Parameters of the roof profiles :
* Angle : slope in degrees relative to the horizontal.
* Run : horizontal distance between the wall and the ridge.
* Thickness : thickness of the roof.
* Overhang : horizontal distance between the eave and the wall.
* Height : height of the ridge above the base (calculated automatically).
* IdRel : Id of the relative profile used for automatic calculations.
---
If Angle = 0 and Run = 0 then the profile is identical to the relative profile.
If Angle = 0 then the angle is calculated so that the height is the same as the relative profile.
If Run = 0 then the run is calculated so that the height is the same as the relative profile.</source>
      <translation>Параметри дахових профілів:
* Кут: нахил у градусах по відношенню до горизонталі.
* Біг: горизонтальна відстань між стіною та хребтом.
* Товщина: товщина даху.
* Обернути - горизонтальну відстань між карнизом та стіною.
* Висота гребеня над основою (обчислена автоматично).
* IdRel : Id відносного профілю, який використовується для автоматичного обчислення.
---
Якщо Angle = 0 і Виконати = 0, то профіль ідентичний відносному профілю.
Якщо кут = 0, тоді кут обчислюється так, що висота дорівнює відносному профілю.
Якщо Run = 0, то пробіл розраховується таким чином, що висота дорівнює відносному профілю.</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1189"/>
      <source>Id</source>
      <translation>Ідентифікатор</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1190"/>
      <source>Angle (deg)</source>
      <translation>Кут (град.)</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1191"/>
      <source>Run (mm)</source>
      <translation>Відстань між стіною і коньковою обшивкою.</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1192"/>
      <source>IdRel</source>
      <translation type="unfinished">IdRel</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1193"/>
      <source>Thickness (mm)</source>
      <translation>Товщина (мм)</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1194"/>
      <source>Overhang (mm)</source>
      <translation>Навіс (мм)</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="1195"/>
      <source>Height (mm)</source>
      <translation>Висота (мм)</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="97"/>
      <source>Window</source>
      <translation>Вікно</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="236"/>
      <location filename="../../ArchWindow.py" line="267"/>
      <location filename="../../ArchWindow.py" line="319"/>
      <source>Create Window</source>
      <translation>Створити вікно</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="287"/>
      <source>Choose a face on an existing object or select a preset</source>
      <translation>Виберіть грань на існуючому обʼєкті або виберіть заготовку</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="363"/>
      <source>Window not based on sketch. Window not aligned or resized.</source>
      <translation>Вікно не базується на ескізі. Вікно не вирівнюється або змінює розмір.</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="382"/>
      <source>No Width and/or Height constraint in window sketch. Window not resized.</source>
      <translation>Ширина та/або обмеження висоти в ескізі вікна. Вікно не змінює розмір.</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="385"/>
      <source>No window found. Cannot continue.</source>
      <translation>Вікно не знайдено. Не вдається продовжити.</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="456"/>
      <source>Window options</source>
      <translation>Параметри вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="460"/>
      <source>Auto include in host object</source>
      <translation>Автоматично включати в обʼєкт хоста</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="468"/>
      <source>Sill height</source>
      <translation>Висота підвіконня</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="512"/>
      <location filename="../../ArchStructure.py" line="559"/>
      <location filename="../../ArchProfile.py" line="164"/>
      <source>Preset</source>
      <translation>Налаштування</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="1609"/>
      <source>This window has no defined opening</source>
      <translation>Це вікно не позначене відкриватися</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="1919"/>
      <location filename="../../ArchWindow.py" line="1970"/>
      <location filename="../../ArchWindow.py" line="2170"/>
      <source>Get selected edge</source>
      <translation>Отримати вибране ребро</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2076"/>
      <source>Unable to create component</source>
      <translation>Неможливо створити торрент</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2117"/>
      <source>Window elements</source>
      <translation>Елементи вікна</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2118"/>
      <source>Hole wire</source>
      <translation>Отвір для дроту</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2125"/>
      <source>The number of the wire that defines a hole in the host object. A value of zero will automatically adopt the largest wire</source>
      <translation>Щільність сітки, що визначає отвір у хост-об'єкті. Значення нуля автоматично приймає найбільшу сітку</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2128"/>
      <source>Pick selected</source>
      <translation>Вибрати вибране</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2131"/>
      <source>Edit</source>
      <translation>Правка</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2134"/>
      <source>Create/update component</source>
      <translation>Створити або оновити компонент</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2135"/>
      <source>Base 2D object</source>
      <translation>Базовий двовимірний об’єкт</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2138"/>
      <location filename="../../ArchWindow.py" line="2147"/>
      <source>Wires</source>
      <translation>Каркас</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2144"/>
      <source>Create new component</source>
      <translation>Створити новий компонент</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2149"/>
      <location filename="../../ArchPrecast.py" line="1749"/>
      <source>Offset</source>
      <translation>Зсув</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2150"/>
      <source>Hinge</source>
      <translation>Шарнір</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2151"/>
      <source>Opening mode</source>
      <translation>Режим відкриття</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2152"/>
      <location filename="../../ArchWindow.py" line="2160"/>
      <source>+ default</source>
      <translation>+ за замовчуванням</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2159"/>
      <source>If this is checked, the default Frame value of this window will be added to the value entered here</source>
      <translation>Якщо це позначено, значення кадру за замовчуванням цього вікна буде додано до значення, введеного тут</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2167"/>
      <source>If this is checked, the default Offset value of this window will be added to the value entered here</source>
      <translation>Якщо це позначено, типове значення зсуву у цьому вікні буде додано до введеного тут значення</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2175"/>
      <source>Press to retrieve the selected edge</source>
      <translation>Натисніть, щоб отримати вибраний ребр</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2178"/>
      <source>Invert opening direction</source>
      <translation>Інвертувати напрям відкриття</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="2181"/>
      <source>Invert hinge position</source>
      <translation>Перевернути положення шарніру</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="197"/>
      <source>You must select a base shape object and optionally a mesh object</source>
      <translation>Ви повинні вибрати об’єкт базової фігури та, за бажанням, об’єкт сітки</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="218"/>
      <source>Create Equipment</source>
      <translation>Створити обладнання</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="265"/>
      <source>You must select exactly one base object</source>
      <translation>Необхідно вибрати лише один базовий обʼєкт</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="271"/>
      <source>The selected object must be a mesh</source>
      <translation>Виділений об’єкт повинен бути сіткою</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="277"/>
      <source>This mesh has more than 1000 facets.</source>
      <translation>Ця сітка має понад 1000 граней.</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="282"/>
      <source>This operation can take a long time. Proceed?</source>
      <translation>Ця операція може зайняти тривалий час. Продовжити?</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="296"/>
      <source>The mesh has more than 500 facets. This will take a couple of minutes...</source>
      <translation>Сітка має більш ніж 500 граней. Це займе кілька хвилин...</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="298"/>
      <source>Create 3 views</source>
      <translation>Створити 3 вигляди</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="75"/>
      <source>Shapes elevation</source>
      <translation>Висота фігур</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="76"/>
      <source>Choose which field provides shapes elevations:</source>
      <translation>Виберіть, яке поле надає висоту фігур:</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="119"/>
      <source>No shape found in this file</source>
      <translation>У цьому файлі не знайдено форми</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="150"/>
      <source>Shapefile module not found</source>
      <translation>Модуль Shapefile не знайдено</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="154"/>
      <source>The shapefile python library was not found on your system. Would you like to download it now from &lt;a href=&quot;https://github.com/GeospatialPython/pyshp&quot;&gt;https://github.com/GeospatialPython/pyshp&lt;/a&gt;? It will be placed in your macros folder.</source>
      <translation>Бібліотека shapefile python не була знайдена у вашій системі. Ви хочете зараз завантажити її з сайту &lt;a href=&quot;https://github.com/GeospatialPython/pyshp&quot;&gt;https://github.com/GeospatialPython/pyshp&lt;/a&gt;? Вона буде поміщена в теку macros.</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="163"/>
      <source>Error: Unable to download from:</source>
      <translation>Помилка: неможливо завантажити із:</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="184"/>
      <source>Could not download shapefile module. Aborting.</source>
      <translation>Не вдалося завантажити shapefile модуль. Скасування операції.</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="190"/>
      <source>Shapefile module not downloaded. Aborting.</source>
      <translation>Модуль Shapefile не завантажено. Скасування операції.</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="195"/>
      <source>Shapefile module not found. Aborting.</source>
      <translation>Модуль Shapefile не завантажено. Скасування операції.</translation>
    </message>
    <message>
      <location filename="../../importSHP.py" line="202"/>
      <source>The shapefile library can be downloaded from the following URL and installed in your macros folder:</source>
      <translation>Бібліотеку shapefile можна завантажити за вказаною нижче URL-адресою та встановити у теці макросів:</translation>
    </message>
    <message>
      <location filename="../../ArchBuilding.py" line="242"/>
      <source>You can put anything but Site and Building objects in a Building object.

Building object is not allowed to accept Site and Building objects.

Site and Building objects will be removed from the selection.

You can change that in the preferences.</source>
      <translation>Ви можете помістити все, крім ділянок та будівель, в об'єкт будівлі.

Об’єкт будівлі не може приймати ділянки та об'єкти будівель.

Ділянки та об'єкти будівель буде вилучено з виділеного.

Ви можете змінити це в налаштуваннях.</translation>
    </message>
    <message>
      <location filename="../../ArchBuilding.py" line="252"/>
      <source>There is no valid object in the selection.

Building creation aborted.</source>
      <translation>У виділенні немає дійсного об’єкта.

Створення будівлі перервано.</translation>
    </message>
    <message>
      <location filename="../../ArchBuilding.py" line="260"/>
      <source>Create Building</source>
      <translation>Створити будівлю</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="51"/>
      <source>Frame</source>
      <translation>Каркас</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="97"/>
      <source>Create Frame</source>
      <translation>Створити каркас</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="327"/>
      <source>Crossing point not found in profile.</source>
      <translation>Точку перетину не знайдено в профілі.</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="81"/>
      <source>Create Grid</source>
      <translation>Створити сітку</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="231"/>
      <source>Auto height is larger than height</source>
      <translation>Автоматична висота більша за висоту</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="242"/>
      <source>Total row size is larger than height</source>
      <translation>Загальний розмір рядка перевищує висоту</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="266"/>
      <source>Auto width is larger than width</source>
      <translation>Автоматична ширина перевищує ширину</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="277"/>
      <source>Total column size is larger than width</source>
      <translation>Загальний розмір колони більший за ширину</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="535"/>
      <source>Grid</source>
      <translation>Сітка</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="536"/>
      <source>Total width</source>
      <translation>Загальна ширина</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="537"/>
      <source>Total height</source>
      <translation>Загальна висота</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="538"/>
      <source>Add row</source>
      <translation>Додати рядок</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="539"/>
      <source>Del row</source>
      <translation>Видалити рядок</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="542"/>
      <source>Add col</source>
      <translation>Додати стовпчик</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="545"/>
      <source>Del col</source>
      <translation>Видалити стовпчик</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="548"/>
      <source>Create span</source>
      <translation>Створити проміжок</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="551"/>
      <source>Remove span</source>
      <translation>Видалити проміжок</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="554"/>
      <source>Rows</source>
      <translation>Рядки</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="558"/>
      <source>Columns</source>
      <translation>Стовпці</translation>
    </message>
    <message>
      <location filename="../../ArchFloor.py" line="162"/>
      <source>You can put anything but the following objects: Site, Building, and Floor - in a Floor object.

Floor object is not allowed to accept Site, Building, or Floor objects.

Site, Building, and Floor objects will be removed from the selection.

You can change that in the preferences.</source>
      <translation>Ви можете помістити що завгодно, крім таких об’єктів: Ділянка, Будівля та Підлога - в об’єкт Поверх.

Об’єкт підлоги не може приймати об’єкти ділянки, будівлі чи підлоги.

Об'єкти сайту, будівлі та підлоги буде видалено з виділеного.

Ви можете змінити це в налаштуваннях.</translation>
    </message>
    <message>
      <location filename="../../ArchFloor.py" line="172"/>
      <source>There is no valid object in the selection.

Floor creation aborted.</source>
      <translation>У вибраному об’єкті немає дійсного об'єкта.

Створення поверху перервано.</translation>
    </message>
    <message>
      <location filename="../../ArchFloor.py" line="180"/>
      <source>Create Floor</source>
      <translation>Створити ярус</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1268"/>
      <source>Precast elements</source>
      <translation>Елементи виготовлення на бетонному заводі</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1269"/>
      <source>Slab type</source>
      <translation>Тип плити</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1270"/>
      <source>Chamfer</source>
      <translation>Фаска</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1271"/>
      <source>Dent length</source>
      <translation>Довжина западини.</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1272"/>
      <source>Dent width</source>
      <translation>Ширина западини</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1273"/>
      <source>Dent height</source>
      <translation>Висота зубця</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1274"/>
      <source>Slab base</source>
      <translation>Основа плити</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1275"/>
      <source>Number of holes</source>
      <translation>Кількість отворів</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1276"/>
      <source>Major diameter of holes</source>
      <translation>Основний діаметр отворів</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1277"/>
      <source>Minor diameter of holes</source>
      <translation>Невеликий діаметр отворів</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1278"/>
      <source>Spacing between holes</source>
      <translation>Відстань між отворами</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1279"/>
      <source>Number of grooves</source>
      <translation>Кількість пазів</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1280"/>
      <source>Depth of grooves</source>
      <translation>Глибина пазу</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1281"/>
      <source>Height of grooves</source>
      <translation>Висота пазів</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1282"/>
      <source>Spacing between grooves</source>
      <translation>Інтервал між пазами</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1283"/>
      <source>Number of risers</source>
      <translation>Кількість підйомів</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1284"/>
      <source>Length of down floor</source>
      <translation>Довжина нижньої підлоги</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1285"/>
      <source>Height of risers</source>
      <translation>Висота підсходинків</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1286"/>
      <source>Depth of treads</source>
      <translation>Глибина східців</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1739"/>
      <source>Precast options</source>
      <translation>Збірні варіанти</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1740"/>
      <source>Dents list</source>
      <translation>Список зубів.</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1741"/>
      <source>Add dent</source>
      <translation>Додати зуб.</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1742"/>
      <source>Remove dent</source>
      <translation>Видалити зуб.</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="582"/>
      <location filename="../../ArchPrecast.py" line="1744"/>
      <location filename="../../ArchWall.py" line="586"/>
      <source>Width</source>
      <translation>Ширина</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="591"/>
      <location filename="../../ArchPrecast.py" line="1745"/>
      <location filename="../../ArchWall.py" line="594"/>
      <source>Height</source>
      <translation>Висота</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1746"/>
      <source>Slant</source>
      <translation>Схил</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1747"/>
      <source>Level</source>
      <translation>Рівень</translation>
    </message>
    <message>
      <location filename="../../ArchPrecast.py" line="1748"/>
      <source>Rotation</source>
      <translation>Поворот</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="85"/>
      <source>Curtain Wall</source>
      <translation>Навісна стіна</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="120"/>
      <location filename="../../ArchTruss.py" line="104"/>
      <source>Please select only one base object or none</source>
      <translation>Будь ласка, виберіть лише один базовий обʼєкт або жодного</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="127"/>
      <location filename="../../ArchCurtainWall.py" line="160"/>
      <source>Create Curtain Wall</source>
      <translation>Створити навісну стіну</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="144"/>
      <source>Create profile</source>
      <translation>Створити профіль</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="153"/>
      <source>Profile settings</source>
      <translation>Налаштування профілю</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="211"/>
      <source>Create Profile</source>
      <translation>Створити профіль</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="698"/>
      <source>Profile</source>
      <translation>Профіль</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="129"/>
      <location filename="../../ArchPipe.py" line="140"/>
      <source>Create Pipe</source>
      <translation>Створити трубу</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="176"/>
      <source>Please select exactly 2 or 3 Pipe objects</source>
      <translation>Будь ласка, виберіть точно 2 або 3 обʼєкти труби</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="183"/>
      <source>Please select only Pipe objects</source>
      <translation>Оберіть тільки обʼєкт Труба</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="188"/>
      <source>Create Connector</source>
      <translation>Створити Зʼєднувач</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="284"/>
      <source>Unable to build the base path</source>
      <translation>Не вдалося створити базовий шлях</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="306"/>
      <source>Unable to build the profile</source>
      <translation>Не вдається побудувати профіль</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="341"/>
      <source>Unable to build the pipe</source>
      <translation>Неможливо побудувати трубу</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="363"/>
      <source>The base object is not a Part</source>
      <translation>Базовий обʼєкт не є частиною</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="368"/>
      <source>Too many wires in the base shape</source>
      <translation>Забагато каркасів у базовій формі</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="373"/>
      <source>The base wire is closed</source>
      <translation>Базова сітка закрита</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="396"/>
      <source>The profile is not a 2D Part</source>
      <translation>Профіль не є двомірною частиною</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="401"/>
      <source>The profile is not closed</source>
      <translation>Профіль не закритий</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="508"/>
      <source>Only the 3 first wires will be connected</source>
      <translation>Будуть підключені лише 3 перші сітки</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="532"/>
      <location filename="../../ArchPipe.py" line="587"/>
      <source>Common vertex not found</source>
      <translation>Звичайна вершина не знайдена</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="557"/>
      <source>Pipes are already aligned</source>
      <translation>Труби вже вирівняні</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="611"/>
      <source>At least 2 pipes must align</source>
      <translation>Принаймні 2 труби повинні вирівнюватися</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="117"/>
      <source>Wall</source>
      <translation>Стіна</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="127"/>
      <source>Walls can only be based on Part or Mesh objects</source>
      <translation>Стіни можуть бути засновані тільки на обʼєкти деталі, або сітки</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="335"/>
      <location filename="../../ArchWall.py" line="425"/>
      <location filename="../../ArchWall.py" line="737"/>
      <source>Create Wall</source>
      <translation>Створити стінку</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="379"/>
      <source>First point of wall</source>
      <translation>Перша точка стіни</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="421"/>
      <location filename="../../ArchWall.py" line="413"/>
      <source>Next point</source>
      <translation>Наступна точка</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="556"/>
      <source>Wall options</source>
      <translation>Параметри стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="560"/>
      <source>Wall Presets...</source>
      <translation>Пресети Стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="566"/>
      <source>This list shows all the MultiMaterials objects of this document. Create some to define wall types.</source>
      <translation>У цьому списку показані всі об’єкти MultiMaterials цього документа. Створіть кілька для визначення типів стін.</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="602"/>
      <source>Alignment</source>
      <translation>Вирівнювання</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="606"/>
      <source>Left</source>
      <translation>Ліворуч</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="607"/>
      <source>Right</source>
      <translation>Направо</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="611"/>
      <location filename="../../ArchWall.py" line="614"/>
      <source>Con&amp;tinue</source>
      <translation>Продовжити</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="625"/>
      <source>Use sketches</source>
      <translation>Використовувати ескізи</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="812"/>
      <source>Merge Wall</source>
      <translation>Обʼєднати стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="822"/>
      <source>The selected wall contains no subwall to merge</source>
      <translation>Виділена стіна не містить вкладених стін для обʼєднання</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="827"/>
      <location filename="../../ArchWall.py" line="833"/>
      <source>Please select only wall objects</source>
      <translation>Будь-ласка виберіть тільки стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="835"/>
      <source>Merge Walls</source>
      <translation>Обʼєднати стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1257"/>
      <source>Cannot compute blocks for wall</source>
      <translation>Не можна обчислити блоки для стіни</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1100"/>
      <location filename="../../ArchWall.py" line="1275"/>
      <source>This mesh is an invalid solid</source>
      <translation>Ця сітка є недійсним тілом</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="1391"/>
      <source>Error: Unable to modify the base object of this wall</source>
      <translation>Помилка: Неможливо змінити основний об'єкт цієї стіни</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="69"/>
      <source>Truss</source>
      <translation>Вʼязка</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="109"/>
      <location filename="../../ArchTruss.py" line="140"/>
      <source>Create Truss</source>
      <translation>Створити Вʼязку</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="359"/>
      <source>Unable to retrieve value from object</source>
      <translation>Неможливо виправити значення з об’єкта</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="616"/>
      <source>Import CSV File</source>
      <translation>Імпортувати файл CSV</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="686"/>
      <source>Unable to recognize that file type</source>
      <translation>Не вдалося розпізнати тип файлу</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="808"/>
      <source>Schedule</source>
      <translation>Запланувати</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="249"/>
      <source>Create Space</source>
      <translation>Задати простір</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="1010"/>
      <source>Set text position</source>
      <translation>Вказати положення тексту</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="1018"/>
      <source>Space boundaries</source>
      <translation>Проміжок меж</translation>
    </message>
    <message>
      <location filename="../../importIFClegacy.py" line="156"/>
      <source>Couldn&apos;t locate IfcOpenShell</source>
      <translation type="unfinished">Couldn&apos;t locate IfcOpenShell</translation>
    </message>
    <message>
      <location filename="../../importIFClegacy.py" line="507"/>
      <source>IfcOpenShell not found or disabled, falling back on internal parser.</source>
      <translation>IfcOpenShell не знайдено або вимкнуто, falling back у внутрішньому синтаксичному аналізаторі.</translation>
    </message>
    <message>
      <location filename="../../importIFClegacy.py" line="516"/>
      <source>IFC Schema not found, IFC import disabled.</source>
      <translation>Схема IFC не знайдена. Імпортування IFC вимкнено.</translation>
    </message>
    <message>
      <location filename="../../importIFClegacy.py" line="1206"/>
      <source>Error: IfcOpenShell is not installed</source>
      <translation>Помилка: IfcOpenShell не встановлено</translation>
    </message>
    <message>
      <location filename="../../importIFClegacy.py" line="1217"/>
      <source>Error: your IfcOpenShell version is too old</source>
      <translation>Помилка: ваша версія IfcOpenShell застаріла</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="110"/>
      <source>Page</source>
      <translation>Сторінка</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="186"/>
      <location filename="../../ArchPanel.py" line="224"/>
      <source>Create Panel</source>
      <translation>Створити панель</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="432"/>
      <source>Create Panel Cut</source>
      <translation>Створити панель вирізів</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="466"/>
      <source>Create Panel Sheet</source>
      <translation>Створіть Панель Листів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1154"/>
      <location filename="../../ArchPanel.py" line="736"/>
      <source>Facemaker returned an error</source>
      <translation>Facemaker повернув помилку</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="957"/>
      <source>Error computing shape of</source>
      <translation>Помилка обчислення форми</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1966"/>
      <location filename="../../ArchPanel.py" line="990"/>
      <source>Couldn&apos;t compute a shape</source>
      <translation type="unfinished">Couldn&apos;t compute a shape</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="2001"/>
      <source>Tools</source>
      <translation>Інструменти</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="2007"/>
      <source>Edit views positions</source>
      <translation>Розташування вигляду Редагування</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="2116"/>
      <source>This object has no face</source>
      <translation>У цього обʼєкта відсутня поверхня</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="199"/>
      <source>BuildingPart</source>
      <translation>Будівельна частина</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="320"/>
      <source>Create BuildingPart</source>
      <translation>Створити Будівельну частину</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="79"/>
      <source>Structure</source>
      <translation>Структура</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="127"/>
      <location filename="../../ArchStructure.py" line="541"/>
      <source>Beam</source>
      <translation type="unfinished">Beam</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="130"/>
      <location filename="../../ArchStructure.py" line="542"/>
      <source>Column</source>
      <translation>Колонна
</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="220"/>
      <source>Create Structures From Selection</source>
      <translation>Створити Структуру з обраного</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="261"/>
      <source>Please select the base object first and then the edges to use as extrusion paths</source>
      <translation>Будь ласка, спочатку оберіть основний об'єкт, а потім краї, які використовуються як шляхи екструзії</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="292"/>
      <source>Create Structural System</source>
      <translation>Створити Структурну систему</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="314"/>
      <source>Please select at least an axis object</source>
      <translation>Будь ласка оберіть принаймні осі обʼєктів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="367"/>
      <location filename="../../ArchStructure.py" line="427"/>
      <source>Create Structure</source>
      <translation>Створити структуру</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="396"/>
      <source>First point of the beam</source>
      <translation>Перша точка променю</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="398"/>
      <source>Base point of column</source>
      <translation>Базова точка стовпця</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="536"/>
      <source>Structure options</source>
      <translation>Опції структури</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="540"/>
      <source>Drawing mode</source>
      <translation>Режим креслення</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="605"/>
      <source>Switch L/H</source>
      <translation>Перемкнути L/H</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="607"/>
      <source>Switch L/W</source>
      <translation>Перемкнути L/W</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1064"/>
      <source>Error: The base shape couldn&apos;t be extruded along this tool object</source>
      <translation type="unfinished">Error: The base shape couldn&apos;t be extruded along this tool object</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1539"/>
      <source>Node Tools</source>
      <translation>Інструменти вузлів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1546"/>
      <source>Reset nodes</source>
      <translation>Скинути вузли</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1557"/>
      <source>Edit nodes</source>
      <translation>Редагувати вузли</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1567"/>
      <source>Extend nodes</source>
      <translation>Розширити вузли</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1574"/>
      <source>Extends the nodes of this element to reach the nodes of another element</source>
      <translation>Розширює вузли цього елемента, щоб досягнути до вузлів іншого елемента</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1584"/>
      <source>Connect nodes</source>
      <translation>Обʼєднати вузли</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1591"/>
      <source>Connects nodes of this element with the nodes of another element</source>
      <translation>З’єднує вузли цього елемента з вузлами іншого елемента</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1601"/>
      <source>Toggle all nodes</source>
      <translation>Перемкнути всі вузли</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1606"/>
      <source>Toggles all structural nodes of the document on/off</source>
      <translation>Увімкнути/вимкнути всі структурні вузли документа</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1615"/>
      <source>Extrusion Tools</source>
      <translation>Інструменти видавлювання</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1622"/>
      <location filename="../../ArchStructure.py" line="1855"/>
      <source>Select tool...</source>
      <translation>Інструмент виділення...</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1629"/>
      <source>Select object or edges to be used as a Tool (extrusion path)</source>
      <translation>Оберіть об'єкт або ребра для використання в якості Інструмент (шлях екструзії)</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1656"/>
      <location filename="../../ArchStructure.py" line="1719"/>
      <source>Choose another Structure object:</source>
      <translation>Виберіть інший Конструктивний обʼєкт:</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1662"/>
      <location filename="../../ArchStructure.py" line="1725"/>
      <source>The chosen object is not a Structure</source>
      <translation>Обраний обʼєкт не є структурою</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1668"/>
      <location filename="../../ArchStructure.py" line="1731"/>
      <source>The chosen object has no structural nodes</source>
      <translation>Вибраний обʼєкт не має структурних вузлів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1676"/>
      <location filename="../../ArchStructure.py" line="1739"/>
      <source>One of these objects has more than 2 nodes</source>
      <translation>Один з цих обʼєктів має більше 2 вузлів</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1694"/>
      <location filename="../../ArchStructure.py" line="1757"/>
      <source>Unable to find a suitable intersection point</source>
      <translation>Не вдається знайти відповідну точку перетину</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1700"/>
      <source>Intersection found.
</source>
      <translation>Виявлено перетин.
</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1762"/>
      <source>Intersection found.</source>
      <translation>Виявлено перетин.</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="1817"/>
      <source>Done</source>
      <translation>Готово</translation>
    </message>
  </context>
  <context>
    <name>ArchMaterial</name>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="14"/>
      <source>Arch material</source>
      <translation>Матеріали арки</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="26"/>
      <source>Choose a preset card</source>
      <translation>Виберіть попередньо встановлену карту</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="30"/>
      <source>Choose preset...</source>
      <translation>Виберіть пресет...</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="44"/>
      <source>Copy values from an existing material in the document</source>
      <translation>Копіювати значення з існуючого матеріалу в документ</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="48"/>
      <source>Copy existing...</source>
      <translation>Копіювання вже існує...</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="58"/>
      <source>Name</source>
      <translation>Назва</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="65"/>
      <source>The name/label of this material</source>
      <translation>Назва/мітка цього матеріалу</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="94"/>
      <source>Description</source>
      <translation>Опис</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="101"/>
      <source>An optional description for this material</source>
      <translation>Необовʼязковий опис для цього матеріалу</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="112"/>
      <source>Color</source>
      <translation>Колір</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="125"/>
      <source>The color of this material</source>
      <translation>Колір цього матеріалу</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="139"/>
      <source>Section Color</source>
      <translation>Колір секції</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="163"/>
      <source>Transparency</source>
      <translation>Прозорість</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="170"/>
      <source>A transparency value for this material</source>
      <translation>Значення прозорості для цього матеріалу</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="184"/>
      <source>Standard code</source>
      <translation>Стандартний код</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="191"/>
      <source>A standard (MasterFormat, Omniclass...) code for this material</source>
      <translation>Стандарт (MasterFormat, Omniclass...) коду для цього матеріалу</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="204"/>
      <source>Opens a browser dialog to choose a class from a BIM standard</source>
      <translation>Відкриває діалог для браузера для вибору класу з стандарту BIM</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="223"/>
      <source>URL</source>
      <translation>Посилання</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="236"/>
      <source>A URL describing this material</source>
      <translation>URL, що описує цей матеріал</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="249"/>
      <source>Opens the URL in a browser</source>
      <translation>Відкриває URL-адресу у браузері</translation>
    </message>
    <message>
      <location filename="../ui/ArchMaterial.ui" line="268"/>
      <source>Father</source>
      <translation>Родоначальник</translation>
    </message>
  </context>
  <context>
    <name>Arch_3Views</name>
    <message>
      <location filename="../../ArchEquipment.py" line="248"/>
      <source>3 views from mesh</source>
      <translation>3 перегляди з полігональної сітки</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="252"/>
      <source>Creates 3 views (top, front, side) from a mesh-based object</source>
      <translation>Створює 3 види (зверху, на передній біч) з об'єкта, що базується на сітці</translation>
    </message>
  </context>
  <context>
    <name>Arch_Add</name>
    <message>
      <location filename="../../ArchCommands.py" line="1587"/>
      <source>Add component</source>
      <translation>Додати компонент</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1590"/>
      <source>Adds the selected components to the active object</source>
      <translation>Додає обраний компонент до активного об’єкту</translation>
    </message>
  </context>
  <context>
    <name>Arch_Axis</name>
    <message>
      <location filename="../../ArchAxis.py" line="94"/>
      <source>Axis</source>
      <translation>Вісь</translation>
    </message>
    <message>
      <location filename="../../ArchAxis.py" line="96"/>
      <source>Creates a set of axes</source>
      <translation>Створює набір осей</translation>
    </message>
  </context>
  <context>
    <name>Arch_AxisSystem</name>
    <message>
      <location filename="../../ArchAxisSystem.py" line="76"/>
      <source>Axis System</source>
      <translation>Система осей</translation>
    </message>
    <message>
      <location filename="../../ArchAxisSystem.py" line="80"/>
      <source>Creates an axis system from a set of axes</source>
      <translation>Створює систему осей із набору осей</translation>
    </message>
  </context>
  <context>
    <name>Arch_AxisTools</name>
    <message>
      <location filename="../../ArchAxis.py" line="1027"/>
      <location filename="../../ArchAxis.py" line="1028"/>
      <source>Axis tools</source>
      <translation>Інструменти осей</translation>
    </message>
  </context>
  <context>
    <name>Arch_Building</name>
    <message>
      <location filename="../../ArchBuilding.py" line="207"/>
      <source>Building</source>
      <translation>Будівля</translation>
    </message>
    <message>
      <location filename="../../ArchBuilding.py" line="211"/>
      <source>Creates a building object including selected objects.</source>
      <translation>Створює об’єкт будівлі при цьому включає туди обрані об’єкти</translation>
    </message>
  </context>
  <context>
    <name>Arch_BuildingPart</name>
    <message>
      <location filename="../../ArchBuildingPart.py" line="301"/>
      <source>BuildingPart</source>
      <translation>Будівельна частина</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="306"/>
      <source>Creates a BuildingPart object including selected objects</source>
      <translation>Створює об’єкт BuildingPart, включаючи вибрані об’єкти</translation>
    </message>
  </context>
  <context>
    <name>Arch_Check</name>
    <message>
      <location filename="../../ArchCommands.py" line="1843"/>
      <source>Check</source>
      <translation>Перевірити</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1846"/>
      <source>Checks the selected objects for problems</source>
      <translation>Перевірити виділені обʼєкти на наявність проблем</translation>
    </message>
  </context>
  <context>
    <name>Arch_CloneComponent</name>
    <message>
      <location filename="../../ArchCommands.py" line="1950"/>
      <source>Clone component</source>
      <translation>Скопіювати компонент</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1955"/>
      <source>Clones an object as an undefined architectural component</source>
      <translation>Клонує об'єкт як невизначений архітектурний компонент</translation>
    </message>
  </context>
  <context>
    <name>Arch_CloseHoles</name>
    <message>
      <location filename="../../ArchCommands.py" line="1821"/>
      <source>Close holes</source>
      <translation>Закрити отвори</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1824"/>
      <source>Closes holes in open shapes, turning them solids</source>
      <translation>Закриває отвори у відкритих формах, перетворюючи їх на суцільні тіла</translation>
    </message>
  </context>
  <context>
    <name>Arch_Component</name>
    <message>
      <location filename="../../ArchCommands.py" line="1914"/>
      <source>Component</source>
      <translation>Компонент</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1918"/>
      <source>Creates an undefined architectural component</source>
      <translation>Створює невизначений архітектурний компонент</translation>
    </message>
  </context>
  <context>
    <name>Arch_CurtainWall</name>
    <message>
      <location filename="../../ArchCurtainWall.py" line="103"/>
      <source>Curtain Wall</source>
      <translation>Навісна стіна</translation>
    </message>
    <message>
      <location filename="../../ArchCurtainWall.py" line="108"/>
      <source>Creates a curtain wall object from selected line or from scratch</source>
      <translation>Створює об’єкт навісної стіни з виділеної лінії або з нуля</translation>
    </message>
  </context>
  <context>
    <name>Arch_CutLine</name>
    <message>
      <location filename="../../ArchCutPlane.py" line="86"/>
      <source>Cut with line</source>
      <translation>Вирізати з лінією</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="89"/>
      <source>Cut an object with a line</source>
      <translation>Вирізати предмет з лінією</translation>
    </message>
  </context>
  <context>
    <name>Arch_CutPlane</name>
    <message>
      <location filename="../../ArchCutPlane.py" line="117"/>
      <source>Cut with plane</source>
      <translation>Вирізати з площиною</translation>
    </message>
    <message>
      <location filename="../../ArchCutPlane.py" line="120"/>
      <source>Cut an object with a plane</source>
      <translation>Вирізати обʼєкт з площиною</translation>
    </message>
  </context>
  <context>
    <name>Arch_Equipment</name>
    <message>
      <location filename="../../ArchEquipment.py" line="176"/>
      <source>Equipment</source>
      <translation>Обладнання</translation>
    </message>
    <message>
      <location filename="../../ArchEquipment.py" line="181"/>
      <source>Creates an equipment object from a selected object (Part or Mesh)</source>
      <translation>Створює об'єкт спорядження з обраного об'єкту (частина або Сітка)</translation>
    </message>
  </context>
  <context>
    <name>Arch_Fence</name>
    <message>
      <location filename="../../ArchFence.py" line="436"/>
      <source>Fence</source>
      <translation>Паркан</translation>
    </message>
    <message>
      <location filename="../../ArchFence.py" line="440"/>
      <source>Creates a fence object from a selected section, post and path</source>
      <translation>Створює об'єкт огорожі з вибраного розділу, посту і шляху</translation>
    </message>
  </context>
  <context>
    <name>Arch_Floor</name>
    <message>
      <location filename="../../ArchFloor.py" line="115"/>
      <source>Level</source>
      <translation>Рівень</translation>
    </message>
    <message>
      <location filename="../../ArchFloor.py" line="120"/>
      <source>Creates a Building Part object that represents a level, including selected objects</source>
      <translation>Створює об'єкт з будівельною частиною, який представляє рівень, включаючи обрані об'єкти</translation>
    </message>
  </context>
  <context>
    <name>Arch_Frame</name>
    <message>
      <location filename="../../ArchFrame.py" line="81"/>
      <source>Frame</source>
      <translation>Каркас</translation>
    </message>
    <message>
      <location filename="../../ArchFrame.py" line="86"/>
      <source>Creates a frame object from a planar 2D object (the extrusion path(s)) and a profile. Make sure objects are selected in that order.</source>
      <translation>Створює каркасний об'кт з плаского 2D об’єкту (траєкторія видавлювання) та профілю. Переконайтесь, що об’єкти вибрані в такому порядку.</translation>
    </message>
  </context>
  <context>
    <name>Arch_Grid</name>
    <message>
      <location filename="../../ArchGrid.py" line="72"/>
      <source>Grid</source>
      <translation>Сітка</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="76"/>
      <source>Creates a customizable grid object</source>
      <translation>Створює настроювану сітку обʼєкту</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="109"/>
      <source>The number of rows</source>
      <translation>Кількість рядків</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="116"/>
      <source>The number of columns</source>
      <translation>Кількість стовпців</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="123"/>
      <source>The sizes for rows</source>
      <translation>Розміри для рядків</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="130"/>
      <source>The sizes of columns</source>
      <translation>Розміри стовпців</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="139"/>
      <source>The span ranges of cells that are merged together</source>
      <translation>Масштабні межі клітин, які обʼєднані разом</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="148"/>
      <source>The type of 3D points produced by this grid object</source>
      <translation>Тип тривимірних точок, утворених цим обʼєктом сітки</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="162"/>
      <source>The total width of this grid</source>
      <translation>Загальна ширина цієї сітки</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="169"/>
      <source>The total height of this grid</source>
      <translation>Загальна висота цієї сітки</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="179"/>
      <source>Creates automatic column divisions (set to 0 to disable)</source>
      <translation>Автоматичне створення підрозділів стовпців (0 вимкнено)</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="188"/>
      <source>Creates automatic row divisions (set to 0 to disable)</source>
      <translation>Створює автоматичні поділи рядків (0 - вимкнено)</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="198"/>
      <source>When in edge midpoint mode, if this grid must reorient its children along edge normals or not</source>
      <translation>У режимі середньої точки краю, якщо ця сітка повинна переорієнтувати своїх дітей за крайовими нормалями чи ні</translation>
    </message>
    <message>
      <location filename="../../ArchGrid.py" line="205"/>
      <source>The indices of faces to hide</source>
      <translation>Індекси поверхонь, що сховаються</translation>
    </message>
  </context>
  <context>
    <name>Arch_IfcSpreadsheet</name>
    <message>
      <location filename="../../ArchCommands.py" line="2025"/>
      <source>Create IFC spreadsheet...</source>
      <translation>Створити таблицю IFC ...</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="2030"/>
      <source>Creates a spreadsheet to store IFC properties of an object.</source>
      <translation>Створює таблицю для зберігання властивостей IFC об'єкта.</translation>
    </message>
  </context>
  <context>
    <name>Arch_Material</name>
    <message>
      <location filename="../../ArchMaterial.py" line="122"/>
      <source>Material</source>
      <translation>Матеріал</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="127"/>
      <source>Creates or edits the material definition of a selected object.</source>
      <translation>Створює або редагує визначення матеріалу, для обраного об'єкту.</translation>
    </message>
  </context>
  <context>
    <name>Arch_MaterialTools</name>
    <message>
      <location filename="../../ArchMaterial.py" line="1247"/>
      <location filename="../../ArchMaterial.py" line="1248"/>
      <source>Material tools</source>
      <translation>Інструменти матеріалу</translation>
    </message>
  </context>
  <context>
    <name>Arch_MergeWalls</name>
    <message>
      <location filename="../../ArchWall.py" line="776"/>
      <source>Merge Walls</source>
      <translation>Обʼєднати стіни</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="779"/>
      <source>Merges the selected walls, if possible</source>
      <translation>По можливості об’єднує вибрані стіни</translation>
    </message>
  </context>
  <context>
    <name>Arch_MeshToShape</name>
    <message>
      <location filename="../../ArchCommands.py" line="1721"/>
      <source>Mesh to Shape</source>
      <translation>Меш у форму</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1724"/>
      <source>Turns selected meshes into Part Shape objects</source>
      <translation>Перетворює вибрані полігональні сітки у обʼєкти фігур</translation>
    </message>
  </context>
  <context>
    <name>Arch_MultiMaterial</name>
    <message>
      <location filename="../../ArchMaterial.py" line="164"/>
      <source>Multi-Material</source>
      <translation>Мульти-Матеріал</translation>
    </message>
    <message>
      <location filename="../../ArchMaterial.py" line="168"/>
      <source>Creates or edits multi-materials</source>
      <translation>Створює або редагує мультиматеріали</translation>
    </message>
  </context>
  <context>
    <name>Arch_Nest</name>
    <message>
      <location filename="../../ArchPanel.py" line="2028"/>
      <source>Nest</source>
      <translation>Гніздо</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="2032"/>
      <source>Nests a series of selected shapes in a container</source>
      <translation>Перетворює серію позначених фігур у контейнер</translation>
    </message>
  </context>
  <context>
    <name>Arch_Panel</name>
    <message>
      <location filename="../../ArchPanel.py" line="159"/>
      <source>Panel</source>
      <translation>Панель</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="164"/>
      <source>Creates a panel object from scratch or from a selected object (sketch, wire, face or solid)</source>
      <translation>Створює об'єкт панелі з нуля або з вибраного об'єкта (ескіз, дрот, поверхні або суцільний)</translation>
    </message>
  </context>
  <context>
    <name>Arch_PanelTools</name>
    <message>
      <location filename="../../ArchPanel.py" line="2216"/>
      <location filename="../../ArchPanel.py" line="2217"/>
      <source>Panel tools</source>
      <translation>Інструменти панелі</translation>
    </message>
  </context>
  <context>
    <name>Arch_Panel_Cut</name>
    <message>
      <location filename="../../ArchPanel.py" line="417"/>
      <source>Panel Cut</source>
      <translation>Панель різати</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="421"/>
      <source>Creates 2D views of selected panels</source>
      <translation>Створює 2D види вибраних панелей</translation>
    </message>
  </context>
  <context>
    <name>Arch_Panel_Sheet</name>
    <message>
      <location filename="../../ArchPanel.py" line="452"/>
      <source>Panel Sheet</source>
      <translation>Панель Листи</translation>
    </message>
    <message>
      <location filename="../../ArchPanel.py" line="456"/>
      <source>Creates a 2D sheet which can contain panel cuts</source>
      <translation>Створює 2D-аркуш, який може містити перетини панелей</translation>
    </message>
  </context>
  <context>
    <name>Arch_Pipe</name>
    <message>
      <location filename="../../ArchPipe.py" line="109"/>
      <source>Pipe</source>
      <translation>Труба</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="113"/>
      <source>Creates a pipe object from a given Wire or Line</source>
      <translation>Створює трубковий обʼєкт з даної ламаної або лінії</translation>
    </message>
  </context>
  <context>
    <name>Arch_PipeConnector</name>
    <message>
      <location filename="../../ArchPipe.py" line="157"/>
      <source>Connector</source>
      <translation>Зʼєднувач</translation>
    </message>
    <message>
      <location filename="../../ArchPipe.py" line="162"/>
      <source>Creates a connector between 2 or 3 selected pipes</source>
      <translation>Створює з'єднувач між 2 або 3 вибраними трубами</translation>
    </message>
  </context>
  <context>
    <name>Arch_PipeTools</name>
    <message>
      <location filename="../../ArchPipe.py" line="668"/>
      <location filename="../../ArchPipe.py" line="669"/>
      <source>Pipe tools</source>
      <translation>Інструменти для труб</translation>
    </message>
  </context>
  <context>
    <name>Arch_Profile</name>
    <message>
      <location filename="../../ArchProfile.py" line="124"/>
      <source>Profile</source>
      <translation>Профіль</translation>
    </message>
    <message>
      <location filename="../../ArchProfile.py" line="126"/>
      <source>Creates a profile object</source>
      <translation>Створює обʼєкт профілю</translation>
    </message>
  </context>
  <context>
    <name>Arch_Project</name>
    <message>
      <location filename="../../ArchProject.py" line="104"/>
      <source>Project</source>
      <translation>Проект</translation>
    </message>
    <message>
      <location filename="../../ArchProject.py" line="109"/>
      <source>Creates a project entity aggregating the selected sites.</source>
      <translation>Створює об'єкт, який поєднує обрані ділянки.</translation>
    </message>
  </context>
  <context>
    <name>Arch_Rebar</name>
    <message>
      <location filename="../../ArchRebar.py" line="112"/>
      <source>Custom Rebar</source>
      <translation>Користувацька арматура</translation>
    </message>
    <message>
      <location filename="../../ArchRebar.py" line="117"/>
      <source>Creates a Reinforcement bar from the selected face of solid object and/or a sketch</source>
      <translation>Створює арматурний стрижень із вибраної грані твердого об'єкта та/або ескізу</translation>
    </message>
  </context>
  <context>
    <name>Arch_RebarTools</name>
    <message>
      <location filename="../../InitGui.py" line="155"/>
      <source>Rebar tools</source>
      <translation>Арматурні інструменти</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="160"/>
      <source>Create various types of rebars, including U-shaped, L-shaped, and stirrup</source>
      <translation>Створюйте різні типи арматурних прутів, включаючи U-подібні, L-подібні та стременові</translation>
    </message>
  </context>
  <context>
    <name>Arch_Reference</name>
    <message>
      <location filename="../../ArchReference.py" line="848"/>
      <source>External reference</source>
      <translation>Зовнішній посилання</translation>
    </message>
    <message>
      <location filename="../../ArchReference.py" line="852"/>
      <source>Creates an external reference object</source>
      <translation>Створює зовнішній еталонний обʼєкт</translation>
    </message>
  </context>
  <context>
    <name>Arch_Remove</name>
    <message>
      <location filename="../../ArchCommands.py" line="1636"/>
      <source>Remove component</source>
      <translation>Видалити компонент</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1640"/>
      <source>Remove the selected components from their parents, or create a hole in a component</source>
      <translation>Видалення обраних компонентів від своїх батьків, або створити  отвір в компоненті</translation>
    </message>
  </context>
  <context>
    <name>Arch_RemoveShape</name>
    <message>
      <location filename="../../ArchCommands.py" line="1801"/>
      <source>Remove Shape from Arch</source>
      <translation>Видалити фігуру з Arch</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1804"/>
      <source>Removes cubic shapes from Arch components</source>
      <translation>Видаляє з компонентів Arch кубічні фігури</translation>
    </message>
  </context>
  <context>
    <name>Arch_Roof</name>
    <message>
      <location filename="../../ArchRoof.py" line="221"/>
      <source>Roof</source>
      <translation>Покрівля</translation>
    </message>
    <message>
      <location filename="../../ArchRoof.py" line="225"/>
      <source>Creates a roof object from the selected wire.</source>
      <translation>Створює дах з вибраного каркасу.</translation>
    </message>
  </context>
  <context>
    <name>Arch_Schedule</name>
    <message>
      <location filename="../../ArchSchedule.py" line="65"/>
      <source>Schedule</source>
      <translation>Запланувати</translation>
    </message>
    <message>
      <location filename="../../ArchSchedule.py" line="68"/>
      <source>Creates a schedule to collect data from the model</source>
      <translation>Створює графік для збору даних з моделі</translation>
    </message>
  </context>
  <context>
    <name>Arch_SectionPlane</name>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1059"/>
      <source>Section Plane</source>
      <translation>Поверхня перерізу</translation>
    </message>
    <message>
      <location filename="../../ArchSectionPlane.py" line="1063"/>
      <source>Creates a section plane object, including the selected objects</source>
      <translation>Створює об'єкт площини розділу, включаючи обрані об'єкти</translation>
    </message>
  </context>
  <context>
    <name>Arch_SelectNonSolidMeshes</name>
    <message>
      <location filename="../../ArchCommands.py" line="1764"/>
      <source>Select non-manifold meshes</source>
      <translation>Обрати немноговидні сітки</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1768"/>
      <source>Selects all non-manifold meshes from the document or from the selected groups</source>
      <translation>Виділяє всі немноговидні сітки з документу або обраних груп</translation>
    </message>
  </context>
  <context>
    <name>Arch_Site</name>
    <message>
      <location filename="../../ArchSite.py" line="527"/>
      <source>Site</source>
      <translation>Ділянка</translation>
    </message>
    <message>
      <location filename="../../ArchSite.py" line="531"/>
      <source>Creates a site object including selected objects.</source>
      <translation>Створює об’єкт поверхні, який включає обрані об’єкти</translation>
    </message>
  </context>
  <context>
    <name>Arch_Space</name>
    <message>
      <location filename="../../ArchStairs.py" line="238"/>
      <source>Creates a stairs object</source>
      <translation>Створює сходові обʼєкти</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="236"/>
      <source>Space</source>
      <translation>Простір</translation>
    </message>
    <message>
      <location filename="../../ArchSpace.py" line="240"/>
      <source>Creates a space object from selected boundary objects</source>
      <translation>Створює пропусковий обʼєкт з обраних граничних обʼєктів</translation>
    </message>
  </context>
  <context>
    <name>Arch_SplitMesh</name>
    <message>
      <location filename="../../ArchCommands.py" line="1691"/>
      <source>Split Mesh</source>
      <translation>Розділити меш</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1694"/>
      <source>Splits selected meshes into independent components</source>
      <translation>Розбиває меш на незалежні компоненти</translation>
    </message>
  </context>
  <context>
    <name>Arch_Stairs</name>
    <message>
      <location filename="../../ArchStairs.py" line="236"/>
      <source>Stairs</source>
      <translation>Сходи</translation>
    </message>
  </context>
  <context>
    <name>Arch_StructuralSystem</name>
    <message>
      <location filename="../../ArchStructure.py" line="274"/>
      <source>Structural System</source>
      <translation>Структурна система</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="278"/>
      <source>Create a structural system object from a selected structure and axis</source>
      <translation>Створити об'єкт структурної системи з вибраної структури та осі</translation>
    </message>
  </context>
  <context>
    <name>Arch_Structure</name>
    <message>
      <location filename="../../ArchStructure.py" line="330"/>
      <source>Structure</source>
      <translation>Структура</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="335"/>
      <source>Creates a structure object from scratch or from a selected object (sketch, wire, face or solid)</source>
      <translation>Створює об’єкт структуру з нуля, або з обраного об’єкту (ескіз, дріт, поверхня або суцільна)</translation>
    </message>
  </context>
  <context>
    <name>Arch_StructureTools</name>
    <message>
      <location filename="../../ArchStructure.py" line="2035"/>
      <location filename="../../ArchStructure.py" line="2036"/>
      <source>Structure tools</source>
      <translation>Структурні інструменти</translation>
    </message>
  </context>
  <context>
    <name>Arch_StructuresFromSelection</name>
    <message>
      <location filename="../../ArchStructure.py" line="205"/>
      <source>Multiple Structures</source>
      <translation>Множинна структура</translation>
    </message>
    <message>
      <location filename="../../ArchStructure.py" line="209"/>
      <source>Create multiple Arch Structure objects from a selected base, using each selected edge as an extrusion path</source>
      <translation>Створення кількох об'єктів Арочних структурних об'єктів із вибраної бази, використовуючи кожен вибраний край як шлях до видавлювання</translation>
    </message>
  </context>
  <context>
    <name>Arch_Survey</name>
    <message>
      <location filename="../../ArchCommands.py" line="1873"/>
      <source>Survey</source>
      <translation>Перегляд</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1874"/>
      <source>Starts survey</source>
      <translation>Почати перегляд</translation>
    </message>
  </context>
  <context>
    <name>Arch_ToggleIfcBrepFlag</name>
    <message>
      <location filename="../../ArchCommands.py" line="1893"/>
      <source>Toggle IFC Brep flag</source>
      <translation>Перемикач перемикання прапорців IFC</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="1897"/>
      <source>Force an object to be exported as Brep or not</source>
      <translation>Примусово експортувати об'єкт як Brep чи ні</translation>
    </message>
  </context>
  <context>
    <name>Arch_ToggleSubs</name>
    <message>
      <location filename="../../ArchCommands.py" line="2063"/>
      <source>Toggle subcomponents</source>
      <translation>Перемкнути підкомпоненти</translation>
    </message>
    <message>
      <location filename="../../ArchCommands.py" line="2066"/>
      <source>Shows or hides the subcomponents of this object</source>
      <translation>Показує або приховує підкомпоненти цього об’єкта</translation>
    </message>
  </context>
  <context>
    <name>Arch_Truss</name>
    <message>
      <location filename="../../ArchTruss.py" line="87"/>
      <source>Truss</source>
      <translation>Вʼязка</translation>
    </message>
    <message>
      <location filename="../../ArchTruss.py" line="92"/>
      <source>Creates a truss object from selected line or from scratch</source>
      <translation>Створює об'єкт трасування з вибраної лінії чи з нуля</translation>
    </message>
  </context>
  <context>
    <name>Arch_Wall</name>
    <message>
      <location filename="../../ArchWall.py" line="291"/>
      <source>Wall</source>
      <translation>Стіна</translation>
    </message>
    <message>
      <location filename="../../ArchWall.py" line="296"/>
      <source>Creates a wall object from scratch or from a selected object (wire, face or solid)</source>
      <translation>Створює об’єкт стіни з нуля, або з обраного об’єкту (дріт, поверхня або суцільна)</translation>
    </message>
  </context>
  <context>
    <name>Arch_Window</name>
    <message>
      <location filename="../../ArchWindow.py" line="179"/>
      <source>Window</source>
      <translation>Вікно</translation>
    </message>
    <message>
      <location filename="../../ArchWindow.py" line="184"/>
      <source>Creates a window object from a selected object (wire, rectangle or sketch)</source>
      <translation>Створює віконний об'єкт з обраного об'єкта (дрот, прямокутник або ескіз)</translation>
    </message>
  </context>
  <context>
    <name>BimServer</name>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="14"/>
      <source>BimServer</source>
      <translation type="unfinished">BimServer</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="20"/>
      <source>Server</source>
      <translation>Сервер</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="28"/>
      <source>The name of the BimServer you are currently connecting to. Change settings in Arch Preferences</source>
      <translation>Назва BimServer до якого ви підключаєтесь. Змінити налаштування в параметрах Arch</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="31"/>
      <source>Bim Server</source>
      <translation>Бім сервер</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="38"/>
      <source>Connect</source>
      <translation>Підключитися</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="55"/>
      <source>Idle</source>
      <translation>Бездіяльний</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="79"/>
      <source>Open in browser</source>
      <translation>Відкрити у браузері</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="90"/>
      <source>Project</source>
      <translation>Проект</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="97"/>
      <source>The list of projects present on the Bim Server</source>
      <translation>Список проектів на Bim Server</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="109"/>
      <source>Download</source>
      <translation>Завантажити</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="115"/>
      <source>Available revisions:</source>
      <translation>Доступні перевірки:</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="144"/>
      <source>Open</source>
      <translation>Відкрити</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="156"/>
      <location filename="../ui/BimServerTaskPanel.ui" line="205"/>
      <source>Upload</source>
      <translation>Вивантажити</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="164"/>
      <source>Root object:</source>
      <translation>Кореневий обʼєкт:</translation>
    </message>
    <message>
      <location filename="../ui/BimServerTaskPanel.ui" line="178"/>
      <source>Comment</source>
      <translation>Коментар</translation>
    </message>
  </context>
  <context>
    <name>Dialog</name>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="14"/>
      <source>Schedule definition</source>
      <translation>Визначення розкладу</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="22"/>
      <source>Schedule name:</source>
      <translation>Назва розкладу:</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="29"/>
      <source>Unnamed schedule</source>
      <translation>Розклад без назви</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="60"/>
      <source>Description</source>
      <translation>Опис</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="63"/>
      <source>A description for this operation</source>
      <translation>Опис цієї операції</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="68"/>
      <source>Property</source>
      <translation>Властивість</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="71"/>
      <source>The property to retrieve from each object.
Can be &quot;Count&quot; to count the objects, or property names
like &quot;Length&quot; or &quot;Shape.Volume&quot; to retrieve
a certain property.</source>
      <translation type="unfinished">The property to retrieve from each object.
Can be &quot;Count&quot; to count the objects, or property names
like &quot;Length&quot; or &quot;Shape.Volume&quot; to retrieve
a certain property.</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="79"/>
      <source>Unit</source>
      <translation>Одиниця</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="82"/>
      <source>An optional unit to express the resulting value. Ex: m^3 (you can also write m³ or m3)</source>
      <translation>Опціональна одиниця для вираження отриманого значення. Наприклад: м^3 (ви також можете записати m3 або m3)</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="87"/>
      <source>Objects</source>
      <translation>Обʼєкти</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="90"/>
      <source>An optional semicolon (;) separated list of object names
(internal names, not labels), to be considered by this operation.
If the list contains groups, children will be added.
Leave blank to use all objects from the document</source>
      <translation>Необов’язковий розділений крапкою з комою (;) список назв об’єктів
(внутрішні імена, а не мітки), які слід враховувати під час цієї операції.
Якщо список містить групи, діти будуть додані.
Залиште порожнім, щоб використовувати всі обʼєкти з документа</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="98"/>
      <source>Filter</source>
      <translation>Фільтр</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="101"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;An optional semicolon (;) separated list of property:value filters. Prepend ! to a property name to invert the effect of the filer (exclude objects that match the filter). Objects whose property contains the value will be matched. Examples of valid filters (everything is case-insensitive):&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Name:Wall&lt;/span&gt; - Will only consider objects with &amp;quot;wall&amp;quot; in their name (internal name)&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Name:Wall&lt;/span&gt; - Will only consider objects which DON&apos;T have &amp;quot;wall&amp;quot; in their name (internal name)&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Description:Win&lt;/span&gt; - Will only consider objects with &amp;quot;win&amp;quot; in their description&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Label:Win&lt;/span&gt; - Will only consider objects which DO NOT have &amp;quot;win&amp;quot; in their label&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IfcType:Wall&lt;/span&gt; - Will only consider objects which Ifc Type is &amp;quot;Wall&amp;quot;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Tag:Wall&lt;/span&gt; - Will only consider objects which tag is NOT &amp;quot;Wall&amp;quot;&lt;/p&gt;&lt;p&gt;If you leave this field empty, no filtering is applied&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;An optional semicolon (;) separated list of property:value filters. Prepend ! to a property name to invert the effect of the filer (exclude objects that match the filter). Objects whose property contains the value will be matched. Examples of valid filters (everything is case-insensitive):&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Name:Wall&lt;/span&gt; - Will only consider objects with &amp;quot;wall&amp;quot; in their name (internal name)&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Name:Wall&lt;/span&gt; - Will only consider objects which DON&apos;T have &amp;quot;wall&amp;quot; in their name (internal name)&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Description:Win&lt;/span&gt; - Will only consider objects with &amp;quot;win&amp;quot; in their description&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Label:Win&lt;/span&gt; - Will only consider objects which DO NOT have &amp;quot;win&amp;quot; in their label&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IfcType:Wall&lt;/span&gt; - Will only consider objects which Ifc Type is &amp;quot;Wall&amp;quot;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!Tag:Wall&lt;/span&gt; - Will only consider objects which tag is NOT &amp;quot;Wall&amp;quot;&lt;/p&gt;&lt;p&gt;If you leave this field empty, no filtering is applied&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="111"/>
      <source>If this is enabled, an associated spreadsheet containing the results will be maintained together with this schedule object</source>
      <translation>Якщо цей параметр увімкнено, то асоційована таблиця з якою будуть зберігатися разом з цим обʼєктом операцій</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="114"/>
      <source>Associate spreadsheet</source>
      <translation>Повʼязати таблицю</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="121"/>
      <source>If this is turned on, additional lines will be filled with each object considered. If not, only the totals.</source>
      <translation>Якщо це увімкнено, то додаткові лінії будуть заповнені кожним об’єктом розглянуті. Якщо ні, тільки загалом.</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="124"/>
      <source>Detailed results</source>
      <translation>Детальні результати</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="135"/>
      <source>Adds a line below the selected line/cell</source>
      <translation>Додає рядок нижче вибраного лінії / секції</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="138"/>
      <source>Add row</source>
      <translation>Додати рядок</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="150"/>
      <source>Deletes the selected line</source>
      <translation>Видалити вибраниу лінію</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="153"/>
      <source>Del row</source>
      <translation>Видалити рядок</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="165"/>
      <source>Clears the whole list</source>
      <translation>Очищує весь список</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="168"/>
      <source>Clear</source>
      <translation>Очистити</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="180"/>
      <source>Put selected objects into the &quot;Objects&quot; column of the selected row</source>
      <translation type="unfinished">Put selected objects into the &quot;Objects&quot; column of the selected row</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="183"/>
      <source>Add selection</source>
      <translation>Додати виділення</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="190"/>
      <source>Imports the contents of a CSV file</source>
      <translation>Імпортує вміст CSV файлу</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="193"/>
      <source>Import</source>
      <translation>Імпортувати</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="205"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This exports the results to a CSV or Markdown file. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note for CSV export:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;In Libreoffice, you can keep this CSV file linked by right-clicking the Sheets tab bar -&amp;gt; New sheet -&amp;gt; From file -&amp;gt; Link (Note: as of LibreOffice v6.x the correct path now is: Sheet -&amp;gt; Insert Sheet... -&amp;gt; From file -&amp;gt; Browse...)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This exports the results to a CSV or Markdown file. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note for CSV export:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;In Libreoffice, you can keep this CSV file linked by right-clicking the Sheets tab bar -&amp;gt; New sheet -&amp;gt; From file -&amp;gt; Link (Note: as of LibreOffice v6.x the correct path now is: Sheet -&amp;gt; Insert Sheet... -&amp;gt; From file -&amp;gt; Browse...)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/ArchSchedule.ui" line="208"/>
      <source>Export</source>
      <translation>Експорт</translation>
    </message>
    <message>
      <location filename="../ui/DialogBimServerLogin.ui" line="14"/>
      <source>BimServer Login</source>
      <translation type="unfinished">BimServer Login</translation>
    </message>
    <message>
      <location filename="../ui/DialogBimServerLogin.ui" line="22"/>
      <source>BimServer URL:</source>
      <translation>URL-адреса BimServer:</translation>
    </message>
    <message>
      <location filename="../ui/DialogBimServerLogin.ui" line="36"/>
      <source>Login (email):</source>
      <translation>Логін (email):</translation>
    </message>
    <message>
      <location filename="../ui/DialogBimServerLogin.ui" line="50"/>
      <source>Password:</source>
      <translation>Пароль:</translation>
    </message>
    <message>
      <location filename="../ui/DialogBimServerLogin.ui" line="62"/>
      <source>Keep me logged in across FreeCAD sessions</source>
      <translation>Запамʼятати мене для входу через сеанси FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/DialogDisplayText.ui" line="14"/>
      <source>Dialog</source>
      <translation>Діалогове вікно</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="14"/>
      <source>IFC properties editor</source>
      <translation>Редактор властивостей IFC</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="22"/>
      <source>IFC UUID:</source>
      <translation>Інтерфейс UID:</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="29"/>
      <source>Leave this empty to generate one at export</source>
      <translation>Залиште це поле пустим, щоб згенерувати щось під час експорту</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="38"/>
      <source>List of IFC properties for this object. Double-click to edit, drag and drop to reorganize</source>
      <translation>Список властивостей IFC для цього обʼєкта. Двічі клацніть, перетягніть для редагування</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="63"/>
      <source>Delete selected property/set</source>
      <translation>Видалити вибрану властивість/набір</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="76"/>
      <source>Force exporting geometry as BREP</source>
      <translation>Примусово експортувати геометрію як BREP</translation>
    </message>
    <message>
      <location filename="../ui/DialogIfcProperties.ui" line="83"/>
      <source>Force export full FreeCAD parametric data</source>
      <translation>Примусово експортувати повні параметричні дані FreeCAD</translation>
    </message>
  </context>
  <context>
    <name>Draft</name>
    <message>
      <location filename="../../InitGui.py" line="262"/>
      <location filename="../../InitGui.py" line="265"/>
      <location filename="../../InitGui.py" line="268"/>
      <location filename="../../InitGui.py" line="272"/>
      <source>Draft</source>
      <translation>Ухил</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="312"/>
      <location filename="../../InitGui.py" line="315"/>
      <location filename="../../InitGui.py" line="318"/>
      <source>Import-Export</source>
      <translation>Імпорт-експорт</translation>
    </message>
    <message>
      <location filename="../../ArchBuildingPart.py" line="1377"/>
      <source>Writing camera position</source>
      <translation>Запис положення камери</translation>
    </message>
  </context>
  <context>
    <name>Form</name>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="14"/>
      <source>Git</source>
      <translation type="unfinished">Git</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="20"/>
      <source>Status</source>
      <translation>Статус</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="48"/>
      <source>Log</source>
      <translation>Лог</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="55"/>
      <source>Refresh</source>
      <translation>Оновити</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="64"/>
      <source>List of files to be committed:</source>
      <translation>Список файлів, які потрібно зафіксувати:</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="93"/>
      <source>Diff</source>
      <translation>Відмінність</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="100"/>
      <source>Select all</source>
      <translation>Виділити все</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="112"/>
      <location filename="../ui/GitTaskPanel.ui" line="147"/>
      <source>Commit</source>
      <translation>Зафіксувати зміни</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="120"/>
      <source>Commit message</source>
      <translation>Фіксувати повідомлення</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="159"/>
      <source>Remote repositories</source>
      <translation>Віддалені сховища</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="183"/>
      <source>Pull</source>
      <translation>Тягнути</translation>
    </message>
    <message>
      <location filename="../ui/GitTaskPanel.ui" line="190"/>
      <source>Push</source>
      <translation>Штовхати</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="14"/>
      <source>Multimaterial definition</source>
      <translation>Багатоматеріальне визначення</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="21"/>
      <source>Copy existing...</source>
      <translation>Копіювання вже існує...</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="29"/>
      <source>Edit definition</source>
      <translation>Редагувати визначення</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="37"/>
      <source>Name:</source>
      <translation>Назва:</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="49"/>
      <source>Composition:</source>
      <translation>Композиція:</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="59"/>
      <source>Total thickness</source>
      <translation>Загальна товщина</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="68"/>
      <source>Add</source>
      <translation>Додати</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="75"/>
      <source>Up</source>
      <translation>Вгору</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="82"/>
      <source>Down</source>
      <translation>Вниз</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="89"/>
      <source>Del</source>
      <translation>Видал.</translation>
    </message>
    <message>
      <location filename="../ui/ArchMultiMaterial.ui" line="96"/>
      <source>Invert</source>
      <translation>Інвертувати</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="14"/>
      <source>Nesting</source>
      <translation>Укладання</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="20"/>
      <source>Container</source>
      <translation>Контейнер</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="36"/>
      <source>Pick selected</source>
      <translation>Вибрати вибране</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="46"/>
      <source>Shapes</source>
      <translation>Форми</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="57"/>
      <source>Add selected</source>
      <translation>Додати вибрані</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="64"/>
      <source>Remove</source>
      <translation>Видалити</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="76"/>
      <source>Nesting parameters</source>
      <translation>Параметри владання</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="82"/>
      <source>Tolerance</source>
      <translation>Точність</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="89"/>
      <source>Closer than this, two points are considered equal</source>
      <translation>Ближче ніж це, два точки вважаються рівними</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="102"/>
      <source>Arcs subdivisions</source>
      <translation>Поділ арок</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="109"/>
      <source>The number of segments to divide non-linear edges into, for calculations. If curved shapes overlap, try raising this value</source>
      <translation>Кількість відрізків, на які потрібно поділити нелінійні ребра для розрахунків. Якщо криві фігури перекриваються, спробуйте підвищити це значення</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="125"/>
      <source>Rotations</source>
      <translation>Обертання</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="132"/>
      <source>A comma-separated list of angles to try and rotate the shapes</source>
      <translation>Список кутів, розділених комами, для спроби обертання фігур</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="135"/>
      <source>0,90,180,270</source>
      <translation type="unfinished">0,90,180,270</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="145"/>
      <source>Nesting operation</source>
      <translation>Операція вкладання</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="154"/>
      <source>pass %p</source>
      <translation>пропуск %p</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="163"/>
      <source>Start</source>
      <translation>Початок</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="170"/>
      <source>Stop</source>
      <translation>Стоп</translation>
    </message>
    <message>
      <location filename="../ui/ArchNest.ui" line="177"/>
      <source>Preview</source>
      <translation>Попередній перегляд</translation>
    </message>
  </context>
  <context>
    <name>Gui::Dialog::DlgSettingsArch</name>
    <message>
      <location filename="../ui/preferences-arch.ui" line="14"/>
      <source>General settings</source>
      <translation>Загальні параметри</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="26"/>
      <source>Object creation</source>
      <translation>Створення обʼєкта</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="32"/>
      <source>Auto-join walls</source>
      <translation>Авто-зʼєднання стін</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="48"/>
      <source>If this is checked, when 2 similar walls are being connected, their underlying sketches will be joined into one, and the two walls will become one</source>
      <translation>Якщо позначено цей пункт, коли 2 подібні стіни будуть підключені, їхні основні ескізи будуть обʼєднані в одну, а обидві стіни стануть одним</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="51"/>
      <source>Join walls base sketches when possible</source>
      <translation>По можливості зʼєднайте основи ескізів стін</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="64"/>
      <source>Two possible strategies to avoid circular dependencies: Create one more object (unchecked) or remove external geometry of base sketch (checked)</source>
      <translation>Дві можливі стратегії для уникнення циклічних залежностей: Створіть ще один обʼєкт (безконтрольно) або видаліть зовнішню геометрію основи ескізу (перевірено)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="67"/>
      <source>Remove external geometry of base sketches when needed</source>
      <translation>Видалити зовнішню геометрію базових ескізів, коли це необхідно</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="80"/>
      <source>If this is checked, when an object becomes Subtraction or Addition of an Arch object, it will receive the Draft Construction color.</source>
      <translation>Якщо позначено цей пункт, коли обʼєкт Віднімається або Додається до Арки обʼєкта, він отримає колір Чернетки креслення.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="83"/>
      <source>Apply Draft construction style to subcomponents</source>
      <translation>Застосувати стиль створення креслення до підкомпонентів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="114"/>
      <source>Do not compute areas for object with more than:</source>
      <translation>Не обчислюйте області для обʼєкту більше, ніж:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="121"/>
      <source> faces</source>
      <translation> поверхні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="157"/>
      <source>Interval between file checks for references</source>
      <translation>Інтервал між перевірками файлів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="164"/>
      <source> seconds</source>
      <translation>секунди</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="188"/>
      <source>By default, new objects will have their &quot;Move with host&quot; property set to False, which means they won&apos;t move when their host object is moved.</source>
      <translation type="unfinished">By default, new objects will have their &quot;Move with host&quot; property set to False, which means they won&apos;t move when their host object is moved.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="191"/>
      <source>Set &quot;Move with host&quot; property to True by default</source>
      <translation type="unfinished">Set &quot;Move with host&quot; property to True by default</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="207"/>
      <source>Set &quot;Move base&quot; property to True by default</source>
      <translation type="unfinished">Set &quot;Move base&quot; property to True by default</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="223"/>
      <source>If this is checked, when an Arch object has a material, the object will take the color of the material. This can be overridden for each object.</source>
      <translation>Якщо позначено цей пункт, коли величина має матеріал, обʼєкт буде приймати колір матеріалу. Це може бути перевизначено для кожного обʼєкта.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="226"/>
      <source>Use material color as shape color</source>
      <translation>Використовувати колір матеріалу як колір фігури</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="260"/>
      <source>IFC version</source>
      <translation>Версія IFC</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="280"/>
      <source>The IFC version will change which attributes and products are supported</source>
      <translation>Версія IFC зміниться, які атрибути та продукти підтримуються</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="290"/>
      <source>IFC4</source>
      <translation type="unfinished">IFC4</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="295"/>
      <source>IFC2X3</source>
      <translation type="unfinished">IFC2X3</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="308"/>
      <source>Mesh to Shape Conversion</source>
      <translation>Перетворення Сітки в Форму</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="316"/>
      <source>If this is checked, conversion is faster but the result might still contain triangulated faces</source>
      <translation>Якщо позначено цей пункт, перетворення відбувається швидше, але результат все ще може містити тріангульовані поверхні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="319"/>
      <source>Fast conversion</source>
      <translation>Швидке перетворення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="335"/>
      <source>Tolerance:</source>
      <translation>Допуск:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="345"/>
      <source>Tolerance value to use when checking if 2 adjacent faces as planar</source>
      <translation>Значення точності для використання при перевірці чи 2 суміжні поверхні як площина</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="368"/>
      <source>If this is checked, flat groups of faces will be force-flattened, resulting in possible gaps and non-solid results</source>
      <translation>Якщо позначено цей пункт, рівними групами облич будуть згладжені силою, що призведе до можливих прогалин та несуцільних результатів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="371"/>
      <source>Force flat faces</source>
      <translation>Примусове пласкі поверхонь</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="388"/>
      <source>If this is checked, holes in faces will be performed by subtraction rather than using wires orientation</source>
      <translation>Якщо це позначено, отвори на гранях виконуватимуться шляхом віднімання, а не з використанням орієнтації каркасів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="391"/>
      <source>Cut method</source>
      <translation>Метод вирізу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="412"/>
      <source>2D rendering</source>
      <translation>2D візуалізація</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="420"/>
      <source>Show debug information during 2D rendering</source>
      <translation>Показувати інформацію для налагодження під час візуалізації 2D</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="423"/>
      <source>Show renderer debug messages</source>
      <translation>Показати  повідомлення про налагодження рендерингу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="456"/>
      <source>Cut areas line thickness ratio</source>
      <translation>Співвідношення товщини лінії зрізу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="476"/>
      <source>Specifies how many times the viewed line thickness must be applied to cut lines</source>
      <translation>Визначає, скільки разів товщина заданої лінії повинна застосовуватися для розрізання ліній</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="512"/>
      <source>Symbol line thickness ratio</source>
      <translation>Коефіцієнт товщини символу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="565"/>
      <source>Hidden geometry pattern</source>
      <translation>Приховати геометрію шаблонів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="585"/>
      <source>This is the SVG stroke-dasharray property to apply
to projections of hidden objects.</source>
      <translation>Це властивість SVG штрихпунктирних ліній щоб застосувати
до проекцій прихованих обʼєктів.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="589"/>
      <source>30, 10</source>
      <translation type="unfinished">30, 10</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="625"/>
      <source>Pattern scale</source>
      <translation>Масштаб штриховки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="645"/>
      <source>Scaling factor for patterns used by object that have
a Footprint display mode</source>
      <translation>Масштабуючий коефіцієнт для шаблонів, що використовуються для обʼєкту
в режимі режимі відображення відбиток</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="673"/>
      <source>Bim server</source>
      <translation>Bim сервер</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="681"/>
      <source>Address</source>
      <translation>Адреса</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="688"/>
      <source>The URL of a bim server instance (www.bimserver.org) to connect to.</source>
      <translation>URL-адреса бім серверу (www.bimserver.org) для підключення.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="691"/>
      <source>http://localhost:8082</source>
      <translation type="unfinished">http://localhost:8082</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="708"/>
      <source>If this is selected, the &quot;Open BimServer in browser&quot;
button will open the Bim Server interface in an external browser
instead of the FreeCAD web workbench</source>
      <translation type="unfinished">If this is selected, the &quot;Open BimServer in browser&quot;
button will open the Bim Server interface in an external browser
instead of the FreeCAD web workbench</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="713"/>
      <source>Open in external browser</source>
      <translation>Відкрити у зовнішньому браузері</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="731"/>
      <source>Survey</source>
      <translation>Перегляд</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="739"/>
      <source>If this is checked, the text that gets placed in the clipboard will include the unit. Otherwise, it will be a simple number expressed in internal units (millimeters)</source>
      <translation>Якщо позначено цей пункт, текст, що розміщується в буфер обміну, буде містити його. В іншому випадку це буде просте число, виражене у внутрішніх одиницях (міліметрах)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-arch.ui" line="742"/>
      <source>Include unit when sending measurements to clipboard</source>
      <translation>Включати одиницю вимірювання при відправленні в буфер обміну</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="14"/>
      <source>Defaults</source>
      <translation>За замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="26"/>
      <source>Walls</source>
      <translation>Стіни</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="34"/>
      <location filename="../ui/preferences-archdefaults.ui" line="211"/>
      <location filename="../ui/preferences-archdefaults.ui" line="452"/>
      <location filename="../ui/preferences-archdefaults.ui" line="702"/>
      <location filename="../ui/preferences-archdefaults.ui" line="859"/>
      <source>Width:</source>
      <translation>Ширина:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="41"/>
      <location filename="../ui/preferences-archdefaults.ui" line="80"/>
      <location filename="../ui/preferences-archdefaults.ui" line="179"/>
      <location filename="../ui/preferences-archdefaults.ui" line="218"/>
      <location filename="../ui/preferences-archdefaults.ui" line="257"/>
      <location filename="../ui/preferences-archdefaults.ui" line="340"/>
      <location filename="../ui/preferences-archdefaults.ui" line="379"/>
      <location filename="../ui/preferences-archdefaults.ui" line="462"/>
      <location filename="../ui/preferences-archdefaults.ui" line="504"/>
      <location filename="../ui/preferences-archdefaults.ui" line="546"/>
      <location filename="../ui/preferences-archdefaults.ui" line="670"/>
      <location filename="../ui/preferences-archdefaults.ui" line="709"/>
      <location filename="../ui/preferences-archdefaults.ui" line="748"/>
      <source> mm</source>
      <translation> мм</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="73"/>
      <location filename="../ui/preferences-archdefaults.ui" line="250"/>
      <location filename="../ui/preferences-archdefaults.ui" line="494"/>
      <location filename="../ui/preferences-archdefaults.ui" line="741"/>
      <source>Height:</source>
      <translation>Висота:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="103"/>
      <source>Use sketches</source>
      <translation>Використовувати ескізи</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="132"/>
      <location filename="../ui/preferences-archdefaults.ui" line="293"/>
      <location filename="../ui/preferences-archdefaults.ui" line="415"/>
      <location filename="../ui/preferences-archdefaults.ui" line="941"/>
      <location filename="../ui/preferences-archdefaults.ui" line="1044"/>
      <source>Color:</source>
      <translation>Колір:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="139"/>
      <source>This is the default color for new Wall objects</source>
      <translation>Це колір за замовчуванням для нових обʼєктів Стіна</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="164"/>
      <source>Structures</source>
      <translation>Структури</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="172"/>
      <location filename="../ui/preferences-archdefaults.ui" line="663"/>
      <location filename="../ui/preferences-archdefaults.ui" line="820"/>
      <source>Length:</source>
      <translation>Довжина:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="300"/>
      <source>This is the default color for new Structure objects</source>
      <translation>Це колір за замовчуванням для нових обʼєктів Структура</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="325"/>
      <source>Rebars</source>
      <translation>Арматура</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="333"/>
      <source>Diameter</source>
      <translation>Діаметр</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="372"/>
      <source>Offset</source>
      <translation>Зсув</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="444"/>
      <source>Windows</source>
      <translation>Вікна</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="459"/>
      <source>The default width for new windows</source>
      <translation>Ширина нових вікон за замовчанням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="501"/>
      <source>The default height for new windows</source>
      <translation>Висота нових вікон за замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="536"/>
      <source>Thickness:</source>
      <translation>Товщина:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="543"/>
      <source>The default thickness for new windows</source>
      <translation>Товщина нових вікон за замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="582"/>
      <location filename="../ui/preferences-archdefaults.ui" line="1079"/>
      <source>Transparency:</source>
      <translation>Прозорість:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="602"/>
      <source>Frame color:</source>
      <translation>Колір рамки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="626"/>
      <source>Glass color:</source>
      <translation>Колір скла:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="655"/>
      <source>Stairs</source>
      <translation>Сходи</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="784"/>
      <source>Number of steps:</source>
      <translation>Кількість кроків:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="812"/>
      <source>Panels</source>
      <translation>Панелі</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="827"/>
      <location filename="../ui/preferences-archdefaults.ui" line="866"/>
      <location filename="../ui/preferences-archdefaults.ui" line="905"/>
      <location filename="../ui/preferences-archdefaults.ui" line="985"/>
      <source>mm</source>
      <translation>мм</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="898"/>
      <source>Thickness</source>
      <translation>Товщина</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="970"/>
      <source>Pipes</source>
      <translation>Труби</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="978"/>
      <source>Diameter:</source>
      <translation>Діаметр:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1025"/>
      <source>Helpers (grids, axes, etc...)</source>
      <translation>Помічники (сітки, осі та інше)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1071"/>
      <source>Spaces</source>
      <translation>Області</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1118"/>
      <source>Line style:</source>
      <translation>Стиль лінії:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1135"/>
      <source>Solid</source>
      <translation>Суцільне тіло</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1140"/>
      <source>Dashed</source>
      <translation>Пунктирна</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1145"/>
      <source>Dotted</source>
      <translation>В крапку</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1150"/>
      <source>Dashdot</source>
      <translation>Тире крапка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-archdefaults.ui" line="1158"/>
      <source>Line color</source>
      <translation>Колір лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="14"/>
      <source>IFC import</source>
      <translation>Імпорт IFC</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="41"/>
      <source>Show this dialog when importing</source>
      <translation>Показати це діалогове вікно під час імпорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="54"/>
      <source>Shows verbose debug messages during import and export
of IFC files in the Report view panel</source>
      <translation>Показує детальні повідомлення налагодження під час імпорту та експорту
файлів IFC в панелі Вид Звіту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="58"/>
      <source>Show debug messages</source>
      <translation>Показати повідомлення відлагодження</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="71"/>
      <source>Clones are used when objects have shared geometry
One object is the base object, the others are clones.</source>
      <translation>Клони використовуються, коли обʼєкти мають спільну геометрію
Один обʼєкт це базовий обʼєкт, інші клони.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="75"/>
      <source>Create clones when objects have shared geometry</source>
      <translation>Створити клонів, коли обʼєкти мають спільну геометрію</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="99"/>
      <location filename="../ui/preferences-ifc.ui" line="119"/>
      <source>EXPERIMENTAL
The number of cores to use in multicore mode.
Keep 0 to disable multicore mode.
The maximum value should be your number of cores minus 1,
for example, 3 if you have a 4-core CPU.

Set it to 1 to use multicore mode in single-core mode; this is safer
if you start getting crashes when you set multiple cores.</source>
      <translation>Експериментально!
Кількість ядер для використання в багатоядерному режимі.
Залиште 0, щоб вимкнути багатоядерний режим.
Максимальне значення має бути вашою кількістю ядер мінус 1,
наприклад 3, якщо у вас є 4-ядерний процесор.

Встановіть значення 1, щоб використовувати багатоядерний режим 
в одноядерному режимі; це безпечніше, якщо ви почали отримувати 
аварійні завершення роботи при встановленні декількох ядер.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="129"/>
      <source>Number of cores to use (experimental)</source>
      <translation>Кількість ядер для використання (експериментально)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="157"/>
      <source>Import options</source>
      <translation>Налаштування імпорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="165"/>
      <source>Import arch IFC objects as</source>
      <translation>Імпортувати обʼєкти arch IFC як</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="172"/>
      <location filename="../ui/preferences-ifc.ui" line="221"/>
      <source>Specifies what kind of objects will be created in FreeCAD</source>
      <translation>Вказує які обʼєкти будуть створені в FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="182"/>
      <source>Parametric Arch objects</source>
      <translation>Параметричні обʼєкти Arch</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="187"/>
      <location filename="../ui/preferences-ifc.ui" line="231"/>
      <source>Non-parametric Arch objects</source>
      <translation>Непараметричні обʼєкти Arch</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="192"/>
      <location filename="../ui/preferences-ifc.ui" line="236"/>
      <source>Simple Part shapes</source>
      <translation>Прості форми</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="197"/>
      <source>One compound per floor</source>
      <translation>Одне сполучення на поверх</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="202"/>
      <source>Do not import Arch objects</source>
      <translation>Не імпортувати об’єкти Arch</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="214"/>
      <source>Import struct IFC objects as</source>
      <translation>Імпорт будівельних обʼєктів IFC як</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="241"/>
      <source>One compound for all</source>
      <translation>Одне сполучення для всіх</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="246"/>
      <source>Do not import structural objects</source>
      <translation>Не імпортувати структурні обʼєкти</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="258"/>
      <source>Root element:</source>
      <translation>Кореневий елемент:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="265"/>
      <source>Only subtypes of the specified element will be imported.
Keep the element IfcProduct to import all building elements.</source>
      <translation>Імпортуються лише підтипи зазначеного елементу.
Тримайте елемент IfcProduct для імпорту всіх елементів будівлі.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="284"/>
      <source>Openings will be imported as subtractions, otherwise wall shapes
will already have their openings subtracted</source>
      <translation>Отвори будуть імпортовані як віднімання, 
інакше форми стін вже буде містити отвори</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="288"/>
      <source>Separate openings</source>
      <translation>Окремі отвори</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="301"/>
      <source>The importer will try to detect extrusions.
Note that this might slow things down.</source>
      <translation>Програма Імпорту буде намагатися виявити видавлювання.
Зверніть увагу, що це може уповільнити процес.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="305"/>
      <source>Detect extrusions</source>
      <translation>Виявити видавлювання</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="318"/>
      <source>Split walls made of multiple layers</source>
      <translation>Розділяє стіни з декількох шарів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="321"/>
      <source>Split multilayer walls</source>
      <translation>Розділити багатошарові стіни</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="334"/>
      <source>Object names will be prefixed with the IFC ID number</source>
      <translation>Назви обʼєктів будуть попередньо заповнені номером IFC ID</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="337"/>
      <source>Prefix names with ID number</source>
      <translation>Префікс імен з ідентифікаційним номером</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="350"/>
      <source>If several materials with the same name and color are found in the IFC file,
they will be treated as one.</source>
      <translation>Якщо у файлі IFC знайдено декілька матеріалів з такою ж назвою і кольором,
їх вважатимуть одним.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="354"/>
      <source>Merge materials with same name and same color</source>
      <translation>Обʼєднати матеріали з таким імʼям і тим самим кольором</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="367"/>
      <source>Each object will have their IFC properties stored in a spreadsheet object</source>
      <translation>Кожен обʼєкт матиме свої властивості IFC, які зберігатимуться в простому обʼєкті</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="370"/>
      <source>Import IFC properties in spreadsheet</source>
      <translation>Імпортувати властивості IFC в таблиці</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="383"/>
      <source>IFC files can contain unclean or non-solid geometry. If this option is checked, all the geometry is imported, regardless of their validity.</source>
      <translation>Файли IFC можуть містити нечисті або несуцільні геометрії. Якщо цей параметр позначено, вся геометрія імпортується незалежно від їх дійсності.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="386"/>
      <source>Allow invalid shapes</source>
      <translation>Дозволити неприпустимі фігури</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="401"/>
      <source>Exclude list:</source>
      <translation>Список виключень:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="408"/>
      <source>Comma-separated list of IFC entities to be excluded from imports</source>
      <translation>Список структур IFC, які буде виключено з імпорту, розділений комами</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="429"/>
      <source>Fit view during import on the imported objects.
This will slow down the import, but one can watch the import.</source>
      <translation>Підганяє вид під час імпортування до імпортованих об’єктів.
Це уповільнить імпорт, але за цим можна спостерігати.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="433"/>
      <source>Fit view while importing</source>
      <translation>Підігнати вид під час імпорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="446"/>
      <source>Creates a full parametric model on import using stored
FreeCAD object properties</source>
      <translation>Створює повну параметричну модель при імпорті з використанням збережених
властивостей обʼєкта FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="450"/>
      <source>Import full FreeCAD parametric definitions if available</source>
      <translation>Імпортувати повні параметричні визначення FreeCAD, якщо вони доступні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="463"/>
      <source>If this option is checked, the default &apos;Project&apos;, &apos;Site&apos;, &apos;Building&apos;, and &apos;Storeys&apos;
objects that are usually found in an IFC file are not imported, and all objects
are placed in a &apos;Group&apos; instead.
&apos;Buildings&apos; and &apos;Storeys&apos; are still imported if there is more than one.</source>
      <translation type="unfinished">If this option is checked, the default &apos;Project&apos;, &apos;Site&apos;, &apos;Building&apos;, and &apos;Storeys&apos;
objects that are usually found in an IFC file are not imported, and all objects
are placed in a &apos;Group&apos; instead.
&apos;Buildings&apos; and &apos;Storeys&apos; are still imported if there is more than one.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc.ui" line="469"/>
      <source>Replace &apos;Project&apos;, &apos;Site&apos;, &apos;Building&apos;, and &apos;Storey&apos; with &apos;Group&apos;</source>
      <translation>Замінити &apos;Проєкт&apos;&apos; &apos;Сайт&apos; &apos;Будівля&apos;і &apos;Магазин&apos; &apos;Групою&apos;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="14"/>
      <source>DAE</source>
      <translation type="unfinished">DAE</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="57"/>
      <location filename="../ui/preferences-dae.ui" line="26"/>
      <source>Export options</source>
      <translation>Налаштування експорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="34"/>
      <source>Scaling factor</source>
      <translation>Масштабний коефіцієнт</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="41"/>
      <source>All dimensions in the file will be scaled with this factor</source>
      <translation>Всі розміри у файлі будуть масштабовані за допомогою цього коефіцієнта</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="67"/>
      <source>Mesher</source>
      <translation>Створення сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="74"/>
      <source>Meshing program that should be used.
If using Netgen, make sure that it is available.</source>
      <translation>Вибирає яку програму для створення сітки використати.
Якщо вибрана Netgen, переконайтеся, що вона доступна.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="85"/>
      <source>Builtin</source>
      <translation>Вбудований</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="90"/>
      <source>Mefisto</source>
      <translation>Мефісто</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="95"/>
      <source>Netgen</source>
      <translation type="unfinished">Netgen</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="108"/>
      <source>Builtin and mefisto mesher options</source>
      <translation>Вбудовані і параметри mefisto</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="116"/>
      <source>Tessellation</source>
      <translation>Теселяція</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="123"/>
      <source>Tessellation value to use with the Builtin and the Mefisto meshing program.</source>
      <translation>Зазначені значення Тесселяції для використання в програмі Builtin та Mefisto meshing.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="147"/>
      <source>Netgen mesher options</source>
      <translation>Нетгенні параметри сіток</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="155"/>
      <source>Grading</source>
      <translation>Оцінювання</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="162"/>
      <source>Grading value to use for meshing using Netgen.
This value describes how fast the mesh size decreases.
The gradient of the local mesh size h(x) is bound by |Δh(x)| ≤ 1/value.</source>
      <translation>Класичне значення для сітки за допомогою Netgen.
Це значення описує наскільки швидко зменшиться розмір сітки.
Градієнт локальної сітки h(x) звʼязаний з |÷ h(x)| ≤ 1/value.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="187"/>
      <source>Segments per edge</source>
      <translation>Кількість сегментів на ребро</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="194"/>
      <source>Maximum number of segments per edge</source>
      <translation>Максимальна кількість сегментів за ребро</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="217"/>
      <source>Segments per radius</source>
      <translation>Сегмент на радіус</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="224"/>
      <source>Number of segments per radius</source>
      <translation>Кількість сегментів на радіус</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="247"/>
      <source>Allow a second order mesh</source>
      <translation>Дозволити сітку другого замовлення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="250"/>
      <source>Second order</source>
      <translation>Другий порядок</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="267"/>
      <source>Allows optimization</source>
      <translation>Дозволяє оптимізацію</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="270"/>
      <source>Optimize</source>
      <translation>Оптимізувати</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="290"/>
      <source>Allow quadrilateral faces</source>
      <translation>Дозволити чотиристоронні поверхні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dae.ui" line="293"/>
      <source>Allow quads</source>
      <translation>Дозволити квадрування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="14"/>
      <source>IFC export</source>
      <translation>Експорт у IFC</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="35"/>
      <location filename="../ui/preferences-ifc.ui" line="35"/>
      <source>General options</source>
      <translation>Загальні налаштування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="41"/>
      <source>Show this dialog when exporting</source>
      <translation>Показати це діалогове вікно при експорті</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="65"/>
      <location filename="../ui/preferences-ifc-export.ui" line="78"/>
      <source>The type of objects that you wish to export:
- Standard model: solid objects.
- Structural analysis: wireframe model for structural calculations.
- Standard + structural: both types of models.</source>
      <translation>Тип об’єктів, які ви хочете експортувати:
- Стандартна модель: твердотільні предмети.
- Структурний аналіз: каркасна модель для структурних розрахунків.
- Стандарт + конструкція: обидва типи моделей.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="71"/>
      <source>Export type</source>
      <translation>Тип експорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="91"/>
      <source>Standard model</source>
      <translation>Стандартна модель</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="96"/>
      <source>Structural analysis</source>
      <translation>Структурний аналіз</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="101"/>
      <source>Standard + structural</source>
      <translation>Стандартний + структурний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="111"/>
      <source>Some IFC viewers don&apos;t like objects exported as extrusions.
Use this to force all objects to be exported as BREP geometry.</source>
      <translation type="unfinished">Some IFC viewers don&apos;t like objects exported as extrusions.
Use this to force all objects to be exported as BREP geometry.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="115"/>
      <source>Force export as Brep</source>
      <translation>Примусовий експорт як Brep</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="128"/>
      <source>Use triangulation options set in the DAE options page</source>
      <translation>Використовувати параметри тріангуляції для встановлення на сторінці параметрів ДАЕ</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="131"/>
      <source>Use DAE triangulation options</source>
      <translation>Використовування ДАЕ опцій тріангуляції</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="144"/>
      <source>Curved shapes that cannot be represented as curves in IFC
are decomposed into flat facets.
If this is checked, an additional calculation is done to join coplanar facets.</source>
      <translation>Криві форми, позначені не можуть бути представлені у вигляді кривих у IFC
відокремлені у пласкі поверхні.
Якщо позначено цей пункт, додаткові розрахунки виконуються для приєднання до компланарів.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="149"/>
      <source>Join coplanar facets when triangulating</source>
      <translation>При триангуляції з’єднайте компланарні грані</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="162"/>
      <source>When exporting objects without unique ID (UID), the generated UID
will be stored inside the FreeCAD object for reuse next time that object
is exported. This leads to smaller differences between file versions.</source>
      <translation>При експорті обʼєктів без унікального ID (UID), згенерований UID
буде зберігатися в обʼєкті FreeCAD для повторного використання наступного разу, коли об’єкт
експортується. Це призводить до меншої різниці між версіями файла.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="167"/>
      <source>Store IFC unique ID in FreeCAD objects</source>
      <translation>Зберігати унікальний ідентифікатор IFC у обʼєктах FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="183"/>
      <source>IFCOpenShell is a library that allows to import IFC files.
Its serializer functionality allows to give it an OCC shape and it will
produce adequate IFC geometry: NURBS, faceted, or anything else.
Note: The serializer is still an experimental feature!</source>
      <translation>IFCOpenShell - бібліотека, яка дозволяє імпортувати файли ISS.
Функціональність її серіалізатора дозволяє надати їй форму OCC і вона
створить адекватну геометрію IFC IFC : NURBS, грандіозну чи ще щось.
Примітка: Сервіалізатор все ще експериментальна функція!</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="189"/>
      <source>Use IfcOpenShell serializer if available</source>
      <translation>Використовувати серіалізатор IfcOpenShell якщо доступний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="202"/>
      <source>2D objects will be exported as IfcAnnotation</source>
      <translation>2D обʼєкти буде експортовано як IfcAnnotation</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="205"/>
      <source>Export 2D objects as IfcAnnotations</source>
      <translation>Експортувати 2D об’єкти як IfcAnnotations</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="221"/>
      <source>All FreeCAD object properties will be stored inside the exported objects,
allowing to recreate a full parametric model on reimport.</source>
      <translation>Всі властивості об’єктів FreeCAD будуть зберігатися в експортованих об’єктах,
дозволяючи відтворити повну параметричну модель під час імпорту.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="225"/>
      <source>Export full FreeCAD parametric model</source>
      <translation>Експорт усієї параметричної моделі FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="238"/>
      <source>When possible, similar entities will be used only once in the file if possible.
This can reduce the file size a lot, but will make it less easily readable.</source>
      <translation>Коли це можливо, подібні обʼєкти будуть використані лише один раз у файлі, якщо це можливо.
Це може зменшити розмір файлу, але зробить його менш зручним.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="242"/>
      <source>Reuse similar entities</source>
      <translation>Повторно використовувати подібні обʼєкти</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="258"/>
      <source>When possible, IFC objects that are extruded rectangles will be
exported as IfcRectangleProfileDef.
However, some other applications might have problems importing that entity.
If this is your case, you can disable this and then all profiles will be exported as IfcArbitraryClosedProfileDef.</source>
      <translation>Коли це можливо, об’єкти IFC, виділені прямокутники
експортуватимуться як IfcRectangleProfileDef.
Однак, деякі інші програми можуть виникнути проблеми при імпорті цього обʼєкту.
Якщо це ваш випадок, ви можете відключити це, а потім усі профілі будуть експортовані як IfcArbitraryClosedProfileDef.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="264"/>
      <source>Disable IfcRectangleProfileDef</source>
      <translation>Вимкнути IfcRectangleProfileDef</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="277"/>
      <source>Some IFC types such as IfcWall or IfcBeam have special standard versions
like IfcWallStandardCase or IfcBeamStandardCase.
If this option is turned on, FreeCAD will automatically export such objects
as standard cases when the necessary conditions are met.</source>
      <translation>Деякі типи IFC типу IfcWall або IfcBeam мають спеціальні стандартні версії
, такі як IfcWallStandardCase або IfcBeamStandardCase.
Якщо ця опція активована, FreeCAD буде автоматично експортувати такі обʼєкти
як стандартні випадки при виконанні необхідних умов.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="283"/>
      <source>Auto-detect and export as standard cases when applicable</source>
      <translation>Автовизначення та експорт як стандартні випадки при наявності</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="296"/>
      <source>If no site is found in the FreeCAD document, a default one will be added.
A site is not mandatory but a common practice is to have at least one in the file.</source>
      <translation>Якщо сайт не знайдено у документі FreeCAD, додаватиметься за замовчуванням.
Сайт не є обовʼязковим, але загальна практика - мати принаймні один у файлі.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="300"/>
      <source>Add default site if one is not found in the document</source>
      <translation>Додати сайт за промовчанням, якщо він не знайдений в документі</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="313"/>
      <source>If no building is found in the FreeCAD document, a default one will be added.
Warning: The IFC standard asks for at least one building in each file. By turning this option off, you will produce a non-standard IFC file.
However, at FreeCAD, we believe having a building should not be mandatory, and this option is there to have a chance to demonstrate our point of view.</source>
      <translation>Якщо в документі FreeCAD не знайдено жодної будівлі, буде додано за замовчуванням.
Увага: стандарт IFC запитує принаймні одну будівлю в кожному файлі. Вимкнувши цю опцію, ви створите нестандартний файл IF.
Однак, за допомогою FreeCAD, ми вважаємо, що будівля не повинна бути обовʼязковою, і ця опція для того, щоб показати нашу точку зору.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="318"/>
      <source>Add default building if one is not found in the document (no standard)</source>
      <translation>Додати стандартну будівлю, якщо її не знайдено в документі (немає стандартного)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="334"/>
      <source>If no building storey is found in the FreeCAD document, a default one will be added.
A building storey is not mandatory but a common practice to have at least one in the file.</source>
      <translation>Якщо в документі FreeCAD не знайдено жодного будівельного поверху, буде додано такий за замовчуванням.
Будівельний поверх не є обов’язковою, але звичайною практикою є принаймні один у файлі.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="338"/>
      <source>Add default building storey if one is not found in the document</source>
      <translation>Додати типовий будівельний поверх, якщо його не знайдено в документі</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="353"/>
      <location filename="../ui/preferences-ifc-export.ui" line="368"/>
      <source>The units you want your IFC file to be exported to.

Note that IFC files are ALWAYS written in metric units; imperial units
are only a conversion factor applied on top of them.
However, some BIM applications will use this factor to choose which
unit to work with when opening the file.</source>
      <translation>Основні розділи, до яких ви хочете експортувати ваш ISS-файл.

Зверніть увагу, що IFC файли є ALWAYS написані в метричних одиницях; імперські одиниці
є лише фактором конверсії, застосованою до них згори. Однак
Однак деякі програми BIM використовуватимуть цей фактор, щоб вибрати, з якого
підрозділу працювати при відкритті файлу.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="361"/>
      <source>IFC file units</source>
      <translation>IFC величини файлу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="383"/>
      <source>Metric</source>
      <translation>Метрична</translation>
    </message>
    <message>
      <location filename="../ui/preferences-ifc-export.ui" line="388"/>
      <source>Imperial</source>
      <translation>Англійська (Імперська) система</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../InitGui.py" line="75"/>
      <source>Structure tools</source>
      <translation>Структурні інструменти</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="91"/>
      <source>Axis tools</source>
      <translation>Інструменти осей</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="99"/>
      <source>Panel tools</source>
      <translation>Інструменти панелі</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="109"/>
      <source>Material tools</source>
      <translation>Інструменти матеріалу</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="115"/>
      <source>Pipe tools</source>
      <translation>Інструменти для труб</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="168"/>
      <source>Rebar tools</source>
      <translation>Арматурні інструменти</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="184"/>
      <source>Arch tools</source>
      <translation>Інструменти арки</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="188"/>
      <source>Draft creation tools</source>
      <translation>Інструменти створення ескізу</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="193"/>
      <source>Draft annotation tools</source>
      <translation>Інструменти анотації креслення</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="198"/>
      <source>Draft modification tools</source>
      <translation>Інструменти редагування ескізу</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="206"/>
      <location filename="../../InitGui.py" line="211"/>
      <source>&amp;Arch</source>
      <translation>&amp;Арка</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="207"/>
      <location filename="../../InitGui.py" line="240"/>
      <source>Utilities</source>
      <translation>Утиліти</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="215"/>
      <location filename="../../InitGui.py" line="223"/>
      <location filename="../../InitGui.py" line="231"/>
      <location filename="../../InitGui.py" line="239"/>
      <source>&amp;Draft</source>
      <translation>&amp;Ескіз</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="216"/>
      <source>Creation</source>
      <translation>Створення</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="224"/>
      <source>Annotation</source>
      <translation>Анотація</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="232"/>
      <source>Modification</source>
      <translation>Модифікація</translation>
    </message>
  </context>
</TS>
