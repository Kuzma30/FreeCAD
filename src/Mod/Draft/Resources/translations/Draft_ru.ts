<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
  <context>
    <name>App::Property</name>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="45"/>
      <source>The points of the Bezier curve</source>
      <translation type="unfinished">The points of the Bezier curve</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="48"/>
      <source>The degree of the Bezier function</source>
      <translation type="unfinished">The degree of the Bezier function</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="51"/>
      <source>Continuity</source>
      <translation type="unfinished">Continuity</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="56"/>
      <source>If the Bezier curve should be closed or not</source>
      <translation type="unfinished">If the Bezier curve should be closed or not</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="61"/>
      <source>Create a face if this curve is closed</source>
      <translation type="unfinished">Create a face if this curve is closed</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="64"/>
      <source>The length of this object</source>
      <translation type="unfinished">The length of this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="56"/>
      <location filename="../../draftobjects/circle.py" line="61"/>
      <location filename="../../draftobjects/wire.py" line="91"/>
      <location filename="../../draftobjects/polygon.py" line="69"/>
      <location filename="../../draftobjects/bezcurve.py" line="67"/>
      <location filename="../../draftobjects/rectangle.py" line="73"/>
      <source>The area of this object</source>
      <translation type="unfinished">The area of this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="45"/>
      <source>Start angle of the elliptical arc</source>
      <translation type="unfinished">Start angle of the elliptical arc</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="52"/>
      <source>End angle of the elliptical arc 

                (for a full circle, give it same value as First Angle)</source>
      <translation type="unfinished">End angle of the elliptical arc 

                (for a full circle, give it same value as First Angle)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="55"/>
      <source>Minor radius of the ellipse</source>
      <translation type="unfinished">Minor radius of the ellipse</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="58"/>
      <source>Major radius of the ellipse</source>
      <translation type="unfinished">Major radius of the ellipse</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="61"/>
      <location filename="../../draftobjects/circle.py" line="58"/>
      <location filename="../../draftobjects/polygon.py" line="66"/>
      <location filename="../../draftobjects/rectangle.py" line="60"/>
      <source>Create a face</source>
      <translation type="unfinished">Create a face</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="64"/>
      <source>Area of this object</source>
      <translation type="unfinished">Area of this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/circle.py" line="45"/>
      <source>Start angle of the arc</source>
      <translation type="unfinished">Start angle of the arc</translation>
    </message>
    <message>
      <location filename="../../draftobjects/circle.py" line="52"/>
      <source>End angle of the arc (for a full circle, 
                give it same value as First Angle)</source>
      <translation type="unfinished">End angle of the arc (for a full circle, 
                give it same value as First Angle)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/circle.py" line="55"/>
      <source>Radius of the circle</source>
      <translation type="unfinished">Radius of the circle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="44"/>
      <source>Linked faces</source>
      <translation type="unfinished">Linked faces</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="49"/>
      <source>Specifies if splitter lines must be removed</source>
      <translation type="unfinished">Specifies if splitter lines must be removed</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="54"/>
      <source>An optional extrusion value to be applied to all faces</source>
      <translation type="unfinished">An optional extrusion value to be applied to all faces</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="59"/>
      <source>An optional offset value to be applied to all faces</source>
      <translation type="unfinished">An optional offset value to be applied to all faces</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="62"/>
      <source>This specifies if the shapes sew</source>
      <translation type="unfinished">This specifies if the shapes sew</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="67"/>
      <source>The area of the faces of this Facebinder</source>
      <translation type="unfinished">The area of the faces of this Facebinder</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="83"/>
      <location filename="../../draftobjects/patharray.py" line="176"/>
      <source>The base object that will be duplicated</source>
      <translation type="unfinished">The base object that will be duplicated</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="101"/>
      <source>The type of array to create.
- Ortho: places the copies in the direction of the global X, Y, Z axes.
- Polar: places the copies along a circular arc, up to a specified angle, and with certain orientation defined by a center and an axis.
- Circular: places the copies in concentric circular layers around the base object.</source>
      <translation type="unfinished">The type of array to create.
- Ortho: places the copies in the direction of the global X, Y, Z axes.
- Polar: places the copies along a circular arc, up to a specified angle, and with certain orientation defined by a center and an axis.
- Circular: places the copies in concentric circular layers around the base object.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="111"/>
      <source>Specifies if the copies should be fused together if they touch each other (slower)</source>
      <translation type="unfinished">Specifies if the copies should be fused together if they touch each other (slower)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="120"/>
      <source>Number of copies in X direction</source>
      <translation type="unfinished">Number of copies in X direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="125"/>
      <source>Number of copies in Y direction</source>
      <translation type="unfinished">Number of copies in Y direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="130"/>
      <source>Number of copies in Z direction</source>
      <translation type="unfinished">Number of copies in Z direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="138"/>
      <source>Distance and orientation of intervals in X direction</source>
      <translation type="unfinished">Distance and orientation of intervals in X direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="148"/>
      <source>Distance and orientation of intervals in Y direction</source>
      <translation type="unfinished">Distance and orientation of intervals in Y direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="158"/>
      <source>Distance and orientation of intervals in Z direction</source>
      <translation type="unfinished">Distance and orientation of intervals in Z direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="174"/>
      <source>The axis direction around which the elements in a polar or a circular array will be created</source>
      <translation type="unfinished">The axis direction around which the elements in a polar or a circular array will be created</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="184"/>
      <source>Center point for polar and circular arrays.
The &apos;Axis&apos; passes through this point.</source>
      <translation type="unfinished">Center point for polar and circular arrays.
The &apos;Axis&apos; passes through this point.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="204"/>
      <source>The axis object that overrides the value of &apos;Axis&apos; and &apos;Center&apos;, for example, a datum line.
Its placement, position and rotation, will be used when creating polar and circular arrays.
Leave this property empty to be able to set &apos;Axis&apos; and &apos;Center&apos; manually.</source>
      <translation type="unfinished">The axis object that overrides the value of &apos;Axis&apos; and &apos;Center&apos;, for example, a datum line.
Its placement, position and rotation, will be used when creating polar and circular arrays.
Leave this property empty to be able to set &apos;Axis&apos; and &apos;Center&apos; manually.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="215"/>
      <source>Number of copies in the polar direction</source>
      <translation type="unfinished">Number of copies in the polar direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="223"/>
      <source>Distance and orientation of intervals in &apos;Axis&apos; direction</source>
      <translation type="unfinished">Distance and orientation of intervals in &apos;Axis&apos; direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="230"/>
      <source>Angle to cover with copies</source>
      <translation type="unfinished">Angle to cover with copies</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="241"/>
      <source>Distance between circular layers</source>
      <translation type="unfinished">Distance between circular layers</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="250"/>
      <source>Distance between copies in the same circular layer</source>
      <translation type="unfinished">Distance between copies in the same circular layer</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="260"/>
      <source>Number of circular layers. The &apos;Base&apos; object counts as one layer.</source>
      <translation type="unfinished">Number of circular layers. The &apos;Base&apos; object counts as one layer.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="272"/>
      <source>A parameter that determines how many symmetry planes the circular array will have.</source>
      <translation type="unfinished">A parameter that determines how many symmetry planes the circular array will have.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="289"/>
      <source>Total number of elements in the array.
This property is read-only, as the number depends on the parameters of the array.</source>
      <translation type="unfinished">Total number of elements in the array.
This property is read-only, as the number depends on the parameters of the array.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="298"/>
      <location filename="../../draftobjects/pointarray.py" line="114"/>
      <location filename="../../draftobjects/pathtwistedarray.py" line="138"/>
      <location filename="../../draftobjects/patharray.py" line="214"/>
      <source>Show the individual array elements (only for Link arrays)</source>
      <translation type="unfinished">Show the individual array elements (only for Link arrays)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="45"/>
      <source>The points of the B-spline</source>
      <translation type="unfinished">The points of the B-spline</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="48"/>
      <source>If the B-spline is closed or not</source>
      <translation type="unfinished">If the B-spline is closed or not</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="53"/>
      <source>Create a face if this spline is closed</source>
      <translation type="unfinished">Create a face if this spline is closed</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="66"/>
      <source>Parameterization factor</source>
      <translation type="unfinished">Parameterization factor</translation>
    </message>
    <message>
      <location filename="../../draftobjects/point.py" line="45"/>
      <source>X Location</source>
      <translation type="unfinished">X Location</translation>
    </message>
    <message>
      <location filename="../../draftobjects/point.py" line="48"/>
      <source>Y Location</source>
      <translation type="unfinished">Y Location</translation>
    </message>
    <message>
      <location filename="../../draftobjects/point.py" line="51"/>
      <source>Z Location</source>
      <translation type="unfinished">Z Location</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draftlink.py" line="101"/>
      <source>Force sync pattern placements even when array elements are expanded</source>
      <translation type="unfinished">Force sync pattern placements even when array elements are expanded</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draftlink.py" line="112"/>
      <source>Show the individual array elements</source>
      <translation type="unfinished">Show the individual array elements</translation>
    </message>
    <message>
      <location filename="../../draftobjects/clone.py" line="45"/>
      <source>The objects included in this clone</source>
      <translation type="unfinished">The objects included in this clone</translation>
    </message>
    <message>
      <location filename="../../draftobjects/clone.py" line="48"/>
      <source>The scale factor of this clone</source>
      <translation type="unfinished">The scale factor of this clone</translation>
    </message>
    <message>
      <location filename="../../draftobjects/clone.py" line="55"/>
      <source>If Clones includes several objects,
set True for fusion or False for compound</source>
      <translation type="unfinished">If Clones includes several objects,
set True for fusion or False for compound</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="46"/>
      <source>Text string</source>
      <translation type="unfinished">Text string</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="49"/>
      <source>Font file name</source>
      <translation type="unfinished">Font file name</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="52"/>
      <source>Height of text</source>
      <translation type="unfinished">Height of text</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="55"/>
      <source>Inter-character spacing</source>
      <translation type="unfinished">Inter-character spacing</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="58"/>
      <source>Fill letters with faces</source>
      <translation type="unfinished">Fill letters with faces</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="48"/>
      <source>The vertices of the wire</source>
      <translation type="unfinished">The vertices of the wire</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="51"/>
      <source>If the wire is closed or not</source>
      <translation type="unfinished">If the wire is closed or not</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="56"/>
      <source>The base object is the wire, it&apos;s formed from 2 objects</source>
      <translation type="unfinished">The base object is the wire, it&apos;s formed from 2 objects</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="61"/>
      <source>The tool object is the wire, it&apos;s formed from 2 objects</source>
      <translation type="unfinished">The tool object is the wire, it&apos;s formed from 2 objects</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="64"/>
      <source>The start point of this line</source>
      <translation type="unfinished">The start point of this line</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="67"/>
      <source>The end point of this line</source>
      <translation type="unfinished">The end point of this line</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="70"/>
      <source>The length of this line</source>
      <translation type="unfinished">The length of this line</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="73"/>
      <location filename="../../draftobjects/polygon.py" line="58"/>
      <location filename="../../draftobjects/rectangle.py" line="52"/>
      <source>Radius to use to fillet the corners</source>
      <translation type="unfinished">Radius to use to fillet the corners</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="78"/>
      <location filename="../../draftobjects/polygon.py" line="63"/>
      <location filename="../../draftobjects/rectangle.py" line="57"/>
      <source>Size of the chamfer to give to the corners</source>
      <translation type="unfinished">Size of the chamfer to give to the corners</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="83"/>
      <source>Create a face if this object is closed</source>
      <translation type="unfinished">Create a face if this object is closed</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="88"/>
      <source>The number of subdivisions of each edge</source>
      <translation type="unfinished">The number of subdivisions of each edge</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="58"/>
      <source>The base object this 2D view must represent</source>
      <translation type="unfinished">The base object this 2D view must represent</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="63"/>
      <source>The projection vector of this object</source>
      <translation type="unfinished">The projection vector of this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="69"/>
      <source>The way the viewed object must be projected</source>
      <translation type="unfinished">The way the viewed object must be projected</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="82"/>
      <source>The indices of the faces to be projected in Individual Faces mode</source>
      <translation type="unfinished">The indices of the faces to be projected in Individual Faces mode</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="85"/>
      <source>Show hidden lines</source>
      <translation type="unfinished">Show hidden lines</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="92"/>
      <source>Fuse wall and structure objects of same type and material</source>
      <translation type="unfinished">Fuse wall and structure objects of same type and material</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="97"/>
      <source>Tessellate Ellipses and B-splines into line segments</source>
      <translation type="unfinished">Tessellate Ellipses and B-splines into line segments</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="105"/>
      <source>For Cutlines and Cutfaces modes, 
                    this leaves the faces at the cut location</source>
      <translation type="unfinished">For Cutlines and Cutfaces modes, 
                    this leaves the faces at the cut location</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="113"/>
      <source>Length of line segments if tessellating Ellipses or B-splines 
                    into line segments</source>
      <translation type="unfinished">Length of line segments if tessellating Ellipses or B-splines 
                    into line segments</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="120"/>
      <source>If this is True, this object will include only visible objects</source>
      <translation type="unfinished">If this is True, this object will include only visible objects</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="127"/>
      <source>A list of exclusion points. Any edge touching any of those points will not be drawn.</source>
      <translation type="unfinished">A list of exclusion points. Any edge touching any of those points will not be drawn.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="133"/>
      <source>If this is True, only solid geometry is handled. This overrides the base object&apos;s Only Solids property</source>
      <translation type="unfinished">If this is True, only solid geometry is handled. This overrides the base object&apos;s Only Solids property</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="139"/>
      <source>If this is True, the contents are clipped to the borders of the section plane, if applicable. This overrides the base object&apos;s Clip property</source>
      <translation type="unfinished">If this is True, the contents are clipped to the borders of the section plane, if applicable. This overrides the base object&apos;s Clip property</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="144"/>
      <source>This object will be recomputed only if this is True.</source>
      <translation type="unfinished">This object will be recomputed only if this is True.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="46"/>
      <source>Length of the rectangle</source>
      <translation type="unfinished">Length of the rectangle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="49"/>
      <source>Height of the rectangle</source>
      <translation type="unfinished">Height of the rectangle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="65"/>
      <source>Horizontal subdivisions of this rectangle</source>
      <translation type="unfinished">Horizontal subdivisions of this rectangle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="70"/>
      <source>Vertical subdivisions of this rectangle</source>
      <translation type="unfinished">Vertical subdivisions of this rectangle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="65"/>
      <source>The linked object</source>
      <translation type="unfinished">The linked object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="68"/>
      <source>Projection direction</source>
      <translation type="unfinished">Projection direction</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="73"/>
      <source>The width of the lines inside this object</source>
      <translation type="unfinished">The width of the lines inside this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="79"/>
      <source>The size of the texts inside this object</source>
      <translation type="unfinished">The size of the texts inside this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="83"/>
      <source>The spacing between lines of text</source>
      <translation type="unfinished">The spacing between lines of text</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="86"/>
      <source>The color of the projected objects</source>
      <translation type="unfinished">The color of the projected objects</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="89"/>
      <source>Shape Fill Style</source>
      <translation type="unfinished">Shape Fill Style</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="93"/>
      <source>Line Style</source>
      <translation type="unfinished">Line Style</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="101"/>
      <source>If checked, source objects are displayed regardless of being visible in the 3D model</source>
      <translation type="unfinished">If checked, source objects are displayed regardless of being visible in the 3D model</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="47"/>
      <source>The start point of this line.</source>
      <translation type="unfinished">The start point of this line.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="52"/>
      <source>The end point of this line.</source>
      <translation type="unfinished">The end point of this line.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="57"/>
      <source>The length of this line.</source>
      <translation type="unfinished">The length of this line.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="64"/>
      <source>Radius to use to fillet the corner.</source>
      <translation type="unfinished">Radius to use to fillet the corner.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="81"/>
      <source>Base object that will be duplicated</source>
      <translation type="unfinished">Base object that will be duplicated</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="89"/>
      <source>Object containing points used to distribute the base object, for example, a sketch or a Part compound.
The sketch or compound must contain at least one explicit point or vertex object.</source>
      <translation type="unfinished">Object containing points used to distribute the base object, for example, a sketch or a Part compound.
The sketch or compound must contain at least one explicit point or vertex object.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="97"/>
      <source>Total number of elements in the array.
This property is read-only, as the number depends on the points contained within &apos;Point Object&apos;.</source>
      <translation type="unfinished">Total number of elements in the array.
This property is read-only, as the number depends on the points contained within &apos;Point Object&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="106"/>
      <location filename="../../draftobjects/pointarray.py" line="142"/>
      <source>Additional placement, shift and rotation, that will be applied to each copy</source>
      <translation type="unfinished">Additional placement, shift and rotation, that will be applied to each copy</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="137"/>
      <source>The normal direction of the text of the dimension</source>
      <translation type="unfinished">The normal direction of the text of the dimension</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="147"/>
      <source>The object measured by this dimension object</source>
      <translation type="unfinished">The object measured by this dimension object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="163"/>
      <source>The object, and specific subelements of it,
that this dimension object is measuring.

There are various possibilities:
- An object, and one of its edges.
- An object, and two of its vertices.
- An arc object, and its edge.</source>
      <translation type="unfinished">The object, and specific subelements of it,
that this dimension object is measuring.

There are various possibilities:
- An object, and one of its edges.
- An object, and two of its vertices.
- An arc object, and its edge.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="187"/>
      <source>A point through which the dimension line, or an extrapolation of it, will pass.

- For linear dimensions, this property controls how close the dimension line
is to the measured object.
- For radial dimensions, this controls the direction of the dimension line
that displays the measured radius or diameter.
- For angular dimensions, this controls the radius of the dimension arc
that displays the measured angle.</source>
      <translation type="unfinished">A point through which the dimension line, or an extrapolation of it, will pass.

- For linear dimensions, this property controls how close the dimension line
is to the measured object.
- For radial dimensions, this controls the direction of the dimension line
that displays the measured radius or diameter.
- For angular dimensions, this controls the radius of the dimension arc
that displays the measured angle.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="229"/>
      <source>Starting point of the dimension line.

If it is a radius dimension it will be the center of the arc.
If it is a diameter dimension it will be a point that lies on the arc.</source>
      <translation type="unfinished">Starting point of the dimension line.

If it is a radius dimension it will be the center of the arc.
If it is a diameter dimension it will be a point that lies on the arc.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="244"/>
      <source>Ending point of the dimension line.

If it is a radius or diameter dimension
it will be a point that lies on the arc.</source>
      <translation type="unfinished">Ending point of the dimension line.

If it is a radius or diameter dimension
it will be a point that lies on the arc.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="257"/>
      <source>The direction of the dimension line.
If this remains &apos;(0,0,0)&apos;, the direction will be calculated automatically.</source>
      <translation type="unfinished">The direction of the dimension line.
If this remains &apos;(0,0,0)&apos;, the direction will be calculated automatically.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="275"/>
      <source>The value of the measurement.

This property is read-only because the value is calculated
from the &apos;Start&apos; and &apos;End&apos; properties.

If the &apos;Linked Geometry&apos; is an arc or circle, this &apos;Distance&apos;
is the radius or diameter, depending on the &apos;Diameter&apos; property.</source>
      <translation type="unfinished">The value of the measurement.

This property is read-only because the value is calculated
from the &apos;Start&apos; and &apos;End&apos; properties.

If the &apos;Linked Geometry&apos; is an arc or circle, this &apos;Distance&apos;
is the radius or diameter, depending on the &apos;Diameter&apos; property.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="287"/>
      <source>When measuring circular arcs, it determines whether to display
the radius or the diameter value</source>
      <translation type="unfinished">When measuring circular arcs, it determines whether to display
the radius or the diameter value</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="504"/>
      <source>Starting angle of the dimension line (circular arc).
The arc is drawn counter-clockwise.</source>
      <translation type="unfinished">Starting angle of the dimension line (circular arc).
The arc is drawn counter-clockwise.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="516"/>
      <source>Ending angle of the dimension line (circular arc).
The arc is drawn counter-clockwise.</source>
      <translation type="unfinished">Ending angle of the dimension line (circular arc).
The arc is drawn counter-clockwise.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="532"/>
      <source>The center point of the dimension line, which is a circular arc.

This is normally the point where two line segments, or their extensions
intersect, resulting in the measured &apos;Angle&apos; between them.</source>
      <translation type="unfinished">The center point of the dimension line, which is a circular arc.

This is normally the point where two line segments, or their extensions
intersect, resulting in the measured &apos;Angle&apos; between them.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="547"/>
      <source>The value of the measurement.

This property is read-only because the value is calculated from
the &apos;First Angle&apos; and &apos;Last Angle&apos; properties.</source>
      <translation type="unfinished">The value of the measurement.

This property is read-only because the value is calculated from
the &apos;First Angle&apos; and &apos;Last Angle&apos; properties.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="67"/>
      <source>The position of the tip of the leader line.
This point can be decorated with an arrow or another symbol.</source>
      <translation type="unfinished">The position of the tip of the leader line.
This point can be decorated with an arrow or another symbol.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="80"/>
      <source>Object, and optionally subelement, whose properties will be displayed
as &apos;Text&apos;, depending on &apos;Label Type&apos;.

&apos;Target&apos; won&apos;t be used if &apos;Label Type&apos; is set to &apos;Custom&apos;.</source>
      <translation type="unfinished">Object, and optionally subelement, whose properties will be displayed
as &apos;Text&apos;, depending on &apos;Label Type&apos;.

&apos;Target&apos; won&apos;t be used if &apos;Label Type&apos; is set to &apos;Custom&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="109"/>
      <source>The list of points defining the leader line; normally a list of three points.

The first point should be the position of the text, that is, the &apos;Placement&apos;,
and the last point should be the tip of the line, that is, the &apos;Target Point&apos;.
The middle point is calculated automatically depending on the chosen
&apos;Straight Direction&apos; and the &apos;Straight Distance&apos; value and sign.

If &apos;Straight Direction&apos; is set to &apos;Custom&apos;, the &apos;Points&apos; property
can be set as a list of arbitrary points.</source>
      <translation type="unfinished">The list of points defining the leader line; normally a list of three points.

The first point should be the position of the text, that is, the &apos;Placement&apos;,
and the last point should be the tip of the line, that is, the &apos;Target Point&apos;.
The middle point is calculated automatically depending on the chosen
&apos;Straight Direction&apos; and the &apos;Straight Distance&apos; value and sign.

If &apos;Straight Direction&apos; is set to &apos;Custom&apos;, the &apos;Points&apos; property
can be set as a list of arbitrary points.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="123"/>
      <source>The direction of the straight segment of the leader line.

If &apos;Custom&apos; is chosen, the points of the leader can be specified by
assigning a custom list to the &apos;Points&apos; attribute.</source>
      <translation type="unfinished">The direction of the straight segment of the leader line.

If &apos;Custom&apos; is chosen, the points of the leader can be specified by
assigning a custom list to the &apos;Points&apos; attribute.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="142"/>
      <source>The length of the straight segment of the leader line.

This is an oriented distance; if it is negative, the line will be drawn
to the left or below the &apos;Text&apos;, otherwise to the right or above it,
depending on the value of &apos;Straight Direction&apos;.</source>
      <translation type="unfinished">The length of the straight segment of the leader line.

This is an oriented distance; if it is negative, the line will be drawn
to the left or below the &apos;Text&apos;, otherwise to the right or above it,
depending on the value of &apos;Straight Direction&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="153"/>
      <source>The placement of the &apos;Text&apos; element in 3D space</source>
      <translation type="unfinished">The placement of the &apos;Text&apos; element in 3D space</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="161"/>
      <source>The text to display when &apos;Label Type&apos; is set to &apos;Custom&apos;</source>
      <translation type="unfinished">The text to display when &apos;Label Type&apos; is set to &apos;Custom&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="175"/>
      <source>The text displayed by this label.

This property is read-only, as the final text depends on &apos;Label Type&apos;,
and the object defined in &apos;Target&apos;.
The &apos;Custom Text&apos; is displayed only if &apos;Label Type&apos; is set to &apos;Custom&apos;.</source>
      <translation type="unfinished">The text displayed by this label.

This property is read-only, as the final text depends on &apos;Label Type&apos;,
and the object defined in &apos;Target&apos;.
The &apos;Custom Text&apos; is displayed only if &apos;Label Type&apos; is set to &apos;Custom&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="209"/>
      <source>The type of information displayed by this label.

If &apos;Custom&apos; is chosen, the contents of &apos;Custom Text&apos; will be used.
For other types, the string will be calculated automatically from the object defined in &apos;Target&apos;.
&apos;Tag&apos; and &apos;Material&apos; only work for objects that have these properties, like Arch objects.

For &apos;Position&apos;, &apos;Length&apos;, and &apos;Area&apos; these properties will be extracted from the main object in &apos;Target&apos;,
or from the subelement &apos;VertexN&apos;, &apos;EdgeN&apos;, or &apos;FaceN&apos;, respectively, if it is specified.</source>
      <translation type="unfinished">The type of information displayed by this label.

If &apos;Custom&apos; is chosen, the contents of &apos;Custom Text&apos; will be used.
For other types, the string will be calculated automatically from the object defined in &apos;Target&apos;.
&apos;Tag&apos; and &apos;Material&apos; only work for objects that have these properties, like Arch objects.

For &apos;Position&apos;, &apos;Length&apos;, and &apos;Area&apos; these properties will be extracted from the main object in &apos;Target&apos;,
or from the subelement &apos;VertexN&apos;, &apos;EdgeN&apos;, or &apos;FaceN&apos;, respectively, if it is specified.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="94"/>
      <source>The base object that will be duplicated.</source>
      <translation type="unfinished">The base object that will be duplicated.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="106"/>
      <location filename="../../draftobjects/patharray.py" line="184"/>
      <source>The object along which the copies will be distributed. It must contain &apos;Edges&apos;.</source>
      <translation type="unfinished">The object along which the copies will be distributed. It must contain &apos;Edges&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="115"/>
      <source>Number of copies to create.</source>
      <translation type="unfinished">Number of copies to create.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="126"/>
      <source>Rotation factor of the twisted array.</source>
      <translation type="unfinished">Rotation factor of the twisted array.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/polygon.py" line="47"/>
      <source>Number of faces</source>
      <translation type="unfinished">Number of faces</translation>
    </message>
    <message>
      <location filename="../../draftobjects/polygon.py" line="50"/>
      <source>Radius of the control circle</source>
      <translation type="unfinished">Radius of the control circle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/polygon.py" line="55"/>
      <source>How the polygon must be drawn from the control circle</source>
      <translation type="unfinished">How the polygon must be drawn from the control circle</translation>
    </message>
    <message>
      <location filename="../../draftobjects/block.py" line="42"/>
      <source>The components of this block</source>
      <translation type="unfinished">The components of this block</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="48"/>
      <source>The base object used by this object</source>
      <translation type="unfinished">The base object used by this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="55"/>
      <source>The PAT file used by this object</source>
      <translation type="unfinished">The PAT file used by this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="64"/>
      <source>The pattern name used by this object</source>
      <translation type="unfinished">The pattern name used by this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="73"/>
      <source>The pattern scale used by this object</source>
      <translation type="unfinished">The pattern scale used by this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="82"/>
      <source>The pattern rotation used by this object</source>
      <translation type="unfinished">The pattern rotation used by this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="92"/>
      <source>If set to False, hatch is applied as is to the faces, without translation (this might give wrong results for non-XY faces)</source>
      <translation type="unfinished">If set to False, hatch is applied as is to the faces, without translation (this might give wrong results for non-XY faces)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/text.py" line="54"/>
      <source>The placement of the base point of the first line</source>
      <translation type="unfinished">The placement of the base point of the first line</translation>
    </message>
    <message>
      <location filename="../../draftobjects/text.py" line="65"/>
      <source>The text displayed by this object.
It is a list of strings; each element in the list will be displayed in its own line.</source>
      <translation type="unfinished">The text displayed by this object.
It is a list of strings; each element in the list will be displayed in its own line.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="199"/>
      <source>List of connected edges in the &apos;Path Object&apos;.
If these are present, the copies will be created along these subelements only.
Leave this property empty to create copies along the entire &apos;Path Object&apos;.</source>
      <translation type="unfinished">List of connected edges in the &apos;Path Object&apos;.
If these are present, the copies will be created along these subelements only.
Leave this property empty to create copies along the entire &apos;Path Object&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="206"/>
      <source>Number of copies to create</source>
      <translation type="unfinished">Number of copies to create</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="225"/>
      <source>Additional translation that will be applied to each copy.
This is useful to adjust for the difference between shape centre and shape reference point.</source>
      <translation type="unfinished">Additional translation that will be applied to each copy.
This is useful to adjust for the difference between shape centre and shape reference point.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="234"/>
      <source>Alignment vector for &apos;Tangent&apos; mode</source>
      <translation type="unfinished">Alignment vector for &apos;Tangent&apos; mode</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="242"/>
      <source>Force use of &apos;Vertical Vector&apos; as local Z direction when using &apos;Original&apos; or &apos;Tangent&apos; alignment mode</source>
      <translation type="unfinished">Force use of &apos;Vertical Vector&apos; as local Z direction when using &apos;Original&apos; or &apos;Tangent&apos; alignment mode</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="250"/>
      <source>Direction of the local Z axis when &apos;Force Vertical&apos; is true</source>
      <translation type="unfinished">Direction of the local Z axis when &apos;Force Vertical&apos; is true</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="258"/>
      <source>Method to orient the copies along the path.
- Original: X is curve tangent, Y is normal, and Z is the cross product.
- Frenet: aligns the object following the local coordinate system along the path.
- Tangent: similar to &apos;Original&apos; but the local X axis is pre-aligned to &apos;Tangent Vector&apos;.

To get better results with &apos;Original&apos; or &apos;Tangent&apos; you may have to set &apos;Force Vertical&apos; to true.</source>
      <translation type="unfinished">Method to orient the copies along the path.
- Original: X is curve tangent, Y is normal, and Z is the cross product.
- Frenet: aligns the object following the local coordinate system along the path.
- Tangent: similar to &apos;Original&apos; but the local X axis is pre-aligned to &apos;Tangent Vector&apos;.

To get better results with &apos;Original&apos; or &apos;Tangent&apos; you may have to set &apos;Force Vertical&apos; to true.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="269"/>
      <source>Orient the copies along the path depending on the &apos;Align Mode&apos;.
Otherwise the copies will have the same orientation as the original Base object.</source>
      <translation type="unfinished">Orient the copies along the path depending on the &apos;Align Mode&apos;.
Otherwise the copies will have the same orientation as the original Base object.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wpproxy.py" line="42"/>
      <source>The placement of this object</source>
      <translation type="unfinished">The placement of this object</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="85"/>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="84"/>
      <source>General scaling factor that affects the annotation consistently
because it scales the text, and the line decorations, if any,
in the same proportion.</source>
      <translation type="unfinished">General scaling factor that affects the annotation consistently
because it scales the text, and the line decorations, if any,
in the same proportion.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="102"/>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="103"/>
      <source>Annotation style to apply to this object.
When using a saved style some of the view properties will become read-only;
they will only be editable by changing the style through the &apos;Annotation style editor&apos; tool.</source>
      <translation type="unfinished">Annotation style to apply to this object.
When using a saved style some of the view properties will become read-only;
they will only be editable by changing the style through the &apos;Annotation style editor&apos; tool.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/layer.py" line="60"/>
      <source>The objects that are part of this layer</source>
      <translation type="unfinished">The objects that are part of this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_base.py" line="107"/>
      <source>Defines an SVG pattern.</source>
      <translation type="unfinished">Defines an SVG pattern.</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_base.py" line="120"/>
      <source>Defines the size of the SVG pattern.</source>
      <translation type="unfinished">Defines the size of the SVG pattern.</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="117"/>
      <location filename="../../draftviewproviders/view_label.py" line="140"/>
      <source>Line width</source>
      <translation type="unfinished">Line width</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="121"/>
      <location filename="../../draftviewproviders/view_label.py" line="145"/>
      <source>Line color</source>
      <translation type="unfinished">Line color</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="69"/>
      <source>If it is true, the objects contained within this layer will adopt the line color of the layer</source>
      <translation type="unfinished">If it is true, the objects contained within this layer will adopt the line color of the layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="81"/>
      <source>If it is true, the objects contained within this layer will adopt the shape color of the layer</source>
      <translation type="unfinished">If it is true, the objects contained within this layer will adopt the shape color of the layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="93"/>
      <source>If it is true, the print color will be used when objects in this layer are placed on a TechDraw page</source>
      <translation type="unfinished">If it is true, the print color will be used when objects in this layer are placed on a TechDraw page</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="104"/>
      <source>The line color of the objects contained within this layer</source>
      <translation type="unfinished">The line color of the objects contained within this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="118"/>
      <source>The shape color of the objects contained within this layer</source>
      <translation type="unfinished">The shape color of the objects contained within this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="132"/>
      <source>The line width of the objects contained within this layer</source>
      <translation type="unfinished">The line width of the objects contained within this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="142"/>
      <source>The draw style of the objects contained within this layer</source>
      <translation type="unfinished">The draw style of the objects contained within this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="151"/>
      <source>The transparency of the objects contained within this layer</source>
      <translation type="unfinished">The transparency of the objects contained within this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="161"/>
      <source>The line color of the objects contained within this layer, when used on a TechDraw page</source>
      <translation type="unfinished">The line color of the objects contained within this layer, when used on a TechDraw page</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="58"/>
      <location filename="../../draftviewproviders/view_label.py" line="72"/>
      <source>The size of the text</source>
      <translation type="unfinished">The size of the text</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="63"/>
      <location filename="../../draftviewproviders/view_label.py" line="77"/>
      <source>The font of the text</source>
      <translation type="unfinished">The font of the text</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="70"/>
      <location filename="../../draftviewproviders/view_label.py" line="84"/>
      <location filename="../../draftviewproviders/view_label.py" line="103"/>
      <source>The vertical alignment of the text</source>
      <translation type="unfinished">The vertical alignment of the text</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="75"/>
      <location filename="../../draftviewproviders/view_label.py" line="90"/>
      <source>Text color</source>
      <translation type="unfinished">Text color</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="97"/>
      <source>The maximum number of characters on each line of the text box</source>
      <translation type="unfinished">The maximum number of characters on each line of the text box</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="81"/>
      <location filename="../../draftviewproviders/view_label.py" line="110"/>
      <source>Line spacing (relative to font size)</source>
      <translation type="unfinished">Line spacing (relative to font size)</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="117"/>
      <source>The size of the arrow</source>
      <translation type="unfinished">The size of the arrow</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="122"/>
      <source>The type of arrow of this label</source>
      <translation type="unfinished">The type of arrow of this label</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="130"/>
      <source>The type of frame around the text of this object</source>
      <translation type="unfinished">The type of frame around the text of this object</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="135"/>
      <source>Display a leader line or not</source>
      <translation type="unfinished">Display a leader line or not</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="135"/>
      <source>Font name</source>
      <translation type="unfinished">Font name</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="140"/>
      <source>Font size</source>
      <translation type="unfinished">Font size</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="147"/>
      <source>Spacing between text and dimension line</source>
      <translation type="unfinished">Spacing between text and dimension line</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="154"/>
      <source>Rotate the dimension text 180 degrees</source>
      <translation type="unfinished">Rotate the dimension text 180 degrees</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="162"/>
      <source>Text Position.
Leave &apos;(0,0,0)&apos; for automatic position</source>
      <translation type="unfinished">Text Position.
Leave &apos;(0,0,0)&apos; for automatic position</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="174"/>
      <source>Text override.
Write &apos;$dim&apos; so that it is replaced by the dimension length.</source>
      <translation type="unfinished">Text override.
Write &apos;$dim&apos; so that it is replaced by the dimension length.</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="181"/>
      <source>The number of decimals to show</source>
      <translation type="unfinished">The number of decimals to show</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="186"/>
      <source>Show the unit suffix</source>
      <translation type="unfinished">Show the unit suffix</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="196"/>
      <source>A unit to express the measurement.
Leave blank for system default.
Use &apos;arch&apos; to force US arch notation</source>
      <translation type="unfinished">A unit to express the measurement.
Leave blank for system default.
Use &apos;arch&apos; to force US arch notation</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="205"/>
      <source>Arrow size</source>
      <translation type="unfinished">Arrow size</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="210"/>
      <source>Arrow type</source>
      <translation type="unfinished">Arrow type</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="218"/>
      <source>Rotate the dimension arrows 180 degrees</source>
      <translation type="unfinished">Rotate the dimension arrows 180 degrees</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="228"/>
      <source>The distance the dimension line is extended
past the extension lines</source>
      <translation type="unfinished">The distance the dimension line is extended
past the extension lines</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="233"/>
      <source>Length of the extension lines</source>
      <translation type="unfinished">Length of the extension lines</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="241"/>
      <source>Length of the extension line
beyond the dimension line</source>
      <translation type="unfinished">Length of the extension line
beyond the dimension line</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="248"/>
      <source>Shows the dimension line and arrows</source>
      <translation type="unfinished">Shows the dimension line and arrows</translation>
    </message>
  </context>
  <context>
    <name>Dialog</name>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="14"/>
      <source>Annotation Styles Editor</source>
      <translation type="unfinished">Annotation Styles Editor</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="20"/>
      <source>Style name</source>
      <translation type="unfinished">Style name</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="38"/>
      <source>The name of your style. Existing style names can be edited.</source>
      <translation type="unfinished">The name of your style. Existing style names can be edited.</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="50"/>
      <source>Add new...</source>
      <translation type="unfinished">Add new...</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="79"/>
      <source>Renames the selected style</source>
      <translation type="unfinished">Renames the selected style</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="82"/>
      <source>Rename</source>
      <translation type="unfinished">Rename</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="110"/>
      <source>Deletes the selected style</source>
      <translation type="unfinished">Deletes the selected style</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="113"/>
      <source>Delete</source>
      <translation type="unfinished">Delete</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="120"/>
      <source>Import styles from json file</source>
      <translation type="unfinished">Import styles from json file</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="130"/>
      <source>Export styles to json file</source>
      <translation type="unfinished">Export styles to json file</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="164"/>
      <source>Text</source>
      <translation type="unfinished">Text</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="170"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="192"/>
      <source>The font to use for texts and dimensions</source>
      <translation type="unfinished">The font to use for texts and dimensions</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="173"/>
      <source>Font name</source>
      <translation type="unfinished">Font name</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="199"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="209"/>
      <source>Font size in the system units</source>
      <translation type="unfinished">Font size in the system units</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="202"/>
      <source>Font size</source>
      <translation type="unfinished">Font size</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="219"/>
      <source>Line spacing in system units</source>
      <translation type="unfinished">Line spacing in system units</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="222"/>
      <source>Line spacing</source>
      <translation type="unfinished">Line spacing</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="239"/>
      <source>Units</source>
      <translation type="unfinished">Units</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="245"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="255"/>
      <source>A multiplier factor that affects the size of texts and markers</source>
      <translation type="unfinished">A multiplier factor that affects the size of texts and markers</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="248"/>
      <source>Scale multiplier</source>
      <translation type="unfinished">Scale multiplier</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="268"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="278"/>
      <source>If it is checked it will show the unit next to the dimension value</source>
      <translation type="unfinished">If it is checked it will show the unit next to the dimension value</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="271"/>
      <source>Show unit</source>
      <translation type="unfinished">Show unit</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="291"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="301"/>
      <source>Specify a valid length unit like mm, m, in, ft, to force displaying the dimension value in this unit</source>
      <translation type="unfinished">Specify a valid length unit like mm, m, in, ft, to force displaying the dimension value in this unit</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="294"/>
      <source>Unit override</source>
      <translation type="unfinished">Unit override</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="308"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="318"/>
      <source>The number of decimals to show for dimension values</source>
      <translation type="unfinished">The number of decimals to show for dimension values</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="311"/>
      <source>Decimals</source>
      <translation type="unfinished">Decimals</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="331"/>
      <source>Line and arrows</source>
      <translation type="unfinished">Line and arrows</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="337"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="347"/>
      <source>If it is checked it will display the dimension line</source>
      <translation type="unfinished">If it is checked it will display the dimension line</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="340"/>
      <source>Show lines</source>
      <translation type="unfinished">Show lines</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="363"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="373"/>
      <source>The width of the dimension lines</source>
      <translation type="unfinished">The width of the dimension lines</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="366"/>
      <source>Line width</source>
      <translation type="unfinished">Line width</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="376"/>
      <source>px</source>
      <translation type="unfinished">px</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="386"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="396"/>
      <source>The color of dimension lines, arrows and texts</source>
      <translation type="unfinished">The color of dimension lines, arrows and texts</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="389"/>
      <source>Line / text color</source>
      <translation type="unfinished">Line / text color</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="410"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="432"/>
      <source>The type of arrows or markers to use at the end of dimension lines</source>
      <translation type="unfinished">The type of arrows or markers to use at the end of dimension lines</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="413"/>
      <source>Arrow type</source>
      <translation type="unfinished">Arrow type</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="436"/>
      <source>Dot</source>
      <translation type="unfinished">Dot</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="441"/>
      <source>Circle</source>
      <translation type="unfinished">Circle</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="446"/>
      <source>Arrow</source>
      <translation type="unfinished">Arrow</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="451"/>
      <source>Tick</source>
      <translation type="unfinished">Tick</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="456"/>
      <source>Tick-2</source>
      <translation type="unfinished">Tick-2</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="464"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="474"/>
      <source>The size of the dimension arrows or markers in system units</source>
      <translation type="unfinished">The size of the dimension arrows or markers in system units</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="467"/>
      <source>Arrow size</source>
      <translation type="unfinished">Arrow size</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="484"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="494"/>
      <source>The distance that the dimension line is additionally extended</source>
      <translation type="unfinished">The distance that the dimension line is additionally extended</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="487"/>
      <source>Dimension overshoot</source>
      <translation type="unfinished">Dimension overshoot</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="504"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="514"/>
      <source>The length of the extension lines</source>
      <translation type="unfinished">The length of the extension lines</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="507"/>
      <source>Extension lines</source>
      <translation type="unfinished">Extension lines</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="524"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="534"/>
      <source>The distance that the extension lines are additionally extended beyond the dimension line</source>
      <translation type="unfinished">The distance that the extension lines are additionally extended beyond the dimension line</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="527"/>
      <source>Extension overshoot</source>
      <translation type="unfinished">Extension overshoot</translation>
    </message>
  </context>
  <context>
    <name>Draft</name>
    <message>
      <location filename="../../InitGui.py" line="160"/>
      <location filename="../../InitGui.py" line="163"/>
      <location filename="../../InitGui.py" line="167"/>
      <location filename="../../InitGui.py" line="170"/>
      <location filename="../../InitGui.py" line="174"/>
      <source>Draft</source>
      <translation>Притяжка</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="222"/>
      <location filename="../../InitGui.py" line="225"/>
      <location filename="../../InitGui.py" line="228"/>
      <location filename="../../InitGui.py" line="231"/>
      <source>Import-Export</source>
      <translation type="unfinished">Import-Export</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="110"/>
      <source>_BSpline.createGeometry: Closed with same first/last Point. Geometry not updated.</source>
      <translation type="unfinished">_BSpline.createGeometry: Closed with same first/last Point. Geometry not updated.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="257"/>
      <location filename="../../draftobjects/pointarray.py" line="322"/>
      <source>Point object doesn&apos;t have a discrete point, it cannot be used for an array.</source>
      <translation type="unfinished">Point object doesn&apos;t have a discrete point, it cannot be used for an array.</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="281"/>
      <source>Toggles Grid On/Off</source>
      <translation type="unfinished">Toggles Grid On/Off</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="300"/>
      <source>Object snapping</source>
      <translation type="unfinished">Object snapping</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="330"/>
      <source>Toggles Visual Aid Dimensions On/Off</source>
      <translation type="unfinished">Toggles Visual Aid Dimensions On/Off</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="351"/>
      <source>Toggles Ortho On/Off</source>
      <translation type="unfinished">Toggles Ortho On/Off</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="370"/>
      <source>Toggles Constrain to Working Plane On/Off</source>
      <translation type="unfinished">Toggles Constrain to Working Plane On/Off</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="157"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="160"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="144"/>
      <source>True</source>
      <translation>Да</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="161"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="162"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="148"/>
      <source>False</source>
      <translation>Нет</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="210"/>
      <source>Scale</source>
      <translation type="unfinished">Scale</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="211"/>
      <source>X factor</source>
      <translation type="unfinished">X factor</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="212"/>
      <source>Y factor</source>
      <translation type="unfinished">Y factor</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="213"/>
      <source>Z factor</source>
      <translation type="unfinished">Z factor</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="214"/>
      <source>Uniform scaling</source>
      <translation type="unfinished">Uniform scaling</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="215"/>
      <source>Working plane orientation</source>
      <translation type="unfinished">Working plane orientation</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="216"/>
      <source>Copy</source>
      <translation type="unfinished">Copy</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="217"/>
      <source>Modify subelements</source>
      <translation type="unfinished">Modify subelements</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="218"/>
      <source>Pick from/to points</source>
      <translation type="unfinished">Pick from/to points</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="219"/>
      <source>Create a clone</source>
      <translation>Создать клон</translation>
    </message>
    <message>
      <location filename="../../importDXF.py" line="140"/>
      <source>Download of dxf libraries failed.
Please install the dxf Library addon manually
from menu Tools -&gt; Addon Manager</source>
      <translation type="unfinished">Download of dxf libraries failed.
Please install the dxf Library addon manually
from menu Tools -&gt; Addon Manager</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="93"/>
      <location filename="../../draftguitools/gui_lineslope.py" line="96"/>
      <source>Slope</source>
      <translation type="unfinished">Slope</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_hatch.py" line="58"/>
      <source>You must choose a base object before using this command</source>
      <translation type="unfinished">You must choose a base object before using this command</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="96"/>
      <source>Clone</source>
      <translation type="unfinished">Clone</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="348"/>
      <source>Offset direction is not defined. Please move the mouse on either side of the object first to indicate a direction</source>
      <translation type="unfinished">Offset direction is not defined. Please move the mouse on either side of the object first to indicate a direction</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="94"/>
      <source>Delete original objects</source>
      <translation type="unfinished">Delete original objects</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="99"/>
      <source>Create chamfer</source>
      <translation type="unfinished">Create chamfer</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="407"/>
      <source>Save style</source>
      <translation type="unfinished">Save style</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="408"/>
      <source>Name of this new style:</source>
      <translation type="unfinished">Name of this new style:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="417"/>
      <source>Warning</source>
      <translation type="unfinished">Warning</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="418"/>
      <source>Name exists. Overwrite?</source>
      <translation type="unfinished">Name exists. Overwrite?</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="456"/>
      <source>Error: json module not found. Unable to save style</source>
      <translation type="unfinished">Error: json module not found. Unable to save style</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_wpproxy.py" line="120"/>
      <source>Writing camera position</source>
      <translation type="unfinished">Writing camera position</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_wpproxy.py" line="139"/>
      <source>Writing objects shown/hidden state</source>
      <translation type="unfinished">Writing objects shown/hidden state</translation>
    </message>
  </context>
  <context>
    <name>DraftCircularArrayTaskPanel</name>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="26"/>
      <source>Circular array</source>
      <translation type="unfinished">Circular array</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="44"/>
      <source>(Placeholder for the icon)</source>
      <translation type="unfinished">(Placeholder for the icon)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="53"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="63"/>
      <source>Distance from one layer of objects to the next layer of objects.</source>
      <translation type="unfinished">Distance from one layer of objects to the next layer of objects.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="56"/>
      <source>Radial distance</source>
      <translation type="unfinished">Radial distance</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="76"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="87"/>
      <source>Distance from one element in one ring of the array to the next element in the same ring.
It cannot be zero.</source>
      <translation type="unfinished">Distance from one element in one ring of the array to the next element in the same ring.
It cannot be zero.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="80"/>
      <source>Tangential distance</source>
      <translation type="unfinished">Tangential distance</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="101"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="112"/>
      <source>Number of circular layers or rings to create, including a copy of the original object.
It must be at least 2.</source>
      <translation type="unfinished">Number of circular layers or rings to create, including a copy of the original object.
It must be at least 2.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="105"/>
      <source>Number of circular layers</source>
      <translation type="unfinished">Number of circular layers</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="126"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="136"/>
      <source>The number of symmetry lines in the circular array.</source>
      <translation type="unfinished">The number of symmetry lines in the circular array.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="129"/>
      <source>Symmetry</source>
      <translation type="unfinished">Symmetry</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="151"/>
      <source>The coordinates of the point through which the axis of rotation passes.
Change the direction of the axis itself in the property editor.</source>
      <translation type="unfinished">The coordinates of the point through which the axis of rotation passes.
Change the direction of the axis itself in the property editor.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="155"/>
      <source>Center of rotation</source>
      <translation type="unfinished">Center of rotation</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="163"/>
      <source>X</source>
      <translation type="unfinished">X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="183"/>
      <source>Y</source>
      <translation type="unfinished">Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="203"/>
      <source>Z</source>
      <translation type="unfinished">Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="225"/>
      <source>Reset the coordinates of the center of rotation.</source>
      <translation type="unfinished">Reset the coordinates of the center of rotation.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="228"/>
      <source>Reset point</source>
      <translation type="unfinished">Reset point</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="240"/>
      <source>If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</source>
      <translation type="unfinished">If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="244"/>
      <source>Fuse</source>
      <translation>Объединение</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="251"/>
      <source>If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</source>
      <translation type="unfinished">If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="255"/>
      <source>Link array</source>
      <translation type="unfinished">Link array</translation>
    </message>
  </context>
  <context>
    <name>DraftOrthoArrayTaskPanel</name>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="26"/>
      <source>Orthogonal array</source>
      <translation type="unfinished">Orthogonal array</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="44"/>
      <source>(Placeholder for the icon)</source>
      <translation type="unfinished">(Placeholder for the icon)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="51"/>
      <source>Number of elements in the array in the specified direction, including a copy of the original object.
The number must be at least 1 in each direction.</source>
      <translation type="unfinished">Number of elements in the array in the specified direction, including a copy of the original object.
The number must be at least 1 in each direction.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="55"/>
      <source>Number of elements</source>
      <translation type="unfinished">Number of elements</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="63"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="132"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="223"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="314"/>
      <source>X</source>
      <translation type="unfinished">X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="80"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="155"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="243"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="334"/>
      <source>Y</source>
      <translation type="unfinished">Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="97"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="175"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="266"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="354"/>
      <source>Z</source>
      <translation type="unfinished">Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="119"/>
      <source>Distance between the elements in the X direction.
Normally, only the X value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</source>
      <translation type="unfinished">Distance between the elements in the X direction.
Normally, only the X value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="124"/>
      <source>X intervals</source>
      <translation type="unfinished">X intervals</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="197"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="288"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="379"/>
      <source>Reset the distances.</source>
      <translation type="unfinished">Reset the distances.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="200"/>
      <source>Reset X</source>
      <translation type="unfinished">Reset X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="210"/>
      <source>Distance between the elements in the Y direction.
Normally, only the Y value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</source>
      <translation type="unfinished">Distance between the elements in the Y direction.
Normally, only the Y value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="215"/>
      <source>Y intervals</source>
      <translation type="unfinished">Y intervals</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="291"/>
      <source>Reset Y</source>
      <translation type="unfinished">Reset Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="301"/>
      <source>Distance between the elements in the Z direction.
Normally, only the Z value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</source>
      <translation type="unfinished">Distance between the elements in the Z direction.
Normally, only the Z value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="306"/>
      <source>Z intervals</source>
      <translation type="unfinished">Z intervals</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="382"/>
      <source>Reset Z</source>
      <translation type="unfinished">Reset Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="394"/>
      <source>If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</source>
      <translation type="unfinished">If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="398"/>
      <source>Fuse</source>
      <translation>Объединение</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="405"/>
      <source>If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</source>
      <translation type="unfinished">If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="409"/>
      <source>Link array</source>
      <translation type="unfinished">Link array</translation>
    </message>
  </context>
  <context>
    <name>DraftPolarArrayTaskPanel</name>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="26"/>
      <source>Polar array</source>
      <translation type="unfinished">Polar array</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="44"/>
      <source>(Placeholder for the icon)</source>
      <translation type="unfinished">(Placeholder for the icon)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="53"/>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="65"/>
      <source>Sweeping angle of the polar distribution.
A negative angle produces a polar pattern in the opposite direction.
The maximum absolute value is 360 degrees.</source>
      <translation type="unfinished">Sweeping angle of the polar distribution.
A negative angle produces a polar pattern in the opposite direction.
The maximum absolute value is 360 degrees.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="58"/>
      <source>Polar angle</source>
      <translation type="unfinished">Polar angle</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="86"/>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="97"/>
      <source>Number of elements in the array, including a copy of the original object.
It must be at least 2.</source>
      <translation type="unfinished">Number of elements in the array, including a copy of the original object.
It must be at least 2.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="90"/>
      <source>Number of elements</source>
      <translation type="unfinished">Number of elements</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="113"/>
      <source>The coordinates of the point through which the axis of rotation passes.
Change the direction of the axis itself in the property editor.</source>
      <translation type="unfinished">The coordinates of the point through which the axis of rotation passes.
Change the direction of the axis itself in the property editor.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="117"/>
      <source>Center of rotation</source>
      <translation type="unfinished">Center of rotation</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="125"/>
      <source>X</source>
      <translation type="unfinished">X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="145"/>
      <source>Y</source>
      <translation type="unfinished">Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="165"/>
      <source>Z</source>
      <translation type="unfinished">Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="187"/>
      <source>Reset the coordinates of the center of rotation.</source>
      <translation type="unfinished">Reset the coordinates of the center of rotation.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="190"/>
      <source>Reset point</source>
      <translation type="unfinished">Reset point</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="202"/>
      <source>If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</source>
      <translation type="unfinished">If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="206"/>
      <source>Fuse</source>
      <translation>Объединение</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="213"/>
      <source>If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</source>
      <translation type="unfinished">If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="217"/>
      <source>Link array</source>
      <translation type="unfinished">Link array</translation>
    </message>
  </context>
  <context>
    <name>DraftShapeStringGui</name>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="26"/>
      <source>ShapeString</source>
      <translation type="unfinished">ShapeString</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="46"/>
      <source>X</source>
      <translation type="unfinished">X</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="53"/>
      <location filename="../ui/TaskShapeString.ui" line="70"/>
      <location filename="../ui/TaskShapeString.ui" line="87"/>
      <source>Enter coordinates or select point with mouse.</source>
      <translation type="unfinished">Enter coordinates or select point with mouse.</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="63"/>
      <source>Y</source>
      <translation type="unfinished">Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="80"/>
      <source>Z</source>
      <translation type="unfinished">Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="114"/>
      <source>Reset 3d point selection</source>
      <translation type="unfinished">Reset 3d point selection</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="120"/>
      <source>Reset Point</source>
      <translation type="unfinished">Reset Point</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="131"/>
      <source>String</source>
      <translation type="unfinished">String</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="138"/>
      <source>Text to be made into ShapeString</source>
      <translation type="unfinished">Text to be made into ShapeString</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="149"/>
      <source>Height</source>
      <translation type="unfinished">Height</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="156"/>
      <source>Height of the result</source>
      <translation type="unfinished">Height of the result</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="176"/>
      <source>Font file</source>
      <translation type="unfinished">Font file</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddConstruction</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="333"/>
      <source>Add to Construction group</source>
      <translation type="unfinished">Add to Construction group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="337"/>
      <source>Adds the selected objects to the construction group,
and changes their appearance to the construction style.
It creates a construction group if it doesn&apos;t exist.</source>
      <translation type="unfinished">Adds the selected objects to the construction group,
and changes their appearance to the construction style.
It creates a construction group if it doesn&apos;t exist.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddNamedGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="392"/>
      <source>Add a new named group</source>
      <translation type="unfinished">Add a new named group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="395"/>
      <source>Add a new group with a given name.</source>
      <translation type="unfinished">Add a new group with a given name.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddPoint</name>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="58"/>
      <source>Add point</source>
      <translation type="unfinished">Add point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="61"/>
      <source>Adds a point to an existing Wire or B-spline.</source>
      <translation type="unfinished">Adds a point to an existing Wire or B-spline.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddToGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="74"/>
      <source>Move to group...</source>
      <translation type="unfinished">Move to group...</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="78"/>
      <source>Moves the selected objects to an existing group, or removes them from any group.
Create a group first to use this tool.</source>
      <translation type="unfinished">Moves the selected objects to an existing group, or removes them from any group.
Create a group first to use this tool.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AnnotationStyleEditor</name>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="82"/>
      <source>Annotation styles...</source>
      <translation type="unfinished">Annotation styles...</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="85"/>
      <source>Manage or create annotation styles</source>
      <translation type="unfinished">Manage or create annotation styles</translation>
    </message>
  </context>
  <context>
    <name>Draft_ApplyStyle</name>
    <message>
      <location filename="../../draftguitools/gui_styles.py" line="48"/>
      <source>Apply current style</source>
      <translation type="unfinished">Apply current style</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_styles.py" line="52"/>
      <source>Applies the current style defined in the toolbar (line width and colors) to the selected objects and groups.</source>
      <translation type="unfinished">Applies the current style defined in the toolbar (line width and colors) to the selected objects and groups.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Arc</name>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="67"/>
      <source>Arc</source>
      <translation type="unfinished">Arc</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="71"/>
      <source>Creates a circular arc by a center point and a radius.
CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates a circular arc by a center point and a radius.
CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ArcTools</name>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="656"/>
      <source>Arc tools</source>
      <translation type="unfinished">Arc tools</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="659"/>
      <source>Create various types of circular arcs.</source>
      <translation type="unfinished">Create various types of circular arcs.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Arc_3Points</name>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="533"/>
      <source>Arc by 3 points</source>
      <translation type="unfinished">Arc by 3 points</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="537"/>
      <source>Creates a circular arc by picking 3 points.
CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates a circular arc by picking 3 points.
CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Array</name>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="69"/>
      <source>Array</source>
      <translation type="unfinished">Array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="73"/>
      <source>Creates an array from a selected object.
By default, it is a 2x2 orthogonal array.
Once the array is created its type can be changed
to polar or circular, and its properties can be modified.</source>
      <translation type="unfinished">Creates an array from a selected object.
By default, it is a 2x2 orthogonal array.
Once the array is created its type can be changed
to polar or circular, and its properties can be modified.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ArrayTools</name>
    <message>
      <location filename="../../draftguitools/gui_arrays.py" line="72"/>
      <source>Array tools</source>
      <translation type="unfinished">Array tools</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arrays.py" line="76"/>
      <source>Create various types of arrays, including rectangular, polar, circular, path, and point</source>
      <translation type="unfinished">Create various types of arrays, including rectangular, polar, circular, path, and point</translation>
    </message>
  </context>
  <context>
    <name>Draft_AutoGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="221"/>
      <source>Autogroup</source>
      <translation type="unfinished">Autogroup</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="225"/>
      <source>Select a group to add all Draft and Arch objects to.</source>
      <translation type="unfinished">Select a group to add all Draft and Arch objects to.</translation>
    </message>
  </context>
  <context>
    <name>Draft_BSpline</name>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="61"/>
      <source>B-spline</source>
      <translation type="unfinished">B-spline</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="65"/>
      <source>Creates a multiple-point B-spline. CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates a multiple-point B-spline. CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_BezCurve</name>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="65"/>
      <source>Bézier curve</source>
      <translation type="unfinished">Bézier curve</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="69"/>
      <source>Creates an N-degree Bézier curve. The more points you pick, the higher the degree.
CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates an N-degree Bézier curve. The more points you pick, the higher the degree.
CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_BezierTools</name>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="493"/>
      <source>Bézier tools</source>
      <translation type="unfinished">Bézier tools</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="496"/>
      <source>Create various types of Bézier curves.</source>
      <translation type="unfinished">Create various types of Bézier curves.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Circle</name>
    <message>
      <location filename="../../draftguitools/gui_circles.py" line="81"/>
      <source>Circle</source>
      <translation type="unfinished">Circle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_circles.py" line="85"/>
      <source>Creates a circle (full circular arc).
CTRL to snap, ALT to select tangent objects.</source>
      <translation type="unfinished">Creates a circle (full circular arc).
CTRL to snap, ALT to select tangent objects.</translation>
    </message>
  </context>
  <context>
    <name>Draft_CircularArray</name>
    <message>
      <location filename="../../draftguitools/gui_circulararray.py" line="66"/>
      <source>Circular array</source>
      <translation type="unfinished">Circular array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_circulararray.py" line="70"/>
      <source>Creates copies of the selected object, and places the copies in a radial pattern
creating various circular layers.

The array can be turned into an orthogonal or a polar array by changing its type.</source>
      <translation type="unfinished">Creates copies of the selected object, and places the copies in a radial pattern
creating various circular layers.

The array can be turned into an orthogonal or a polar array by changing its type.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Clone</name>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="71"/>
      <source>Clone</source>
      <translation type="unfinished">Clone</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="75"/>
      <source>Creates a clone of the selected objects.
The resulting clone can be scaled in each of its three directions.</source>
      <translation type="unfinished">Creates a clone of the selected objects.
The resulting clone can be scaled in each of its three directions.</translation>
    </message>
  </context>
  <context>
    <name>Draft_CubicBezCurve</name>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="250"/>
      <source>Cubic Bézier curve</source>
      <translation type="unfinished">Cubic Bézier curve</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="254"/>
      <source>Creates a Bézier curve made of 2nd degree (quadratic) and 3rd degree (cubic) segments. Click and drag to define each segment.
After the curve is created you can go back to edit each control point and set the properties of each knot.
CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates a Bézier curve made of 2nd degree (quadratic) and 3rd degree (cubic) segments. Click and drag to define each segment.
After the curve is created you can go back to edit each control point and set the properties of each knot.
CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_DelPoint</name>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="94"/>
      <source>Remove point</source>
      <translation type="unfinished">Remove point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="97"/>
      <source>Removes a point from an existing Wire or B-spline.</source>
      <translation type="unfinished">Removes a point from an existing Wire or B-spline.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Dimension</name>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="85"/>
      <source>Dimension</source>
      <translation>Размер</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="89"/>
      <source>Creates a dimension.

- Pick three points to create a simple linear dimension.
- Select a straight line to create a linear dimension linked to that line.
- Select an arc or circle to create a radius or diameter dimension linked to that arc.
- Select two straight lines to create an angular dimension between them.
CTRL to snap, SHIFT to constrain, ALT to select an edge or arc.

You may select a single line or single circular arc before launching this command
to create the corresponding linked dimension.
You may also select an &apos;App::MeasureDistance&apos; object before launching this command
to turn it into a &apos;Draft Dimension&apos; object.</source>
      <translation type="unfinished">Creates a dimension.

- Pick three points to create a simple linear dimension.
- Select a straight line to create a linear dimension linked to that line.
- Select an arc or circle to create a radius or diameter dimension linked to that arc.
- Select two straight lines to create an angular dimension between them.
CTRL to snap, SHIFT to constrain, ALT to select an edge or arc.

You may select a single line or single circular arc before launching this command
to create the corresponding linked dimension.
You may also select an &apos;App::MeasureDistance&apos; object before launching this command
to turn it into a &apos;Draft Dimension&apos; object.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Downgrade</name>
    <message>
      <location filename="../../draftguitools/gui_downgrade.py" line="59"/>
      <source>Downgrade</source>
      <translation type="unfinished">Downgrade</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_downgrade.py" line="63"/>
      <source>Downgrades the selected objects into simpler shapes.
The result of the operation depends on the types of objects, which may be able to be downgraded several times in a row.
For example, it explodes the selected polylines into simpler faces, wires, and then edges. It can also subtract faces.</source>
      <translation type="unfinished">Downgrades the selected objects into simpler shapes.
The result of the operation depends on the types of objects, which may be able to be downgraded several times in a row.
For example, it explodes the selected polylines into simpler faces, wires, and then edges. It can also subtract faces.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Draft2Sketch</name>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="58"/>
      <source>Draft to Sketch</source>
      <translation type="unfinished">Draft to Sketch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="62"/>
      <source>Convert bidirectionally between Draft objects and Sketches.
Many Draft objects will be converted into a single non-constrained Sketch.
However, a single sketch with disconnected traces will be converted into several individual Draft objects.</source>
      <translation type="unfinished">Convert bidirectionally between Draft objects and Sketches.
Many Draft objects will be converted into a single non-constrained Sketch.
However, a single sketch with disconnected traces will be converted into several individual Draft objects.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Drawing</name>
    <message>
      <location filename="../../draftguitools/gui_drawing.py" line="72"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_drawing.py" line="76"/>
      <source>Creates a 2D projection on a Drawing Workbench page from the selected objects.
This command is OBSOLETE since the Drawing Workbench became obsolete in 0.17.
Use TechDraw Workbench instead for generating technical drawings.</source>
      <translation type="unfinished">Creates a 2D projection on a Drawing Workbench page from the selected objects.
This command is OBSOLETE since the Drawing Workbench became obsolete in 0.17.
Use TechDraw Workbench instead for generating technical drawings.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Edit</name>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="285"/>
      <source>Edit</source>
      <translation>Редактировать</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="289"/>
      <source>Edits the active object.
Press E or ALT+LeftClick to display context menu
on supported nodes and on supported objects.</source>
      <translation type="unfinished">Edits the active object.
Press E or ALT+LeftClick to display context menu
on supported nodes and on supported objects.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Ellipse</name>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="60"/>
      <source>Ellipse</source>
      <translation type="unfinished">Ellipse</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="63"/>
      <source>Creates an ellipse. CTRL to snap.</source>
      <translation type="unfinished">Creates an ellipse. CTRL to snap.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Facebinder</name>
    <message>
      <location filename="../../draftguitools/gui_facebinders.py" line="61"/>
      <source>Facebinder</source>
      <translation type="unfinished">Facebinder</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_facebinders.py" line="64"/>
      <source>Creates a facebinder object from selected faces.</source>
      <translation type="unfinished">Creates a facebinder object from selected faces.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Fillet</name>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="65"/>
      <source>Fillet</source>
      <translation>Скругление</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="68"/>
      <source>Creates a fillet between two selected wires or edges.</source>
      <translation type="unfinished">Creates a fillet between two selected wires or edges.</translation>
    </message>
  </context>
  <context>
    <name>Draft_FlipDimension</name>
    <message>
      <location filename="../../draftguitools/gui_dimension_ops.py" line="64"/>
      <source>Flip dimension</source>
      <translation type="unfinished">Flip dimension</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimension_ops.py" line="68"/>
      <source>Flip the normal direction of the selected dimensions (linear, radial, angular).
If other objects are selected they are ignored.</source>
      <translation type="unfinished">Flip the normal direction of the selected dimensions (linear, radial, angular).
If other objects are selected they are ignored.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Hatch</name>
    <message>
      <location filename="../../draftguitools/gui_hatch.py" line="38"/>
      <source>Hatch</source>
      <translation type="unfinished">Hatch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_hatch.py" line="42"/>
      <source>Creates hatches on the faces of a selected object</source>
      <translation type="unfinished">Creates hatches on the faces of a selected object</translation>
    </message>
  </context>
  <context>
    <name>Draft_Heal</name>
    <message>
      <location filename="../../draftguitools/gui_heal.py" line="59"/>
      <source>Heal</source>
      <translation type="unfinished">Heal</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_heal.py" line="63"/>
      <source>Heal faulty Draft objects saved with an earlier version of the program.
If an object is selected it will try to heal that object in particular,
otherwise it will try to heal all objects in the active document.</source>
      <translation type="unfinished">Heal faulty Draft objects saved with an earlier version of the program.
If an object is selected it will try to heal that object in particular,
otherwise it will try to heal all objects in the active document.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Join</name>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="67"/>
      <source>Join</source>
      <translation type="unfinished">Join</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="71"/>
      <source>Joins the selected lines or polylines into a single object.
The lines must share a common point at the start or at the end for the operation to succeed.</source>
      <translation type="unfinished">Joins the selected lines or polylines into a single object.
The lines must share a common point at the start or at the end for the operation to succeed.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Label</name>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="65"/>
      <source>Label</source>
      <translation type="unfinished">Label</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="69"/>
      <source>Creates a label, optionally attached to a selected object or subelement.

First select a vertex, an edge, or a face of an object, then call this command,
and then set the position of the leader line and the textual label.
The label will be able to display information about this object, and about the selected subelement,
if any.

If many objects or many subelements are selected, only the first one in each case
will be used to provide information to the label.</source>
      <translation type="unfinished">Creates a label, optionally attached to a selected object or subelement.

First select a vertex, an edge, or a face of an object, then call this command,
and then set the position of the leader line and the textual label.
The label will be able to display information about this object, and about the selected subelement,
if any.

If many objects or many subelements are selected, only the first one in each case
will be used to provide information to the label.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Layer</name>
    <message>
      <location filename="../../draftguitools/gui_layers.py" line="53"/>
      <source>Layer</source>
      <translation type="unfinished">Layer</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_layers.py" line="57"/>
      <source>Adds a layer to the document.
Objects added to this layer can share the same visual properties such as line color, line width, and shape color.</source>
      <translation type="unfinished">Adds a layer to the document.
Objects added to this layer can share the same visual properties such as line color, line width, and shape color.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Line</name>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="64"/>
      <source>Line</source>
      <translation type="unfinished">Line</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="68"/>
      <source>Creates a 2-point line. CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates a 2-point line. CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_LinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="126"/>
      <source>LinkArray</source>
      <translation type="unfinished">LinkArray</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="130"/>
      <source>Like the Array tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</source>
      <translation type="unfinished">Like the Array tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Mirror</name>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="64"/>
      <source>Mirror</source>
      <translation type="unfinished">Mirror</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="68"/>
      <source>Mirrors the selected objects along a line defined by two points.</source>
      <translation type="unfinished">Mirrors the selected objects along a line defined by two points.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Move</name>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="64"/>
      <source>Move</source>
      <translation type="unfinished">Move</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="68"/>
      <source>Moves the selected objects from one base point to another point.
If the &quot;copy&quot; option is active, it will create displaced copies.
CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Moves the selected objects from one base point to another point.
If the &quot;copy&quot; option is active, it will create displaced copies.
CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Offset</name>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="64"/>
      <source>Offset</source>
      <translation type="unfinished">Offset</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="68"/>
      <source>Offsets of the selected object.
It can also create an offset copy of the original object.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</source>
      <translation type="unfinished">Offsets of the selected object.
It can also create an offset copy of the original object.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</translation>
    </message>
  </context>
  <context>
    <name>Draft_OrthoArray</name>
    <message>
      <location filename="../../draftguitools/gui_orthoarray.py" line="66"/>
      <source>Array</source>
      <translation type="unfinished">Array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_orthoarray.py" line="70"/>
      <source>Creates copies of the selected object, and places the copies in an orthogonal pattern,
meaning the copies follow the specified direction in the X, Y, Z axes.

The array can be turned into a polar or a circular array by changing its type.</source>
      <translation type="unfinished">Creates copies of the selected object, and places the copies in an orthogonal pattern,
meaning the copies follow the specified direction in the X, Y, Z axes.

The array can be turned into a polar or a circular array by changing its type.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathArray</name>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="74"/>
      <source>Path array</source>
      <translation type="unfinished">Path array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="78"/>
      <source>Creates copies of the selected object along a selected path.
First select the object, and then select the path.
The path can be a polyline, B-spline, Bezier curve, or even edges from other objects.</source>
      <translation type="unfinished">Creates copies of the selected object along a selected path.
First select the object, and then select the path.
The path can be a polyline, B-spline, Bezier curve, or even edges from other objects.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathLinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="176"/>
      <source>Path Link array</source>
      <translation type="unfinished">Path Link array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="180"/>
      <source>Like the PathArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</source>
      <translation type="unfinished">Like the PathArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathTwistedArray</name>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="69"/>
      <source>Path twisted array</source>
      <translation type="unfinished">Path twisted array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="73"/>
      <source>Creates copies of the selected object along a selected path, and twists the copies.
First select the object, and then select the path.
The path can be a polyline, B-spline, Bezier curve, or even edges from other objects.</source>
      <translation type="unfinished">Creates copies of the selected object along a selected path, and twists the copies.
First select the object, and then select the path.
The path can be a polyline, B-spline, Bezier curve, or even edges from other objects.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathTwistedLinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="138"/>
      <source>Path twisted Link array</source>
      <translation type="unfinished">Path twisted Link array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="142"/>
      <source>Like the PathTwistedArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</source>
      <translation type="unfinished">Like the PathTwistedArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Point</name>
    <message>
      <location filename="../../draftguitools/gui_points.py" line="63"/>
      <source>Point</source>
      <translation type="unfinished">Point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_points.py" line="66"/>
      <source>Creates a point object. Click anywhere on the 3D view.</source>
      <translation type="unfinished">Creates a point object. Click anywhere on the 3D view.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PointArray</name>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="77"/>
      <source>Point array</source>
      <translation type="unfinished">Point array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="81"/>
      <source>Creates copies of the selected object, and places the copies at the position of various points.

The points need to be grouped under a compound of points before using this tool.
To create this compound, select various points and then use the Part Compound tool,
or use the Draft Upgrade tool to create a &apos;Block&apos;, or create a Sketch and add simple points to it.

Select the base object, and then select the compound or the sketch to create the point array.</source>
      <translation type="unfinished">Creates copies of the selected object, and places the copies at the position of various points.

The points need to be grouped under a compound of points before using this tool.
To create this compound, select various points and then use the Part Compound tool,
or use the Draft Upgrade tool to create a &apos;Block&apos;, or create a Sketch and add simple points to it.

Select the base object, and then select the compound or the sketch to create the point array.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PointLinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="155"/>
      <source>PointLinkArray</source>
      <translation type="unfinished">PointLinkArray</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="159"/>
      <source>Like the PointArray tool, but creates a &apos;Point link array&apos; instead.
A &apos;Point link array&apos; is more efficient when handling many copies.</source>
      <translation type="unfinished">Like the PointArray tool, but creates a &apos;Point link array&apos; instead.
A &apos;Point link array&apos; is more efficient when handling many copies.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PolarArray</name>
    <message>
      <location filename="../../draftguitools/gui_polararray.py" line="66"/>
      <source>Polar array</source>
      <translation type="unfinished">Polar array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polararray.py" line="70"/>
      <source>Creates copies of the selected object, and places the copies in a polar pattern
defined by a center of rotation and its angle.

The array can be turned into an orthogonal or a circular array by changing its type.</source>
      <translation type="unfinished">Creates copies of the selected object, and places the copies in a polar pattern
defined by a center of rotation and its angle.

The array can be turned into an orthogonal or a circular array by changing its type.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Polygon</name>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="58"/>
      <source>Polygon</source>
      <translation type="unfinished">Polygon</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="62"/>
      <source>Creates a regular polygon (triangle, square, pentagon, ...), by defining the number of sides and the circumscribed radius.
CTRL to snap, SHIFT to constrain</source>
      <translation type="unfinished">Creates a regular polygon (triangle, square, pentagon, ...), by defining the number of sides and the circumscribed radius.
CTRL to snap, SHIFT to constrain</translation>
    </message>
  </context>
  <context>
    <name>Draft_Rectangle</name>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="55"/>
      <source>Rectangle</source>
      <translation type="unfinished">Rectangle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="58"/>
      <source>Creates a 2-point rectangle. CTRL to snap.</source>
      <translation type="unfinished">Creates a 2-point rectangle. CTRL to snap.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Rotate</name>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="63"/>
      <source>Rotate</source>
      <translation type="unfinished">Rotate</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="67"/>
      <source>Rotates the selected objects. Choose the center of rotation, then the initial angle, and then the final angle.
If the &quot;copy&quot; option is active, it will create rotated copies.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</source>
      <translation type="unfinished">Rotates the selected objects. Choose the center of rotation, then the initial angle, and then the final angle.
If the &quot;copy&quot; option is active, it will create rotated copies.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Scale</name>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="72"/>
      <source>Scale</source>
      <translation type="unfinished">Scale</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="76"/>
      <source>Scales the selected objects from a base point.
CTRL to snap, SHIFT to constrain, ALT to copy.</source>
      <translation type="unfinished">Scales the selected objects from a base point.
CTRL to snap, SHIFT to constrain, ALT to copy.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SelectGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="169"/>
      <source>Select group</source>
      <translation type="unfinished">Select group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="173"/>
      <source>Selects the contents of selected groups. For selected non-group objects, the contents of the group they are in is selected.</source>
      <translation type="unfinished">Selects the contents of selected groups. For selected non-group objects, the contents of the group they are in is selected.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SelectPlane</name>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="67"/>
      <source>SelectPlane</source>
      <translation type="unfinished">SelectPlane</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="71"/>
      <source>Select the face of solid body to create a working plane on which to sketch Draft objects.
You may also select a three vertices or a Working Plane Proxy.</source>
      <translation type="unfinished">Select the face of solid body to create a working plane on which to sketch Draft objects.
You may also select a three vertices or a Working Plane Proxy.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SetStyle</name>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="61"/>
      <source>Set style</source>
      <translation type="unfinished">Set style</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="62"/>
      <source>Sets default styles</source>
      <translation type="unfinished">Sets default styles</translation>
    </message>
  </context>
  <context>
    <name>Draft_Shape2DView</name>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="60"/>
      <source>Shape 2D view</source>
      <translation type="unfinished">Shape 2D view</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="64"/>
      <source>Creates a 2D projection of the selected objects on the XY plane.
The initial projection direction is the negative of the current active view direction.
You can select individual faces to project, or the entire solid, and also include hidden lines.
These projections can be used to create technical drawings with the TechDraw Workbench.</source>
      <translation type="unfinished">Creates a 2D projection of the selected objects on the XY plane.
The initial projection direction is the negative of the current active view direction.
You can select individual faces to project, or the entire solid, and also include hidden lines.
These projections can be used to create technical drawings with the TechDraw Workbench.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ShapeString</name>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="67"/>
      <source>Shape from text</source>
      <translation type="unfinished">Shape from text</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="71"/>
      <source>Creates a shape from a text string by choosing a specific font and a placement.
The closed shapes can be used for extrusions and boolean operations.</source>
      <translation type="unfinished">Creates a shape from a text string by choosing a specific font and a placement.
The closed shapes can be used for extrusions and boolean operations.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ShowSnapBar</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="690"/>
      <source>Show snap toolbar</source>
      <translation type="unfinished">Show snap toolbar</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="693"/>
      <source>Show the snap toolbar if it is hidden.</source>
      <translation type="unfinished">Show the snap toolbar if it is hidden.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Slope</name>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="71"/>
      <source>Set slope</source>
      <translation type="unfinished">Set slope</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="75"/>
      <source>Sets the slope of the selected line by changing the value of the Z value of one of its points.
If a polyline is selected, it will apply the slope transformation to each of its segments.

The slope will always change the Z value, therefore this command only works well for
straight Draft lines that are drawn in the XY plane. Selected objects that aren&apos;t single lines will be ignored.</source>
      <translation type="unfinished">Sets the slope of the selected line by changing the value of the Z value of one of its points.
If a polyline is selected, it will apply the slope transformation to each of its segments.

The slope will always change the Z value, therefore this command only works well for
straight Draft lines that are drawn in the XY plane. Selected objects that aren&apos;t single lines will be ignored.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Angle</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="394"/>
      <source>Angle</source>
      <translation type="unfinished">Angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="398"/>
      <source>Set snapping to points in a circular arc located at multiples of 30 and 45 degree angles.</source>
      <translation type="unfinished">Set snapping to points in a circular arc located at multiples of 30 and 45 degree angles.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Center</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="431"/>
      <source>Center</source>
      <translation type="unfinished">Center</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="434"/>
      <source>Set snapping to the center of a circular arc.</source>
      <translation type="unfinished">Set snapping to the center of a circular arc.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Dimensions</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="613"/>
      <source>Show dimensions</source>
      <translation type="unfinished">Show dimensions</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="617"/>
      <source>Show temporary linear dimensions when editing an object and using other snapping methods.</source>
      <translation type="unfinished">Show temporary linear dimensions when editing an object and using other snapping methods.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Endpoint</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="357"/>
      <source>Endpoint</source>
      <translation type="unfinished">Endpoint</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="360"/>
      <source>Set snapping to endpoints of an edge.</source>
      <translation type="unfinished">Set snapping to endpoints of an edge.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Extension</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="467"/>
      <source>Extension</source>
      <translation type="unfinished">Extension</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="470"/>
      <source>Set snapping to the extension of an edge.</source>
      <translation type="unfinished">Set snapping to the extension of an edge.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Grid</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="248"/>
      <source>Grid</source>
      <translation type="unfinished">Grid</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="251"/>
      <source>Set snapping to the intersection of grid lines.</source>
      <translation type="unfinished">Set snapping to the intersection of grid lines.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Intersection</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="284"/>
      <source>Intersection</source>
      <translation>Пересечение</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="287"/>
      <source>Set snapping to the intersection of edges.</source>
      <translation type="unfinished">Set snapping to the intersection of edges.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Lock</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="141"/>
      <source>Main snapping toggle On/Off</source>
      <translation type="unfinished">Main snapping toggle On/Off</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="144"/>
      <source>Activates or deactivates all snap methods at once.</source>
      <translation type="unfinished">Activates or deactivates all snap methods at once.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Midpoint</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="177"/>
      <source>Midpoint</source>
      <translation type="unfinished">Midpoint</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="180"/>
      <source>Set snapping to the midpoint of an edge.</source>
      <translation type="unfinished">Set snapping to the midpoint of an edge.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Near</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="501"/>
      <source>Nearest</source>
      <translation type="unfinished">Nearest</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="504"/>
      <source>Set snapping to the nearest point of an edge.</source>
      <translation type="unfinished">Set snapping to the nearest point of an edge.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Ortho</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="538"/>
      <source>Orthogonal</source>
      <translation type="unfinished">Orthogonal</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="542"/>
      <source>Set snapping to a direction that is a multiple of 45 degrees from a point.</source>
      <translation type="unfinished">Set snapping to a direction that is a multiple of 45 degrees from a point.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Parallel</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="320"/>
      <source>Parallel</source>
      <translation type="unfinished">Parallel</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="324"/>
      <source>Set snapping to a direction that is parallel to an edge.</source>
      <translation type="unfinished">Set snapping to a direction that is parallel to an edge.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Perpendicular</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="213"/>
      <source>Perpendicular</source>
      <translation type="unfinished">Perpendicular</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="217"/>
      <source>Set snapping to a direction that is perpendicular to an edge.</source>
      <translation type="unfinished">Set snapping to a direction that is perpendicular to an edge.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Special</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="575"/>
      <source>Special</source>
      <translation type="unfinished">Special</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="579"/>
      <source>Set snapping to the special points defined inside an object.</source>
      <translation type="unfinished">Set snapping to the special points defined inside an object.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_WorkingPlane</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="655"/>
      <source>Working plane</source>
      <translation type="unfinished">Working plane</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="659"/>
      <source>Restricts snapping to a point in the current working plane.
If you select a point outside the working plane, for example, by using other snapping methods,
it will snap to that point&apos;s projection in the current working plane.</source>
      <translation type="unfinished">Restricts snapping to a point in the current working plane.
If you select a point outside the working plane, for example, by using other snapping methods,
it will snap to that point&apos;s projection in the current working plane.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Split</name>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="57"/>
      <source>Split</source>
      <translation type="unfinished">Split</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="61"/>
      <source>Splits the selected line or polyline into two independent lines
or polylines by clicking anywhere along the original object.
It works best when choosing a point on a straight segment and not a corner vertex.</source>
      <translation type="unfinished">Splits the selected line or polyline into two independent lines
or polylines by clicking anywhere along the original object.
It works best when choosing a point on a straight segment and not a corner vertex.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Stretch</name>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="65"/>
      <source>Stretch</source>
      <translation type="unfinished">Stretch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="69"/>
      <source>Stretches the selected objects.
Select an object, then draw a rectangle to pick the vertices that will be stretched,
then draw a line to specify the distance and direction of stretching.</source>
      <translation type="unfinished">Stretches the selected objects.
Select an object, then draw a rectangle to pick the vertices that will be stretched,
then draw a line to specify the distance and direction of stretching.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SubelementHighlight</name>
    <message>
      <location filename="../../draftguitools/gui_subelements.py" line="64"/>
      <source>Subelement highlight</source>
      <translation type="unfinished">Subelement highlight</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_subelements.py" line="68"/>
      <source>Highlight the subelements of the selected objects, so that they can then be edited with the move, rotate, and scale tools.</source>
      <translation type="unfinished">Highlight the subelements of the selected objects, so that they can then be edited with the move, rotate, and scale tools.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Text</name>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="61"/>
      <source>Text</source>
      <translation type="unfinished">Text</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="64"/>
      <source>Creates a multi-line annotation. CTRL to snap.</source>
      <translation type="unfinished">Creates a multi-line annotation. CTRL to snap.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleConstructionMode</name>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="103"/>
      <source>Toggle construction mode</source>
      <translation type="unfinished">Toggle construction mode</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="108"/>
      <source>Toggles the Construction mode.
When this is active, the following objects created will be included in the construction group, and will be drawn with the specified color and properties.</source>
      <translation type="unfinished">Toggles the Construction mode.
When this is active, the following objects created will be included in the construction group, and will be drawn with the specified color and properties.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleContinueMode</name>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="144"/>
      <source>Toggle continue mode</source>
      <translation type="unfinished">Toggle continue mode</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="148"/>
      <source>Toggles the Continue mode.
When this is active, any drawing tool that is terminated will automatically start again.
This can be used to draw several objects one after the other in succession.</source>
      <translation type="unfinished">Toggles the Continue mode.
When this is active, any drawing tool that is terminated will automatically start again.
This can be used to draw several objects one after the other in succession.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleDisplayMode</name>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="187"/>
      <source>Toggle normal/wireframe display</source>
      <translation type="unfinished">Toggle normal/wireframe display</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="191"/>
      <source>Switches the display mode of selected objects from flatlines to wireframe and back.
This is helpful to quickly visualize objects that are hidden by other objects.
This is intended to be used with closed shapes and solids, and doesn&apos;t affect open wires.</source>
      <translation type="unfinished">Switches the display mode of selected objects from flatlines to wireframe and back.
This is helpful to quickly visualize objects that are hidden by other objects.
This is intended to be used with closed shapes and solids, and doesn&apos;t affect open wires.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleGrid</name>
    <message>
      <location filename="../../draftguitools/gui_grid.py" line="59"/>
      <source>Toggle grid</source>
      <translation type="unfinished">Toggle grid</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_grid.py" line="62"/>
      <source>Toggles the Draft grid on and off.</source>
      <translation type="unfinished">Toggles the Draft grid on and off.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Trimex</name>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="77"/>
      <source>Trimex</source>
      <translation type="unfinished">Trimex</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="83"/>
      <source>Trims or extends the selected object, or extrudes single faces.
CTRL snaps, SHIFT constrains to current segment or to normal, ALT inverts.</source>
      <translation type="unfinished">Trims or extends the selected object, or extrudes single faces.
CTRL snaps, SHIFT constrains to current segment or to normal, ALT inverts.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Upgrade</name>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="59"/>
      <source>Upgrade</source>
      <translation type="unfinished">Upgrade</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="63"/>
      <source>Upgrades the selected objects into more complex shapes.
The result of the operation depends on the types of objects, which may be able to be upgraded several times in a row.
For example, it can join the selected objects into one, convert simple edges into parametric polylines,
convert closed edges into filled faces and parametric polygons, and merge faces into a single face.</source>
      <translation type="unfinished">Upgrades the selected objects into more complex shapes.
The result of the operation depends on the types of objects, which may be able to be upgraded several times in a row.
For example, it can join the selected objects into one, convert simple edges into parametric polylines,
convert closed edges into filled faces and parametric polygons, and merge faces into a single face.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Wire</name>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="317"/>
      <source>Polyline</source>
      <translation type="unfinished">Polyline</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="321"/>
      <source>Creates a multiple-points line (polyline). CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Creates a multiple-points line (polyline). CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_WireToBSpline</name>
    <message>
      <location filename="../../draftguitools/gui_wire2spline.py" line="65"/>
      <source>Wire to B-spline</source>
      <translation type="unfinished">Wire to B-spline</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_wire2spline.py" line="69"/>
      <source>Converts a selected polyline to a B-spline, or a B-spline to a polyline.</source>
      <translation type="unfinished">Converts a selected polyline to a B-spline, or a B-spline to a polyline.</translation>
    </message>
  </context>
  <context>
    <name>Draft_WorkingPlaneProxy</name>
    <message>
      <location filename="../../draftguitools/gui_planeproxy.py" line="54"/>
      <source>Create working plane proxy</source>
      <translation type="unfinished">Create working plane proxy</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_planeproxy.py" line="58"/>
      <source>Creates a proxy object from the current working plane.
Once the object is created double click it in the tree view to restore the camera position and objects&apos; visibilities.
Then you can use it to save a different camera position and objects&apos; states any time you need.</source>
      <translation type="unfinished">Creates a proxy object from the current working plane.
Once the object is created double click it in the tree view to restore the camera position and objects&apos; visibilities.
Then you can use it to save a different camera position and objects&apos; states any time you need.</translation>
    </message>
  </context>
  <context>
    <name>Form</name>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="14"/>
      <source>Style settings</source>
      <translation type="unfinished">Style settings</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="22"/>
      <source>Fills the values below with a stored style preset</source>
      <translation type="unfinished">Fills the values below with a stored style preset</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="26"/>
      <source>Load preset</source>
      <translation type="unfinished">Load preset</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="40"/>
      <source>Save current style as a preset...</source>
      <translation type="unfinished">Save current style as a preset...</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="57"/>
      <source>Lines and faces</source>
      <translation type="unfinished">Lines and faces</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="66"/>
      <source>Line color</source>
      <translation type="unfinished">Line color</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="73"/>
      <source>The color of lines</source>
      <translation type="unfinished">The color of lines</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="80"/>
      <source>Line width</source>
      <translation type="unfinished">Line width</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="87"/>
      <source> px</source>
      <translation type="unfinished"> px</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="94"/>
      <source>Draw style</source>
      <translation type="unfinished">Draw style</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="101"/>
      <source>The line style</source>
      <translation type="unfinished">The line style</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="105"/>
      <source>Solid</source>
      <translation type="unfinished">Solid</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="110"/>
      <source>Dashed</source>
      <translation type="unfinished">Dashed</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="115"/>
      <source>Dotted</source>
      <translation type="unfinished">Dotted</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="120"/>
      <source>DashDot</source>
      <translation type="unfinished">DashDot</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="128"/>
      <source>Display mode</source>
      <translation type="unfinished">Display mode</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="135"/>
      <source>The display mode for faces</source>
      <translation type="unfinished">The display mode for faces</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="139"/>
      <source>Flat Lines</source>
      <translation type="unfinished">Flat Lines</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="144"/>
      <source>Wireframe</source>
      <translation type="unfinished">Wireframe</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="149"/>
      <source>Shaded</source>
      <translation type="unfinished">Shaded</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="154"/>
      <source>Points</source>
      <translation type="unfinished">Points</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="162"/>
      <source>Shape color</source>
      <translation type="unfinished">Shape color</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="169"/>
      <source>The color of faces</source>
      <translation type="unfinished">The color of faces</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="176"/>
      <source>Transparency</source>
      <translation type="unfinished">Transparency</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="183"/>
      <source>The transparency of faces</source>
      <translation type="unfinished">The transparency of faces</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="186"/>
      <source> %</source>
      <translation type="unfinished"> %</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="199"/>
      <source>Annotations</source>
      <translation type="unfinished">Annotations</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="205"/>
      <source>Text font</source>
      <translation type="unfinished">Text font</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="218"/>
      <source>The font to use for texts and dimensions</source>
      <translation type="unfinished">The font to use for texts and dimensions</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="225"/>
      <source>Text size</source>
      <translation type="unfinished">Text size</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="232"/>
      <source>The size of texts and dimension texts</source>
      <translation type="unfinished">The size of texts and dimension texts</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="242"/>
      <source>Text spacing</source>
      <translation type="unfinished">Text spacing</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="249"/>
      <source>The space between the text and the dimension line</source>
      <translation type="unfinished">The space between the text and the dimension line</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="259"/>
      <source>Text color</source>
      <translation type="unfinished">Text color</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="266"/>
      <source>The color of texts and dimension texts</source>
      <translation type="unfinished">The color of texts and dimension texts</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="273"/>
      <source>Line spacing</source>
      <translation type="unfinished">Line spacing</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="280"/>
      <source>The spacing between different lines of text</source>
      <translation type="unfinished">The spacing between different lines of text</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="287"/>
      <source>Arrow style</source>
      <translation type="unfinished">Arrow style</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="294"/>
      <source>The type of dimension arrows</source>
      <translation type="unfinished">The type of dimension arrows</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="298"/>
      <source>Dot</source>
      <translation type="unfinished">Dot</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="303"/>
      <source>Circle</source>
      <translation type="unfinished">Circle</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="308"/>
      <source>Arrow</source>
      <translation type="unfinished">Arrow</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="313"/>
      <source>Tick</source>
      <translation type="unfinished">Tick</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="318"/>
      <source>Tick-2</source>
      <translation type="unfinished">Tick-2</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="326"/>
      <source>Arrow size</source>
      <translation type="unfinished">Arrow size</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="333"/>
      <source>The size of dimension arrows</source>
      <translation type="unfinished">The size of dimension arrows</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="343"/>
      <source>Show unit</source>
      <translation type="unfinished">Show unit</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="350"/>
      <source>If the unit suffix is shown on dimension texts or not</source>
      <translation type="unfinished">If the unit suffix is shown on dimension texts or not</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="363"/>
      <source>Unit override</source>
      <translation type="unfinished">Unit override</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="370"/>
      <source>The unit to use for dimensions. Leave blank to use current FreeCAD unit</source>
      <translation type="unfinished">The unit to use for dimensions. Leave blank to use current FreeCAD unit</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="382"/>
      <source>Apply above style to selected object(s)</source>
      <translation type="unfinished">Apply above style to selected object(s)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="385"/>
      <source>Selected</source>
      <translation type="unfinished">Selected</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="397"/>
      <source>Texts/dims</source>
      <translation type="unfinished">Texts/dims</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="14"/>
      <source>Working plane setup</source>
      <translation type="unfinished">Working plane setup</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="20"/>
      <source>Select a face or working plane proxy or 3 vertices.
Or choose one of the options below</source>
      <translation type="unfinished">Select a face or working plane proxy or 3 vertices.
Or choose one of the options below</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="31"/>
      <source>Sets the working plane to the XY plane (ground plane)</source>
      <translation type="unfinished">Sets the working plane to the XY plane (ground plane)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="34"/>
      <source>Top (XY)</source>
      <translation type="unfinished">Top (XY)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="41"/>
      <source>Sets the working plane to the XZ plane (front plane)</source>
      <translation type="unfinished">Sets the working plane to the XZ plane (front plane)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="44"/>
      <source>Front (XZ)</source>
      <translation type="unfinished">Front (XZ)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="51"/>
      <source>Sets the working plane to the YZ plane (side plane)</source>
      <translation type="unfinished">Sets the working plane to the YZ plane (side plane)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="54"/>
      <source>Side (YZ)</source>
      <translation type="unfinished">Side (YZ)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="61"/>
      <source>Sets the working plane facing the current view</source>
      <translation type="unfinished">Sets the working plane facing the current view</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="64"/>
      <source>Align to view</source>
      <translation type="unfinished">Align to view</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="71"/>
      <source>The working plane will align to the current
view each time a command is started</source>
      <translation type="unfinished">The working plane will align to the current
view each time a command is started</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="78"/>
      <source>Automatic</source>
      <translation type="unfinished">Automatic</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="87"/>
      <source>Offset</source>
      <translation type="unfinished">Offset</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="94"/>
      <source>An optional offset to give to the working plane
above its base position. Use this together with one
of the buttons above</source>
      <translation type="unfinished">An optional offset to give to the working plane
above its base position. Use this together with one
of the buttons above</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="106"/>
      <location filename="../ui/TaskSelectPlane.ui" line="118"/>
      <source>If this is selected, the working plane will be
centered on the current view when pressing one
of the buttons above</source>
      <translation type="unfinished">If this is selected, the working plane will be
centered on the current view when pressing one
of the buttons above</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="111"/>
      <source>Center plane on view</source>
      <translation type="unfinished">Center plane on view</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="135"/>
      <source>Or select a single vertex to move the current
working plane without changing its orientation.
Then, press the button below</source>
      <translation type="unfinished">Or select a single vertex to move the current
working plane without changing its orientation.
Then, press the button below</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="147"/>
      <source>Moves the working plane without changing its
orientation. If no point is selected, the plane
will be moved to the center of the view</source>
      <translation type="unfinished">Moves the working plane without changing its
orientation. If no point is selected, the plane
will be moved to the center of the view</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="152"/>
      <source>Move working plane</source>
      <translation type="unfinished">Move working plane</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="161"/>
      <location filename="../ui/TaskSelectPlane.ui" line="171"/>
      <source>The spacing between the smaller grid lines</source>
      <translation type="unfinished">The spacing between the smaller grid lines</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="164"/>
      <source>Grid spacing</source>
      <translation type="unfinished">Grid spacing</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="181"/>
      <location filename="../ui/TaskSelectPlane.ui" line="191"/>
      <source>The number of squares between each main line of the grid</source>
      <translation type="unfinished">The number of squares between each main line of the grid</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="184"/>
      <source>Main line every</source>
      <translation type="unfinished">Main line every</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="198"/>
      <source>Grid extension</source>
      <translation type="unfinished">Grid extension</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="205"/>
      <source> lines</source>
      <translation type="unfinished"> lines</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="218"/>
      <location filename="../ui/TaskSelectPlane.ui" line="230"/>
      <source>The distance at which a point can be snapped to
when approaching the mouse. You can also change this
value by using the [ and ] keys while drawing</source>
      <translation type="unfinished">The distance at which a point can be snapped to
when approaching the mouse. You can also change this
value by using the [ and ] keys while drawing</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="223"/>
      <source>Snapping radius</source>
      <translation type="unfinished">Snapping radius</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="241"/>
      <source>Centers the view on the current working plane</source>
      <translation type="unfinished">Centers the view on the current working plane</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="244"/>
      <source>Center view</source>
      <translation type="unfinished">Center view</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="251"/>
      <source>Resets the working plane to its previous position</source>
      <translation type="unfinished">Resets the working plane to its previous position</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="254"/>
      <source>Previous</source>
      <translation type="unfinished">Previous</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="14"/>
      <source>Form</source>
      <translation type="unfinished">Form</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="20"/>
      <source>PAT file:</source>
      <translation type="unfinished">PAT file:</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="27"/>
      <source>pattern files (*.pat)</source>
      <translation type="unfinished">pattern files (*.pat)</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="34"/>
      <source>Pattern:</source>
      <translation type="unfinished">Pattern:</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="44"/>
      <source>Scale</source>
      <translation type="unfinished">Scale</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="64"/>
      <source>Rotation:</source>
      <translation type="unfinished">Rotation:</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="71"/>
      <source>°</source>
      <translation type="unfinished">°</translation>
    </message>
  </context>
  <context>
    <name>Gui::Dialog::DlgSettingsDraft</name>
    <message>
      <location filename="../ui/preferences-draft.ui" line="14"/>
      <source>General settings</source>
      <translation type="unfinished">General settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="32"/>
      <source>General Draft Settings</source>
      <translation type="unfinished">General Draft Settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="40"/>
      <source>Default working plane</source>
      <translation type="unfinished">Default working plane</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="67"/>
      <source>None</source>
      <translation type="unfinished">None</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="72"/>
      <source>XY (Top)</source>
      <translation type="unfinished">XY (Top)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="77"/>
      <source>XZ (Front)</source>
      <translation type="unfinished">XZ (Front)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="82"/>
      <source>YZ (Side)</source>
      <translation type="unfinished">YZ (Side)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="94"/>
      <source>Internal precision level</source>
      <translation type="unfinished">Internal precision level</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="120"/>
      <source>The number of decimals in internal coordinates operations (for ex. 3 = 0.001). Values between 6 and 8 are usually considered the best trade-off among FreeCAD users.</source>
      <translation type="unfinished">The number of decimals in internal coordinates operations (for ex. 3 = 0.001). Values between 6 and 8 are usually considered the best trade-off among FreeCAD users.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="143"/>
      <source>Tolerance</source>
      <translation>Точность</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="169"/>
      <source>This is the value used by functions that use a tolerance.
Values with differences below this value will be treated as same. This value will be obsoleted soon so the precision level above controls both.</source>
      <translation type="unfinished">This is the value used by functions that use a tolerance.
Values with differences below this value will be treated as same. This value will be obsoleted soon so the precision level above controls both.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="194"/>
      <source>If this option is checked, the layers drop-down list will also show groups, allowing you to automatically add objects to groups too.</source>
      <translation type="unfinished">If this option is checked, the layers drop-down list will also show groups, allowing you to automatically add objects to groups too.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="197"/>
      <source>Show groups in layers list drop-down button</source>
      <translation type="unfinished">Show groups in layers list drop-down button</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="216"/>
      <source>Draft tools options</source>
      <translation type="unfinished">Draft tools options</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="230"/>
      <source>When drawing lines, set focus on Length instead of X coordinate.
This allows to point the direction and type the distance.</source>
      <translation type="unfinished">When drawing lines, set focus on Length instead of X coordinate.
This allows to point the direction and type the distance.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="234"/>
      <source>Set focus on Length instead of X coordinate</source>
      <translation type="unfinished">Set focus on Length instead of X coordinate</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="247"/>
      <source>Normally, after copying objects, the copies get selected.
If this option is checked, the base objects will be selected instead.</source>
      <translation type="unfinished">Normally, after copying objects, the copies get selected.
If this option is checked, the base objects will be selected instead.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="251"/>
      <source>Select base objects after copying</source>
      <translation type="unfinished">Select base objects after copying</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="264"/>
      <source>If this option is set, when creating Draft objects on top of an existing face of another object, the &quot;Support&quot; property of the Draft object will be set to the base object. This was the standard behaviour before FreeCAD 0.19</source>
      <translation type="unfinished">If this option is set, when creating Draft objects on top of an existing face of another object, the &quot;Support&quot; property of the Draft object will be set to the base object. This was the standard behaviour before FreeCAD 0.19</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="267"/>
      <source>Set the Support property when possible</source>
      <translation type="unfinished">Set the Support property when possible</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="280"/>
      <source>If this is checked, objects will appear as filled by default.
Otherwise, they will appear as wireframe</source>
      <translation type="unfinished">If this is checked, objects will appear as filled by default.
Otherwise, they will appear as wireframe</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="284"/>
      <source>Fill objects with faces whenever possible</source>
      <translation type="unfinished">Fill objects with faces whenever possible</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="300"/>
      <source>If this is checked, copy mode will be kept across command,
otherwise commands will always start in no-copy mode</source>
      <translation type="unfinished">If this is checked, copy mode will be kept across command,
otherwise commands will always start in no-copy mode</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="304"/>
      <source>Global copy mode</source>
      <translation type="unfinished">Global copy mode</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="320"/>
      <source>Force Draft Tools to create Part primitives instead of Draft objects.
Note that this is not fully supported, and many object will be not editable with Draft Modifiers.</source>
      <translation type="unfinished">Force Draft Tools to create Part primitives instead of Draft objects.
Note that this is not fully supported, and many object will be not editable with Draft Modifiers.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="324"/>
      <source>Use Part Primitives when available</source>
      <translation type="unfinished">Use Part Primitives when available</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="341"/>
      <source>Prefix labels of Clones with:</source>
      <translation type="unfinished">Prefix labels of Clones with:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="372"/>
      <source>Construction Geometry</source>
      <translation type="unfinished">Construction Geometry</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="380"/>
      <source>Construction group name</source>
      <translation type="unfinished">Construction group name</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="387"/>
      <source>This is the default group name for construction geometry</source>
      <translation type="unfinished">This is the default group name for construction geometry</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="390"/>
      <source>Construction</source>
      <translation type="unfinished">Construction</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="407"/>
      <source>Construction geometry color</source>
      <translation type="unfinished">Construction geometry color</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="427"/>
      <source>This is the default color for objects being drawn while in construction mode.</source>
      <translation type="unfinished">This is the default color for objects being drawn while in construction mode.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="14"/>
      <source>User interface settings</source>
      <translation type="unfinished">User interface settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="26"/>
      <source>In-Command Shortcuts</source>
      <translation type="unfinished">In-Command Shortcuts</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="37"/>
      <source>Relative</source>
      <translation type="unfinished">Relative</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="59"/>
      <source>R</source>
      <translation type="unfinished">R</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="81"/>
      <source>Continue</source>
      <translation type="unfinished">Continue</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="103"/>
      <source>T</source>
      <translation type="unfinished">T</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="125"/>
      <source>Close</source>
      <translation>Закрыть</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="147"/>
      <source>O</source>
      <translation type="unfinished">O</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="169"/>
      <source>Copy</source>
      <translation type="unfinished">Copy</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="191"/>
      <source>P</source>
      <translation type="unfinished">P</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="213"/>
      <source>Subelement Mode</source>
      <translation type="unfinished">Subelement Mode</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="235"/>
      <source>D</source>
      <translation type="unfinished">D</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="257"/>
      <source>Fill</source>
      <translation type="unfinished">Fill</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="279"/>
      <source>L</source>
      <translation type="unfinished">L</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="301"/>
      <source>Exit</source>
      <translation type="unfinished">Exit</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="323"/>
      <source>A</source>
      <translation type="unfinished">A</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="345"/>
      <source>Select Edge</source>
      <translation type="unfinished">Select Edge</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="367"/>
      <source>E</source>
      <translation type="unfinished">E</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="389"/>
      <source>Add Hold</source>
      <translation type="unfinished">Add Hold</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="411"/>
      <source>Q</source>
      <translation type="unfinished">Q</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="433"/>
      <source>Length</source>
      <translation>Длина</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="455"/>
      <source>H</source>
      <translation type="unfinished">H</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="477"/>
      <source>Wipe</source>
      <translation type="unfinished">Wipe</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="499"/>
      <source>W</source>
      <translation type="unfinished">W</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="521"/>
      <source>Set WP</source>
      <translation type="unfinished">Set WP</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="543"/>
      <source>U</source>
      <translation type="unfinished">U</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="565"/>
      <source>Cycle Snap</source>
      <translation type="unfinished">Cycle Snap</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="587"/>
      <source>`</source>
      <translation type="unfinished">`</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="609"/>
      <source>Global</source>
      <translation type="unfinished">Global</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="631"/>
      <source>G</source>
      <translation type="unfinished">G</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="653"/>
      <source>Snap</source>
      <translation type="unfinished">Snap</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="675"/>
      <source>S</source>
      <translation type="unfinished">S</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="697"/>
      <source>Increase Radius</source>
      <translation type="unfinished">Increase Radius</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="719"/>
      <source>[</source>
      <translation type="unfinished">[</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="741"/>
      <source>Decrease Radius</source>
      <translation type="unfinished">Decrease Radius</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="763"/>
      <source>]</source>
      <translation type="unfinished">]</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="785"/>
      <source>Restrict X</source>
      <translation type="unfinished">Restrict X</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="807"/>
      <source>X</source>
      <translation type="unfinished">X</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="829"/>
      <source>Restrict Y</source>
      <translation type="unfinished">Restrict Y</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="851"/>
      <source>Y</source>
      <translation type="unfinished">Y</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="873"/>
      <source>Restrict Z</source>
      <translation type="unfinished">Restrict Z</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="895"/>
      <source>Z</source>
      <translation type="unfinished">Z</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="928"/>
      <source>Enable draft statusbar customization</source>
      <translation type="unfinished">Enable draft statusbar customization</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="931"/>
      <source>Draft Statusbar</source>
      <translation type="unfinished">Draft Statusbar</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="951"/>
      <source>Enable snap statusbar widget</source>
      <translation type="unfinished">Enable snap statusbar widget</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="954"/>
      <source>Draft snap widget</source>
      <translation type="unfinished">Draft snap widget</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="970"/>
      <source>Enable draft statusbar annotation scale widget</source>
      <translation type="unfinished">Enable draft statusbar annotation scale widget</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="973"/>
      <source>Annotation scale widget</source>
      <translation type="unfinished">Annotation scale widget</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="14"/>
      <source>Grid and snapping</source>
      <translation type="unfinished">Grid and snapping</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="35"/>
      <source>Snapping</source>
      <translation type="unfinished">Snapping</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="43"/>
      <source>If this is checked, snapping is activated without the need to press the snap mod key</source>
      <translation type="unfinished">If this is checked, snapping is activated without the need to press the snap mod key</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="46"/>
      <source>Always snap (disable snap mod)</source>
      <translation type="unfinished">Always snap (disable snap mod)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="66"/>
      <source>Constrain mod</source>
      <translation type="unfinished">Constrain mod</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="86"/>
      <source>The Constraining modifier key</source>
      <translation type="unfinished">The Constraining modifier key</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="96"/>
      <location filename="../ui/preferences-draftsnap.ui" line="151"/>
      <location filename="../ui/preferences-draftsnap.ui" line="206"/>
      <source>Shift</source>
      <translation type="unfinished">Shift</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="101"/>
      <location filename="../ui/preferences-draftsnap.ui" line="156"/>
      <location filename="../ui/preferences-draftsnap.ui" line="211"/>
      <source>Ctrl</source>
      <translation type="unfinished">Ctrl</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="106"/>
      <location filename="../ui/preferences-draftsnap.ui" line="161"/>
      <location filename="../ui/preferences-draftsnap.ui" line="216"/>
      <source>Alt</source>
      <translation type="unfinished">Alt</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="118"/>
      <source>Snap mod</source>
      <translation type="unfinished">Snap mod</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="138"/>
      <source>The snap modifier key</source>
      <translation type="unfinished">The snap modifier key</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="173"/>
      <source>Alt mod</source>
      <translation type="unfinished">Alt mod</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="193"/>
      <source>The Alt modifier key</source>
      <translation type="unfinished">The Alt modifier key</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="228"/>
      <source>If checked, the Snap toolbar will be shown whenever you use snapping</source>
      <translation type="unfinished">If checked, the Snap toolbar will be shown whenever you use snapping</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="231"/>
      <source>Show Draft Snap toolbar</source>
      <translation type="unfinished">Show Draft Snap toolbar</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="251"/>
      <source>Hide Draft snap toolbar after use</source>
      <translation type="unfinished">Hide Draft snap toolbar after use</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="272"/>
      <source>Grid</source>
      <translation type="unfinished">Grid</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="278"/>
      <source>If checked, a grid will appear when drawing</source>
      <translation type="unfinished">If checked, a grid will appear when drawing</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="281"/>
      <source>Use grid</source>
      <translation type="unfinished">Use grid</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="300"/>
      <source>If checked, the Draft grid will always be visible when the Draft workbench is active. Otherwise only when using a command</source>
      <translation type="unfinished">If checked, the Draft grid will always be visible when the Draft workbench is active. Otherwise only when using a command</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="303"/>
      <source>Always show the grid</source>
      <translation type="unfinished">Always show the grid</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="319"/>
      <source>If checked, an additional border is displayed around the grid, showing the main square size in the bottom left border</source>
      <translation type="unfinished">If checked, an additional border is displayed around the grid, showing the main square size in the bottom left border</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="322"/>
      <source>Show grid border</source>
      <translation type="unfinished">Show grid border</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="338"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the outline of a human figure is displayed at the bottom left corner of the grid. This option is only effective if the BIM workbench is installed and if &amp;quot;Show grid border&amp;quot; option is enabled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the outline of a human figure is displayed at the bottom left corner of the grid. This option is only effective if the BIM workbench is installed and if &amp;quot;Show grid border&amp;quot; option is enabled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="341"/>
      <source>Show human figure</source>
      <translation type="unfinished">Show human figure</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="357"/>
      <source>If set, the grid will have its two main axes colored in red, green or blue when they match global axes</source>
      <translation type="unfinished">If set, the grid will have its two main axes colored in red, green or blue when they match global axes</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="360"/>
      <source>Use colored axes</source>
      <translation type="unfinished">Use colored axes</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="381"/>
      <source>Main lines every</source>
      <translation type="unfinished">Main lines every</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="404"/>
      <source>Mainlines will be drawn thicker. Specify here how many squares between mainlines.</source>
      <translation type="unfinished">Mainlines will be drawn thicker. Specify here how many squares between mainlines.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="430"/>
      <source>Grid spacing</source>
      <translation type="unfinished">Grid spacing</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="453"/>
      <source>The spacing between each grid line</source>
      <translation type="unfinished">The spacing between each grid line</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="485"/>
      <source>Grid size</source>
      <translation type="unfinished">Grid size</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="505"/>
      <source>The number of horizontal or vertical lines of the grid</source>
      <translation type="unfinished">The number of horizontal or vertical lines of the grid</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="511"/>
      <source> lines</source>
      <translation type="unfinished"> lines</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="534"/>
      <source>Grid color and transparency</source>
      <translation type="unfinished">Grid color and transparency</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="554"/>
      <source>The color of the grid</source>
      <translation type="unfinished">The color of the grid</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="574"/>
      <source>The overall transparency of the grid</source>
      <translation type="unfinished">The overall transparency of the grid</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="595"/>
      <source>Draft Edit preferences</source>
      <translation type="unfinished">Draft Edit preferences</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="598"/>
      <source>Edit</source>
      <translation>Редактировать</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="621"/>
      <source>Maximum number of contemporary edited objects</source>
      <translation type="unfinished">Maximum number of contemporary edited objects</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="644"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sets the maximum number of objects Draft Edit&lt;/p&gt;&lt;p&gt;can process at the same time&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sets the maximum number of objects Draft Edit&lt;/p&gt;&lt;p&gt;can process at the same time&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="691"/>
      <source>Draft edit pick radius</source>
      <translation type="unfinished">Draft edit pick radius</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="714"/>
      <source>Controls pick radius of edit nodes</source>
      <translation type="unfinished">Controls pick radius of edit nodes</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="14"/>
      <source>Texts and dimensions</source>
      <translation type="unfinished">Texts and dimensions</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="26"/>
      <source>Text settings</source>
      <translation type="unfinished">Text settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="34"/>
      <source>Font family</source>
      <translation type="unfinished">Font family</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="47"/>
      <source>This is the default font name for all Draft texts and dimensions.
It can be a font name such as &quot;Arial&quot;, a default style such as &quot;sans&quot;, &quot;serif&quot;
or &quot;mono&quot;, or a family such as &quot;Arial,Helvetica,sans&quot; or a name with a style
such as &quot;Arial:Bold&quot;</source>
      <translation type="unfinished">This is the default font name for all Draft texts and dimensions.
It can be a font name such as &quot;Arial&quot;, a default style such as &quot;sans&quot;, &quot;serif&quot;
or &quot;mono&quot;, or a family such as &quot;Arial,Helvetica,sans&quot; or a name with a style
such as &quot;Arial:Bold&quot;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="59"/>
      <source>Internal font</source>
      <translation type="unfinished">Internal font</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="76"/>
      <source>Font size</source>
      <translation type="unfinished">Font size</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="89"/>
      <source>Default height for texts and dimensions</source>
      <translation type="unfinished">Default height for texts and dimensions</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="209"/>
      <location filename="../ui/preferences-dxf.ui" line="535"/>
      <location filename="../ui/preferences-drafttexts.ui" line="92"/>
      <location filename="../ui/preferences-drafttexts.ui" line="211"/>
      <location filename="../ui/preferences-drafttexts.ui" line="247"/>
      <location filename="../ui/preferences-drafttexts.ui" line="283"/>
      <location filename="../ui/preferences-drafttexts.ui" line="365"/>
      <location filename="../ui/preferences-drafttexts.ui" line="432"/>
      <source>mm</source>
      <translation type="unfinished">mm</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="116"/>
      <source>Dimension settings</source>
      <translation type="unfinished">Dimension settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="124"/>
      <source>Display mode</source>
      <translation type="unfinished">Display mode</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="144"/>
      <source>text above (2D)</source>
      <translation type="unfinished">text above (2D)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="149"/>
      <source> text inside (3D)</source>
      <translation type="unfinished"> text inside (3D)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="161"/>
      <source>Number of decimals</source>
      <translation type="unfinished">Number of decimals</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="201"/>
      <source>Extension lines size</source>
      <translation type="unfinished">Extension lines size</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="208"/>
      <source>The default size of dimensions extension lines</source>
      <translation type="unfinished">The default size of dimensions extension lines</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="237"/>
      <source>Extension line overshoot</source>
      <translation type="unfinished">Extension line overshoot</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="244"/>
      <source>The default length of extension line above dimension line</source>
      <translation type="unfinished">The default length of extension line above dimension line</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="273"/>
      <source>Dimension line overshoot</source>
      <translation type="unfinished">Dimension line overshoot</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="280"/>
      <source>The default distance the dimension line is extended past extension lines</source>
      <translation type="unfinished">The default distance the dimension line is extended past extension lines</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="309"/>
      <source>Arrows style</source>
      <translation type="unfinished">Arrows style</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="323"/>
      <source>Dot</source>
      <translation type="unfinished">Dot</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="328"/>
      <source>Circle</source>
      <translation type="unfinished">Circle</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="333"/>
      <source>Arrow</source>
      <translation type="unfinished">Arrow</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="338"/>
      <source>Tick</source>
      <translation type="unfinished">Tick</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="343"/>
      <source>Tick-2</source>
      <translation type="unfinished">Tick-2</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="355"/>
      <source>Arrows size</source>
      <translation type="unfinished">Arrows size</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="362"/>
      <source>The default size of arrows</source>
      <translation type="unfinished">The default size of arrows</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="388"/>
      <source>Text orientation</source>
      <translation type="unfinished">Text orientation</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="395"/>
      <source>This is the orientation of the dimension texts when those dimensions are vertical. Default is left, which is the ISO standard.</source>
      <translation type="unfinished">This is the orientation of the dimension texts when those dimensions are vertical. Default is left, which is the ISO standard.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="405"/>
      <source>Left (ISO standard)</source>
      <translation type="unfinished">Left (ISO standard)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="410"/>
      <source>Right</source>
      <translation type="unfinished">Right</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="422"/>
      <source>Text spacing</source>
      <translation type="unfinished">Text spacing</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="429"/>
      <source>The space between the dimension line and the dimension text</source>
      <translation type="unfinished">The space between the dimension line and the dimension text</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="455"/>
      <source>Show the unit suffix in dimensions</source>
      <translation type="unfinished">Show the unit suffix in dimensions</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="475"/>
      <source>Override unit</source>
      <translation type="unfinished">Override unit</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="495"/>
      <source>By leaving this field blank, the dimension measurements will be shown in the current unit defined in FreeCAD. By indicating a unit here such as m or cm, you can force new dimensions to be shown in that unit.</source>
      <translation type="unfinished">By leaving this field blank, the dimension measurements will be shown in the current unit defined in FreeCAD. By indicating a unit here such as m or cm, you can force new dimensions to be shown in that unit.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="513"/>
      <source>ShapeString settings</source>
      <translation type="unfinished">ShapeString settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="521"/>
      <source>Default ShapeString font file</source>
      <translation type="unfinished">Default ShapeString font file</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="534"/>
      <source>Select a font file</source>
      <translation type="unfinished">Select a font file</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="14"/>
      <source>Visual settings</source>
      <translation type="unfinished">Visual settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="26"/>
      <source>Visual Settings</source>
      <translation type="unfinished">Visual Settings</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="34"/>
      <source>Snap symbols style</source>
      <translation type="unfinished">Snap symbols style</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="54"/>
      <source>Draft classic style</source>
      <translation type="unfinished">Draft classic style</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="59"/>
      <source>Bitsnpieces style</source>
      <translation type="unfinished">Bitsnpieces style</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="80"/>
      <source>Color</source>
      <translation type="unfinished">Color</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="87"/>
      <source>The default color for snap symbols</source>
      <translation type="unfinished">The default color for snap symbols</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="111"/>
      <source>Check this if you want to use the color/linewidth from the toolbar as default</source>
      <translation type="unfinished">Check this if you want to use the color/linewidth from the toolbar as default</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="114"/>
      <source>Save current color and linewidth across sessions</source>
      <translation type="unfinished">Save current color and linewidth across sessions</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="131"/>
      <source>If checked, a widget indicating the current working plane orientation appears during drawing operations</source>
      <translation type="unfinished">If checked, a widget indicating the current working plane orientation appears during drawing operations</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="134"/>
      <source>Show Working Plane tracker</source>
      <translation type="unfinished">Show Working Plane tracker</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="151"/>
      <source>Default template sheet</source>
      <translation type="unfinished">Default template sheet</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="177"/>
      <source>The default template to use when creating a new drawing sheet</source>
      <translation type="unfinished">The default template to use when creating a new drawing sheet</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="194"/>
      <source>Alternate SVG patterns location</source>
      <translation type="unfinished">Alternate SVG patterns location</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="220"/>
      <source>Here you can specify a directory with custom SVG files containing &lt;pattern&gt; definitions to be added to the standard patterns</source>
      <translation type="unfinished">Here you can specify a directory with custom SVG files containing &lt;pattern&gt; definitions to be added to the standard patterns</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="237"/>
      <source>SVG pattern resolution</source>
      <translation type="unfinished">SVG pattern resolution</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="257"/>
      <source>The resolution to draw the patterns in. Default value is 128. Higher values give better resolutions, lower values make drawing faster</source>
      <translation type="unfinished">The resolution to draw the patterns in. Default value is 128. Higher values give better resolutions, lower values make drawing faster</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="280"/>
      <source>SVG pattern default size</source>
      <translation type="unfinished">SVG pattern default size</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="300"/>
      <source>The default size for SVG patterns</source>
      <translation type="unfinished">The default size for SVG patterns</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="326"/>
      <source>Check this if you want to preserve colors of faces while doing downgrade and upgrade (splitFaces and makeShell only)</source>
      <translation type="unfinished">Check this if you want to preserve colors of faces while doing downgrade and upgrade (splitFaces and makeShell only)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="329"/>
      <source>Preserve colors of faces during downgrade/upgrade</source>
      <translation type="unfinished">Preserve colors of faces during downgrade/upgrade</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="346"/>
      <source>Check this if you want the face names to derive from the originating object name and vice versa while doing downgrade/upgrade (splitFaces and makeShell only)</source>
      <translation type="unfinished">Check this if you want the face names to derive from the originating object name and vice versa while doing downgrade/upgrade (splitFaces and makeShell only)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="349"/>
      <source>Preserve names of faces during downgrade/upgrade</source>
      <translation type="unfinished">Preserve names of faces during downgrade/upgrade</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="367"/>
      <source>Drawing view line definitions</source>
      <translation type="unfinished">Drawing view line definitions</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="375"/>
      <source>Dashed line definition</source>
      <translation type="unfinished">Dashed line definition</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="395"/>
      <location filename="../ui/preferences-draftvisual.ui" line="438"/>
      <location filename="../ui/preferences-draftvisual.ui" line="481"/>
      <source>An SVG linestyle definition</source>
      <translation type="unfinished">An SVG linestyle definition</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="398"/>
      <source>0.09,0.05</source>
      <translation type="unfinished">0.09,0.05</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="418"/>
      <source>Dashdot line definition</source>
      <translation type="unfinished">Dashdot line definition</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="441"/>
      <source>0.09,0.05,0.02,0.05</source>
      <translation type="unfinished">0.09,0.05,0.02,0.05</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="461"/>
      <source>Dotted line definition</source>
      <translation type="unfinished">Dotted line definition</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="484"/>
      <source>0.02,0.02</source>
      <translation type="unfinished">0.02,0.02</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="14"/>
      <source>DWG</source>
      <translation type="unfinished">DWG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="26"/>
      <source>DWG conversion</source>
      <translation type="unfinished">DWG conversion</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="34"/>
      <source>Conversion method:</source>
      <translation type="unfinished">Conversion method:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="41"/>
      <source>This is the method FreeCAD will use to convert DWG files to DXF. If &quot;Automatic&quot; is chosen, FreeCAD will try to find one of the following converters in the same order as they are shown here. If FreeCAD is unable to find any, you might need to choose a specific converter and indicate its path here under. Choose the &quot;dwg2dxf&quot; utility if using LibreDWG, &quot;ODAFileConverter&quot; if using the ODA file converter, or the &quot;dwg2dwg&quot; utility if using the pro version of QCAD.</source>
      <translation type="unfinished">This is the method FreeCAD will use to convert DWG files to DXF. If &quot;Automatic&quot; is chosen, FreeCAD will try to find one of the following converters in the same order as they are shown here. If FreeCAD is unable to find any, you might need to choose a specific converter and indicate its path here under. Choose the &quot;dwg2dxf&quot; utility if using LibreDWG, &quot;ODAFileConverter&quot; if using the ODA file converter, or the &quot;dwg2dwg&quot; utility if using the pro version of QCAD.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="51"/>
      <source>Automatic</source>
      <translation type="unfinished">Automatic</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="56"/>
      <source>LibreDWG</source>
      <translation type="unfinished">LibreDWG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="61"/>
      <source>ODA Converter</source>
      <translation type="unfinished">ODA Converter</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="66"/>
      <source>QCAD pro</source>
      <translation type="unfinished">QCAD pro</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="78"/>
      <source>Path to file converter</source>
      <translation type="unfinished">Path to file converter</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="85"/>
      <source>The path to your DWG file converter executable</source>
      <translation type="unfinished">The path to your DWG file converter executable</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="100"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note:&lt;/span&gt; DXF options apply to DWG files as well.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note:&lt;/span&gt; DXF options apply to DWG files as well.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="14"/>
      <source>DXF</source>
      <translation type="unfinished">DXF</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="35"/>
      <source>This preferences dialog will be shown when importing/ exporting DXF files</source>
      <translation type="unfinished">This preferences dialog will be shown when importing/ exporting DXF files</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="38"/>
      <source>Show this dialog when importing and exporting</source>
      <translation type="unfinished">Show this dialog when importing and exporting</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="51"/>
      <source>Python importer is used, otherwise the newer C++ is used.
Note: C++ importer is faster, but is not as featureful yet</source>
      <translation type="unfinished">Python importer is used, otherwise the newer C++ is used.
Note: C++ importer is faster, but is not as featureful yet</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="55"/>
      <source>Use legacy python importer</source>
      <translation type="unfinished">Use legacy python importer</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="71"/>
      <source>Python exporter is used, otherwise the newer C++ is used.
Note: C++ exporter is faster, but is not as featureful yet</source>
      <translation type="unfinished">Python exporter is used, otherwise the newer C++ is used.
Note: C++ exporter is faster, but is not as featureful yet</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="75"/>
      <source>Use legacy python exporter</source>
      <translation type="unfinished">Use legacy python exporter</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="88"/>
      <source>Automatic update (legacy importer only)</source>
      <translation type="unfinished">Automatic update (legacy importer only)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="96"/>
      <source>Allow FreeCAD to download the Python converter for DXF import and export.
You can also do this manually by installing the &quot;dxf_library&quot; workbench
from the Addon Manager.</source>
      <translation type="unfinished">Allow FreeCAD to download the Python converter for DXF import and export.
You can also do this manually by installing the &quot;dxf_library&quot; workbench
from the Addon Manager.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="101"/>
      <source>Allow FreeCAD to automatically download and update the DXF libraries</source>
      <translation type="unfinished">Allow FreeCAD to automatically download and update the DXF libraries</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="26"/>
      <location filename="../ui/preferences-dxf.ui" line="119"/>
      <location filename="../ui/preferences-oca.ui" line="26"/>
      <source>Import options</source>
      <translation type="unfinished">Import options</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="140"/>
      <source>Note: Not all the options below are used by the new importer yet</source>
      <translation type="unfinished">Note: Not all the options below are used by the new importer yet</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="149"/>
      <source>Import</source>
      <translation type="unfinished">Import</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="156"/>
      <source>If unchecked, texts and mtexts won&apos;t be imported</source>
      <translation type="unfinished">If unchecked, texts and mtexts won&apos;t be imported</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="159"/>
      <source>texts and dimensions</source>
      <translation type="unfinished">texts and dimensions</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="172"/>
      <source>If unchecked, points won&apos;t be imported</source>
      <translation type="unfinished">If unchecked, points won&apos;t be imported</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="175"/>
      <source>points</source>
      <translation type="unfinished">points</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="188"/>
      <source>If checked, paper space objects will be imported too</source>
      <translation type="unfinished">If checked, paper space objects will be imported too</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="191"/>
      <source>layouts</source>
      <translation type="unfinished">layouts</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="204"/>
      <source>If you want the non-named blocks (beginning with a *) to be imported too</source>
      <translation type="unfinished">If you want the non-named blocks (beginning with a *) to be imported too</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="207"/>
      <source>*blocks</source>
      <translation type="unfinished">*blocks</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="224"/>
      <source>Create</source>
      <translation type="unfinished">Create</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="231"/>
      <source>Only standard Part objects will be created (fastest)</source>
      <translation type="unfinished">Only standard Part objects will be created (fastest)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="234"/>
      <source>simple Part shapes</source>
      <translation type="unfinished">simple Part shapes</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="250"/>
      <source>Parametric Draft objects will be created whenever possible</source>
      <translation type="unfinished">Parametric Draft objects will be created whenever possible</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="253"/>
      <source>Draft objects</source>
      <translation type="unfinished">Draft objects</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="266"/>
      <source>Sketches will be created whenever possible</source>
      <translation type="unfinished">Sketches will be created whenever possible</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="269"/>
      <source>Sketches</source>
      <translation type="unfinished">Sketches</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="289"/>
      <source>Scale factor to apply to imported files</source>
      <translation type="unfinished">Scale factor to apply to imported files</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="309"/>
      <source>Scale factor to apply to DXF files on import.
The factor is the conversion between the unit of your DXF file and millimeters.
Example: for files in millimeters: 1, in centimeters: 10,
                             in meters: 1000, in inches: 25.4, in feet: 304.8</source>
      <translation type="unfinished">Scale factor to apply to DXF files on import.
The factor is the conversion between the unit of your DXF file and millimeters.
Example: for files in millimeters: 1, in centimeters: 10,
                             in meters: 1000, in inches: 25.4, in feet: 304.8</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="338"/>
      <source>Colors will be retrieved from the DXF objects whenever possible.
Otherwise default colors will be applied. </source>
      <translation type="unfinished">Colors will be retrieved from the DXF objects whenever possible.
Otherwise default colors will be applied. </translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="342"/>
      <source>Get original colors from the DXF file</source>
      <translation type="unfinished">Get original colors from the DXF file</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="359"/>
      <source>FreeCAD will try to join coincident objects into wires.
Note that this can take a while!</source>
      <translation type="unfinished">FreeCAD will try to join coincident objects into wires.
Note that this can take a while!</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="363"/>
      <source>Join geometry</source>
      <translation type="unfinished">Join geometry</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="380"/>
      <source>Objects from the same layers will be joined into Draft Blocks,
turning the display faster, but making them less easily editable </source>
      <translation type="unfinished">Objects from the same layers will be joined into Draft Blocks,
turning the display faster, but making them less easily editable </translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="384"/>
      <source>Group layers into blocks</source>
      <translation type="unfinished">Group layers into blocks</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="401"/>
      <source>Imported texts will get the standard Draft Text size,
instead of the size they have in the DXF document</source>
      <translation type="unfinished">Imported texts will get the standard Draft Text size,
instead of the size they have in the DXF document</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="405"/>
      <source>Use standard font size for texts</source>
      <translation type="unfinished">Use standard font size for texts</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="422"/>
      <source>If this is checked, DXF layers will be imported as Draft Layers</source>
      <translation type="unfinished">If this is checked, DXF layers will be imported as Draft Layers</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="425"/>
      <source>Use Layers</source>
      <translation type="unfinished">Use Layers</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="445"/>
      <source>Hatches will be converted into simple wires</source>
      <translation type="unfinished">Hatches will be converted into simple wires</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="448"/>
      <source>Import hatch boundaries as wires</source>
      <translation type="unfinished">Import hatch boundaries as wires</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="465"/>
      <source>If polylines have a width defined, they will be rendered
as closed wires with correct width</source>
      <translation type="unfinished">If polylines have a width defined, they will be rendered
as closed wires with correct width</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="469"/>
      <source>Render polylines with width</source>
      <translation type="unfinished">Render polylines with width</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="486"/>
      <source>Ellipse export is poorly supported. Use this to export them as polylines instead.</source>
      <translation type="unfinished">Ellipse export is poorly supported. Use this to export them as polylines instead.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="489"/>
      <source>Treat ellipses and splines as polylines</source>
      <translation type="unfinished">Treat ellipses and splines as polylines</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="518"/>
      <source>Max Spline Segment:</source>
      <translation type="unfinished">Max Spline Segment:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="528"/>
      <source>Maximum length of each of the polyline segments.
If it is set to &apos;0&apos; the whole spline is treated as a straight segment.</source>
      <translation type="unfinished">Maximum length of each of the polyline segments.
If it is set to &apos;0&apos; the whole spline is treated as a straight segment.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="107"/>
      <location filename="../ui/preferences-dxf.ui" line="559"/>
      <source>Export options</source>
      <translation type="unfinished">Export options</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="567"/>
      <source>All objects containing faces will be exported as 3D polyfaces</source>
      <translation type="unfinished">All objects containing faces will be exported as 3D polyfaces</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="570"/>
      <source>Export 3D objects as polyface meshes</source>
      <translation type="unfinished">Export 3D objects as polyface meshes</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="587"/>
      <source>Drawing Views will be exported as blocks.
This might fail for post DXF R12 templates.</source>
      <translation type="unfinished">Drawing Views will be exported as blocks.
This might fail for post DXF R12 templates.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="591"/>
      <source>Export Drawing Views as blocks</source>
      <translation type="unfinished">Export Drawing Views as blocks</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="611"/>
      <source>Exported objects will be projected to reflect the current view direction</source>
      <translation type="unfinished">Exported objects will be projected to reflect the current view direction</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="614"/>
      <source>Project exported objects along current view direction</source>
      <translation type="unfinished">Project exported objects along current view direction</translation>
    </message>
    <message>
      <location filename="../ui/preferences-oca.ui" line="14"/>
      <source>OCA</source>
      <translation type="unfinished">OCA</translation>
    </message>
    <message>
      <location filename="../ui/preferences-oca.ui" line="46"/>
      <source>Check this if you want the areas (3D faces) to be imported too.</source>
      <translation type="unfinished">Check this if you want the areas (3D faces) to be imported too.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-oca.ui" line="49"/>
      <source>Import OCA areas</source>
      <translation type="unfinished">Import OCA areas</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="14"/>
      <source>SVG</source>
      <translation type="unfinished">SVG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="40"/>
      <source>Import style</source>
      <translation type="unfinished">Import style</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="47"/>
      <source>Method chosen for importing SVG object color to FreeCAD</source>
      <translation type="unfinished">Method chosen for importing SVG object color to FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="60"/>
      <source>None (fastest)</source>
      <translation type="unfinished">None (fastest)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="65"/>
      <source>Use default color and linewidth</source>
      <translation type="unfinished">Use default color and linewidth</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="70"/>
      <source>Original color and linewidth</source>
      <translation type="unfinished">Original color and linewidth</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="82"/>
      <source>If checked, no units conversion will occur.
One unit in the SVG file will translate as one millimeter. </source>
      <translation type="unfinished">If checked, no units conversion will occur.
One unit in the SVG file will translate as one millimeter. </translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="86"/>
      <source>Disable units scaling</source>
      <translation type="unfinished">Disable units scaling</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="121"/>
      <source>Export style</source>
      <translation type="unfinished">Export style</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="128"/>
      <source>Style of SVG file to write when exporting a sketch</source>
      <translation type="unfinished">Style of SVG file to write when exporting a sketch</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="141"/>
      <source>Translated (for print &amp; display)</source>
      <translation type="unfinished">Translated (for print &amp; display)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="146"/>
      <source>Raw (for CAM)</source>
      <translation type="unfinished">Raw (for CAM)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="158"/>
      <source>All white lines will appear in black in the SVG for better readability against white backgrounds</source>
      <translation type="unfinished">All white lines will appear in black in the SVG for better readability against white backgrounds</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="161"/>
      <source>Translate white line color to black</source>
      <translation type="unfinished">Translate white line color to black</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="181"/>
      <source>Max segment length for discretized arcs</source>
      <translation type="unfinished">Max segment length for discretized arcs</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="204"/>
      <source>Versions of Open CASCADE older than version 6.8 don&apos;t support arc projection.
In this case arcs will be discretized into small line segments.
This value is the maximum segment length. </source>
      <translation type="unfinished">Versions of Open CASCADE older than version 6.8 don&apos;t support arc projection.
In this case arcs will be discretized into small line segments.
This value is the maximum segment length. </translation>
    </message>
  </context>
  <context>
    <name>ImportAirfoilDAT</name>
    <message>
      <location filename="../../importAirfoilDAT.py" line="193"/>
      <source>Did not find enough coordinates</source>
      <translation type="unfinished">Did not find enough coordinates</translation>
    </message>
  </context>
  <context>
    <name>ImportSVG</name>
    <message>
      <location filename="../../importSVG.py" line="1810"/>
      <source>Unknown SVG export style, switching to Translated</source>
      <translation type="unfinished">Unknown SVG export style, switching to Translated</translation>
    </message>
    <message>
      <location filename="../../importSVG.py" line="1830"/>
      <source>The export list contains no object with a valid bounding box</source>
      <translation type="unfinished">The export list contains no object with a valid bounding box</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../InitGui.py" line="116"/>
      <source>Draft creation tools</source>
      <translation type="unfinished">Draft creation tools</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="121"/>
      <source>Draft annotation tools</source>
      <translation type="unfinished">Draft annotation tools</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="126"/>
      <source>Draft modification tools</source>
      <translation type="unfinished">Draft modification tools</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="131"/>
      <source>Draft utility tools</source>
      <translation type="unfinished">Draft utility tools</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="137"/>
      <source>&amp;Drafting</source>
      <translation type="unfinished">&amp;Drafting</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="141"/>
      <source>&amp;Annotation</source>
      <translation type="unfinished">&amp;Annotation</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="146"/>
      <source>&amp;Modification</source>
      <translation type="unfinished">&amp;Modification</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="151"/>
      <source>&amp;Utilities</source>
      <translation type="unfinished">&amp;Utilities</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="53"/>
      <source>Arc tools</source>
      <translation type="unfinished">Arc tools</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="63"/>
      <source>Bézier tools</source>
      <translation type="unfinished">Bézier tools</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="101"/>
      <source>Array tools</source>
      <translation type="unfinished">Array tools</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snapper.py" line="1598"/>
      <source>Draft Snap</source>
      <translation type="unfinished">Draft Snap</translation>
    </message>
  </context>
  <context>
    <name>draft</name>
    <message>
      <location filename="../../InitGui.py" line="46"/>
      <source>Draft</source>
      <translation>Притяжка</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="73"/>
      <source>ShapeString: string has no wires</source>
      <translation type="unfinished">ShapeString: string has no wires</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="149"/>
      <location filename="../../draftobjects/pointarray.py" line="170"/>
      <source>added property &apos;ExtraPlacement&apos;</source>
      <translation type="unfinished">added property &apos;ExtraPlacement&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="290"/>
      <source>, path object doesn&apos;t have &apos;Edges&apos;.</source>
      <translation type="unfinished">, path object doesn&apos;t have &apos;Edges&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="414"/>
      <location filename="../../draftobjects/patharray.py" line="427"/>
      <location filename="../../draftobjects/patharray.py" line="440"/>
      <source>&apos;PathObj&apos; property will be migrated to &apos;PathObject&apos;</source>
      <translation type="unfinished">&apos;PathObj&apos; property will be migrated to &apos;PathObject&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="587"/>
      <source>Cannot calculate path tangent. Copy not aligned.</source>
      <translation type="unfinished">Cannot calculate path tangent. Copy not aligned.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="605"/>
      <source>Tangent and normal are parallel. Copy not aligned.</source>
      <translation type="unfinished">Tangent and normal are parallel. Copy not aligned.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="622"/>
      <source>Cannot calculate path normal, using default.</source>
      <translation type="unfinished">Cannot calculate path normal, using default.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="631"/>
      <source>Cannot calculate path binormal. Copy not aligned.</source>
      <translation type="unfinished">Cannot calculate path binormal. Copy not aligned.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="637"/>
      <source>AlignMode {} is not implemented</source>
      <translation type="unfinished">AlignMode {} is not implemented</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="95"/>
      <location filename="../../draftobjects/draft_annotation.py" line="118"/>
      <source>added view property &apos;ScaleMultiplier&apos;</source>
      <translation type="unfinished">added view property &apos;ScaleMultiplier&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="141"/>
      <location filename="../../draftobjects/draft_annotation.py" line="149"/>
      <source>migrated &apos;DraftText&apos; type to &apos;Text&apos;</source>
      <translation type="unfinished">migrated &apos;DraftText&apos; type to &apos;Text&apos;</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="190"/>
      <source>Snap Lock</source>
      <translation type="unfinished">Snap Lock</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="191"/>
      <source>Snap Endpoint</source>
      <translation type="unfinished">Snap Endpoint</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="192"/>
      <source>Snap Midpoint</source>
      <translation type="unfinished">Snap Midpoint</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="193"/>
      <source>Snap Center</source>
      <translation type="unfinished">Snap Center</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="194"/>
      <source>Snap Angle</source>
      <translation type="unfinished">Snap Angle</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="195"/>
      <source>Snap Intersection</source>
      <translation type="unfinished">Snap Intersection</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="196"/>
      <source>Snap Perpendicular</source>
      <translation type="unfinished">Snap Perpendicular</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="197"/>
      <source>Snap Extension</source>
      <translation type="unfinished">Snap Extension</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="198"/>
      <source>Snap Parallel</source>
      <translation type="unfinished">Snap Parallel</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="199"/>
      <source>Snap Special</source>
      <translation type="unfinished">Snap Special</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="200"/>
      <source>Snap Near</source>
      <translation type="unfinished">Snap Near</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="201"/>
      <source>Snap Ortho</source>
      <translation type="unfinished">Snap Ortho</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="202"/>
      <source>Snap Grid</source>
      <translation type="unfinished">Snap Grid</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="203"/>
      <source>Snap WorkingPlane</source>
      <translation type="unfinished">Snap WorkingPlane</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="204"/>
      <source>Snap Dimensions</source>
      <translation type="unfinished">Snap Dimensions</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="205"/>
      <source>Toggle Draft Grid</source>
      <translation type="unfinished">Toggle Draft Grid</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="83"/>
      <source>No graphical interface</source>
      <translation type="unfinished">No graphical interface</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="165"/>
      <source>Unable to insert new object into a scaled part</source>
      <translation type="unfinished">Unable to insert new object into a scaled part</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="270"/>
      <source>Symbol not implemented. Using a default symbol.</source>
      <translation type="unfinished">Symbol not implemented. Using a default symbol.</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="338"/>
      <source>Visibility off; removed from list: </source>
      <translation type="unfinished">Visibility off; removed from list: </translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="613"/>
      <source>image is Null</source>
      <translation type="unfinished">image is Null</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="623"/>
      <source>filename does not exist on the system or in the resource file</source>
      <translation type="unfinished">filename does not exist on the system or in the resource file</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="682"/>
      <source>unable to load texture</source>
      <translation type="unfinished">unable to load texture</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1094"/>
      <location filename="../../draftutils/groups.py" line="94"/>
      <location filename="../../draftutils/gui_utils.py" line="734"/>
      <location filename="../../draftfunctions/cut.py" line="57"/>
      <location filename="../../draftmake/make_array.py" line="84"/>
      <location filename="../../draftmake/make_pointarray.py" line="108"/>
      <location filename="../../draftmake/make_label.py" line="200"/>
      <location filename="../../draftmake/make_patharray.py" line="167"/>
      <location filename="../../draftmake/make_patharray.py" line="344"/>
      <location filename="../../draftmake/make_text.py" line="84"/>
      <location filename="../../draftmake/make_text.py" line="182"/>
      <location filename="../../draftmake/make_dimension.py" line="223"/>
      <location filename="../../draftmake/make_dimension.py" line="316"/>
      <location filename="../../draftmake/make_dimension.py" line="461"/>
      <location filename="../../draftmake/make_dimension.py" line="613"/>
      <location filename="../../draftmake/make_layer.py" line="59"/>
      <location filename="../../draftmake/make_layer.py" line="153"/>
      <source>No active document. Aborting.</source>
      <translation type="unfinished">No active document. Aborting.</translation>
    </message>
    <message>
      <location filename="../../draftutils/groups.py" line="131"/>
      <location filename="../../draftutils/gui_utils.py" line="743"/>
      <location filename="../../draftmake/make_pointarray.py" line="117"/>
      <location filename="../../draftmake/make_pointarray.py" line="128"/>
      <location filename="../../draftmake/make_label.py" line="245"/>
      <location filename="../../draftmake/make_orthoarray.py" line="190"/>
      <location filename="../../draftmake/make_patharray.py" line="176"/>
      <location filename="../../draftmake/make_patharray.py" line="187"/>
      <location filename="../../draftmake/make_patharray.py" line="353"/>
      <location filename="../../draftmake/make_patharray.py" line="364"/>
      <location filename="../../draftmake/make_circulararray.py" line="135"/>
      <location filename="../../draftmake/make_polararray.py" line="102"/>
      <location filename="../../draftmake/make_dimension.py" line="330"/>
      <location filename="../../draftmake/make_dimension.py" line="470"/>
      <source>Wrong input: object not in document.</source>
      <translation type="unfinished">Wrong input: object not in document.</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="754"/>
      <source>Does not have &apos;ViewObject.RootNode&apos;.</source>
      <translation type="unfinished">Does not have &apos;ViewObject.RootNode&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="64"/>
      <location filename="../../draftutils/init_draft_statusbar.py" line="79"/>
      <location filename="../../draftutils/init_draft_statusbar.py" line="93"/>
      <location filename="../../draftutils/init_draft_statusbar.py" line="184"/>
      <source>custom</source>
      <translation type="unfinished">custom</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="169"/>
      <source>Unable to convert input into a  scale factor</source>
      <translation type="unfinished">Unable to convert input into a  scale factor</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="185"/>
      <source>Set custom scale</source>
      <translation type="unfinished">Set custom scale</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="188"/>
      <source>Set custom annotation scale in format x:x, x=x</source>
      <translation type="unfinished">Set custom annotation scale in format x:x, x=x</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="247"/>
      <source>Set the scale used by draft annotation tools</source>
      <translation type="unfinished">Set the scale used by draft annotation tools</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="701"/>
      <source>Solids:</source>
      <translation type="unfinished">Solids:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="702"/>
      <source>Faces:</source>
      <translation type="unfinished">Faces:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="703"/>
      <source>Wires:</source>
      <translation type="unfinished">Wires:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="704"/>
      <source>Edges:</source>
      <translation type="unfinished">Edges:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="705"/>
      <source>Vertices:</source>
      <translation type="unfinished">Vertices:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="709"/>
      <source>Face</source>
      <translation>Грань</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="714"/>
      <source>Wire</source>
      <translation type="unfinished">Wire</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="749"/>
      <location filename="../../draftutils/utils.py" line="757"/>
      <source>different types</source>
      <translation type="unfinished">different types</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="771"/>
      <source>Objects have different placements. Distance between the two base points: </source>
      <translation type="unfinished">Objects have different placements. Distance between the two base points: </translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="777"/>
      <source>has a different value</source>
      <translation type="unfinished">has a different value</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="782"/>
      <source>doesn&apos;t exist in one of the objects</source>
      <translation type="unfinished">doesn&apos;t exist in one of the objects</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="898"/>
      <source>%s shares a base with %d other objects. Please check if you want to modify this.</source>
      <translation type="unfinished">%s shares a base with %d other objects. Please check if you want to modify this.</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="912"/>
      <source>%s cannot be modified because its placement is readonly.</source>
      <translation type="unfinished">%s cannot be modified because its placement is readonly.</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1057"/>
      <source>Wrong input: unknown document.</source>
      <translation type="unfinished">Wrong input: unknown document.</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1136"/>
      <source>This function will be deprecated in </source>
      <translation type="unfinished">This function will be deprecated in </translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1138"/>
      <location filename="../../draftutils/utils.py" line="1144"/>
      <source>Please use </source>
      <translation type="unfinished">Please use </translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1143"/>
      <source>This function will be deprecated. </source>
      <translation type="unfinished">This function will be deprecated. </translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="59"/>
      <location filename="../../draftmake/make_fillet.py" line="150"/>
      <location filename="../../draftmake/make_fillet.py" line="156"/>
      <location filename="../../draftmake/make_fillet.py" line="162"/>
      <source>length:</source>
      <translation type="unfinished">length:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="135"/>
      <source>Two elements are needed.</source>
      <translation type="unfinished">Two elements are needed.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="142"/>
      <source>Radius is too large</source>
      <translation type="unfinished">Radius is too large</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="148"/>
      <location filename="../../draftmake/make_fillet.py" line="154"/>
      <location filename="../../draftmake/make_fillet.py" line="160"/>
      <source>Segment</source>
      <translation type="unfinished">Segment</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="182"/>
      <source>Removed original objects.</source>
      <translation type="unfinished">Removed original objects.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_pointarray.py" line="142"/>
      <source>Wrong input: point object doesn&apos;t have &apos;Geometry&apos;, &apos;Links&apos;, or &apos;Components&apos;.</source>
      <translation type="unfinished">Wrong input: point object doesn&apos;t have &apos;Geometry&apos;, &apos;Links&apos;, or &apos;Components&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_pointarray.py" line="157"/>
      <location filename="../../draftmake/make_label.py" line="224"/>
      <location filename="../../draftmake/make_text.py" line="118"/>
      <source>Wrong input: must be a placement, a vector, or a rotation.</source>
      <translation type="unfinished">Wrong input: must be a placement, a vector, or a rotation.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="98"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="306"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="504"/>
      <location filename="../../draftmake/make_circulararray.py" line="127"/>
      <source>Circular array</source>
      <translation type="unfinished">Circular array</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_circulararray.py" line="152"/>
      <source>Wrong input: must be a number or quantity.</source>
      <translation type="unfinished">Wrong input: must be a number or quantity.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="131"/>
      <location filename="../../draftmake/make_circulararray.py" line="161"/>
      <location filename="../../draftmake/make_polararray.py" line="111"/>
      <source>Wrong input: must be an integer number.</source>
      <translation type="unfinished">Wrong input: must be an integer number.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="209"/>
      <location filename="../../draftmake/make_patharray.py" line="204"/>
      <location filename="../../draftmake/make_patharray.py" line="264"/>
      <location filename="../../draftmake/make_patharray.py" line="274"/>
      <location filename="../../draftmake/make_circulararray.py" line="170"/>
      <location filename="../../draftmake/make_polararray.py" line="125"/>
      <location filename="../../draftmake/make_dimension.py" line="230"/>
      <location filename="../../draftmake/make_dimension.py" line="237"/>
      <location filename="../../draftmake/make_dimension.py" line="245"/>
      <location filename="../../draftmake/make_dimension.py" line="373"/>
      <location filename="../../draftmake/make_dimension.py" line="395"/>
      <location filename="../../draftmake/make_dimension.py" line="541"/>
      <location filename="../../draftmake/make_dimension.py" line="620"/>
      <location filename="../../draftmake/make_dimension.py" line="647"/>
      <location filename="../../draftmake/make_dimension.py" line="655"/>
      <source>Wrong input: must be a vector.</source>
      <translation type="unfinished">Wrong input: must be a vector.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="176"/>
      <source>This function is deprecated. Do not use this function directly.</source>
      <translation type="unfinished">This function is deprecated. Do not use this function directly.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="182"/>
      <source>Use one of &apos;make_linear_dimension&apos;, or &apos;make_linear_dimension_obj&apos;.</source>
      <translation type="unfinished">Use one of &apos;make_linear_dimension&apos;, or &apos;make_linear_dimension_obj&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="239"/>
      <location filename="../../draftmake/make_dimension.py" line="324"/>
      <source>Wrong input: object must not be a list.</source>
      <translation type="unfinished">Wrong input: object must not be a list.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="337"/>
      <location filename="../../draftmake/make_dimension.py" line="477"/>
      <source>Wrong input: object doesn&apos;t have a &apos;Shape&apos; to measure.</source>
      <translation type="unfinished">Wrong input: object doesn&apos;t have a &apos;Shape&apos; to measure.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="348"/>
      <source>Wrong input: object doesn&apos;t have at least one element in &apos;Vertexes&apos; to use for measuring.</source>
      <translation type="unfinished">Wrong input: object doesn&apos;t have at least one element in &apos;Vertexes&apos; to use for measuring.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="355"/>
      <location filename="../../draftmake/make_dimension.py" line="492"/>
      <source>Wrong input: must be an integer.</source>
      <translation type="unfinished">Wrong input: must be an integer.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="362"/>
      <source>i1: values below 1 are not allowed; will be set to 1.</source>
      <translation type="unfinished">i1: values below 1 are not allowed; will be set to 1.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="366"/>
      <location filename="../../draftmake/make_dimension.py" line="387"/>
      <source>Wrong input: vertex not in object.</source>
      <translation type="unfinished">Wrong input: vertex not in object.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="383"/>
      <source>i2: values below 1 are not allowed; will be set to the last vertex in the object.</source>
      <translation type="unfinished">i2: values below 1 are not allowed; will be set to the last vertex in the object.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="485"/>
      <source>Wrong input: object doesn&apos;t have at least one element in &apos;Edges&apos; to use for measuring.</source>
      <translation type="unfinished">Wrong input: object doesn&apos;t have at least one element in &apos;Edges&apos; to use for measuring.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="501"/>
      <source>index: values below 1 are not allowed; will be set to 1.</source>
      <translation type="unfinished">index: values below 1 are not allowed; will be set to 1.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="510"/>
      <source>Wrong input: index doesn&apos;t correspond to an edge in the object.</source>
      <translation type="unfinished">Wrong input: index doesn&apos;t correspond to an edge in the object.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="518"/>
      <source>Wrong input: index doesn&apos;t correspond to a circular edge.</source>
      <translation type="unfinished">Wrong input: index doesn&apos;t correspond to a circular edge.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="527"/>
      <location filename="../../draftmake/make_dimension.py" line="533"/>
      <source>Wrong input: must be a string, &apos;radius&apos; or &apos;diameter&apos;.</source>
      <translation type="unfinished">Wrong input: must be a string, &apos;radius&apos; or &apos;diameter&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="628"/>
      <location filename="../../draftmake/make_dimension.py" line="634"/>
      <source>Wrong input: must be a list with two angles.</source>
      <translation type="unfinished">Wrong input: must be a list with two angles.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_sketch.py" line="110"/>
      <source>No shape found</source>
      <translation type="unfinished">No shape found</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_sketch.py" line="117"/>
      <source>All Shapes must be planar</source>
      <translation type="unfinished">All Shapes must be planar</translation>
    </message>
    <message>
      <location filename="../../WorkingPlane.py" line="739"/>
      <location filename="../../draftmake/make_sketch.py" line="133"/>
      <location filename="../../draftmake/make_sketch.py" line="146"/>
      <source>All Shapes must be coplanar</source>
      <translation type="unfinished">All Shapes must be coplanar</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="69"/>
      <source>Layers</source>
      <translation type="unfinished">Layers</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_layers.py" line="47"/>
      <location filename="../../draftmake/make_layer.py" line="149"/>
      <location filename="../../draftmake/make_layer.py" line="166"/>
      <source>Layer</source>
      <translation type="unfinished">Layer</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="161"/>
      <source>Wrong input: it must be a string.</source>
      <translation type="unfinished">Wrong input: it must be a string.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="175"/>
      <location filename="../../draftmake/make_layer.py" line="183"/>
      <location filename="../../draftmake/make_layer.py" line="202"/>
      <location filename="../../draftmake/make_layer.py" line="210"/>
      <source>Wrong input: must be a tuple of three floats 0.0 to 1.0.</source>
      <translation type="unfinished">Wrong input: must be a tuple of three floats 0.0 to 1.0.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="348"/>
      <location filename="../../draftmake/make_orthoarray.py" line="175"/>
      <location filename="../../draftmake/make_patharray.py" line="196"/>
      <location filename="../../draftmake/make_patharray.py" line="373"/>
      <location filename="../../draftmake/make_polararray.py" line="118"/>
      <location filename="../../draftmake/make_layer.py" line="225"/>
      <source>Wrong input: must be a number.</source>
      <translation type="unfinished">Wrong input: must be a number.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="237"/>
      <location filename="../../draftmake/make_layer.py" line="246"/>
      <source>Wrong input: must be &apos;Solid&apos;, &apos;Dashed&apos;, &apos;Dotted&apos;, or &apos;Dashdot&apos;.</source>
      <translation type="unfinished">Wrong input: must be &apos;Solid&apos;, &apos;Dashed&apos;, &apos;Dotted&apos;, or &apos;Dashdot&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="254"/>
      <source>Wrong input: must be a number between 0 and 100.</source>
      <translation type="unfinished">Wrong input: must be a number between 0 and 100.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="264"/>
      <location filename="../../draftmake/make_patharray.py" line="221"/>
      <source>Wrong input: must be a list or tuple of strings, or a single string.</source>
      <translation type="unfinished">Wrong input: must be a list or tuple of strings, or a single string.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_patharray.py" line="257"/>
      <source>Wrong input: must be &apos;Original&apos;, &apos;Frenet&apos;, or &apos;Tangent&apos;.</source>
      <translation type="unfinished">Wrong input: must be &apos;Original&apos;, &apos;Frenet&apos;, or &apos;Tangent&apos;.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="95"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="266"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="446"/>
      <location filename="../../draftmake/make_polararray.py" line="94"/>
      <source>Polar array</source>
      <translation type="unfinished">Polar array</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="276"/>
      <source>Wrong input: subelement not in object.</source>
      <translation type="unfinished">Wrong input: subelement not in object.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="285"/>
      <source>Wrong input: label_type must be a string.</source>
      <translation type="unfinished">Wrong input: label_type must be a string.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="292"/>
      <source>Wrong input: label_type must be one of the following: </source>
      <translation type="unfinished">Wrong input: label_type must be one of the following: </translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="306"/>
      <location filename="../../draftmake/make_label.py" line="316"/>
      <location filename="../../draftmake/make_text.py" line="95"/>
      <location filename="../../draftmake/make_text.py" line="103"/>
      <source>Wrong input: must be a list of strings or a single string.</source>
      <translation type="unfinished">Wrong input: must be a list of strings or a single string.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="330"/>
      <location filename="../../draftmake/make_label.py" line="339"/>
      <source>Wrong input: must be a string, &apos;Horizontal&apos;, &apos;Vertical&apos;, or &apos;Custom&apos;.</source>
      <translation type="unfinished">Wrong input: must be a string, &apos;Horizontal&apos;, &apos;Vertical&apos;, or &apos;Custom&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="357"/>
      <source>Wrong input: must be a list of at least two vectors.</source>
      <translation type="unfinished">Wrong input: must be a list of at least two vectors.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="389"/>
      <source>Direction is not &apos;Custom&apos;; points won&apos;t be used.</source>
      <translation type="unfinished">Direction is not &apos;Custom&apos;; points won&apos;t be used.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="426"/>
      <source>Wrong input: must be a list of two elements. For example, [object, &apos;Edge1&apos;].</source>
      <translation type="unfinished">Wrong input: must be a list of two elements. For example, [object, &apos;Edge1&apos;].</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="63"/>
      <source>Internal orthogonal array</source>
      <translation type="unfinished">Internal orthogonal array</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="102"/>
      <source>Wrong input: must be a number or vector.</source>
      <translation type="unfinished">Wrong input: must be a number or vector.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="107"/>
      <location filename="../../draftmake/make_orthoarray.py" line="110"/>
      <location filename="../../draftmake/make_orthoarray.py" line="113"/>
      <source>Input: single value expanded to vector.</source>
      <translation type="unfinished">Input: single value expanded to vector.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="139"/>
      <location filename="../../draftmake/make_orthoarray.py" line="146"/>
      <location filename="../../draftmake/make_orthoarray.py" line="153"/>
      <source>Input: number of elements must be at least 1. It is set to 1.</source>
      <translation type="unfinished">Input: number of elements must be at least 1. It is set to 1.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="98"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="270"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="407"/>
      <location filename="../../draftmake/make_orthoarray.py" line="300"/>
      <source>Orthogonal array</source>
      <translation type="unfinished">Orthogonal array</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="382"/>
      <source>Orthogonal array 2D</source>
      <translation type="unfinished">Orthogonal array 2D</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="449"/>
      <source>Rectangular array</source>
      <translation type="unfinished">Rectangular array</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="522"/>
      <source>Rectangular array 2D</source>
      <translation type="unfinished">Rectangular array 2D</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="130"/>
      <location filename="../../draftmake/make_arc_3points.py" line="139"/>
      <source>Points:</source>
      <translation type="unfinished">Points:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="135"/>
      <location filename="../../draftmake/make_arc_3points.py" line="144"/>
      <source>Wrong input: must be list or tuple of three points exactly.</source>
      <translation type="unfinished">Wrong input: must be list or tuple of three points exactly.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="151"/>
      <source>Placement:</source>
      <translation type="unfinished">Placement:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="152"/>
      <source>Wrong input: incorrect type of placement.</source>
      <translation type="unfinished">Wrong input: incorrect type of placement.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="166"/>
      <source>Wrong input: incorrect type of points.</source>
      <translation type="unfinished">Wrong input: incorrect type of points.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="172"/>
      <source>Cannot generate shape:</source>
      <translation type="unfinished">Cannot generate shape:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="179"/>
      <source>Radius:</source>
      <translation type="unfinished">Radius:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="180"/>
      <source>Center:</source>
      <translation type="unfinished">Center:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="183"/>
      <source>Create primitive object</source>
      <translation type="unfinished">Create primitive object</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="208"/>
      <location filename="../../draftmake/make_arc_3points.py" line="221"/>
      <source>Final placement:</source>
      <translation type="unfinished">Final placement:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="210"/>
      <source>Face: True</source>
      <translation type="unfinished">Face: True</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="212"/>
      <source>Support:</source>
      <translation type="unfinished">Support:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="213"/>
      <source>Map mode:</source>
      <translation type="unfinished">Map mode:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="83"/>
      <source>Task panel:</source>
      <translation type="unfinished">Task panel:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="207"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="197"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="183"/>
      <source>At least one element must be selected.</source>
      <translation type="unfinished">At least one element must be selected.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="201"/>
      <source>Number of elements must be at least 1.</source>
      <translation type="unfinished">Number of elements must be at least 1.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="218"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="208"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="190"/>
      <source>Selection is not suitable for array.</source>
      <translation type="unfinished">Selection is not suitable for array.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="219"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="389"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="211"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="384"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="191"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="337"/>
      <source>Object:</source>
      <translation type="unfinished">Object:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="326"/>
      <source>Interval X reset:</source>
      <translation type="unfinished">Interval X reset:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="335"/>
      <source>Interval Y reset:</source>
      <translation type="unfinished">Interval Y reset:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="344"/>
      <source>Interval Z reset:</source>
      <translation type="unfinished">Interval Z reset:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="358"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="353"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="306"/>
      <source>Fuse:</source>
      <translation type="unfinished">Fuse:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="372"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="367"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="320"/>
      <source>Create Link array:</source>
      <translation type="unfinished">Create Link array:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="385"/>
      <source>Number of X elements:</source>
      <translation type="unfinished">Number of X elements:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="388"/>
      <source>Interval X:</source>
      <translation type="unfinished">Interval X:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="390"/>
      <source>Number of Y elements:</source>
      <translation type="unfinished">Number of Y elements:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="393"/>
      <source>Interval Y:</source>
      <translation type="unfinished">Interval Y:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="395"/>
      <source>Number of Z elements:</source>
      <translation type="unfinished">Number of Z elements:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="398"/>
      <source>Interval Z:</source>
      <translation type="unfinished">Interval Z:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="504"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="407"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="446"/>
      <source>Aborted:</source>
      <translation type="unfinished">Aborted:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="211"/>
      <source>Number of layers must be at least 2.</source>
      <translation type="unfinished">Number of layers must be at least 2.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="228"/>
      <source>Radial distance is zero. Resulting array may not look correct.</source>
      <translation type="unfinished">Radial distance is zero. Resulting array may not look correct.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="235"/>
      <source>Radial distance is negative. It is made positive to proceed.</source>
      <translation type="unfinished">Radial distance is negative. It is made positive to proceed.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="239"/>
      <source>Tangential distance cannot be zero.</source>
      <translation type="unfinished">Tangential distance cannot be zero.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="247"/>
      <source>Tangential distance is negative. It is made positive to proceed.</source>
      <translation type="unfinished">Tangential distance is negative. It is made positive to proceed.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="349"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="297"/>
      <source>Center reset:</source>
      <translation type="unfinished">Center reset:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="390"/>
      <source>Radial distance:</source>
      <translation type="unfinished">Radial distance:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="392"/>
      <source>Tangential distance:</source>
      <translation type="unfinished">Tangential distance:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="395"/>
      <source>Number of circular layers:</source>
      <translation type="unfinished">Number of circular layers:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="397"/>
      <source>Symmetry parameter:</source>
      <translation type="unfinished">Symmetry parameter:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="400"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="342"/>
      <source>Center of rotation:</source>
      <translation type="unfinished">Center of rotation:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_shapestring.py" line="56"/>
      <source>ShapeString</source>
      <translation type="unfinished">ShapeString</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_shapestring.py" line="70"/>
      <source>Default</source>
      <translation type="unfinished">Default</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="141"/>
      <location filename="../../drafttaskpanels/task_shapestring.py" line="184"/>
      <source>Create ShapeString</source>
      <translation type="unfinished">Create ShapeString</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="195"/>
      <source>Number of elements must be at least 2.</source>
      <translation type="unfinished">Number of elements must be at least 2.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="204"/>
      <source>The angle is above 360 degrees. It is set to this value to proceed.</source>
      <translation type="unfinished">The angle is above 360 degrees. It is set to this value to proceed.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="212"/>
      <source>The angle is below -360 degrees. It is set to this value to proceed.</source>
      <translation type="unfinished">The angle is below -360 degrees. It is set to this value to proceed.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="338"/>
      <source>Number of elements:</source>
      <translation type="unfinished">Number of elements:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="339"/>
      <source>Polar angle:</source>
      <translation type="unfinished">Polar angle:</translation>
    </message>
    <message>
      <location filename="../../importDXF.py" line="164"/>
      <source>The DXF import/export libraries needed by FreeCAD to handle
the DXF format were not found on this system.
Please either enable FreeCAD to download these libraries:
  1 - Load Draft workbench
  2 - Menu Edit &gt; Preferences &gt; Import-Export &gt; DXF &gt; Enable downloads
Or download these libraries manually, as explained on
https://github.com/yorikvanhavre/Draft-dxf-importer
To enabled FreeCAD to download these libraries, answer Yes.</source>
      <translation type="unfinished">The DXF import/export libraries needed by FreeCAD to handle
the DXF format were not found on this system.
Please either enable FreeCAD to download these libraries:
  1 - Load Draft workbench
  2 - Menu Edit &gt; Preferences &gt; Import-Export &gt; DXF &gt; Enable downloads
Or download these libraries manually, as explained on
https://github.com/yorikvanhavre/Draft-dxf-importer
To enabled FreeCAD to download these libraries, answer Yes.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="64"/>
      <source>Change slope</source>
      <translation type="unfinished">Change slope</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_sketcher_objects.py" line="70"/>
      <source>Sketch is too complex to edit: it is suggested to use sketcher default editor</source>
      <translation type="unfinished">Sketch is too complex to edit: it is suggested to use sketcher default editor</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="81"/>
      <source>Select an object to join</source>
      <translation type="unfinished">Select an object to join</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="103"/>
      <source>Join lines</source>
      <translation type="unfinished">Join lines</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="113"/>
      <source>Selection:</source>
      <translation type="unfinished">Selection:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="316"/>
      <source>Select a Draft object to edit</source>
      <translation type="unfinished">Select a Draft object to edit</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="584"/>
      <source>No edit point found for selected object</source>
      <translation type="unfinished">No edit point found for selected object</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="842"/>
      <source>Too many objects selected, max number set to:</source>
      <translation type="unfinished">Too many objects selected, max number set to:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="850"/>
      <source>: this object is not editable</source>
      <translation type="unfinished">: this object is not editable</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_grid.py" line="51"/>
      <source>Toggle grid</source>
      <translation type="unfinished">Toggle grid</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_styles.py" line="80"/>
      <source>Change Style</source>
      <translation type="unfinished">Change Style</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="72"/>
      <location filename="../../draftguitools/gui_downgrade.py" line="72"/>
      <source>Select an object to upgrade</source>
      <translation type="unfinished">Select an object to upgrade</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_downgrade.py" line="89"/>
      <source>Downgrade</source>
      <translation type="unfinished">Downgrade</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="160"/>
      <source>Pick a face, 3 vertices or a WP Proxy to define the drawing plane</source>
      <translation type="unfinished">Pick a face, 3 vertices or a WP Proxy to define the drawing plane</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1112"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="292"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="360"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="565"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="576"/>
      <source>Current working plane</source>
      <translation type="unfinished">Current working plane</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="295"/>
      <source>Working plane aligned to global placement of</source>
      <translation type="unfinished">Working plane aligned to global placement of</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="832"/>
      <location filename="../../WorkingPlane.py" line="854"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="408"/>
      <source>Top</source>
      <translation type="unfinished">Top</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="834"/>
      <location filename="../../WorkingPlane.py" line="868"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="420"/>
      <source>Front</source>
      <translation type="unfinished">Front</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="836"/>
      <location filename="../../WorkingPlane.py" line="882"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="432"/>
      <source>Side</source>
      <translation type="unfinished">Side</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="558"/>
      <source>Dir</source>
      <translation type="unfinished">Dir</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1353"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="562"/>
      <location filename="../../draftguitools/gui_offset.py" line="260"/>
      <location filename="../../draftguitools/gui_offset.py" line="278"/>
      <location filename="../../draftguitools/gui_offset.py" line="340"/>
      <source>Offset</source>
      <translation type="unfinished">Offset</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="574"/>
      <source>Custom</source>
      <translation type="unfinished">Custom</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snapper.py" line="1663"/>
      <source>(ON)</source>
      <translation type="unfinished">(ON)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snapper.py" line="1669"/>
      <source>(OFF)</source>
      <translation type="unfinished">(OFF)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="92"/>
      <source>Select an object to scale</source>
      <translation type="unfinished">Select an object to scale</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="111"/>
      <source>Pick base point</source>
      <translation type="unfinished">Pick base point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="138"/>
      <source>Pick reference distance from base point</source>
      <translation type="unfinished">Pick reference distance from base point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="77"/>
      <location filename="../../DraftGui.py" line="1053"/>
      <location filename="../../draftguitools/gui_scale.py" line="208"/>
      <location filename="../../draftguitools/gui_scale.py" line="233"/>
      <location filename="../../draftguitools/gui_scale.py" line="371"/>
      <location filename="../../draftguitools/gui_move.py" line="215"/>
      <location filename="../../draftguitools/gui_rotate.py" line="296"/>
      <source>Copy</source>
      <translation type="unfinished">Copy</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="212"/>
      <location filename="../../draftguitools/gui_scale.py" line="242"/>
      <location filename="../../draftguitools/gui_scale.py" line="374"/>
      <source>Scale</source>
      <translation type="unfinished">Scale</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="215"/>
      <source>Some subelements could not be scaled.</source>
      <translation type="unfinished">Some subelements could not be scaled.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="351"/>
      <source>Unable to scale object:</source>
      <translation type="unfinished">Unable to scale object:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="355"/>
      <source>Unable to scale objects:</source>
      <translation type="unfinished">Unable to scale objects:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="361"/>
      <source>This object type cannot be scaled directly. Please use the clone method.</source>
      <translation type="unfinished">This object type cannot be scaled directly. Please use the clone method.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="421"/>
      <source>Pick new distance from base point</source>
      <translation type="unfinished">Pick new distance from base point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="69"/>
      <source>Click anywhere on a line to split it.</source>
      <translation type="unfinished">Click anywhere on a line to split it.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="112"/>
      <source>Split line</source>
      <translation type="unfinished">Split line</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_points.py" line="148"/>
      <location filename="../../draftguitools/gui_points.py" line="162"/>
      <source>Create Point</source>
      <translation type="unfinished">Create Point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="80"/>
      <source>Select an object to stretch</source>
      <translation type="unfinished">Select an object to stretch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="141"/>
      <source>Pick first point of selection rectangle</source>
      <translation type="unfinished">Pick first point of selection rectangle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="177"/>
      <source>Pick opposite point of selection rectangle</source>
      <translation type="unfinished">Pick opposite point of selection rectangle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="186"/>
      <source>Pick start point of displacement</source>
      <translation type="unfinished">Pick start point of displacement</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="249"/>
      <source>Pick end point of displacement</source>
      <translation type="unfinished">Pick end point of displacement</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="529"/>
      <source>Turning one Rectangle into a Wire</source>
      <translation type="unfinished">Turning one Rectangle into a Wire</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="558"/>
      <source>Stretch</source>
      <translation type="unfinished">Stretch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="112"/>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="91"/>
      <source>Please select exactly two objects, the base object and the path object, before calling this command.</source>
      <translation type="unfinished">Please select exactly two objects, the base object and the path object, before calling this command.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="115"/>
      <source>Path twisted array</source>
      <translation type="unfinished">Path twisted array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="70"/>
      <source>Annotation style editor</source>
      <translation type="unfinished">Annotation style editor</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="306"/>
      <source>Open styles file</source>
      <translation type="unfinished">Open styles file</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="308"/>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="330"/>
      <source>JSON file (*.json)</source>
      <translation type="unfinished">JSON file (*.json)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="328"/>
      <source>Save styles file</source>
      <translation type="unfinished">Save styles file</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="83"/>
      <source>Pick location point</source>
      <translation type="unfinished">Pick location point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="129"/>
      <source>Create Text</source>
      <translation type="unfinished">Create Text</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimension_ops.py" line="57"/>
      <source>Flip dimension</source>
      <translation type="unfinished">Flip dimension</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="131"/>
      <source>Main toggle snap</source>
      <translation type="unfinished">Main toggle snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="170"/>
      <source>Midpoint snap</source>
      <translation type="unfinished">Midpoint snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="206"/>
      <source>Perpendicular snap</source>
      <translation type="unfinished">Perpendicular snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="241"/>
      <source>Grid snap</source>
      <translation type="unfinished">Grid snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="277"/>
      <source>Intersection snap</source>
      <translation type="unfinished">Intersection snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="313"/>
      <source>Parallel snap</source>
      <translation type="unfinished">Parallel snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="350"/>
      <source>Endpoint snap</source>
      <translation type="unfinished">Endpoint snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="387"/>
      <source>Angle snap (30 and 45 degrees)</source>
      <translation type="unfinished">Angle snap (30 and 45 degrees)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="424"/>
      <source>Arc center snap</source>
      <translation type="unfinished">Arc center snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="460"/>
      <source>Edge extension snap</source>
      <translation type="unfinished">Edge extension snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="494"/>
      <source>Near snap</source>
      <translation type="unfinished">Near snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="531"/>
      <source>Orthogonal snap</source>
      <translation type="unfinished">Orthogonal snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="568"/>
      <source>Special point snap</source>
      <translation type="unfinished">Special point snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="606"/>
      <source>Dimension display</source>
      <translation type="unfinished">Dimension display</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="648"/>
      <source>Working plane snap</source>
      <translation type="unfinished">Working plane snap</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="683"/>
      <source>Show snap toolbar</source>
      <translation type="unfinished">Show snap toolbar</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="85"/>
      <location filename="../../draftguitools/gui_arcs.py" line="93"/>
      <source>Pick center point</source>
      <translation type="unfinished">Pick center point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="206"/>
      <location filename="../../draftguitools/gui_polygons.py" line="217"/>
      <location filename="../../draftguitools/gui_polygons.py" line="279"/>
      <location filename="../../draftguitools/gui_arcs.py" line="279"/>
      <location filename="../../draftguitools/gui_arcs.py" line="298"/>
      <location filename="../../draftguitools/gui_arcs.py" line="452"/>
      <source>Pick radius</source>
      <translation type="unfinished">Pick radius</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="243"/>
      <source>Create Polygon (Part)</source>
      <translation type="unfinished">Create Polygon (Part)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="263"/>
      <source>Create Polygon</source>
      <translation type="unfinished">Create Polygon</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="114"/>
      <source>Please select exactly two objects, the base object and the point object, before calling this command.</source>
      <translation type="unfinished">Please select exactly two objects, the base object and the point object, before calling this command.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="134"/>
      <source>Point array</source>
      <translation type="unfinished">Point array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="84"/>
      <source>Select an object to clone</source>
      <translation type="unfinished">Select an object to clone</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="90"/>
      <location filename="../../draftguitools/gui_shapestrings.py" line="104"/>
      <source>Pick ShapeString location point</source>
      <translation type="unfinished">Pick ShapeString location point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_drawing.py" line="87"/>
      <source>The Drawing Workbench is obsolete since 0.17, consider using the TechDraw Workbench instead.</source>
      <translation type="unfinished">The Drawing Workbench is obsolete since 0.17, consider using the TechDraw Workbench instead.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="73"/>
      <location filename="../../draftguitools/gui_drawing.py" line="91"/>
      <source>Select an object to project</source>
      <translation type="unfinished">Select an object to project</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="81"/>
      <source>Select an object to offset</source>
      <translation type="unfinished">Select an object to offset</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="88"/>
      <source>Offset only works on one object at a time.</source>
      <translation type="unfinished">Offset only works on one object at a time.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="98"/>
      <source>Cannot offset this object type</source>
      <translation type="unfinished">Cannot offset this object type</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="134"/>
      <source>Offset of Bezier curves is currently not supported</source>
      <translation type="unfinished">Offset of Bezier curves is currently not supported</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="177"/>
      <location filename="../../draftguitools/gui_offset.py" line="154"/>
      <source>Pick distance</source>
      <translation type="unfinished">Pick distance</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="87"/>
      <source>Pick target point</source>
      <translation type="unfinished">Pick target point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="167"/>
      <source>Create Label</source>
      <translation type="unfinished">Create Label</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="199"/>
      <location filename="../../draftguitools/gui_labels.py" line="226"/>
      <source>Pick endpoint of leader line</source>
      <translation type="unfinished">Pick endpoint of leader line</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="209"/>
      <location filename="../../draftguitools/gui_labels.py" line="236"/>
      <source>Pick text position</source>
      <translation type="unfinished">Pick text position</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="71"/>
      <source>Select an object to convert.</source>
      <translation type="unfinished">Select an object to convert.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="104"/>
      <source>Convert to Sketch</source>
      <translation type="unfinished">Convert to Sketch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="118"/>
      <source>Convert to Draft</source>
      <translation type="unfinished">Convert to Draft</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="147"/>
      <source>Convert Draft/Sketch</source>
      <translation type="unfinished">Convert Draft/Sketch</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_subelements.py" line="115"/>
      <source>Select an object to edit</source>
      <translation type="unfinished">Select an object to edit</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_heal.py" line="51"/>
      <source>Heal</source>
      <translation type="unfinished">Heal</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="98"/>
      <source>Select objects to trim or extend</source>
      <translation type="unfinished">Select objects to trim or extend</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1359"/>
      <location filename="../../DraftGui.py" line="1418"/>
      <location filename="../../draftguitools/gui_trimex.py" line="217"/>
      <source>Distance</source>
      <translation type="unfinished">Distance</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1360"/>
      <location filename="../../DraftGui.py" line="1419"/>
      <location filename="../../draftguitools/gui_trimex.py" line="218"/>
      <source>Offset distance</source>
      <translation type="unfinished">Offset distance</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="933"/>
      <location filename="../../draftguitools/gui_trimex.py" line="221"/>
      <source>Angle</source>
      <translation type="unfinished">Angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="222"/>
      <source>Offset angle</source>
      <translation type="unfinished">Offset angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="491"/>
      <source>Unable to trim these objects, only Draft wires and arcs are supported.</source>
      <translation type="unfinished">Unable to trim these objects, only Draft wires and arcs are supported.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="498"/>
      <source>Unable to trim these objects, too many wires</source>
      <translation type="unfinished">Unable to trim these objects, too many wires</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="515"/>
      <source>These objects don&apos;t intersect.</source>
      <translation type="unfinished">These objects don&apos;t intersect.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="518"/>
      <source>Too many intersection points.</source>
      <translation type="unfinished">Too many intersection points.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="114"/>
      <source>Create 2D view</source>
      <translation type="unfinished">Create 2D view</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_facebinders.py" line="75"/>
      <source>Select faces from existing objects</source>
      <translation type="unfinished">Select faces from existing objects</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="73"/>
      <source>No active Draft Toolbar.</source>
      <translation type="unfinished">No active Draft Toolbar.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="94"/>
      <source>Construction mode</source>
      <translation type="unfinished">Construction mode</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="135"/>
      <source>Continue mode</source>
      <translation type="unfinished">Continue mode</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="177"/>
      <source>Toggle display mode</source>
      <translation type="unfinished">Toggle display mode</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="129"/>
      <location filename="../../draftguitools/gui_ellipses.py" line="77"/>
      <location filename="../../draftguitools/gui_lines.py" line="88"/>
      <location filename="../../draftguitools/gui_rectangles.py" line="75"/>
      <source>Pick first point</source>
      <translation type="unfinished">Pick first point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="170"/>
      <source>Create Line</source>
      <translation type="unfinished">Create Line</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="193"/>
      <source>Create Wire</source>
      <translation type="unfinished">Create Wire</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="152"/>
      <location filename="../../draftguitools/gui_lines.py" line="226"/>
      <location filename="../../draftguitools/gui_lines.py" line="235"/>
      <location filename="../../draftguitools/gui_lines.py" line="242"/>
      <location filename="../../draftguitools/gui_lines.py" line="250"/>
      <location filename="../../draftguitools/gui_lines.py" line="260"/>
      <location filename="../../draftguitools/gui_splines.py" line="148"/>
      <source>Pick next point</source>
      <translation type="unfinished">Pick next point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="347"/>
      <source>Unable to create a Wire from selected objects</source>
      <translation type="unfinished">Unable to create a Wire from selected objects</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="370"/>
      <source>Convert to Wire</source>
      <translation type="unfinished">Convert to Wire</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="133"/>
      <source>Create Plane</source>
      <translation type="unfinished">Create Plane</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="151"/>
      <source>Create Rectangle</source>
      <translation type="unfinished">Create Rectangle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="202"/>
      <location filename="../../draftguitools/gui_rectangles.py" line="210"/>
      <source>Pick opposite point</source>
      <translation type="unfinished">Pick opposite point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="129"/>
      <location filename="../../draftguitools/gui_ellipses.py" line="147"/>
      <source>Create Ellipse</source>
      <translation type="unfinished">Create Ellipse</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="89"/>
      <source>Upgrade</source>
      <translation type="unfinished">Upgrade</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="221"/>
      <location filename="../../draftguitools/gui_dimensions.py" line="257"/>
      <location filename="../../draftguitools/gui_dimensions.py" line="272"/>
      <location filename="../../draftguitools/gui_dimensions.py" line="303"/>
      <source>Create Dimension</source>
      <translation type="unfinished">Create Dimension</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="319"/>
      <source>Create Dimension (radial)</source>
      <translation type="unfinished">Create Dimension (radial)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="526"/>
      <source>Edge too short!</source>
      <translation type="unfinished">Edge too short!</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="546"/>
      <source>Edges don&apos;t intersect!</source>
      <translation type="unfinished">Edges don&apos;t intersect!</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="95"/>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="589"/>
      <source>This object does not support possible coincident points, please try again.</source>
      <translation type="unfinished">This object does not support possible coincident points, please try again.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="168"/>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="633"/>
      <source>Active object must have more than two points/nodes</source>
      <translation type="unfinished">Active object must have more than two points/nodes</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="753"/>
      <source>Selection is not a Knot</source>
      <translation type="unfinished">Selection is not a Knot</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="784"/>
      <source>Endpoint of BezCurve can&apos;t be smoothed</source>
      <translation type="unfinished">Endpoint of BezCurve can&apos;t be smoothed</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="80"/>
      <source>Fillet radius</source>
      <translation type="unfinished">Fillet radius</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="83"/>
      <source>Radius of fillet</source>
      <translation type="unfinished">Radius of fillet</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="120"/>
      <source>Enter radius.</source>
      <translation type="unfinished">Enter radius.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="139"/>
      <source>Delete original objects:</source>
      <translation type="unfinished">Delete original objects:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="144"/>
      <source>Chamfer mode:</source>
      <translation type="unfinished">Chamfer mode:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="161"/>
      <source>Two elements needed.</source>
      <translation type="unfinished">Two elements needed.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="168"/>
      <source>Test object</source>
      <translation type="unfinished">Test object</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="169"/>
      <source>Test object removed</source>
      <translation type="unfinished">Test object removed</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="171"/>
      <source>Fillet cannot be created</source>
      <translation type="unfinished">Fillet cannot be created</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="203"/>
      <source>Create fillet</source>
      <translation type="unfinished">Create fillet</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="82"/>
      <source>Select an object to array</source>
      <translation type="unfinished">Select an object to array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="108"/>
      <source>Array</source>
      <translation type="unfinished">Array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="136"/>
      <location filename="../../draftguitools/gui_beziers.py" line="341"/>
      <source>Bézier curve has been closed</source>
      <translation type="unfinished">Bézier curve has been closed</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="144"/>
      <location filename="../../draftguitools/gui_beziers.py" line="377"/>
      <location filename="../../draftguitools/gui_splines.py" line="138"/>
      <source>Last point has been removed</source>
      <translation type="unfinished">Last point has been removed</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="155"/>
      <location filename="../../draftguitools/gui_splines.py" line="153"/>
      <source>Pick next point, or finish (A) or close (O)</source>
      <translation type="unfinished">Pick next point, or finish (A) or close (O)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="219"/>
      <location filename="../../draftguitools/gui_beziers.py" line="469"/>
      <source>Create BezCurve</source>
      <translation type="unfinished">Create BezCurve</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="385"/>
      <source>Click and drag to define next knot</source>
      <translation type="unfinished">Click and drag to define next knot</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="394"/>
      <source>Click and drag to define next knot, or finish (A) or close (O)</source>
      <translation type="unfinished">Click and drag to define next knot, or finish (A) or close (O)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="84"/>
      <source>Select an object to rotate</source>
      <translation type="unfinished">Select an object to rotate</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="101"/>
      <source>Pick rotation center</source>
      <translation type="unfinished">Pick rotation center</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="196"/>
      <location filename="../../draftguitools/gui_rotate.py" line="411"/>
      <source>Base angle</source>
      <translation type="unfinished">Base angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="199"/>
      <location filename="../../draftguitools/gui_rotate.py" line="414"/>
      <source>The base angle you wish to start the rotation from</source>
      <translation type="unfinished">The base angle you wish to start the rotation from</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="204"/>
      <location filename="../../draftguitools/gui_rotate.py" line="417"/>
      <source>Pick base angle</source>
      <translation type="unfinished">Pick base angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="210"/>
      <location filename="../../draftguitools/gui_rotate.py" line="426"/>
      <source>Rotation</source>
      <translation type="unfinished">Rotation</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="216"/>
      <location filename="../../draftguitools/gui_rotate.py" line="432"/>
      <source>The amount of rotation you wish to perform.
The final angle will be the base angle plus this amount.</source>
      <translation type="unfinished">The amount of rotation you wish to perform.
The final angle will be the base angle plus this amount.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="223"/>
      <location filename="../../draftguitools/gui_rotate.py" line="440"/>
      <source>Pick rotation angle</source>
      <translation type="unfinished">Pick rotation angle</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1290"/>
      <location filename="../../draftguitools/gui_rotate.py" line="300"/>
      <source>Rotate</source>
      <translation type="unfinished">Rotate</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="222"/>
      <location filename="../../draftguitools/gui_rotate.py" line="304"/>
      <source>Some subelements could not be moved.</source>
      <translation type="unfinished">Some subelements could not be moved.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="307"/>
      <location filename="../../draftguitools/gui_arcs.py" line="310"/>
      <location filename="../../draftguitools/gui_arcs.py" line="489"/>
      <location filename="../../draftguitools/gui_arcs.py" line="490"/>
      <source>Start angle</source>
      <translation type="unfinished">Start angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="317"/>
      <location filename="../../draftguitools/gui_arcs.py" line="495"/>
      <source>Pick start angle</source>
      <translation type="unfinished">Pick start angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="321"/>
      <location filename="../../draftguitools/gui_arcs.py" line="324"/>
      <location filename="../../draftguitools/gui_arcs.py" line="497"/>
      <location filename="../../draftguitools/gui_arcs.py" line="498"/>
      <source>Aperture angle</source>
      <translation type="unfinished">Aperture angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="329"/>
      <source>Pick aperture</source>
      <translation type="unfinished">Pick aperture</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="357"/>
      <source>Create Circle (Part)</source>
      <translation type="unfinished">Create Circle (Part)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="376"/>
      <source>Create Circle</source>
      <translation type="unfinished">Create Circle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="411"/>
      <source>Create Arc (Part)</source>
      <translation type="unfinished">Create Arc (Part)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="432"/>
      <source>Create Arc</source>
      <translation type="unfinished">Create Arc</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="511"/>
      <source>Pick aperture angle</source>
      <translation type="unfinished">Pick aperture angle</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="559"/>
      <location filename="../../draftguitools/gui_arcs.py" line="602"/>
      <source>Arc by 3 points</source>
      <translation type="unfinished">Arc by 3 points</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="65"/>
      <source>Add to group</source>
      <translation type="unfinished">Add to group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="68"/>
      <source>Ungroup</source>
      <translation type="unfinished">Ungroup</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="70"/>
      <source>Add new group</source>
      <translation type="unfinished">Add new group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="163"/>
      <source>Select group</source>
      <translation type="unfinished">Select group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="205"/>
      <source>No new selection. You must select non-empty groups or objects inside groups.</source>
      <translation type="unfinished">No new selection. You must select non-empty groups or objects inside groups.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="215"/>
      <source>Autogroup</source>
      <translation type="unfinished">Autogroup</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="876"/>
      <location filename="../../DraftGui.py" line="920"/>
      <location filename="../../DraftGui.py" line="1377"/>
      <location filename="../../DraftGui.py" line="2438"/>
      <location filename="../../DraftGui.py" line="2458"/>
      <location filename="../../draftguitools/gui_groups.py" line="256"/>
      <location filename="../../draftguitools/gui_groups.py" line="261"/>
      <source>None</source>
      <translation type="unfinished">None</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="267"/>
      <source>Add new Layer</source>
      <translation type="unfinished">Add new Layer</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="325"/>
      <source>Add to construction group</source>
      <translation type="unfinished">Add to construction group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="384"/>
      <source>Add a new group with a given name</source>
      <translation type="unfinished">Add a new group with a given name</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="417"/>
      <source>Add group</source>
      <translation type="unfinished">Add group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="419"/>
      <source>Group name</source>
      <translation type="unfinished">Group name</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="427"/>
      <source>Group</source>
      <translation type="unfinished">Group</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="87"/>
      <source>Select an object to move</source>
      <translation type="unfinished">Select an object to move</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="106"/>
      <source>Pick start point</source>
      <translation type="unfinished">Pick start point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="167"/>
      <location filename="../../draftguitools/gui_move.py" line="315"/>
      <source>Pick end point</source>
      <translation type="unfinished">Pick end point</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="219"/>
      <source>Move</source>
      <translation type="unfinished">Move</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="155"/>
      <source>Path array</source>
      <translation type="unfinished">Path array</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="126"/>
      <source>Spline has been closed</source>
      <translation type="unfinished">Spline has been closed</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="191"/>
      <source>Create B-spline</source>
      <translation type="unfinished">Create B-spline</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="78"/>
      <source>Select an object to mirror</source>
      <translation type="unfinished">Select an object to mirror</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="97"/>
      <source>Pick start point of mirror line</source>
      <translation type="unfinished">Pick start point of mirror line</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="126"/>
      <source>Mirror</source>
      <translation type="unfinished">Mirror</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="177"/>
      <location filename="../../draftguitools/gui_mirror.py" line="204"/>
      <source>Pick end point of mirror line</source>
      <translation type="unfinished">Pick end point of mirror line</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="57"/>
      <location filename="../../DraftGui.py" line="947"/>
      <source>Relative</source>
      <translation type="unfinished">Relative</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="62"/>
      <location filename="../../DraftGui.py" line="959"/>
      <source>Global</source>
      <translation type="unfinished">Global</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="67"/>
      <location filename="../../DraftGui.py" line="994"/>
      <location filename="../../DraftGui.py" line="1448"/>
      <source>Continue</source>
      <translation type="unfinished">Continue</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="72"/>
      <location filename="../../DraftGui.py" line="1018"/>
      <source>Close</source>
      <translation>Закрыть</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="82"/>
      <source>Subelement mode</source>
      <translation type="unfinished">Subelement mode</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="87"/>
      <source>Fill</source>
      <translation type="unfinished">Fill</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="92"/>
      <source>Exit</source>
      <translation type="unfinished">Exit</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="97"/>
      <source>Snap On/Off</source>
      <translation type="unfinished">Snap On/Off</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="102"/>
      <source>Increase snap radius</source>
      <translation type="unfinished">Increase snap radius</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="107"/>
      <source>Decrease snap radius</source>
      <translation type="unfinished">Decrease snap radius</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="112"/>
      <source>Restrict X</source>
      <translation type="unfinished">Restrict X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="117"/>
      <source>Restrict Y</source>
      <translation type="unfinished">Restrict Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="122"/>
      <source>Restrict Z</source>
      <translation type="unfinished">Restrict Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="127"/>
      <location filename="../../DraftGui.py" line="1040"/>
      <source>Select edge</source>
      <translation type="unfinished">Select edge</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="132"/>
      <source>Add custom snap point</source>
      <translation type="unfinished">Add custom snap point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="137"/>
      <source>Length mode</source>
      <translation type="unfinished">Length mode</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="142"/>
      <location filename="../../DraftGui.py" line="1024"/>
      <source>Wipe</source>
      <translation type="unfinished">Wipe</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="147"/>
      <source>Set Working Plane</source>
      <translation type="unfinished">Set Working Plane</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="152"/>
      <source>Cycle snap object</source>
      <translation type="unfinished">Cycle snap object</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="157"/>
      <source>Toggle near snap on/off</source>
      <translation type="unfinished">Toggle near snap on/off</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="351"/>
      <source>Draft Command Bar</source>
      <translation type="unfinished">Draft Command Bar</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="838"/>
      <source>Auto</source>
      <translation type="unfinished">Auto</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="919"/>
      <source>active command:</source>
      <translation type="unfinished">active command:</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="921"/>
      <source>Active Draft command</source>
      <translation type="unfinished">Active Draft command</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="922"/>
      <source>X coordinate of next point</source>
      <translation type="unfinished">X coordinate of next point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="923"/>
      <location filename="../../DraftGui.py" line="1378"/>
      <source>X</source>
      <translation type="unfinished">X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="924"/>
      <source>Y</source>
      <translation type="unfinished">Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="925"/>
      <source>Z</source>
      <translation type="unfinished">Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="926"/>
      <source>Y coordinate of next point</source>
      <translation type="unfinished">Y coordinate of next point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="927"/>
      <source>Z coordinate of next point</source>
      <translation type="unfinished">Z coordinate of next point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="928"/>
      <source>Enter point</source>
      <translation type="unfinished">Enter point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="931"/>
      <source>Enter a new point with the given coordinates</source>
      <translation type="unfinished">Enter a new point with the given coordinates</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="932"/>
      <source>Length</source>
      <translation>Длина</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="934"/>
      <source>Length of current segment</source>
      <translation type="unfinished">Length of current segment</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="935"/>
      <source>Angle of current segment</source>
      <translation type="unfinished">Angle of current segment</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="939"/>
      <source>Check this to lock the current angle</source>
      <translation type="unfinished">Check this to lock the current angle</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="943"/>
      <location filename="../../DraftGui.py" line="1428"/>
      <source>Radius</source>
      <translation type="unfinished">Radius</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="944"/>
      <location filename="../../DraftGui.py" line="1429"/>
      <source>Radius of Circle</source>
      <translation type="unfinished">Radius of Circle</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="957"/>
      <source>Coordinates relative to last point or to coordinate system origin
if is the first point to set</source>
      <translation type="unfinished">Coordinates relative to last point or to coordinate system origin
if is the first point to set</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="967"/>
      <source>Coordinates relative to global coordinate system.
Uncheck to use working plane coordinate system</source>
      <translation type="unfinished">Coordinates relative to global coordinate system.
Uncheck to use working plane coordinate system</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="969"/>
      <source>Filled</source>
      <translation type="unfinished">Filled</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="978"/>
      <source>Check this if the object should appear as filled, otherwise it will appear as wireframe.
Not available if Draft preference option &apos;Use Part Primitives&apos; is enabled</source>
      <translation type="unfinished">Check this if the object should appear as filled, otherwise it will appear as wireframe.
Not available if Draft preference option &apos;Use Part Primitives&apos; is enabled</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="980"/>
      <source>Finish</source>
      <translation type="unfinished">Finish</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="984"/>
      <source>Finishes the current drawing or editing operation</source>
      <translation type="unfinished">Finishes the current drawing or editing operation</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="991"/>
      <source>If checked, command will not finish until you press the command button again</source>
      <translation type="unfinished">If checked, command will not finish until you press the command button again</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1004"/>
      <source>If checked, an OCC-style offset will be performedinstead of the classic offset</source>
      <translation type="unfinished">If checked, an OCC-style offset will be performedinstead of the classic offset</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1005"/>
      <source>&amp;OCC-style offset</source>
      <translation type="unfinished">&amp;OCC-style offset</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1015"/>
      <source>&amp;Undo (CTRL+Z)</source>
      <translation type="unfinished">&amp;Undo (CTRL+Z)</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1016"/>
      <source>Undo the last segment</source>
      <translation type="unfinished">Undo the last segment</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1022"/>
      <source>Finishes and closes the current line</source>
      <translation type="unfinished">Finishes and closes the current line</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1031"/>
      <source>Wipes the existing segments of this line and starts again from the last point</source>
      <translation type="unfinished">Wipes the existing segments of this line and starts again from the last point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1033"/>
      <source>Set WP</source>
      <translation type="unfinished">Set WP</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1037"/>
      <source>Reorients the working plane on the last segment</source>
      <translation type="unfinished">Reorients the working plane on the last segment</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1048"/>
      <source>Selects an existing edge to be measured by this dimension</source>
      <translation type="unfinished">Selects an existing edge to be measured by this dimension</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1049"/>
      <source>Sides</source>
      <translation type="unfinished">Sides</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1050"/>
      <source>Number of sides</source>
      <translation type="unfinished">Number of sides</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1060"/>
      <source>If checked, objects will be copied instead of moved. Preferences -&gt; Draft -&gt; Global copy mode to keep this mode in next commands</source>
      <translation type="unfinished">If checked, objects will be copied instead of moved. Preferences -&gt; Draft -&gt; Global copy mode to keep this mode in next commands</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1063"/>
      <source>Modify subelements</source>
      <translation type="unfinished">Modify subelements</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1072"/>
      <source>If checked, subelements will be modified instead of entire objects</source>
      <translation type="unfinished">If checked, subelements will be modified instead of entire objects</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1073"/>
      <source>Text string to draw</source>
      <translation type="unfinished">Text string to draw</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1074"/>
      <source>String</source>
      <translation type="unfinished">String</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1075"/>
      <source>Height of text</source>
      <translation type="unfinished">Height of text</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1076"/>
      <source>Height</source>
      <translation type="unfinished">Height</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1077"/>
      <source>Intercharacter spacing</source>
      <translation type="unfinished">Intercharacter spacing</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1078"/>
      <source>Tracking</source>
      <translation type="unfinished">Tracking</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1079"/>
      <source>Full path to font file:</source>
      <translation type="unfinished">Full path to font file:</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1082"/>
      <source>Open a FileChooser for font file</source>
      <translation type="unfinished">Open a FileChooser for font file</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1083"/>
      <source>Create text</source>
      <translation type="unfinished">Create text</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1089"/>
      <source>Press this button to create the text object, or finish your text with two blank lines</source>
      <translation type="unfinished">Press this button to create the text object, or finish your text with two blank lines</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1116"/>
      <source>Change default style for new objects</source>
      <translation type="unfinished">Change default style for new objects</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1117"/>
      <source>Toggle construction mode</source>
      <translation type="unfinished">Toggle construction mode</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1118"/>
      <location filename="../../DraftGui.py" line="2444"/>
      <location filename="../../DraftGui.py" line="2464"/>
      <source>Autogroup off</source>
      <translation type="unfinished">Autogroup off</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1242"/>
      <source>Line</source>
      <translation type="unfinished">Line</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1257"/>
      <source>DWire</source>
      <translation type="unfinished">DWire</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1280"/>
      <source>Circle</source>
      <translation type="unfinished">Circle</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1285"/>
      <source>Arc</source>
      <translation type="unfinished">Arc</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1296"/>
      <source>Point</source>
      <translation type="unfinished">Point</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1332"/>
      <source>Label</source>
      <translation type="unfinished">Label</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1415"/>
      <source>Trimex</source>
      <translation type="unfinished">Trimex</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1529"/>
      <source>Pick Object</source>
      <translation type="unfinished">Pick Object</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1535"/>
      <source>Edit</source>
      <translation>Редактировать</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1585"/>
      <source>Local u0394X</source>
      <translation type="unfinished">Local u0394X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1586"/>
      <source>Local u0394Y</source>
      <translation type="unfinished">Local u0394Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1587"/>
      <source>Local u0394Z</source>
      <translation type="unfinished">Local u0394Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1589"/>
      <source>Local X</source>
      <translation type="unfinished">Local X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1590"/>
      <source>Local Y</source>
      <translation type="unfinished">Local Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1591"/>
      <source>Local Z</source>
      <translation type="unfinished">Local Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1593"/>
      <source>Global u0394X</source>
      <translation type="unfinished">Global u0394X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1594"/>
      <source>Global u0394Y</source>
      <translation type="unfinished">Global u0394Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1595"/>
      <source>Global u0394Z</source>
      <translation type="unfinished">Global u0394Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1597"/>
      <source>Global X</source>
      <translation type="unfinished">Global X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1598"/>
      <source>Global Y</source>
      <translation type="unfinished">Global Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1599"/>
      <source>Global Z</source>
      <translation type="unfinished">Global Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1846"/>
      <source>Invalid Size value. Using 200.0.</source>
      <translation type="unfinished">Invalid Size value. Using 200.0.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1856"/>
      <source>Invalid Tracking value. Using 0.</source>
      <translation type="unfinished">Invalid Tracking value. Using 0.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1874"/>
      <source>Please enter a text string.</source>
      <translation type="unfinished">Please enter a text string.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1882"/>
      <source>Select a Font file</source>
      <translation type="unfinished">Select a Font file</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1924"/>
      <source>Please enter a font file.</source>
      <translation type="unfinished">Please enter a font file.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2453"/>
      <source>Autogroup:</source>
      <translation type="unfinished">Autogroup:</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2819"/>
      <source>Faces</source>
      <translation type="unfinished">Faces</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2820"/>
      <source>Remove</source>
      <translation>Удалить</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2821"/>
      <source>Add</source>
      <translation type="unfinished">Add</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2824"/>
      <source>Facebinder elements</source>
      <translation type="unfinished">Facebinder elements</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_base.py" line="430"/>
      <source>Please load the Draft Workbench to enable editing this object</source>
      <translation type="unfinished">Please load the Draft Workbench to enable editing this object</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="370"/>
      <source>Activate this layer</source>
      <translation type="unfinished">Activate this layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="378"/>
      <source>Select layer contents</source>
      <translation type="unfinished">Select layer contents</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="418"/>
      <location filename="../../draftviewproviders/view_layer.py" line="437"/>
      <source>Merge layer duplicates</source>
      <translation type="unfinished">Merge layer duplicates</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="425"/>
      <location filename="../../draftviewproviders/view_layer.py" line="491"/>
      <source>Add new layer</source>
      <translation type="unfinished">Add new layer</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="475"/>
      <source>Relabeling layer:</source>
      <translation type="unfinished">Relabeling layer:</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="480"/>
      <source>Merging layer:</source>
      <translation type="unfinished">Merging layer:</translation>
    </message>
    <message>
      <location filename="../../WorkingPlane.py" line="748"/>
      <source>Selected Shapes must define a plane</source>
      <translation type="unfinished">Selected Shapes must define a plane</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/mirror.py" line="90"/>
      <source>No object given</source>
      <translation type="unfinished">No object given</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/mirror.py" line="94"/>
      <source>The two points are coincident</source>
      <translation type="unfinished">The two points are coincident</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/mirror.py" line="113"/>
      <source>mirrored</source>
      <translation type="unfinished">mirrored</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="151"/>
      <source>Object must be a closed shape</source>
      <translation type="unfinished">Object must be a closed shape</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="153"/>
      <source>No solid object created</source>
      <translation type="unfinished">No solid object created</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="277"/>
      <source>Faces must be coplanar to be refined</source>
      <translation type="unfinished">Faces must be coplanar to be refined</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="439"/>
      <location filename="../../draftfunctions/downgrade.py" line="237"/>
      <source>Upgrade: Unknown force method:</source>
      <translation type="unfinished">Upgrade: Unknown force method:</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="459"/>
      <source>Found groups: closing each open object inside</source>
      <translation type="unfinished">Found groups: closing each open object inside</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="465"/>
      <source>Found meshes: turning into Part shapes</source>
      <translation type="unfinished">Found meshes: turning into Part shapes</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="477"/>
      <source>Found 1 solidifiable object: solidifying it</source>
      <translation type="unfinished">Found 1 solidifiable object: solidifying it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="482"/>
      <source>Found 2 objects: fusing them</source>
      <translation type="unfinished">Found 2 objects: fusing them</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="503"/>
      <source>Found object with several coplanar faces: refine them</source>
      <translation type="unfinished">Found object with several coplanar faces: refine them</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="514"/>
      <source>Found 1 non-parametric objects: draftifying it</source>
      <translation type="unfinished">Found 1 non-parametric objects: draftifying it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="531"/>
      <source>Found 1 closed sketch object: creating a face from it</source>
      <translation type="unfinished">Found 1 closed sketch object: creating a face from it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="536"/>
      <source>Found closed wires: creating faces</source>
      <translation type="unfinished">Found closed wires: creating faces</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="546"/>
      <source>Found several wires or edges: wiring them</source>
      <translation type="unfinished">Found several wires or edges: wiring them</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="553"/>
      <location filename="../../draftfunctions/upgrade.py" line="595"/>
      <source>Found several non-treatable objects: creating compound</source>
      <translation type="unfinished">Found several non-treatable objects: creating compound</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="558"/>
      <source>trying: closing it</source>
      <translation type="unfinished">trying: closing it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="560"/>
      <source>Found 1 open wire: closing it</source>
      <translation type="unfinished">Found 1 open wire: closing it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="580"/>
      <source>Found 1 object: draftifying it</source>
      <translation type="unfinished">Found 1 object: draftifying it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="585"/>
      <source>Found points: creating compound</source>
      <translation type="unfinished">Found points: creating compound</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="598"/>
      <source>Unable to upgrade these objects.</source>
      <translation type="unfinished">Unable to upgrade these objects.</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="245"/>
      <source>Found 1 block: exploding it</source>
      <translation type="unfinished">Found 1 block: exploding it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="252"/>
      <source>Found 1 multi-solids compound: exploding it</source>
      <translation type="unfinished">Found 1 multi-solids compound: exploding it</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="266"/>
      <source>Found 1 parametric object: breaking its dependencies</source>
      <translation type="unfinished">Found 1 parametric object: breaking its dependencies</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="274"/>
      <source>Found 2 objects: subtracting them</source>
      <translation type="unfinished">Found 2 objects: subtracting them</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="281"/>
      <source>Found several faces: splitting them</source>
      <translation type="unfinished">Found several faces: splitting them</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="291"/>
      <source>Found several objects: subtracting them from the first one</source>
      <translation type="unfinished">Found several objects: subtracting them from the first one</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="296"/>
      <source>Found 1 face: extracting its wires</source>
      <translation type="unfinished">Found 1 face: extracting its wires</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="302"/>
      <source>Found only wires: extracting their edges</source>
      <translation type="unfinished">Found only wires: extracting their edges</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="306"/>
      <source>No more downgrade possible</source>
      <translation type="unfinished">No more downgrade possible</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="215"/>
      <location filename="../../importDWG.py" line="297"/>
      <source>LibreDWG error</source>
      <translation type="unfinished">LibreDWG error</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="225"/>
      <location filename="../../importDWG.py" line="307"/>
      <source>Converting:</source>
      <translation type="unfinished">Converting:</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="231"/>
      <source>Conversion successful</source>
      <translation type="unfinished">Conversion successful</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="239"/>
      <source>Error during DWG conversion. Try moving the DWG file to a directory path without spaces and non-english characters, or try saving to a lower DWG version.</source>
      <translation type="unfinished">Error during DWG conversion. Try moving the DWG file to a directory path without spaces and non-english characters, or try saving to a lower DWG version.</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="244"/>
      <location filename="../../importDWG.py" line="315"/>
      <source>ODA File Converter not found</source>
      <translation type="unfinished">ODA File Converter not found</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="258"/>
      <location filename="../../importDWG.py" line="326"/>
      <source>QCAD error</source>
      <translation type="unfinished">QCAD error</translation>
    </message>
  </context>
  <context>
    <name>importOCA</name>
    <message>
      <location filename="../../importOCA.py" line="358"/>
      <source>OCA error: couldn&apos;t determine character encoding</source>
      <translation type="unfinished">OCA error: couldn&apos;t determine character encoding</translation>
    </message>
    <message>
      <location filename="../../importOCA.py" line="442"/>
      <source>OCA: found no data to export</source>
      <translation type="unfinished">OCA: found no data to export</translation>
    </message>
    <message>
      <location filename="../../importOCA.py" line="486"/>
      <source>successfully exported</source>
      <translation type="unfinished">successfully exported</translation>
    </message>
  </context>
</TS>
