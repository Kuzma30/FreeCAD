<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk" sourcelanguage="en">
  <context>
    <name>App::Property</name>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="45"/>
      <source>The points of the Bezier curve</source>
      <translation>Точки кривої Безьє</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="48"/>
      <source>The degree of the Bezier function</source>
      <translation>Ступінь функції Безьє</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="51"/>
      <source>Continuity</source>
      <translation>Неперервність</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="56"/>
      <source>If the Bezier curve should be closed or not</source>
      <translation>Якщо крива Безьє має бути замкнута або ні</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="61"/>
      <source>Create a face if this curve is closed</source>
      <translation>Створіть грань, якщо ця крива замкнута</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bezcurve.py" line="64"/>
      <source>The length of this object</source>
      <translation>Довжина цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="56"/>
      <location filename="../../draftobjects/circle.py" line="61"/>
      <location filename="../../draftobjects/wire.py" line="91"/>
      <location filename="../../draftobjects/polygon.py" line="69"/>
      <location filename="../../draftobjects/bezcurve.py" line="67"/>
      <location filename="../../draftobjects/rectangle.py" line="73"/>
      <source>The area of this object</source>
      <translation>Форма цього об’єкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="45"/>
      <source>Start angle of the elliptical arc</source>
      <translation>Початковий кут еліптичної дуги</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="52"/>
      <source>End angle of the elliptical arc 

                (for a full circle, give it same value as First Angle)</source>
      <translation>Кінцевий кут еліптичної дуги
(для повного кола дайте йому те саме значення, що і перший кут)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="55"/>
      <source>Minor radius of the ellipse</source>
      <translation>Малий радіус еліпса</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="58"/>
      <source>Major radius of the ellipse</source>
      <translation>Більший радіус еліпса</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="61"/>
      <location filename="../../draftobjects/circle.py" line="58"/>
      <location filename="../../draftobjects/polygon.py" line="66"/>
      <location filename="../../draftobjects/rectangle.py" line="60"/>
      <source>Create a face</source>
      <translation>Створіть грань</translation>
    </message>
    <message>
      <location filename="../../draftobjects/ellipse.py" line="64"/>
      <source>Area of this object</source>
      <translation>Площа цього обʼєкту</translation>
    </message>
    <message>
      <location filename="../../draftobjects/circle.py" line="45"/>
      <source>Start angle of the arc</source>
      <translation>Початковий кут дуги</translation>
    </message>
    <message>
      <location filename="../../draftobjects/circle.py" line="52"/>
      <source>End angle of the arc (for a full circle, 
                give it same value as First Angle)</source>
      <translation>Кінцевий кут дуги (для повного кола,
надайте йому таке ж значення, як і перший кут)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/circle.py" line="55"/>
      <source>Radius of the circle</source>
      <translation>Радіус кола</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="44"/>
      <source>Linked faces</source>
      <translation>Повʼязані грані</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="49"/>
      <source>Specifies if splitter lines must be removed</source>
      <translation>Вказує, чи потрібно видаляти роздільні лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="54"/>
      <source>An optional extrusion value to be applied to all faces</source>
      <translation>Необов’язкове значення видавлювання, яке застосовується до всіх граней</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="59"/>
      <source>An optional offset value to be applied to all faces</source>
      <translation>Необов’язкове значення видавлювання, яке застосовується до всіх граней</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="62"/>
      <source>This specifies if the shapes sew</source>
      <translation>Це визначає, чи фігури зшиті</translation>
    </message>
    <message>
      <location filename="../../draftobjects/facebinder.py" line="67"/>
      <source>The area of the faces of this Facebinder</source>
      <translation>Площа граней цього Facebinder</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="83"/>
      <location filename="../../draftobjects/patharray.py" line="176"/>
      <source>The base object that will be duplicated</source>
      <translation>Базовий обʼєкт, який буде продубльований</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="101"/>
      <source>The type of array to create.
- Ortho: places the copies in the direction of the global X, Y, Z axes.
- Polar: places the copies along a circular arc, up to a specified angle, and with certain orientation defined by a center and an axis.
- Circular: places the copies in concentric circular layers around the base object.</source>
      <translation>Тип масиву, що створюється.
- Ортогональний: розміщує копії за напрямками глобальних вісей X, Y, Z.
- Полярний: розміщує копії вздовж дуги, на визначений кут та з орієнтацією, яка задається центром та віссю.
- Кільцевий: розміщує копії в концентричних кільцевих шарах навколо базового об'єкту.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="111"/>
      <source>Specifies if the copies should be fused together if they touch each other (slower)</source>
      <translation>Вказує, чи слід копіям поєднуватися, якщо вони торкаються одна одної (повільніше)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="120"/>
      <source>Number of copies in X direction</source>
      <translation>Кількість копій у напрямку осі X</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="125"/>
      <source>Number of copies in Y direction</source>
      <translation>Кількість копій у напрямку осі Y</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="130"/>
      <source>Number of copies in Z direction</source>
      <translation>Кількість копій у напрямку осі Z</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="138"/>
      <source>Distance and orientation of intervals in X direction</source>
      <translation>Відстань та орієнтація інтервалів у напрямку осі X</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="148"/>
      <source>Distance and orientation of intervals in Y direction</source>
      <translation>Відстань та орієнтація інтервалів у напрямку осі Y</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="158"/>
      <source>Distance and orientation of intervals in Z direction</source>
      <translation>Відстань та орієнтація інтервалів у напрямку осі Z</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="174"/>
      <source>The axis direction around which the elements in a polar or a circular array will be created</source>
      <translation>Напрямок осі, навколо якого будуть створені елементи полярного або кругового масиву</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="184"/>
      <source>Center point for polar and circular arrays.
The &apos;Axis&apos; passes through this point.</source>
      <translation>Центральна точка для полярних та кругових масивів.
 &apos;Вісь&apos; проходить через цю точку.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="204"/>
      <source>The axis object that overrides the value of &apos;Axis&apos; and &apos;Center&apos;, for example, a datum line.
Its placement, position and rotation, will be used when creating polar and circular arrays.
Leave this property empty to be able to set &apos;Axis&apos; and &apos;Center&apos; manually.</source>
      <translation>Об'єкт осі, який замінює значення "Вісь" та "Центр", наприклад, базисна лінія.
Його розміщення, положення та обертання будуть використовуватися при створенні полярних та кругових масивів.
Залиште цю властивість порожньою, щоб мати можливість вручну встановити "Осі" та "Центр".</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="215"/>
      <source>Number of copies in the polar direction</source>
      <translation>Кількість копій у напрямку полярних координат.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="223"/>
      <source>Distance and orientation of intervals in &apos;Axis&apos; direction</source>
      <translation>Відстань та орієнтація інтервалів у напрямку "Вісь"</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="230"/>
      <source>Angle to cover with copies</source>
      <translation>Кут для покриття копіями</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="241"/>
      <source>Distance between circular layers</source>
      <translation>Відстань між круговими шарами</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="250"/>
      <source>Distance between copies in the same circular layer</source>
      <translation>Відстань між копіями в одному круговому шарі</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="260"/>
      <source>Number of circular layers. The &apos;Base&apos; object counts as one layer.</source>
      <translation>Кількість кругових шарів. Об'єкт &apos;Base&apos; є одним шаром.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="272"/>
      <source>A parameter that determines how many symmetry planes the circular array will have.</source>
      <translation>Параметр, який визначає, скільки  симетричних площин матиме круговий масив.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="289"/>
      <source>Total number of elements in the array.
This property is read-only, as the number depends on the parameters of the array.</source>
      <translation>Загальна кількість елементів у масиві.
Ця властивість доступна лише для читання, оскільки число залежить від параметрів масиву.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/array.py" line="298"/>
      <location filename="../../draftobjects/pointarray.py" line="114"/>
      <location filename="../../draftobjects/pathtwistedarray.py" line="138"/>
      <location filename="../../draftobjects/patharray.py" line="214"/>
      <source>Show the individual array elements (only for Link arrays)</source>
      <translation>Показати окремі елементи масиву (лише для поєднаних масивів)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="45"/>
      <source>The points of the B-spline</source>
      <translation>Точки B-сплайну</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="48"/>
      <source>If the B-spline is closed or not</source>
      <translation>Якщо B-сплайн замкнений або ні</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="53"/>
      <source>Create a face if this spline is closed</source>
      <translation>Створити грань, якщо сплайн замкнутий</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="66"/>
      <source>Parameterization factor</source>
      <translation>Фактор параметризації</translation>
    </message>
    <message>
      <location filename="../../draftobjects/point.py" line="45"/>
      <source>X Location</source>
      <translation>Розташування по осі X</translation>
    </message>
    <message>
      <location filename="../../draftobjects/point.py" line="48"/>
      <source>Y Location</source>
      <translation>Розташування по осі Y</translation>
    </message>
    <message>
      <location filename="../../draftobjects/point.py" line="51"/>
      <source>Z Location</source>
      <translation>Розташування по осі Z</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draftlink.py" line="101"/>
      <source>Force sync pattern placements even when array elements are expanded</source>
      <translation>Примусова синхронізація різних розмірів навіть при розширенні елементів масиву</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draftlink.py" line="112"/>
      <source>Show the individual array elements</source>
      <translation>Показати окремі елементи масиву</translation>
    </message>
    <message>
      <location filename="../../draftobjects/clone.py" line="45"/>
      <source>The objects included in this clone</source>
      <translation>Обʼєкти, що входять в цей клон</translation>
    </message>
    <message>
      <location filename="../../draftobjects/clone.py" line="48"/>
      <source>The scale factor of this clone</source>
      <translation>Масштабний коефіцієнт цього клону</translation>
    </message>
    <message>
      <location filename="../../draftobjects/clone.py" line="55"/>
      <source>If Clones includes several objects,
set True for fusion or False for compound</source>
      <translation>Якщо "Клони" містять кілька об'єктів,
встановіть "Так" для сплавляння або "Ні" для поєднання.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="46"/>
      <source>Text string</source>
      <translation>Текстовий рядок</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="49"/>
      <source>Font file name</source>
      <translation>Імʼя файлу зі шрифтом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="52"/>
      <source>Height of text</source>
      <translation>Висота тексту</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="55"/>
      <source>Inter-character spacing</source>
      <translation>Міжсимвольна відстань</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="58"/>
      <source>Fill letters with faces</source>
      <translation>Заповніть літери гранями</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="48"/>
      <source>The vertices of the wire</source>
      <translation>Вершини каркасу</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="51"/>
      <source>If the wire is closed or not</source>
      <translation>Якщо каркас закритий чи ні</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="56"/>
      <source>The base object is the wire, it&apos;s formed from 2 objects</source>
      <translation>Базовий об'єкт - полілінія утворена з 2 об’єктів</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="61"/>
      <source>The tool object is the wire, it&apos;s formed from 2 objects</source>
      <translation type="unfinished">The tool object is the wire, it&apos;s formed from 2 objects</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="64"/>
      <source>The start point of this line</source>
      <translation>Початкова точка цієї лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="67"/>
      <source>The end point of this line</source>
      <translation>Кінцева точка цієї лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="70"/>
      <source>The length of this line</source>
      <translation>Довжина цієї лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="73"/>
      <location filename="../../draftobjects/polygon.py" line="58"/>
      <location filename="../../draftobjects/rectangle.py" line="52"/>
      <source>Radius to use to fillet the corners</source>
      <translation>Радіус для заокруглення кутів</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="78"/>
      <location filename="../../draftobjects/polygon.py" line="63"/>
      <location filename="../../draftobjects/rectangle.py" line="57"/>
      <source>Size of the chamfer to give to the corners</source>
      <translation>Розмір фаски для кутів</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="83"/>
      <source>Create a face if this object is closed</source>
      <translation>Створити грань, якщо обʼєкт замкнений</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wire.py" line="88"/>
      <source>The number of subdivisions of each edge</source>
      <translation>Кількість ділень кожного ребра</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="58"/>
      <source>The base object this 2D view must represent</source>
      <translation>Базовий об’єкт, який повинен представляти цей 2D-вигляд</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="63"/>
      <source>The projection vector of this object</source>
      <translation>Вектор проекції цього обʼєкту</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="69"/>
      <source>The way the viewed object must be projected</source>
      <translation>Спосіб, яким обʼєкт, що розглядається, має бути спроектованим</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="82"/>
      <source>The indices of the faces to be projected in Individual Faces mode</source>
      <translation>Індекси граней для проєкції в режимі Individual Faces (Окремі Грані)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="85"/>
      <source>Show hidden lines</source>
      <translation>Відобразити приховані рядки</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="92"/>
      <source>Fuse wall and structure objects of same type and material</source>
      <translation>Запобіжники стін і структури об'єктів одного типу і матеріалу</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="97"/>
      <source>Tessellate Ellipses and B-splines into line segments</source>
      <translation>Поділ еліпсів та B-сплайнів на лінійні відрізки</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="105"/>
      <source>For Cutlines and Cutfaces modes, 
                    this leaves the faces at the cut location</source>
      <translation>Для режимів обрізки ліній та обрізки граней, 
                    це залишає грані в місці вирізу</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="113"/>
      <source>Length of line segments if tessellating Ellipses or B-splines 
                    into line segments</source>
      <translation>Довжина відрізків ліній, якщо фрагментувати еліпси або B-сплайни
на відрізки лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="120"/>
      <source>If this is True, this object will include only visible objects</source>
      <translation>Якщо це Обрано цей об'єкт буде включати лише видимі об'єкти</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="127"/>
      <source>A list of exclusion points. Any edge touching any of those points will not be drawn.</source>
      <translation>Список виняткових точок. Будь-яке ребра, торкаючись будь-якого з цих точок, не буде намальовано.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="133"/>
      <source>If this is True, only solid geometry is handled. This overrides the base object&apos;s Only Solids property</source>
      <translation>Якщо ця опція встановлена, обробляється лише суцільна геометрія. Це замінює властивість "Тільки суцільні тіла" базового об'єкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="139"/>
      <source>If this is True, the contents are clipped to the borders of the section plane, if applicable. This overrides the base object&apos;s Clip property</source>
      <translation>Якщо це Істина, вміст відсікається до меж площини перерізу, якщо це можливо. Це замінює властивість Обрізки базового об'єкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shape2dview.py" line="144"/>
      <source>This object will be recomputed only if this is True.</source>
      <translation>Цей обʼєкт буде переобчислено лише за умови Обрано.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="46"/>
      <source>Length of the rectangle</source>
      <translation>Довжина прямокутника</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="49"/>
      <source>Height of the rectangle</source>
      <translation>Висота прямокутника</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="65"/>
      <source>Horizontal subdivisions of this rectangle</source>
      <translation>Горизонтальні поділ цього прямокутника</translation>
    </message>
    <message>
      <location filename="../../draftobjects/rectangle.py" line="70"/>
      <source>Vertical subdivisions of this rectangle</source>
      <translation>Вертикальні поділ цього прямокутника</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="65"/>
      <source>The linked object</source>
      <translation>Повʼязаний обʼєкт</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="68"/>
      <source>Projection direction</source>
      <translation>Напрямок проекції</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="73"/>
      <source>The width of the lines inside this object</source>
      <translation>Ширина ліній всередині цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="79"/>
      <source>The size of the texts inside this object</source>
      <translation>Розмір тексту всередині цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="83"/>
      <source>The spacing between lines of text</source>
      <translation>Інтервал між лініями тексту</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="86"/>
      <source>The color of the projected objects</source>
      <translation>Колір проектованих обʼєктів</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="89"/>
      <source>Shape Fill Style</source>
      <translation>Тип заливки фігури</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="93"/>
      <source>Line Style</source>
      <translation>Стиль лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/drawingview.py" line="101"/>
      <source>If checked, source objects are displayed regardless of being visible in the 3D model</source>
      <translation>Якщо відмічено, то вихідні об'єкти відображаються, незалежно від того, чи вони видимі в 3D моделі</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="47"/>
      <source>The start point of this line.</source>
      <translation>Початкова точка цієї лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="52"/>
      <source>The end point of this line.</source>
      <translation>Кінцева точка цієї лінії.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="57"/>
      <source>The length of this line.</source>
      <translation>Довжина цієї лінії
</translation>
    </message>
    <message>
      <location filename="../../draftobjects/fillet.py" line="64"/>
      <source>Radius to use to fillet the corner.</source>
      <translation>Радіус скруглення кута.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="81"/>
      <source>Base object that will be duplicated</source>
      <translation>Базовий обʼєкт, який буде продубльований</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="89"/>
      <source>Object containing points used to distribute the base object, for example, a sketch or a Part compound.
The sketch or compound must contain at least one explicit point or vertex object.</source>
      <translation>Об'єкт, що містить точки, що використовуються для розподілу базового об'єкта, наприклад, ескіз або складова частина.
Ескізний або складний повинен містити принаймні одну явну точку або об'єкт вершини.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="97"/>
      <source>Total number of elements in the array.
This property is read-only, as the number depends on the points contained within &apos;Point Object&apos;.</source>
      <translation>Загальна кількість елементів у масиві.
Ця властивість доступна лише для читання, оскільки кількість залежить від точок, що містяться в "Point Object".</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="106"/>
      <location filename="../../draftobjects/pointarray.py" line="142"/>
      <source>Additional placement, shift and rotation, that will be applied to each copy</source>
      <translation>Додаткове розміщення, зміщення та обертання, які застосовуватимуться до кожної копії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="137"/>
      <source>The normal direction of the text of the dimension</source>
      <translation>Перпендикулярний напрямок тексту розмірності</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="147"/>
      <source>The object measured by this dimension object</source>
      <translation>Обʼєкт, виміряний цим розмірним обʼєктом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="163"/>
      <source>The object, and specific subelements of it,
that this dimension object is measuring.

There are various possibilities:
- An object, and one of its edges.
- An object, and two of its vertices.
- An arc object, and its edge.</source>
      <translation>Об'єкт і конкретні його субелементи,
що цей розмірний об’єкт вимірює.

Є різні можливості:
- Предмет і одне з його ребер.
- Об'єкт і дві його вершини.
- Криволінійний об'єкт та його ребро.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="187"/>
      <source>A point through which the dimension line, or an extrapolation of it, will pass.

- For linear dimensions, this property controls how close the dimension line
is to the measured object.
- For radial dimensions, this controls the direction of the dimension line
that displays the measured radius or diameter.
- For angular dimensions, this controls the radius of the dimension arc
that displays the measured angle.</source>
      <translation>Точка, через яку буде проходити розмірна лінія або її екстраполяція.

- Для лінійних розмірів ця властивість визначає, наскільки близько розмірна лінія
відноситься до вимірюваного об'єкта.
- Для радіальних розмірів це керує напрямком розмірної лінії
який відображає виміряний радіус або діаметр.
- Для кутових розмірів це керує радіусом розмірної дуги
який відображає виміряний кут.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="229"/>
      <source>Starting point of the dimension line.

If it is a radius dimension it will be the center of the arc.
If it is a diameter dimension it will be a point that lies on the arc.</source>
      <translation>Початкова точка розмірної лінії.

Якщо це радіус, це буде центр дуги.
Якщо це розмір діаметра, то це буде точка, що лежить на дузі.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="244"/>
      <source>Ending point of the dimension line.

If it is a radius or diameter dimension
it will be a point that lies on the arc.</source>
      <translation>Кінцева точка розмірної лінії.

Якщо це радіус або діаметр
це буде точка, що лежить на дузі.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="257"/>
      <source>The direction of the dimension line.
If this remains &apos;(0,0,0)&apos;, the direction will be calculated automatically.</source>
      <translation>Напрямок розмірної лінії.
Якщо це значення залишається '(0,0,0)', напрямок буде обчислюватися автоматично.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="275"/>
      <source>The value of the measurement.

This property is read-only because the value is calculated
from the &apos;Start&apos; and &apos;End&apos; properties.

If the &apos;Linked Geometry&apos; is an arc or circle, this &apos;Distance&apos;
is the radius or diameter, depending on the &apos;Diameter&apos; property.</source>
      <translation>Значення вимірювання.

Ця властивість доступна лише для читання, оскільки обчислюється значення
із властивостей «Початок» і «Кінець».

Якщо "Звʼязана геометрія" є дугою або колом, ця "Відстань"
– радіус або діаметр, залежно від властивості «Діаметр».</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="287"/>
      <source>When measuring circular arcs, it determines whether to display
the radius or the diameter value</source>
      <translation>Під час вимірювання кругових дуг він визначає, чи відображати його
радіус чи значення діаметра</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="504"/>
      <source>Starting angle of the dimension line (circular arc).
The arc is drawn counter-clockwise.</source>
      <translation>Початковий кут розмірної лінії (дуга окружності).
Дугу малюють проти годинникової стрілки.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="516"/>
      <source>Ending angle of the dimension line (circular arc).
The arc is drawn counter-clockwise.</source>
      <translation>Кут закінчення розмірної лінії (кругова дуга).
Дуга проведена проти годинникової стрілки.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="532"/>
      <source>The center point of the dimension line, which is a circular arc.

This is normally the point where two line segments, or their extensions
intersect, resulting in the measured &apos;Angle&apos; between them.</source>
      <translation>Центральна точка розмірної лінії, яка є круговою дугою.

Зазвичай це точка перетину двох відрізків або їх продовження,
в результаті чого вимірюється "Кут'' між ними.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/dimension.py" line="547"/>
      <source>The value of the measurement.

This property is read-only because the value is calculated from
the &apos;First Angle&apos; and &apos;Last Angle&apos; properties.</source>
      <translation>Значення вимірювання.

Ця властивість доступна лише для читання, оскільки значення обчислюється з
властивості "Перший кут" та "Останній кут".</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="67"/>
      <source>The position of the tip of the leader line.
This point can be decorated with an arrow or another symbol.</source>
      <translation>Положення кінчика лінійки лідера.
Цю точку можна прикрасити стрілкою або іншим символом.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="80"/>
      <source>Object, and optionally subelement, whose properties will be displayed
as &apos;Text&apos;, depending on &apos;Label Type&apos;.

&apos;Target&apos; won&apos;t be used if &apos;Label Type&apos; is set to &apos;Custom&apos;.</source>
      <translation>Об'єкт, і, за потреби, піделемент, властивості якого показуватися 
як &apos;Text&apos;, залежно від &apos;Label Type&apos;.

&apos;Target&apos; не буде використовуватися, якщо &apos;Label Type&apos; встановлений на &apos;Custom&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="109"/>
      <source>The list of points defining the leader line; normally a list of three points.

The first point should be the position of the text, that is, the &apos;Placement&apos;,
and the last point should be the tip of the line, that is, the &apos;Target Point&apos;.
The middle point is calculated automatically depending on the chosen
&apos;Straight Direction&apos; and the &apos;Straight Distance&apos; value and sign.

If &apos;Straight Direction&apos; is set to &apos;Custom&apos;, the &apos;Points&apos; property
can be set as a list of arbitrary points.</source>
      <translation type="unfinished">The list of points defining the leader line; normally a list of three points.

The first point should be the position of the text, that is, the &apos;Placement&apos;,
and the last point should be the tip of the line, that is, the &apos;Target Point&apos;.
The middle point is calculated automatically depending on the chosen
&apos;Straight Direction&apos; and the &apos;Straight Distance&apos; value and sign.

If &apos;Straight Direction&apos; is set to &apos;Custom&apos;, the &apos;Points&apos; property
can be set as a list of arbitrary points.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="123"/>
      <source>The direction of the straight segment of the leader line.

If &apos;Custom&apos; is chosen, the points of the leader can be specified by
assigning a custom list to the &apos;Points&apos; attribute.</source>
      <translation>Напрямок прямого відрізка виносної лінії.

Якщо вибрано "Користувацький", точки виносної лінії можуть бути вказані
присвоєнням спеціального списку атрибуту "Точки".</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="142"/>
      <source>The length of the straight segment of the leader line.

This is an oriented distance; if it is negative, the line will be drawn
to the left or below the &apos;Text&apos;, otherwise to the right or above it,
depending on the value of &apos;Straight Direction&apos;.</source>
      <translation>Довжина прямого відрізка виносної лінії.

Це орієнтована відстань; якщо воно від’ємне, буде проведена лінія
ліворуч або нижче "Тексту", інакше праворуч або над ним,
залежно від значення "Прямий напрямок".</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="153"/>
      <source>The placement of the &apos;Text&apos; element in 3D space</source>
      <translation>Розміщення елемента "Текст" у 3D просторі</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="161"/>
      <source>The text to display when &apos;Label Type&apos; is set to &apos;Custom&apos;</source>
      <translation>Текст для показу коли &apos;Тип мітки&apos; встановлений на &apos;Користувацький&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="175"/>
      <source>The text displayed by this label.

This property is read-only, as the final text depends on &apos;Label Type&apos;,
and the object defined in &apos;Target&apos;.
The &apos;Custom Text&apos; is displayed only if &apos;Label Type&apos; is set to &apos;Custom&apos;.</source>
      <translation>Текст, що показуватися цією міткою.

Ця властивість доступна лише для читання, оскільки остаточний текст залежить від "Тип мітки",
і об'єкту, визначеного у "Ціль".
"Custom Text" показує лише в тому випадку, якщо для параметра "Тип мітки" встановлено значення "Custom".</translation>
    </message>
    <message>
      <location filename="../../draftobjects/label.py" line="209"/>
      <source>The type of information displayed by this label.

If &apos;Custom&apos; is chosen, the contents of &apos;Custom Text&apos; will be used.
For other types, the string will be calculated automatically from the object defined in &apos;Target&apos;.
&apos;Tag&apos; and &apos;Material&apos; only work for objects that have these properties, like Arch objects.

For &apos;Position&apos;, &apos;Length&apos;, and &apos;Area&apos; these properties will be extracted from the main object in &apos;Target&apos;,
or from the subelement &apos;VertexN&apos;, &apos;EdgeN&apos;, or &apos;FaceN&apos;, respectively, if it is specified.</source>
      <translation type="unfinished">The type of information displayed by this label.

If &apos;Custom&apos; is chosen, the contents of &apos;Custom Text&apos; will be used.
For other types, the string will be calculated automatically from the object defined in &apos;Target&apos;.
&apos;Tag&apos; and &apos;Material&apos; only work for objects that have these properties, like Arch objects.

For &apos;Position&apos;, &apos;Length&apos;, and &apos;Area&apos; these properties will be extracted from the main object in &apos;Target&apos;,
or from the subelement &apos;VertexN&apos;, &apos;EdgeN&apos;, or &apos;FaceN&apos;, respectively, if it is specified.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="94"/>
      <source>The base object that will be duplicated.</source>
      <translation>Базовий обʼєкт, який буде продубльований.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="106"/>
      <location filename="../../draftobjects/patharray.py" line="184"/>
      <source>The object along which the copies will be distributed. It must contain &apos;Edges&apos;.</source>
      <translation type="unfinished">The object along which the copies will be distributed. It must contain &apos;Edges&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="115"/>
      <source>Number of copies to create.</source>
      <translation>Кількість копій для створення.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pathtwistedarray.py" line="126"/>
      <source>Rotation factor of the twisted array.</source>
      <translation>Коефіцієнт обертання крученого масиву.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/polygon.py" line="47"/>
      <source>Number of faces</source>
      <translation>Кількість граней</translation>
    </message>
    <message>
      <location filename="../../draftobjects/polygon.py" line="50"/>
      <source>Radius of the control circle</source>
      <translation>Радіус контрольного кола</translation>
    </message>
    <message>
      <location filename="../../draftobjects/polygon.py" line="55"/>
      <source>How the polygon must be drawn from the control circle</source>
      <translation>Як потрібно креслити багатокутник із контрольного кола</translation>
    </message>
    <message>
      <location filename="../../draftobjects/block.py" line="42"/>
      <source>The components of this block</source>
      <translation>Складові цього блоку</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="48"/>
      <source>The base object used by this object</source>
      <translation>Базовий обʼєкт використовується цим об’єктом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="55"/>
      <source>The PAT file used by this object</source>
      <translation>Файл PAT, який використовується цим об’єктом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="64"/>
      <source>The pattern name used by this object</source>
      <translation>Назва шаблону, що використовується цим обʼєктом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="73"/>
      <source>The pattern scale used by this object</source>
      <translation>Масштабований паттерн (повторюване зображення), який використовується цим обʼєктом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="82"/>
      <source>The pattern rotation used by this object</source>
      <translation>Масштабований паттерн (повторюване зображення), який використовується цим обʼєктом</translation>
    </message>
    <message>
      <location filename="../../draftobjects/hatch.py" line="92"/>
      <source>If set to False, hatch is applied as is to the faces, without translation (this might give wrong results for non-XY faces)</source>
      <translation>Якщо встановлено False, штрихування застосовується до граней без перетворення (це може призвести до неправильних результатів для граней, розміщених не на осі XY)</translation>
    </message>
    <message>
      <location filename="../../draftobjects/text.py" line="54"/>
      <source>The placement of the base point of the first line</source>
      <translation>Розміщення базової точки першої лінії</translation>
    </message>
    <message>
      <location filename="../../draftobjects/text.py" line="65"/>
      <source>The text displayed by this object.
It is a list of strings; each element in the list will be displayed in its own line.</source>
      <translation>Текст, що відображається цим об’єктом.
Це список рядків; кожен елемент у списку відображатиметься у своєму рядку.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="199"/>
      <source>List of connected edges in the &apos;Path Object&apos;.
If these are present, the copies will be created along these subelements only.
Leave this property empty to create copies along the entire &apos;Path Object&apos;.</source>
      <translation type="unfinished">List of connected edges in the &apos;Path Object&apos;.
If these are present, the copies will be created along these subelements only.
Leave this property empty to create copies along the entire &apos;Path Object&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="206"/>
      <source>Number of copies to create</source>
      <translation>Кількість копій для створення</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="225"/>
      <source>Additional translation that will be applied to each copy.
This is useful to adjust for the difference between shape centre and shape reference point.</source>
      <translation>Додатковий переклад, який буде застосовано до кожної копії.
Це корисно для регулювання різниці між центром форми та форми орієнтира.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="234"/>
      <source>Alignment vector for &apos;Tangent&apos; mode</source>
      <translation type="unfinished">Alignment vector for &apos;Tangent&apos; mode</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="242"/>
      <source>Force use of &apos;Vertical Vector&apos; as local Z direction when using &apos;Original&apos; or &apos;Tangent&apos; alignment mode</source>
      <translation type="unfinished">Force use of &apos;Vertical Vector&apos; as local Z direction when using &apos;Original&apos; or &apos;Tangent&apos; alignment mode</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="250"/>
      <source>Direction of the local Z axis when &apos;Force Vertical&apos; is true</source>
      <translation type="unfinished">Direction of the local Z axis when &apos;Force Vertical&apos; is true</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="258"/>
      <source>Method to orient the copies along the path.
- Original: X is curve tangent, Y is normal, and Z is the cross product.
- Frenet: aligns the object following the local coordinate system along the path.
- Tangent: similar to &apos;Original&apos; but the local X axis is pre-aligned to &apos;Tangent Vector&apos;.

To get better results with &apos;Original&apos; or &apos;Tangent&apos; you may have to set &apos;Force Vertical&apos; to true.</source>
      <translation type="unfinished">Method to orient the copies along the path.
- Original: X is curve tangent, Y is normal, and Z is the cross product.
- Frenet: aligns the object following the local coordinate system along the path.
- Tangent: similar to &apos;Original&apos; but the local X axis is pre-aligned to &apos;Tangent Vector&apos;.

To get better results with &apos;Original&apos; or &apos;Tangent&apos; you may have to set &apos;Force Vertical&apos; to true.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="269"/>
      <source>Orient the copies along the path depending on the &apos;Align Mode&apos;.
Otherwise the copies will have the same orientation as the original Base object.</source>
      <translation type="unfinished">Orient the copies along the path depending on the &apos;Align Mode&apos;.
Otherwise the copies will have the same orientation as the original Base object.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/wpproxy.py" line="42"/>
      <source>The placement of this object</source>
      <translation>Розміщення цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="85"/>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="84"/>
      <source>General scaling factor that affects the annotation consistently
because it scales the text, and the line decorations, if any,
in the same proportion.</source>
      <translation>Загальний коефіцієнт масштабування, який впливає на анотацію послідовно
тому що він масштабує текст і прикраси ліній, якщо такі є,
в тій же пропорції.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="102"/>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="103"/>
      <source>Annotation style to apply to this object.
When using a saved style some of the view properties will become read-only;
they will only be editable by changing the style through the &apos;Annotation style editor&apos; tool.</source>
      <translation type="unfinished">Annotation style to apply to this object.
When using a saved style some of the view properties will become read-only;
they will only be editable by changing the style through the &apos;Annotation style editor&apos; tool.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/layer.py" line="60"/>
      <source>The objects that are part of this layer</source>
      <translation>Обʼєкти, що входять до складу цього шару</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_base.py" line="107"/>
      <source>Defines an SVG pattern.</source>
      <translation>Визначає шаблон SVG.</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_base.py" line="120"/>
      <source>Defines the size of the SVG pattern.</source>
      <translation>Визначає розмір шаблону SVG.</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="117"/>
      <location filename="../../draftviewproviders/view_label.py" line="140"/>
      <source>Line width</source>
      <translation>Ширина лінії</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_draft_annotation.py" line="121"/>
      <location filename="../../draftviewproviders/view_label.py" line="145"/>
      <source>Line color</source>
      <translation>Колір лінії</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="69"/>
      <source>If it is true, the objects contained within this layer will adopt the line color of the layer</source>
      <translation>Якщо це обрано, об'єкту, що містяться в цьому шарі, матимуть кольори ліній цього шару</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="81"/>
      <source>If it is true, the objects contained within this layer will adopt the shape color of the layer</source>
      <translation>Якщо це обрано, об'єкту, що містяться в цьому шарі, матимуть кольори ліній цього шару</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="93"/>
      <source>If it is true, the print color will be used when objects in this layer are placed on a TechDraw page</source>
      <translation>Якщо це обрано, то друкуватиметься колір коли об’єкти в цьому шарі розміщеному на сторінці Креслення</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="104"/>
      <source>The line color of the objects contained within this layer</source>
      <translation>Колір лінії об'єкта, що міститься в цьому шарі</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="118"/>
      <source>The shape color of the objects contained within this layer</source>
      <translation>Колір форми об'єктів, що міститься в цьому шарі</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="132"/>
      <source>The line width of the objects contained within this layer</source>
      <translation>Колір лінії об'єкта, що міститься в цьому шарі</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="142"/>
      <source>The draw style of the objects contained within this layer</source>
      <translation>Стиль малювання об’єктів, що містяться в цьому шарі</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="151"/>
      <source>The transparency of the objects contained within this layer</source>
      <translation>Прозорість обʼєктів, що міститься в цьому шарі</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="161"/>
      <source>The line color of the objects contained within this layer, when used on a TechDraw page</source>
      <translation>Колір лінії обʼєктів, що міститься в цьому шарі, при використанні сторінки TechDraw</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="58"/>
      <location filename="../../draftviewproviders/view_label.py" line="72"/>
      <source>The size of the text</source>
      <translation>Розмір тексту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="63"/>
      <location filename="../../draftviewproviders/view_label.py" line="77"/>
      <source>The font of the text</source>
      <translation>Шрифт тексту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="70"/>
      <location filename="../../draftviewproviders/view_label.py" line="84"/>
      <location filename="../../draftviewproviders/view_label.py" line="103"/>
      <source>The vertical alignment of the text</source>
      <translation>Вертикальне вирівнювання тексту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="75"/>
      <location filename="../../draftviewproviders/view_label.py" line="90"/>
      <source>Text color</source>
      <translation>Колір тексту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="97"/>
      <source>The maximum number of characters on each line of the text box</source>
      <translation>Максимальна кількість символів в кожному рядку текстового поля</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_text.py" line="81"/>
      <location filename="../../draftviewproviders/view_label.py" line="110"/>
      <source>Line spacing (relative to font size)</source>
      <translation>Міжрядковий інтервал (по відношенню до розміру шрифту)</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="117"/>
      <source>The size of the arrow</source>
      <translation>Розмір стрілки</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="122"/>
      <source>The type of arrow of this label</source>
      <translation>Тип стрілки цієї позначки</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="130"/>
      <source>The type of frame around the text of this object</source>
      <translation>Тип рамки навколо тексту цього обʼєкта</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_label.py" line="135"/>
      <source>Display a leader line or not</source>
      <translation>Показати виносну лінію чи ні</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="135"/>
      <source>Font name</source>
      <translation>Назва шрифту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="140"/>
      <source>Font size</source>
      <translation>Розмір шрифту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="147"/>
      <source>Spacing between text and dimension line</source>
      <translation>Інтервал між текстом та розмірною лінією</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="154"/>
      <source>Rotate the dimension text 180 degrees</source>
      <translation>Поворот тексту з розміром на 180 градусів</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="162"/>
      <source>Text Position.
Leave &apos;(0,0,0)&apos; for automatic position</source>
      <translation type="unfinished">Text Position.
Leave &apos;(0,0,0)&apos; for automatic position</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="174"/>
      <source>Text override.
Write &apos;$dim&apos; so that it is replaced by the dimension length.</source>
      <translation type="unfinished">Text override.
Write &apos;$dim&apos; so that it is replaced by the dimension length.</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="181"/>
      <source>The number of decimals to show</source>
      <translation>Показати кількість десяткових знаків</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="186"/>
      <source>Show the unit suffix</source>
      <translation>Показати суфікс одиниці</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="196"/>
      <source>A unit to express the measurement.
Leave blank for system default.
Use &apos;arch&apos; to force US arch notation</source>
      <translation type="unfinished">A unit to express the measurement.
Leave blank for system default.
Use &apos;arch&apos; to force US arch notation</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="205"/>
      <source>Arrow size</source>
      <translation>Розмір стрілки</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="210"/>
      <source>Arrow type</source>
      <translation>Тип стрілки</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="218"/>
      <source>Rotate the dimension arrows 180 degrees</source>
      <translation>Поверніть розмірні стрілки на 180 градусів</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="228"/>
      <source>The distance the dimension line is extended
past the extension lines</source>
      <translation>Відстань розмірної лінії продовжено
за лініями розширення</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="233"/>
      <source>Length of the extension lines</source>
      <translation>Довжина виносних ліній</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="241"/>
      <source>Length of the extension line
beyond the dimension line</source>
      <translation>Довжина виносної лінії
поза розмірної лінії</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_dimension.py" line="248"/>
      <source>Shows the dimension line and arrows</source>
      <translation>Показує розмірну лінію та стрілки</translation>
    </message>
  </context>
  <context>
    <name>Dialog</name>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="14"/>
      <source>Annotation Styles Editor</source>
      <translation>Редактор стилів анотації</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="20"/>
      <source>Style name</source>
      <translation>Назва стилю</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="38"/>
      <source>The name of your style. Existing style names can be edited.</source>
      <translation>Назва вашого стилю. Існуючі назви стилів можна редагувати.</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="50"/>
      <source>Add new...</source>
      <translation>Додати новий...</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="79"/>
      <source>Renames the selected style</source>
      <translation>Перейменовує вибрані стилі</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="82"/>
      <source>Rename</source>
      <translation>Перейменувати</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="110"/>
      <source>Deletes the selected style</source>
      <translation>Видаляє вибраний стиль</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="113"/>
      <source>Delete</source>
      <translation>Видалити</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="120"/>
      <source>Import styles from json file</source>
      <translation>Імпортувати стилі з json файлу</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="130"/>
      <source>Export styles to json file</source>
      <translation>Експортувати стилі в json файл</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="164"/>
      <source>Text</source>
      <translation>Текст</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="170"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="192"/>
      <source>The font to use for texts and dimensions</source>
      <translation>Шрифт для використання в текстах та розмірах</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="173"/>
      <source>Font name</source>
      <translation>Назва шрифту</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="199"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="209"/>
      <source>Font size in the system units</source>
      <translation>Розмір шрифту в системних одиницях</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="202"/>
      <source>Font size</source>
      <translation>Розмір шрифту</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="219"/>
      <source>Line spacing in system units</source>
      <translation>Інтервал між рядками в системних одиницях</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="222"/>
      <source>Line spacing</source>
      <translation>Інтервал</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="239"/>
      <source>Units</source>
      <translation>Одиниці</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="245"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="255"/>
      <source>A multiplier factor that affects the size of texts and markers</source>
      <translation>Фактор мультиплікатора, що впливає на розмір текстів і маркерів</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="248"/>
      <source>Scale multiplier</source>
      <translation>Множник масштабу</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="268"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="278"/>
      <source>If it is checked it will show the unit next to the dimension value</source>
      <translation>Якщо це позначено, він покаже одиницю виміру поряд із значенням розміру</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="271"/>
      <source>Show unit</source>
      <translation>Показати величини</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="291"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="301"/>
      <source>Specify a valid length unit like mm, m, in, ft, to force displaying the dimension value in this unit</source>
      <translation>Вкажіть одиницю довжини, як мм, дюйм, фут, щоб примусово відобразити значення розміру в цих одиницях</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="294"/>
      <source>Unit override</source>
      <translation>Задати величину</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="308"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="318"/>
      <source>The number of decimals to show for dimension values</source>
      <translation>Кількість десяткових знаків для відображення для значень виміру</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="311"/>
      <source>Decimals</source>
      <translation>Десяткові знаки</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="331"/>
      <source>Line and arrows</source>
      <translation>Лінія та стрілки</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="337"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="347"/>
      <source>If it is checked it will display the dimension line</source>
      <translation>Якщо позначено, то відобразиться розмірна лінія</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="340"/>
      <source>Show lines</source>
      <translation>Показати лінії</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="363"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="373"/>
      <source>The width of the dimension lines</source>
      <translation>Ширина розмірних ліній</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="366"/>
      <source>Line width</source>
      <translation>Ширина лінії</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="376"/>
      <source>px</source>
      <translation>пікс.</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="386"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="396"/>
      <source>The color of dimension lines, arrows and texts</source>
      <translation>Колір розмірних ліній, стрілок та тексту</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="389"/>
      <source>Line / text color</source>
      <translation>Ряд/колір тексту</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="410"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="432"/>
      <source>The type of arrows or markers to use at the end of dimension lines</source>
      <translation>Тип стрілок чи міток для використання в кінці ліній розміру</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="413"/>
      <source>Arrow type</source>
      <translation>Тип стрілки</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="436"/>
      <source>Dot</source>
      <translation>Крапка</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="441"/>
      <source>Circle</source>
      <translation>Коло</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="446"/>
      <source>Arrow</source>
      <translation>Стрілка</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="451"/>
      <source>Tick</source>
      <translation>Позначка</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="456"/>
      <source>Tick-2</source>
      <translation>Тік-2</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="464"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="474"/>
      <source>The size of the dimension arrows or markers in system units</source>
      <translation>Розмір стрілок розміру або маркерів в системних одиницях</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="467"/>
      <source>Arrow size</source>
      <translation>Розмір стрілки</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="484"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="494"/>
      <source>The distance that the dimension line is additionally extended</source>
      <translation>Відстань, на яку розташована лінія розміру додатково продовжена</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="487"/>
      <source>Dimension overshoot</source>
      <translation>Перевищення розміру</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="504"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="514"/>
      <source>The length of the extension lines</source>
      <translation>Довжина виносних ліній</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="507"/>
      <source>Extension lines</source>
      <translation>Виноски</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="524"/>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="534"/>
      <source>The distance that the extension lines are additionally extended beyond the dimension line</source>
      <translation>Відстань, на яку продовжуються додаткові лінії за межами розмірної лінії</translation>
    </message>
    <message>
      <location filename="../ui/dialog_AnnotationStyleEditor.ui" line="527"/>
      <source>Extension overshoot</source>
      <translation>Розширення перекриття</translation>
    </message>
  </context>
  <context>
    <name>Draft</name>
    <message>
      <location filename="../../InitGui.py" line="160"/>
      <location filename="../../InitGui.py" line="163"/>
      <location filename="../../InitGui.py" line="167"/>
      <location filename="../../InitGui.py" line="170"/>
      <location filename="../../InitGui.py" line="174"/>
      <source>Draft</source>
      <translation>Креслення</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="222"/>
      <location filename="../../InitGui.py" line="225"/>
      <location filename="../../InitGui.py" line="228"/>
      <location filename="../../InitGui.py" line="231"/>
      <source>Import-Export</source>
      <translation>Імпорт-експорт</translation>
    </message>
    <message>
      <location filename="../../draftobjects/bspline.py" line="110"/>
      <source>_BSpline.createGeometry: Closed with same first/last Point. Geometry not updated.</source>
      <translation>_BSpline.createGeometry: Закрита з тією ж першою/останньою точкою. Геометрія не оновилася.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="257"/>
      <location filename="../../draftobjects/pointarray.py" line="322"/>
      <source>Point object doesn&apos;t have a discrete point, it cannot be used for an array.</source>
      <translation type="unfinished">Point object doesn&apos;t have a discrete point, it cannot be used for an array.</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="281"/>
      <source>Toggles Grid On/Off</source>
      <translation>Увімкнути/вимкнути сітку</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="300"/>
      <source>Object snapping</source>
      <translation>Привʼязка до обʼєктів</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="330"/>
      <source>Toggles Visual Aid Dimensions On/Off</source>
      <translation>Вмикати розміри видимої пропозиції</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="351"/>
      <source>Toggles Ortho On/Off</source>
      <translation>Вкл./викл Орто</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="370"/>
      <source>Toggles Constrain to Working Plane On/Off</source>
      <translation>Перемикає обмеження в робочій площині Вкл/Викл</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="157"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="160"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="144"/>
      <source>True</source>
      <translation>Так</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="161"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="162"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="148"/>
      <source>False</source>
      <translation>Ні</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="210"/>
      <source>Scale</source>
      <translation>Масштабування</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="211"/>
      <source>X factor</source>
      <translation>Коефіцієнт X</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="212"/>
      <source>Y factor</source>
      <translation>Коефіцієнт Y</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="213"/>
      <source>Z factor</source>
      <translation>Коефіцієнт Z</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="214"/>
      <source>Uniform scaling</source>
      <translation>Рівномірне масштабування</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="215"/>
      <source>Working plane orientation</source>
      <translation>Орієнтація робочої площини</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="216"/>
      <source>Copy</source>
      <translation>Копіювати</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="217"/>
      <source>Modify subelements</source>
      <translation>Змінити піделементи</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="218"/>
      <source>Pick from/to points</source>
      <translation>Оберіть від/до точки</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_scale.py" line="219"/>
      <source>Create a clone</source>
      <translation>Клонувати</translation>
    </message>
    <message>
      <location filename="../../importDXF.py" line="140"/>
      <source>Download of dxf libraries failed.
Please install the dxf Library addon manually
from menu Tools -&gt; Addon Manager</source>
      <translation>Помилка завантаження бібліотек dxf
Будь ласка, встановіть бібліотеку dxf вручну
з меню Інструменти -&gt; Менеджер додатків</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="93"/>
      <location filename="../../draftguitools/gui_lineslope.py" line="96"/>
      <source>Slope</source>
      <translation>Нахил</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_hatch.py" line="58"/>
      <source>You must choose a base object before using this command</source>
      <translation>Вам слід вибрати базовий обʼєкт перед використанням цієї команди</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="96"/>
      <source>Clone</source>
      <translation>Клонувати</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="348"/>
      <source>Offset direction is not defined. Please move the mouse on either side of the object first to indicate a direction</source>
      <translation>Напрямок зміщення не визначено. Будь ласка, перемістіть курсор миші на обидві сторони об'єкта спочатку для вказівки напрямку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="94"/>
      <source>Delete original objects</source>
      <translation>Видалити початкові обʼєкти</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="99"/>
      <source>Create chamfer</source>
      <translation>Створити фаску</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="407"/>
      <source>Save style</source>
      <translation>Зберегти стиль</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="408"/>
      <source>Name of this new style:</source>
      <translation>Назва нового стилю:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="417"/>
      <source>Warning</source>
      <translation>Увага</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="418"/>
      <source>Name exists. Overwrite?</source>
      <translation>Назва вже існує. Перезаписати?</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="456"/>
      <source>Error: json module not found. Unable to save style</source>
      <translation>Помилка: модуль json не знайдено. Неможливо зберегти стиль</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_wpproxy.py" line="120"/>
      <source>Writing camera position</source>
      <translation>Запис положення камери</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_wpproxy.py" line="139"/>
      <source>Writing objects shown/hidden state</source>
      <translation>Запис  об’єктів показати/приховати стан</translation>
    </message>
  </context>
  <context>
    <name>DraftCircularArrayTaskPanel</name>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="26"/>
      <source>Circular array</source>
      <translation>Круговий масив</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="44"/>
      <source>(Placeholder for the icon)</source>
      <translation>(Бланк для піктограми)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="53"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="63"/>
      <source>Distance from one layer of objects to the next layer of objects.</source>
      <translation>Відстань від одного шару обʼєктів до наступного шару обʼєктів.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="56"/>
      <source>Radial distance</source>
      <translation>Радіальна відстань</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="76"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="87"/>
      <source>Distance from one element in one ring of the array to the next element in the same ring.
It cannot be zero.</source>
      <translation>Відстань від одного елементу масиву до одного елементу масиву до наступного елементу у тому ж округленні.
Він не може бути нулем.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="80"/>
      <source>Tangential distance</source>
      <translation>Дотична відстань</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="101"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="112"/>
      <source>Number of circular layers or rings to create, including a copy of the original object.
It must be at least 2.</source>
      <translation>Кількість кругових шарів або кілець для створення, включаючи копію вихідного об’єкта.
Його має бути не менше 2.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="105"/>
      <source>Number of circular layers</source>
      <translation>Кількість циклічних шарів</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="126"/>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="136"/>
      <source>The number of symmetry lines in the circular array.</source>
      <translation>Число симетричних ліній в круговому масиві.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="129"/>
      <source>Symmetry</source>
      <translation>Симетрія</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="151"/>
      <source>The coordinates of the point through which the axis of rotation passes.
Change the direction of the axis itself in the property editor.</source>
      <translation>Координати точки, через яку проходить вісь обертання.
Змініть напрямок вісі в редакторі властивостей.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="155"/>
      <source>Center of rotation</source>
      <translation>Центр обертання</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="163"/>
      <source>X</source>
      <translation>X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="183"/>
      <source>Y</source>
      <translation>Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="203"/>
      <source>Z</source>
      <translation>Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="225"/>
      <source>Reset the coordinates of the center of rotation.</source>
      <translation>Скинути координати центру обертання.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="228"/>
      <source>Reset point</source>
      <translation>Скидання точки</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="240"/>
      <source>If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</source>
      <translation type="unfinished">If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="244"/>
      <source>Fuse</source>
      <translation>Обʼєднання</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="251"/>
      <source>If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</source>
      <translation type="unfinished">If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_CircularArray.ui" line="255"/>
      <source>Link array</source>
      <translation>Масив посилань</translation>
    </message>
  </context>
  <context>
    <name>DraftOrthoArrayTaskPanel</name>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="26"/>
      <source>Orthogonal array</source>
      <translation>Ортогональний масив</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="44"/>
      <source>(Placeholder for the icon)</source>
      <translation>(Бланк для піктограми)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="51"/>
      <source>Number of elements in the array in the specified direction, including a copy of the original object.
The number must be at least 1 in each direction.</source>
      <translation>Кількість елементів у масиві в зазначеному напрямку, включаючи копію оригінального об’єкта.
Номер повинен бути принаймні 1 в кожному напрямку.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="55"/>
      <source>Number of elements</source>
      <translation>Кількість елементів</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="63"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="132"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="223"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="314"/>
      <source>X</source>
      <translation>X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="80"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="155"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="243"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="334"/>
      <source>Y</source>
      <translation>Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="97"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="175"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="266"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="354"/>
      <source>Z</source>
      <translation>Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="119"/>
      <source>Distance between the elements in the X direction.
Normally, only the X value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</source>
      <translation>Відстань між елементами у напрямку X.
Зазвичай необхідно лише значення X; два інших значення можуть дати додатковий зсув у відповідних напрямках.
Негативні значення призведуть створення копій у зворотному напрямку.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="124"/>
      <source>X intervals</source>
      <translation>Х інтервали</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="197"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="288"/>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="379"/>
      <source>Reset the distances.</source>
      <translation>Скинути відстань.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="200"/>
      <source>Reset X</source>
      <translation>Скинути X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="210"/>
      <source>Distance between the elements in the Y direction.
Normally, only the Y value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</source>
      <translation>Відстань між елементами у напрямку Y.
Зазвичай необхідне лише значення Y; Інші два значення можуть дати додатковий зсув у відповідних напрямках.
Негативні значення призведуть до створення копій у негативному напрямку.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="215"/>
      <source>Y intervals</source>
      <translation>Y інтервали</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="291"/>
      <source>Reset Y</source>
      <translation>Скинути Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="301"/>
      <source>Distance between the elements in the Z direction.
Normally, only the Z value is necessary; the other two values can give an additional shift in their respective directions.
Negative values will result in copies produced in the negative direction.</source>
      <translation>Відстань між елементами в напрямку Y.
Зазвичай необхідно лише значення Y; два інших значення можуть дати додатковий зсув у відповідних напрямках.
Негативні значення призведуть до копій вироблених в негативному напрямку.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="306"/>
      <source>Z intervals</source>
      <translation>Z інтервали</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="382"/>
      <source>Reset Z</source>
      <translation>Скинути Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="394"/>
      <source>If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</source>
      <translation type="unfinished">If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="398"/>
      <source>Fuse</source>
      <translation>Обʼєднання</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="405"/>
      <source>If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</source>
      <translation type="unfinished">If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_OrthoArray.ui" line="409"/>
      <source>Link array</source>
      <translation>Масив посилань</translation>
    </message>
  </context>
  <context>
    <name>DraftPolarArrayTaskPanel</name>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="26"/>
      <source>Polar array</source>
      <translation>Полярний масив</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="44"/>
      <source>(Placeholder for the icon)</source>
      <translation>(Бланк для піктограми)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="53"/>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="65"/>
      <source>Sweeping angle of the polar distribution.
A negative angle produces a polar pattern in the opposite direction.
The maximum absolute value is 360 degrees.</source>
      <translation>Кут розгортки полярного розподілу.
Негативний кут створює полярний малюнок у протилежному напрямку.
Максимальне абсолютне значення - 360 градусів.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="58"/>
      <source>Polar angle</source>
      <translation>Полярний кут</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="86"/>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="97"/>
      <source>Number of elements in the array, including a copy of the original object.
It must be at least 2.</source>
      <translation>Кількість елементів у масиві включно із копією початкового обʼєкту.
Кількість елементів має бути принаймні 2.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="90"/>
      <source>Number of elements</source>
      <translation>Кількість елементів</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="113"/>
      <source>The coordinates of the point through which the axis of rotation passes.
Change the direction of the axis itself in the property editor.</source>
      <translation>Координати точки, через яку проходить вісь обертання.
Змініть напрямок вісі в редакторі властивостей.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="117"/>
      <source>Center of rotation</source>
      <translation>Центр обертання</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="125"/>
      <source>X</source>
      <translation>X</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="145"/>
      <source>Y</source>
      <translation>Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="165"/>
      <source>Z</source>
      <translation>Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="187"/>
      <source>Reset the coordinates of the center of rotation.</source>
      <translation>Скинути координати центру обертання.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="190"/>
      <source>Reset point</source>
      <translation>Скидання точки</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="202"/>
      <source>If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</source>
      <translation type="unfinished">If checked, the resulting objects in the array will be fused if they touch each other.
This only works if &quot;Link array&quot; is off.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="206"/>
      <source>Fuse</source>
      <translation>Обʼєднання</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="213"/>
      <source>If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</source>
      <translation type="unfinished">If checked, the resulting object will be a &quot;Link array&quot; instead of a regular array.
A Link array is more efficient when creating multiple copies, but it cannot be fused together.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_PolarArray.ui" line="217"/>
      <source>Link array</source>
      <translation>Масив посилань</translation>
    </message>
  </context>
  <context>
    <name>DraftShapeStringGui</name>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="26"/>
      <source>ShapeString</source>
      <translation>РядФорми</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="46"/>
      <source>X</source>
      <translation>X</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="53"/>
      <location filename="../ui/TaskShapeString.ui" line="70"/>
      <location filename="../ui/TaskShapeString.ui" line="87"/>
      <source>Enter coordinates or select point with mouse.</source>
      <translation>Введіть координати або виберіть точку з мишкою.</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="63"/>
      <source>Y</source>
      <translation>Y</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="80"/>
      <source>Z</source>
      <translation>Z</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="114"/>
      <source>Reset 3d point selection</source>
      <translation>Скинути вибір 3d точки</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="120"/>
      <source>Reset Point</source>
      <translation>Скидання точки</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="131"/>
      <source>String</source>
      <translation>Рядок</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="138"/>
      <source>Text to be made into ShapeString</source>
      <translation>Текст, що має бути перетворений у ShapeString</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="149"/>
      <source>Height</source>
      <translation>Висота</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="156"/>
      <source>Height of the result</source>
      <translation>Висота результату</translation>
    </message>
    <message>
      <location filename="../ui/TaskShapeString.ui" line="176"/>
      <source>Font file</source>
      <translation>Файл шрифту</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddConstruction</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="333"/>
      <source>Add to Construction group</source>
      <translation>Додати до групи Будівництво</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="337"/>
      <source>Adds the selected objects to the construction group,
and changes their appearance to the construction style.
It creates a construction group if it doesn&apos;t exist.</source>
      <translation type="unfinished">Adds the selected objects to the construction group,
and changes their appearance to the construction style.
It creates a construction group if it doesn&apos;t exist.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddNamedGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="392"/>
      <source>Add a new named group</source>
      <translation>Додати нову групу з новим іменем</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="395"/>
      <source>Add a new group with a given name.</source>
      <translation>Додати нову групу із заданим імʼям.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddPoint</name>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="58"/>
      <source>Add point</source>
      <translation>Додати точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="61"/>
      <source>Adds a point to an existing Wire or B-spline.</source>
      <translation>Додає точку до існуючого дроту або B-сплайну.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AddToGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="74"/>
      <source>Move to group...</source>
      <translation>Перемістити до групи...</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="78"/>
      <source>Moves the selected objects to an existing group, or removes them from any group.
Create a group first to use this tool.</source>
      <translation>Переміщує вибрані об'єкти в існуючу групу або видаляє їх з будь-якої групи.
Спочатку створіть групу, щоб використовувати цей інструмент.</translation>
    </message>
  </context>
  <context>
    <name>Draft_AnnotationStyleEditor</name>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="82"/>
      <source>Annotation styles...</source>
      <translation>Стилі анотації...</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="85"/>
      <source>Manage or create annotation styles</source>
      <translation>Керування або створення стилів анотацій</translation>
    </message>
  </context>
  <context>
    <name>Draft_ApplyStyle</name>
    <message>
      <location filename="../../draftguitools/gui_styles.py" line="48"/>
      <source>Apply current style</source>
      <translation>Застосувати поточний стиль</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_styles.py" line="52"/>
      <source>Applies the current style defined in the toolbar (line width and colors) to the selected objects and groups.</source>
      <translation>Застосовує поточний стиль, визначений в панелі інструментів (ширина та кольори) до вибраних об'єктів і груп.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Arc</name>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="67"/>
      <source>Arc</source>
      <translation>Дуга</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="71"/>
      <source>Creates a circular arc by a center point and a radius.
CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює кругову дугу за центром і радіусом.
CTRL для прив'язки, SHIFT для обмеження.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ArcTools</name>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="656"/>
      <source>Arc tools</source>
      <translation>Інструменти дуги</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="659"/>
      <source>Create various types of circular arcs.</source>
      <translation>Створіть різні типи кругових дуг.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Arc_3Points</name>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="533"/>
      <source>Arc by 3 points</source>
      <translation>Дуга за 3 точками</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="537"/>
      <source>Creates a circular arc by picking 3 points.
CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює кругову дугу шляхом підбирання 3 точок.
CTRL для прив'язки, SHIFT для врегулювання.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Array</name>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="69"/>
      <source>Array</source>
      <translation>Масив</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="73"/>
      <source>Creates an array from a selected object.
By default, it is a 2x2 orthogonal array.
Once the array is created its type can be changed
to polar or circular, and its properties can be modified.</source>
      <translation>Створює масив із вибраного об’єкта.
За замовчуванням це ортогональний масив 2x2.
Після створення масиву його тип можна змінити
До полярного або кругового, і його властивості можна змінювати.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ArrayTools</name>
    <message>
      <location filename="../../draftguitools/gui_arrays.py" line="72"/>
      <source>Array tools</source>
      <translation>Інструменти масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arrays.py" line="76"/>
      <source>Create various types of arrays, including rectangular, polar, circular, path, and point</source>
      <translation>Створюйте різні типи масивів, включаючи прямокутні, полярні, кругові, контури та точки</translation>
    </message>
  </context>
  <context>
    <name>Draft_AutoGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="221"/>
      <source>Autogroup</source>
      <translation>Автогрупування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="225"/>
      <source>Select a group to add all Draft and Arch objects to.</source>
      <translation>Виберіть групу, до якої слід додати всі об’єкти Чернеткы та Аркы.</translation>
    </message>
  </context>
  <context>
    <name>Draft_BSpline</name>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="61"/>
      <source>B-spline</source>
      <translation>B-сплайн</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="65"/>
      <source>Creates a multiple-point B-spline. CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює багатоточковий B-сплайн. CTRL для прив'язки, SHIFT для обмеження.</translation>
    </message>
  </context>
  <context>
    <name>Draft_BezCurve</name>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="65"/>
      <source>Bézier curve</source>
      <translation>Крива Безьє</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="69"/>
      <source>Creates an N-degree Bézier curve. The more points you pick, the higher the degree.
CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює криву Безьє N-ступеня. Чим більше точок ви виберете, тим вищий ступінь.
CTRL для прив'язки, SHIFT для обмеження.</translation>
    </message>
  </context>
  <context>
    <name>Draft_BezierTools</name>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="493"/>
      <source>Bézier tools</source>
      <translation>Інструменти для побудови кривих Безьє</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="496"/>
      <source>Create various types of Bézier curves.</source>
      <translation>Створює різні типи кривих Безьє.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Circle</name>
    <message>
      <location filename="../../draftguitools/gui_circles.py" line="81"/>
      <source>Circle</source>
      <translation>Коло</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_circles.py" line="85"/>
      <source>Creates a circle (full circular arc).
CTRL to snap, ALT to select tangent objects.</source>
      <translation>Створює коло (повна кругова дуга).
CTRL для прив'язки, ALT для вибору дотичного обʼєкту.</translation>
    </message>
  </context>
  <context>
    <name>Draft_CircularArray</name>
    <message>
      <location filename="../../draftguitools/gui_circulararray.py" line="66"/>
      <source>Circular array</source>
      <translation>Круговий масив</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_circulararray.py" line="70"/>
      <source>Creates copies of the selected object, and places the copies in a radial pattern
creating various circular layers.

The array can be turned into an orthogonal or a polar array by changing its type.</source>
      <translation>Створює копії обраного об'єкту і розміщує копії у радіальному нарисі
створення різних круглих шарів.

Масив може бути перетворений на ортогональний або полярний масив шляхом зміни його типу.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Clone</name>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="71"/>
      <source>Clone</source>
      <translation>Клонувати</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="75"/>
      <source>Creates a clone of the selected objects.
The resulting clone can be scaled in each of its three directions.</source>
      <translation>Створює клон обраних об'єктів.
Отриманий клон можна масштабувати в кожному з цих трьох напрямків.</translation>
    </message>
  </context>
  <context>
    <name>Draft_CubicBezCurve</name>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="250"/>
      <source>Cubic Bézier curve</source>
      <translation>Кубічна крива Безьє</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="254"/>
      <source>Creates a Bézier curve made of 2nd degree (quadratic) and 3rd degree (cubic) segments. Click and drag to define each segment.
After the curve is created you can go back to edit each control point and set the properties of each knot.
CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює криву Безьє, що складається з сегментів 2-го ступеня (квадратичних) та 3-го ступеня (кубічних). Клацніть та перетягніть, щоб визначити кожен сегмент.
Після створення кривої можна змінити кожну контрольну точку та встановити властивості кожного вузла.
CTRL для прив'язки, SHIFT для обмеження.</translation>
    </message>
  </context>
  <context>
    <name>Draft_DelPoint</name>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="94"/>
      <source>Remove point</source>
      <translation>Видалити точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_line_add_delete.py" line="97"/>
      <source>Removes a point from an existing Wire or B-spline.</source>
      <translation>Видаляє точку з існуючої Ламаної або B-сплайну.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Dimension</name>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="85"/>
      <source>Dimension</source>
      <translation>Розмірність</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="89"/>
      <source>Creates a dimension.

- Pick three points to create a simple linear dimension.
- Select a straight line to create a linear dimension linked to that line.
- Select an arc or circle to create a radius or diameter dimension linked to that arc.
- Select two straight lines to create an angular dimension between them.
CTRL to snap, SHIFT to constrain, ALT to select an edge or arc.

You may select a single line or single circular arc before launching this command
to create the corresponding linked dimension.
You may also select an &apos;App::MeasureDistance&apos; object before launching this command
to turn it into a &apos;Draft Dimension&apos; object.</source>
      <translation type="unfinished">Creates a dimension.

- Pick three points to create a simple linear dimension.
- Select a straight line to create a linear dimension linked to that line.
- Select an arc or circle to create a radius or diameter dimension linked to that arc.
- Select two straight lines to create an angular dimension between them.
CTRL to snap, SHIFT to constrain, ALT to select an edge or arc.

You may select a single line or single circular arc before launching this command
to create the corresponding linked dimension.
You may also select an &apos;App::MeasureDistance&apos; object before launching this command
to turn it into a &apos;Draft Dimension&apos; object.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Downgrade</name>
    <message>
      <location filename="../../draftguitools/gui_downgrade.py" line="59"/>
      <source>Downgrade</source>
      <translation>Зниження</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_downgrade.py" line="63"/>
      <source>Downgrades the selected objects into simpler shapes.
The result of the operation depends on the types of objects, which may be able to be downgraded several times in a row.
For example, it explodes the selected polylines into simpler faces, wires, and then edges. It can also subtract faces.</source>
      <translation>Перетворює обрані об'єкти у простіші форми.
Результат операції залежить від типів об'єктів, які можуть бути понижені кілька разів поспіль.
Наприклад, команда перетворює обрані полілінії до простіших ліній, граней, ребер. Також може відняти грані.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Draft2Sketch</name>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="58"/>
      <source>Draft to Sketch</source>
      <translation>Креслення в Ескіз</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="62"/>
      <source>Convert bidirectionally between Draft objects and Sketches.
Many Draft objects will be converted into a single non-constrained Sketch.
However, a single sketch with disconnected traces will be converted into several individual Draft objects.</source>
      <translation>Конвертувати двонаправлено між об'єктами чернетками і ескізами.
Багато об'єктів з чернеток будуть конвертовані в єдиний необмежений ескіз.
Тим не менш, єдиний ескіз із роз'єднаними слідами буде перетворено на кілька об'єктів Draft для кожного об'єкта.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Drawing</name>
    <message>
      <location filename="../../draftguitools/gui_drawing.py" line="72"/>
      <source>Drawing</source>
      <translation>Рисунок</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_drawing.py" line="76"/>
      <source>Creates a 2D projection on a Drawing Workbench page from the selected objects.
This command is OBSOLETE since the Drawing Workbench became obsolete in 0.17.
Use TechDraw Workbench instead for generating technical drawings.</source>
      <translation>Створює 2D проекцію на сторінці креслення з вибраного об'єкту.
Ця команда є OBSOLETE так як робочий простір малювання став застарілим в 0. 7.
Використовуйте TechDraw Workbench натомість для створення технічних креслень.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Edit</name>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="285"/>
      <source>Edit</source>
      <translation>Правка</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="289"/>
      <source>Edits the active object.
Press E or ALT+LeftClick to display context menu
on supported nodes and on supported objects.</source>
      <translation>Редагує активний об'єкт.
Натисніть E або ALT+LeftClick щоб відобразити контекстне меню
на підтримуваних вузлах та на підтримуваних об'єктах.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Ellipse</name>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="60"/>
      <source>Ellipse</source>
      <translation>Еліпс</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="63"/>
      <source>Creates an ellipse. CTRL to snap.</source>
      <translation>Створює еліпс. CTRL для привʼязки.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Facebinder</name>
    <message>
      <location filename="../../draftguitools/gui_facebinders.py" line="61"/>
      <source>Facebinder</source>
      <translation>Зібрання поверхонь</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_facebinders.py" line="64"/>
      <source>Creates a facebinder object from selected faces.</source>
      <translation>Створює об’єкт фейнджера з вибраних граней.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Fillet</name>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="65"/>
      <source>Fillet</source>
      <translation>Скруглення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="68"/>
      <source>Creates a fillet between two selected wires or edges.</source>
      <translation>Створює скруглення між двома вибраними ламаними або ребрами.</translation>
    </message>
  </context>
  <context>
    <name>Draft_FlipDimension</name>
    <message>
      <location filename="../../draftguitools/gui_dimension_ops.py" line="64"/>
      <source>Flip dimension</source>
      <translation>Перевернути розмірність</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimension_ops.py" line="68"/>
      <source>Flip the normal direction of the selected dimensions (linear, radial, angular).
If other objects are selected they are ignored.</source>
      <translation>Переверніть перпендикулярний напрямок вибраних розмірів (лінійний, радіальний, кутовий).
Якщо вибрано інші об’єкти, вони ігноруються.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Hatch</name>
    <message>
      <location filename="../../draftguitools/gui_hatch.py" line="38"/>
      <source>Hatch</source>
      <translation>Штрихування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_hatch.py" line="42"/>
      <source>Creates hatches on the faces of a selected object</source>
      <translation>Створює штрихування на гранях обраного обʼєкта</translation>
    </message>
  </context>
  <context>
    <name>Draft_Heal</name>
    <message>
      <location filename="../../draftguitools/gui_heal.py" line="59"/>
      <source>Heal</source>
      <translation>Зцілення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_heal.py" line="63"/>
      <source>Heal faulty Draft objects saved with an earlier version of the program.
If an object is selected it will try to heal that object in particular,
otherwise it will try to heal all objects in the active document.</source>
      <translation>Виправити дефектні проекти, збережені в попередній версії програми.
Якщо об’єкт вибрано, він намагатиметься зцілити цей об’єкт, зокрема,
Інакше він спробує виправити всі об’єкти в активному документі.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Join</name>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="67"/>
      <source>Join</source>
      <translation>Зʼєднати</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="71"/>
      <source>Joins the selected lines or polylines into a single object.
The lines must share a common point at the start or at the end for the operation to succeed.</source>
      <translation>Об’єднує позначені лінії або полілінії в один об'єкт.
Рядки повинні бути розділені спільною точкою початку або в кінці для досягнення операції.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Label</name>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="65"/>
      <source>Label</source>
      <translation>Позначка</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="69"/>
      <source>Creates a label, optionally attached to a selected object or subelement.

First select a vertex, an edge, or a face of an object, then call this command,
and then set the position of the leader line and the textual label.
The label will be able to display information about this object, and about the selected subelement,
if any.

If many objects or many subelements are selected, only the first one in each case
will be used to provide information to the label.</source>
      <translation>Створює мітку, опціонально прикріплену до вибраного об'єкта або елемента.

Спочатку виберіть вершину, ребро або грань об'єкта, потім викличте цю команду,
та встановіть позицію лінії виноски та текстову мітку.
Мітка буде відображати інформацію про цей об'єкт, та про виділений піделемент,
якщо такий є.

Якщо вибрано багато об'єктів або багато піделементів, лише перший у кожному конкретному випадку
буде використовуватися для надання інформації про мітку.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Layer</name>
    <message>
      <location filename="../../draftguitools/gui_layers.py" line="53"/>
      <source>Layer</source>
      <translation>Шар</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_layers.py" line="57"/>
      <source>Adds a layer to the document.
Objects added to this layer can share the same visual properties such as line color, line width, and shape color.</source>
      <translation>Додає шар до документа.
Об'єкти додані до цього шару можуть використовувати такі ж візуальні властивості, як колір лінії, ширина та колір фігури.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Line</name>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="64"/>
      <source>Line</source>
      <translation>Лінія</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="68"/>
      <source>Creates a 2-point line. CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює криву Безьє. CTRL для привʼязки, SHIFT для обмеження.</translation>
    </message>
  </context>
  <context>
    <name>Draft_LinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="126"/>
      <source>LinkArray</source>
      <translation>Масив Посилань</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="130"/>
      <source>Like the Array tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</source>
      <translation type="unfinished">Like the Array tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Mirror</name>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="64"/>
      <source>Mirror</source>
      <translation>Віддзеркалити</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="68"/>
      <source>Mirrors the selected objects along a line defined by two points.</source>
      <translation>Віддзеркалити обрані об'єкти вздовж лінії, яка визначена двома точками.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Move</name>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="64"/>
      <source>Move</source>
      <translation>Переміщення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="68"/>
      <source>Moves the selected objects from one base point to another point.
If the &quot;copy&quot; option is active, it will create displaced copies.
CTRL to snap, SHIFT to constrain.</source>
      <translation type="unfinished">Moves the selected objects from one base point to another point.
If the &quot;copy&quot; option is active, it will create displaced copies.
CTRL to snap, SHIFT to constrain.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Offset</name>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="64"/>
      <source>Offset</source>
      <translation>Зсув</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="68"/>
      <source>Offsets of the selected object.
It can also create an offset copy of the original object.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</source>
      <translation>Зсув вибраного об'єкта.
Він також може створити копію зсуву оригінального об’єкта.
CTRL для створення знімків, SHIFT для обмеження. Утримуйте ALT та натисніть га об'єкт, щоб створити нову копію з кожним кліком.</translation>
    </message>
  </context>
  <context>
    <name>Draft_OrthoArray</name>
    <message>
      <location filename="../../draftguitools/gui_orthoarray.py" line="66"/>
      <source>Array</source>
      <translation>Масив</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_orthoarray.py" line="70"/>
      <source>Creates copies of the selected object, and places the copies in an orthogonal pattern,
meaning the copies follow the specified direction in the X, Y, Z axes.

The array can be turned into a polar or a circular array by changing its type.</source>
      <translation>Створює копії обраного об'єкту та розміщує копії за ортогональною схемою,
значення копій йдуть у вказаному напрямку в осі X, Y, Z.

Масив може бути перетворений на полярний або круговий масив шляхом зміни його типу.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathArray</name>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="74"/>
      <source>Path array</source>
      <translation>Шлях масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="78"/>
      <source>Creates copies of the selected object along a selected path.
First select the object, and then select the path.
The path can be a polyline, B-spline, Bezier curve, or even edges from other objects.</source>
      <translation>Створює копії обраного об'єкта вздовж виділеної траєкторії.
Спочатку виділіть об'єкт потім траєкторію.
Траєкторіє може бути полілінія, B-сплайн, крива Безьє або навіть ребра інших об'єктів.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathLinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="176"/>
      <source>Path Link array</source>
      <translation>Траєкторія масиву посилань</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="180"/>
      <source>Like the PathArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</source>
      <translation type="unfinished">Like the PathArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathTwistedArray</name>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="69"/>
      <source>Path twisted array</source>
      <translation>Траєкторія скручування масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="73"/>
      <source>Creates copies of the selected object along a selected path, and twists the copies.
First select the object, and then select the path.
The path can be a polyline, B-spline, Bezier curve, or even edges from other objects.</source>
      <translation>Створює копії обраного об'єкта вздовж виділеної траєкторії та скручує копії.
Спочатку виділіть об'єкт потім траєкторію.
Траєкторіє може бути полілінія, B-сплайн, крива Безьє або навіть ребра інших об'єктів.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PathTwistedLinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="138"/>
      <source>Path twisted Link array</source>
      <translation>Траєкторія скручування масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="142"/>
      <source>Like the PathTwistedArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</source>
      <translation type="unfinished">Like the PathTwistedArray tool, but creates a &apos;Link array&apos; instead.
A &apos;Link array&apos; is more efficient when handling many copies but the &apos;Fuse&apos; option cannot be used.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Point</name>
    <message>
      <location filename="../../draftguitools/gui_points.py" line="63"/>
      <source>Point</source>
      <translation>Точка</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_points.py" line="66"/>
      <source>Creates a point object. Click anywhere on the 3D view.</source>
      <translation>Створює обʼєкт точки. Натисніть будь-де на 3D-перегляді.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PointArray</name>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="77"/>
      <source>Point array</source>
      <translation>Масив точок</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="81"/>
      <source>Creates copies of the selected object, and places the copies at the position of various points.

The points need to be grouped under a compound of points before using this tool.
To create this compound, select various points and then use the Part Compound tool,
or use the Draft Upgrade tool to create a &apos;Block&apos;, or create a Sketch and add simple points to it.

Select the base object, and then select the compound or the sketch to create the point array.</source>
      <translation type="unfinished">Creates copies of the selected object, and places the copies at the position of various points.

The points need to be grouped under a compound of points before using this tool.
To create this compound, select various points and then use the Part Compound tool,
or use the Draft Upgrade tool to create a &apos;Block&apos;, or create a Sketch and add simple points to it.

Select the base object, and then select the compound or the sketch to create the point array.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PointLinkArray</name>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="155"/>
      <source>PointLinkArray</source>
      <translation>Артикул</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="159"/>
      <source>Like the PointArray tool, but creates a &apos;Point link array&apos; instead.
A &apos;Point link array&apos; is more efficient when handling many copies.</source>
      <translation type="unfinished">Like the PointArray tool, but creates a &apos;Point link array&apos; instead.
A &apos;Point link array&apos; is more efficient when handling many copies.</translation>
    </message>
  </context>
  <context>
    <name>Draft_PolarArray</name>
    <message>
      <location filename="../../draftguitools/gui_polararray.py" line="66"/>
      <source>Polar array</source>
      <translation>Полярний масив</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polararray.py" line="70"/>
      <source>Creates copies of the selected object, and places the copies in a polar pattern
defined by a center of rotation and its angle.

The array can be turned into an orthogonal or a circular array by changing its type.</source>
      <translation>Створює копії обраного об'єкту і розміщує копії в полярному візерунку
визначеному за центром обертання та його кута.

Масив може бути перетворений на ортогональний або круговий масив шляхом зміни його типу.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Polygon</name>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="58"/>
      <source>Polygon</source>
      <translation>Багатокутник</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="62"/>
      <source>Creates a regular polygon (triangle, square, pentagon, ...), by defining the number of sides and the circumscribed radius.
CTRL to snap, SHIFT to constrain</source>
      <translation>Створює правильний багатокутник (трикутник, квадрат, п'ятикутник, ...), визначаючи кількість сторін і радіус обрізання.
CTRL для прив'язки, SHIFT для обмеження</translation>
    </message>
  </context>
  <context>
    <name>Draft_Rectangle</name>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="55"/>
      <source>Rectangle</source>
      <translation>Прямокутник</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="58"/>
      <source>Creates a 2-point rectangle. CTRL to snap.</source>
      <translation>Створює 2-точковий прямокутник. CTRL для притягування.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Rotate</name>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="63"/>
      <source>Rotate</source>
      <translation>Обертання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="67"/>
      <source>Rotates the selected objects. Choose the center of rotation, then the initial angle, and then the final angle.
If the &quot;copy&quot; option is active, it will create rotated copies.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</source>
      <translation type="unfinished">Rotates the selected objects. Choose the center of rotation, then the initial angle, and then the final angle.
If the &quot;copy&quot; option is active, it will create rotated copies.
CTRL to snap, SHIFT to constrain. Hold ALT and click to create a copy with each click.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Scale</name>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="72"/>
      <source>Scale</source>
      <translation>Масштабування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="76"/>
      <source>Scales the selected objects from a base point.
CTRL to snap, SHIFT to constrain, ALT to copy.</source>
      <translation>Масштабує виділені об'єкти з базової точки.
CTRL до прив'язки, SHIFT для обмеження, ALT для копіювання.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SelectGroup</name>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="169"/>
      <source>Select group</source>
      <translation>Вибрати групу</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="173"/>
      <source>Selects the contents of selected groups. For selected non-group objects, the contents of the group they are in is selected.</source>
      <translation>Вибирає вміст вибраних груп. Для вибраних незгрупованих об’єктів вибирається вміст групи, в якій вони перебувають.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SelectPlane</name>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="67"/>
      <source>SelectPlane</source>
      <translation>ОбратиПлощину</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="71"/>
      <source>Select the face of solid body to create a working plane on which to sketch Draft objects.
You may also select a three vertices or a Working Plane Proxy.</source>
      <translation>Виберіть грань твердого тіла, щоб створити робочу площину, на якій буде створено об'єкт ескізу.
Також можна вибрати три вершини або робочу площину проксі.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SetStyle</name>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="61"/>
      <source>Set style</source>
      <translation>Встановити стиль</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_setstyle.py" line="62"/>
      <source>Sets default styles</source>
      <translation>Встановлює стилі за замовчуванням</translation>
    </message>
  </context>
  <context>
    <name>Draft_Shape2DView</name>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="60"/>
      <source>Shape 2D view</source>
      <translation>Форма 2D вигляд</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="64"/>
      <source>Creates a 2D projection of the selected objects on the XY plane.
The initial projection direction is the negative of the current active view direction.
You can select individual faces to project, or the entire solid, and also include hidden lines.
These projections can be used to create technical drawings with the TechDraw Workbench.</source>
      <translation>Створює двовимірну проекцію вибраних об’єктів на площині XY.
Початковий напрямок проекції - є протилежним поточному напрямку активного виду.
Ви можете вибрати окремі грані для проектування або тіло повністю, а також включити приховані лінії.
Ці проекції можна використовувати для створення технічних креслень з допомогою робочого середовища TechDraw.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ShapeString</name>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="67"/>
      <source>Shape from text</source>
      <translation>Форми з тексту</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="71"/>
      <source>Creates a shape from a text string by choosing a specific font and a placement.
The closed shapes can be used for extrusions and boolean operations.</source>
      <translation>Створює фігуру з текстового рядка, вибираючи певний шрифт і розміщення.
Закриті фігури можна використовувати для видавлювання та булевих операцій.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ShowSnapBar</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="690"/>
      <source>Show snap toolbar</source>
      <translation>Показувати панель інструментів привʼязки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="693"/>
      <source>Show the snap toolbar if it is hidden.</source>
      <translation>Показувати панель інструментів привʼязки, якщо її приховано.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Slope</name>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="71"/>
      <source>Set slope</source>
      <translation>Встановіть нахил</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="75"/>
      <source>Sets the slope of the selected line by changing the value of the Z value of one of its points.
If a polyline is selected, it will apply the slope transformation to each of its segments.

The slope will always change the Z value, therefore this command only works well for
straight Draft lines that are drawn in the XY plane. Selected objects that aren&apos;t single lines will be ignored.</source>
      <translation type="unfinished">Sets the slope of the selected line by changing the value of the Z value of one of its points.
If a polyline is selected, it will apply the slope transformation to each of its segments.

The slope will always change the Z value, therefore this command only works well for
straight Draft lines that are drawn in the XY plane. Selected objects that aren&apos;t single lines will be ignored.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Angle</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="394"/>
      <source>Angle</source>
      <translation>Кут</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="398"/>
      <source>Set snapping to points in a circular arc located at multiples of 30 and 45 degree angles.</source>
      <translation>Встановіть прив’язку до точок по дузі кола, розташованих під декількома кутами 30 і 45 градусів.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Center</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="431"/>
      <source>Center</source>
      <translation>Центр</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="434"/>
      <source>Set snapping to the center of a circular arc.</source>
      <translation>Встановіть прив’язування до центру дуги окружності.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Dimensions</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="613"/>
      <source>Show dimensions</source>
      <translation>Показати розміри</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="617"/>
      <source>Show temporary linear dimensions when editing an object and using other snapping methods.</source>
      <translation>Показати тимчасові розмірні лінії під час редагування об'єкта та використання інших методів прив'язки.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Endpoint</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="357"/>
      <source>Endpoint</source>
      <translation>Кінцева точка</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="360"/>
      <source>Set snapping to endpoints of an edge.</source>
      <translation>Встановити привʼязування до кінцевих точок на ребрі.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Extension</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="467"/>
      <source>Extension</source>
      <translation>Розширення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="470"/>
      <source>Set snapping to the extension of an edge.</source>
      <translation>Встановіть привʼязування до розширення на ребрі.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Grid</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="248"/>
      <source>Grid</source>
      <translation>Сітка</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="251"/>
      <source>Set snapping to the intersection of grid lines.</source>
      <translation>Встановіть привʼязування до перетину ліній сітки.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Intersection</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="284"/>
      <source>Intersection</source>
      <translation>Перетин</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="287"/>
      <source>Set snapping to the intersection of edges.</source>
      <translation>Встановити привʼязування до перетину ребер.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Lock</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="141"/>
      <source>Main snapping toggle On/Off</source>
      <translation>Основний перемикач привʼязування увімкнення/вимкнення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="144"/>
      <source>Activates or deactivates all snap methods at once.</source>
      <translation>Активує або деактивує всі методи привʼязки одночасно.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Midpoint</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="177"/>
      <source>Midpoint</source>
      <translation>Центр</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="180"/>
      <source>Set snapping to the midpoint of an edge.</source>
      <translation>Встановити привʼязку до середньої точки ребра.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Near</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="501"/>
      <source>Nearest</source>
      <translation>Найближчий</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="504"/>
      <source>Set snapping to the nearest point of an edge.</source>
      <translation>Встановити привʼязку до середньої точки на ребрі.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Ortho</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="538"/>
      <source>Orthogonal</source>
      <translation>Ортогональний вектор</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="542"/>
      <source>Set snapping to a direction that is a multiple of 45 degrees from a point.</source>
      <translation>Встановити прив'язку до напрямку, який є поділом на 45 градусів від точки.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Parallel</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="320"/>
      <source>Parallel</source>
      <translation>Паралельно</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="324"/>
      <source>Set snapping to a direction that is parallel to an edge.</source>
      <translation>Встановіть прив’язку в напрямок, паралельний до ребра.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Perpendicular</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="213"/>
      <source>Perpendicular</source>
      <translation>Перпендикулярний</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="217"/>
      <source>Set snapping to a direction that is perpendicular to an edge.</source>
      <translation>Установити прив'язування в напрямку, перпендикулярному до ребра.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_Special</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="575"/>
      <source>Special</source>
      <translation>Спеціальні</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="579"/>
      <source>Set snapping to the special points defined inside an object.</source>
      <translation>Встановіть прив'язку до спеціальних точок, визначених всередині об'єкта.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Snap_WorkingPlane</name>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="655"/>
      <source>Working plane</source>
      <translation>Робоча площина</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="659"/>
      <source>Restricts snapping to a point in the current working plane.
If you select a point outside the working plane, for example, by using other snapping methods,
it will snap to that point&apos;s projection in the current working plane.</source>
      <translation type="unfinished">Restricts snapping to a point in the current working plane.
If you select a point outside the working plane, for example, by using other snapping methods,
it will snap to that point&apos;s projection in the current working plane.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Split</name>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="57"/>
      <source>Split</source>
      <translation>Розділити</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="61"/>
      <source>Splits the selected line or polyline into two independent lines
or polylines by clicking anywhere along the original object.
It works best when choosing a point on a straight segment and not a corner vertex.</source>
      <translation>Розділяє виділену лінію або полілінію на дві незалежні лінії або полілінії, клацнувши будь-де вздовж вихідного об’єкта.
Найкраще це працює при виборі точки на прямому відрізку, а не на кутовій вершині.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Stretch</name>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="65"/>
      <source>Stretch</source>
      <translation>Розтягнути</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="69"/>
      <source>Stretches the selected objects.
Select an object, then draw a rectangle to pick the vertices that will be stretched,
then draw a line to specify the distance and direction of stretching.</source>
      <translation>Розтягує виділені об’єкти.
Виберіть об’єкт, а потім намалюйте прямокутник, щоб вибрати вершини, які будуть розтягуватися, потім намалюйте лінію, щоб вказати відстань і напрямок розтягування.</translation>
    </message>
  </context>
  <context>
    <name>Draft_SubelementHighlight</name>
    <message>
      <location filename="../../draftguitools/gui_subelements.py" line="64"/>
      <source>Subelement highlight</source>
      <translation>Виділення піделементу</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_subelements.py" line="68"/>
      <source>Highlight the subelements of the selected objects, so that they can then be edited with the move, rotate, and scale tools.</source>
      <translation>Виділіть піделементи вибраних об’єктів, щоб потім їх можна було редагувати за допомогою інструментів переміщення, обертання та масштабування.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Text</name>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="61"/>
      <source>Text</source>
      <translation>Текст</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="64"/>
      <source>Creates a multi-line annotation. CTRL to snap.</source>
      <translation>Створює багаторядкову анотацію. CTRL для прив’язки.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleConstructionMode</name>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="103"/>
      <source>Toggle construction mode</source>
      <translation>Змінити режим конструювання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="108"/>
      <source>Toggles the Construction mode.
When this is active, the following objects created will be included in the construction group, and will be drawn with the specified color and properties.</source>
      <translation>Перемикає режим будівництва.
Коли ця функція активна, наступні створені об’єкти будуть включені до групи конструкцій і малюватимуться із зазначеним кольором та властивостями.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleContinueMode</name>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="144"/>
      <source>Toggle continue mode</source>
      <translation>Увімкнути режим продовження</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="148"/>
      <source>Toggles the Continue mode.
When this is active, any drawing tool that is terminated will automatically start again.
This can be used to draw several objects one after the other in succession.</source>
      <translation>Перемикання режиму "Продовжити".
Коли це активно, будь-який інструмент малювання, який припиняється автоматично запуститься.
Це може бути використане для малювання декількох об'єктів один за іншим у наступності.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleDisplayMode</name>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="187"/>
      <source>Toggle normal/wireframe display</source>
      <translation>Увімкнути/вимкнути  нормальних/каркасне вигляд</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="191"/>
      <source>Switches the display mode of selected objects from flatlines to wireframe and back.
This is helpful to quickly visualize objects that are hidden by other objects.
This is intended to be used with closed shapes and solids, and doesn&apos;t affect open wires.</source>
      <translation type="unfinished">Switches the display mode of selected objects from flatlines to wireframe and back.
This is helpful to quickly visualize objects that are hidden by other objects.
This is intended to be used with closed shapes and solids, and doesn&apos;t affect open wires.</translation>
    </message>
  </context>
  <context>
    <name>Draft_ToggleGrid</name>
    <message>
      <location filename="../../draftguitools/gui_grid.py" line="59"/>
      <source>Toggle grid</source>
      <translation>Показати або приховати сітку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_grid.py" line="62"/>
      <source>Toggles the Draft grid on and off.</source>
      <translation>Вмикає та вимикає сітку креслення.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Trimex</name>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="77"/>
      <source>Trimex</source>
      <translation>Тримекс</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="83"/>
      <source>Trims or extends the selected object, or extrudes single faces.
CTRL snaps, SHIFT constrains to current segment or to normal, ALT inverts.</source>
      <translation>Обрізати або розтягнути обраний об'єкт чи видавити окрему грань. 
CTRL - прив'язка, SHIFT - обмежити поточним сегментом або нормаллю, ALT - інвертувати.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Upgrade</name>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="59"/>
      <source>Upgrade</source>
      <translation>Оновити</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="63"/>
      <source>Upgrades the selected objects into more complex shapes.
The result of the operation depends on the types of objects, which may be able to be upgraded several times in a row.
For example, it can join the selected objects into one, convert simple edges into parametric polylines,
convert closed edges into filled faces and parametric polygons, and merge faces into a single face.</source>
      <translation>Перетворює вибрані об’єкти до більш складних форм.
Результат операції залежить від типів об’єктів, які можна оновлювати кілька разів поспіль.
Наприклад, він може об’єднати вибрані об’єкти в один, перетворити прості ребра в параметричні полілінії,
перетворювати закриті ребра в заповнені грані та параметричні багатокутники та об’єднувати грані в одну.</translation>
    </message>
  </context>
  <context>
    <name>Draft_Wire</name>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="317"/>
      <source>Polyline</source>
      <translation>Ламана лінія</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="321"/>
      <source>Creates a multiple-points line (polyline). CTRL to snap, SHIFT to constrain.</source>
      <translation>Створює полілінію. CTRL для привʼязки, SHIFT для обмеження.</translation>
    </message>
  </context>
  <context>
    <name>Draft_WireToBSpline</name>
    <message>
      <location filename="../../draftguitools/gui_wire2spline.py" line="65"/>
      <source>Wire to B-spline</source>
      <translation>Сітка до B-сплайна</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_wire2spline.py" line="69"/>
      <source>Converts a selected polyline to a B-spline, or a B-spline to a polyline.</source>
      <translation>Перетворення вибраної полілінії на B-сплайн, або B-сплайна на полілінію.</translation>
    </message>
  </context>
  <context>
    <name>Draft_WorkingPlaneProxy</name>
    <message>
      <location filename="../../draftguitools/gui_planeproxy.py" line="54"/>
      <source>Create working plane proxy</source>
      <translation>Створити проксі робочої площини</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_planeproxy.py" line="58"/>
      <source>Creates a proxy object from the current working plane.
Once the object is created double click it in the tree view to restore the camera position and objects&apos; visibilities.
Then you can use it to save a different camera position and objects&apos; states any time you need.</source>
      <translation type="unfinished">Creates a proxy object from the current working plane.
Once the object is created double click it in the tree view to restore the camera position and objects&apos; visibilities.
Then you can use it to save a different camera position and objects&apos; states any time you need.</translation>
    </message>
  </context>
  <context>
    <name>Form</name>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="14"/>
      <source>Style settings</source>
      <translation>Параметри стилю</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="22"/>
      <source>Fills the values below with a stored style preset</source>
      <translation>Заповнює значення нижче за допомогою збереженого набору стилів</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="26"/>
      <source>Load preset</source>
      <translation>Завантажити налаштування</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="40"/>
      <source>Save current style as a preset...</source>
      <translation>Зберегти поточний стиль як налаштування...</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="57"/>
      <source>Lines and faces</source>
      <translation>Лінії та грані</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="66"/>
      <source>Line color</source>
      <translation>Колір лінії</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="73"/>
      <source>The color of lines</source>
      <translation>Колір ліній</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="80"/>
      <source>Line width</source>
      <translation>Ширина лінії</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="87"/>
      <source> px</source>
      <translation> пікс.</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="94"/>
      <source>Draw style</source>
      <translation>Стиль малювання</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="101"/>
      <source>The line style</source>
      <translation>Стиль лінії</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="105"/>
      <source>Solid</source>
      <translation>Суцільне тіло</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="110"/>
      <source>Dashed</source>
      <translation>Пунктирна</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="115"/>
      <source>Dotted</source>
      <translation>В крапку</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="120"/>
      <source>DashDot</source>
      <translation>DashDot</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="128"/>
      <source>Display mode</source>
      <translation>Режим відображення</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="135"/>
      <source>The display mode for faces</source>
      <translation>Режим відображення граней</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="139"/>
      <source>Flat Lines</source>
      <translation>Прямі лінії</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="144"/>
      <source>Wireframe</source>
      <translation>Каркас</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="149"/>
      <source>Shaded</source>
      <translation>Затінений</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="154"/>
      <source>Points</source>
      <translation>Точки</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="162"/>
      <source>Shape color</source>
      <translation>Колір фігури</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="169"/>
      <source>The color of faces</source>
      <translation>Колір поверхонь</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="176"/>
      <source>Transparency</source>
      <translation>Прозорість</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="183"/>
      <source>The transparency of faces</source>
      <translation>Прозорість поверхонь</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="186"/>
      <source> %</source>
      <translation> %</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="199"/>
      <source>Annotations</source>
      <translation>Анотація</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="205"/>
      <source>Text font</source>
      <translation>Шрифт тексту:</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="218"/>
      <source>The font to use for texts and dimensions</source>
      <translation>Шрифт для використання в текстах та розмірах</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="225"/>
      <source>Text size</source>
      <translation>Розмір тексту</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="232"/>
      <source>The size of texts and dimension texts</source>
      <translation>Розмір текстів і розмірних текстів</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="242"/>
      <source>Text spacing</source>
      <translation>Інтервал тексту</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="249"/>
      <source>The space between the text and the dimension line</source>
      <translation>Відступ тексту від розмірної лінії</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="259"/>
      <source>Text color</source>
      <translation>Колір тексту</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="266"/>
      <source>The color of texts and dimension texts</source>
      <translation>Колір текстів і розмірних текстів</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="273"/>
      <source>Line spacing</source>
      <translation>Інтервал</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="280"/>
      <source>The spacing between different lines of text</source>
      <translation>Інтервал між рядками тексту</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="287"/>
      <source>Arrow style</source>
      <translation>Стиль стрілки</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="294"/>
      <source>The type of dimension arrows</source>
      <translation>Тип розмірних стрілок </translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="298"/>
      <source>Dot</source>
      <translation>Крапка</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="303"/>
      <source>Circle</source>
      <translation>Коло</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="308"/>
      <source>Arrow</source>
      <translation>Стрілка</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="313"/>
      <source>Tick</source>
      <translation>Позначка</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="318"/>
      <source>Tick-2</source>
      <translation>Тік-2</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="326"/>
      <source>Arrow size</source>
      <translation>Розмір стрілки</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="333"/>
      <source>The size of dimension arrows</source>
      <translation>Розмір розмірних стрілок </translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="343"/>
      <source>Show unit</source>
      <translation>Показати величини</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="350"/>
      <source>If the unit suffix is shown on dimension texts or not</source>
      <translation>Якщо величина суфіксу одиниці відображається на розмірних текстах чи ні</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="363"/>
      <source>Unit override</source>
      <translation>Задати величину</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="370"/>
      <source>The unit to use for dimensions. Leave blank to use current FreeCAD unit</source>
      <translation>Одиниця виміру використана для розмірності. Залиште порожнім, щоб використовувати поточну одиницю FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="382"/>
      <source>Apply above style to selected object(s)</source>
      <translation>Застосувати вище стиль для вибраного обʼєкту(ів)</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="385"/>
      <source>Selected</source>
      <translation>Вибрано</translation>
    </message>
    <message>
      <location filename="../ui/TaskPanel_SetStyle.ui" line="397"/>
      <source>Texts/dims</source>
      <translation>Тексти / розмірність</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="14"/>
      <source>Working plane setup</source>
      <translation>Налаштування робочої площини</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="20"/>
      <source>Select a face or working plane proxy or 3 vertices.
Or choose one of the options below</source>
      <translation>Виберіть обличчя або робочий простір проксі або три вершини.
Або виберіть один із наведених нижче параметрів</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="31"/>
      <source>Sets the working plane to the XY plane (ground plane)</source>
      <translation>Встановлює робочу площину XY (базова площина)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="34"/>
      <source>Top (XY)</source>
      <translation>Верхня (XY)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="41"/>
      <source>Sets the working plane to the XZ plane (front plane)</source>
      <translation>Встановлює робочу площину на площину XZ (передня площина)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="44"/>
      <source>Front (XZ)</source>
      <translation>Передній (XZ)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="51"/>
      <source>Sets the working plane to the YZ plane (side plane)</source>
      <translation>Встановлює робочу площину на площину YZ (бічна площина)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="54"/>
      <source>Side (YZ)</source>
      <translation>Напрямок (YZ)</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="61"/>
      <source>Sets the working plane facing the current view</source>
      <translation>Встановлює робочу площину, яка стоїть перед поточним виглядом</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="64"/>
      <source>Align to view</source>
      <translation>Вирівняти для перегляду</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="71"/>
      <source>The working plane will align to the current
view each time a command is started</source>
      <translation>Робоча площина буде вирівнюватись поточному
погляду кожного разу при запуску команди</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="78"/>
      <source>Automatic</source>
      <translation>Автоматичний</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="87"/>
      <source>Offset</source>
      <translation>Зсув</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="94"/>
      <source>An optional offset to give to the working plane
above its base position. Use this together with one
of the buttons above</source>
      <translation>Необовʼязкове відступ для призначення робочої площини
над її базовим положенням. Використовуйте це разом з одним з кнопок
вище</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="106"/>
      <location filename="../ui/TaskSelectPlane.ui" line="118"/>
      <source>If this is selected, the working plane will be
centered on the current view when pressing one
of the buttons above</source>
      <translation>Якщо буде обрано, робоча площина буде
центрована на поточному виді під час натискання однієї з кнопок
</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="111"/>
      <source>Center plane on view</source>
      <translation>Центрувати площину на екрані</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="135"/>
      <source>Or select a single vertex to move the current
working plane without changing its orientation.
Then, press the button below</source>
      <translation>Або виберіть одну вершину, щоб перемістити поточний
робочу площу, не змінюючи його орієнтації.
Потім натисніть кнопку нижче</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="147"/>
      <source>Moves the working plane without changing its
orientation. If no point is selected, the plane
will be moved to the center of the view</source>
      <translation>Переміщує робочу площину, не змінюючи її орієнтацію. Якщо жодної точки не обрано, площина
буде переміщено до центру подання</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="152"/>
      <source>Move working plane</source>
      <translation>Перемістити робочу площу</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="161"/>
      <location filename="../ui/TaskSelectPlane.ui" line="171"/>
      <source>The spacing between the smaller grid lines</source>
      <translation>Відступ між меншими лініями сітки</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="164"/>
      <source>Grid spacing</source>
      <translation>Розмір сітки</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="181"/>
      <location filename="../ui/TaskSelectPlane.ui" line="191"/>
      <source>The number of squares between each main line of the grid</source>
      <translation>Кількість квадратів між кожним основним рядком таблиці</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="184"/>
      <source>Main line every</source>
      <translation>Основна лінія кожні</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="198"/>
      <source>Grid extension</source>
      <translation>Розширення сітки</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="205"/>
      <source> lines</source>
      <translation> ліній</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="218"/>
      <location filename="../ui/TaskSelectPlane.ui" line="230"/>
      <source>The distance at which a point can be snapped to
when approaching the mouse. You can also change this
value by using the [ and ] keys while drawing</source>
      <translation>Відстань до якої можна притягування точки
при наближенні до миші. Ви також можете змінити це
значення, використовуючи ключі [ і ], під час креслення</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="223"/>
      <source>Snapping radius</source>
      <translation>Радіус привʼязування</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="241"/>
      <source>Centers the view on the current working plane</source>
      <translation>Центри вигляду на поточній робочій площині</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="244"/>
      <source>Center view</source>
      <translation>Центрування вигляду</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="251"/>
      <source>Resets the working plane to its previous position</source>
      <translation>Скидає робочу площину до попередньої позиції</translation>
    </message>
    <message>
      <location filename="../ui/TaskSelectPlane.ui" line="254"/>
      <source>Previous</source>
      <translation>Попередня</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="14"/>
      <source>Form</source>
      <translation>Форма</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="20"/>
      <source>PAT file:</source>
      <translation>PAT-файл:</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="27"/>
      <source>pattern files (*.pat)</source>
      <translation>файли шаблонів (*.pat)</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="34"/>
      <source>Pattern:</source>
      <translation>Шаблон:</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="44"/>
      <source>Scale</source>
      <translation>Масштабування</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="64"/>
      <source>Rotation:</source>
      <translation>Обертання:</translation>
    </message>
    <message>
      <location filename="../ui/dialogHatch.ui" line="71"/>
      <source>°</source>
      <translation>°</translation>
    </message>
  </context>
  <context>
    <name>Gui::Dialog::DlgSettingsDraft</name>
    <message>
      <location filename="../ui/preferences-draft.ui" line="14"/>
      <source>General settings</source>
      <translation>Загальні параметри</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="32"/>
      <source>General Draft Settings</source>
      <translation>Загальні налаштування креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="40"/>
      <source>Default working plane</source>
      <translation>Стандартна робоча площина</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="67"/>
      <source>None</source>
      <translation>Немає</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="72"/>
      <source>XY (Top)</source>
      <translation>XY (Зверху)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="77"/>
      <source>XZ (Front)</source>
      <translation>XZ (З переду)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="82"/>
      <source>YZ (Side)</source>
      <translation>YZ (З боку)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="94"/>
      <source>Internal precision level</source>
      <translation>Рівень внутрішньої точності</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="120"/>
      <source>The number of decimals in internal coordinates operations (for ex. 3 = 0.001). Values between 6 and 8 are usually considered the best trade-off among FreeCAD users.</source>
      <translation>Кількість десяткових знаків у внутрішніх координатах (для прик. 3 = 0.001). Значення від 6 до 8 зазвичай вважаються найкращими компромісами серед користувачів FreeCAD.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="143"/>
      <source>Tolerance</source>
      <translation>Точність</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="169"/>
      <source>This is the value used by functions that use a tolerance.
Values with differences below this value will be treated as same. This value will be obsoleted soon so the precision level above controls both.</source>
      <translation>Дана функція - це значення, що використовується для обробки цього значення.
Значення з різницею нижче цього значення будуть розглядатися як те саме. Це значення буде застаріло скоро такий рівень точності вище контролює обидва варіанти.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="194"/>
      <source>If this option is checked, the layers drop-down list will also show groups, allowing you to automatically add objects to groups too.</source>
      <translation>Якщо позначено цей пункт, випадаючий список шарів також буде показувати групи, дозволяючи автоматично додавати обʼєкти до груп теж.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="197"/>
      <source>Show groups in layers list drop-down button</source>
      <translation>Показувати групи в випадаючому списку</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="216"/>
      <source>Draft tools options</source>
      <translation>Налаштування інструментів креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="230"/>
      <source>When drawing lines, set focus on Length instead of X coordinate.
This allows to point the direction and type the distance.</source>
      <translation>Під час малювання лінії встановіть фокус на довжині замість координат Х.
Це дозволяє наводити напрям і набирати відстань.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="234"/>
      <source>Set focus on Length instead of X coordinate</source>
      <translation>Встановіть фокус на Довжину, а не на координаті X</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="247"/>
      <source>Normally, after copying objects, the copies get selected.
If this option is checked, the base objects will be selected instead.</source>
      <translation>Як правило, після копіювання обʼєктів, копії будуть обрані.
Якщо позначено цей параметр, замість цього буде обрано базові обʼєкти.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="251"/>
      <source>Select base objects after copying</source>
      <translation>Після копіювання вибрати початкові обʼєкти</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="264"/>
      <source>If this option is set, when creating Draft objects on top of an existing face of another object, the &quot;Support&quot; property of the Draft object will be set to the base object. This was the standard behaviour before FreeCAD 0.19</source>
      <translation type="unfinished">If this option is set, when creating Draft objects on top of an existing face of another object, the &quot;Support&quot; property of the Draft object will be set to the base object. This was the standard behaviour before FreeCAD 0.19</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="267"/>
      <source>Set the Support property when possible</source>
      <translation>Встановлювати властивість підтримки, коли це можливо</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="280"/>
      <source>If this is checked, objects will appear as filled by default.
Otherwise, they will appear as wireframe</source>
      <translation>Якщо позначено цей пункт, обʼєкти будуть бути заповнені за замовчуванням.
В іншому випадку вони будуть відображатися як каркасний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="284"/>
      <source>Fill objects with faces whenever possible</source>
      <translation>Заповніть об’єкти поверхнями, якщо це можливо</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="300"/>
      <source>If this is checked, copy mode will be kept across command,
otherwise commands will always start in no-copy mode</source>
      <translation>Якщо позначено цей пункт, режим копіювання буде зберігатися в команді.
Інакше команди завжди будуть починатися в режимі no-copy</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="304"/>
      <source>Global copy mode</source>
      <translation>Режим глобального копіювання</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="320"/>
      <source>Force Draft Tools to create Part primitives instead of Draft objects.
Note that this is not fully supported, and many object will be not editable with Draft Modifiers.</source>
      <translation>Примусово створювати примітиви креслення замість обʼєктів креслення.
Зверніть увагу, що це не повністю підтримується, і багато обʼєктів не будуть редагуватися в Draft Modifiers.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="324"/>
      <source>Use Part Primitives when available</source>
      <translation>Використовуйте Частину Прімітиви, якщо вони доступні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="341"/>
      <source>Prefix labels of Clones with:</source>
      <translation>Мітки префікса Клонів з:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="372"/>
      <source>Construction Geometry</source>
      <translation>Допоміжна геометрія</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="380"/>
      <source>Construction group name</source>
      <translation>Назва групи конструкцій</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="387"/>
      <source>This is the default group name for construction geometry</source>
      <translation>Стандартне ім’я групи для конструкції</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="390"/>
      <source>Construction</source>
      <translation>Допоміжна</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="407"/>
      <source>Construction geometry color</source>
      <translation>Колір допоміжної геометрії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draft.ui" line="427"/>
      <source>This is the default color for objects being drawn while in construction mode.</source>
      <translation>Стандартний колір для об’єктів створюваних в режимі конструктора.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="14"/>
      <source>User interface settings</source>
      <translation>Налаштування інтерфейсу користувача</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="26"/>
      <source>In-Command Shortcuts</source>
      <translation>Ярлики в команді</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="37"/>
      <source>Relative</source>
      <translation>Відносний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="59"/>
      <source>R</source>
      <translation>R</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="81"/>
      <source>Continue</source>
      <translation>Продовжити</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="103"/>
      <source>T</source>
      <translation>T</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="125"/>
      <source>Close</source>
      <translation>Закрити</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="147"/>
      <source>O</source>
      <translation>O</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="169"/>
      <source>Copy</source>
      <translation>Копіювати</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="191"/>
      <source>P</source>
      <translation>P</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="213"/>
      <source>Subelement Mode</source>
      <translation>Режим піделементу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="235"/>
      <source>D</source>
      <translation>D</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="257"/>
      <source>Fill</source>
      <translation>Заповнити</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="279"/>
      <source>L</source>
      <translation>L</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="301"/>
      <source>Exit</source>
      <translation>Вихід</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="323"/>
      <source>A</source>
      <translation>A</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="345"/>
      <source>Select Edge</source>
      <translation>Обрати ребро</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="367"/>
      <source>E</source>
      <translation>E</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="389"/>
      <source>Add Hold</source>
      <translation>Додати утримувач</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="411"/>
      <source>Q</source>
      <translation>Q</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="433"/>
      <source>Length</source>
      <translation>Довжина</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="455"/>
      <source>H</source>
      <translation>H</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="477"/>
      <source>Wipe</source>
      <translation>Витерти</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="499"/>
      <source>W</source>
      <translation>W</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="521"/>
      <source>Set WP</source>
      <translation>Встановити WP</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="543"/>
      <source>U</source>
      <translation>U</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="565"/>
      <source>Cycle Snap</source>
      <translation>Знімок циклу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="587"/>
      <source>`</source>
      <translation>`</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="609"/>
      <source>Global</source>
      <translation>Глобальна</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="631"/>
      <source>G</source>
      <translation>G</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="653"/>
      <source>Snap</source>
      <translation>Привʼязати</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="675"/>
      <source>S</source>
      <translation>S</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="697"/>
      <source>Increase Radius</source>
      <translation>Збільшити радіус</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="719"/>
      <source>[</source>
      <translation>[</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="741"/>
      <source>Decrease Radius</source>
      <translation>Зменшити радіус</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="763"/>
      <source>]</source>
      <translation>]</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="785"/>
      <source>Restrict X</source>
      <translation>Обмежити X</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="807"/>
      <source>X</source>
      <translation>X</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="829"/>
      <source>Restrict Y</source>
      <translation>Обмежити Y</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="851"/>
      <source>Y</source>
      <translation>Y</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="873"/>
      <source>Restrict Z</source>
      <translation>Обмежити Z</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="895"/>
      <source>Z</source>
      <translation>Z</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="928"/>
      <source>Enable draft statusbar customization</source>
      <translation>Увімкнути можливість налаштування рядка стану</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="931"/>
      <source>Draft Statusbar</source>
      <translation>Креслення рядка стану</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="951"/>
      <source>Enable snap statusbar widget</source>
      <translation>Увімкнути віджет рядка стану</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="954"/>
      <source>Draft snap widget</source>
      <translation>Віджет привʼязки креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="970"/>
      <source>Enable draft statusbar annotation scale widget</source>
      <translation>Увімкнути чорновик в рядку стану віджет</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftinterface.ui" line="973"/>
      <source>Annotation scale widget</source>
      <translation>Віджет шкали анотації</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="14"/>
      <source>Grid and snapping</source>
      <translation>Сітки та привʼязування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="35"/>
      <source>Snapping</source>
      <translation>Привʼязка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="43"/>
      <source>If this is checked, snapping is activated without the need to press the snap mod key</source>
      <translation>Якщо вибрано, використання привʼязки буде активоване без необхідності натискати клавішу привʼязки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="46"/>
      <source>Always snap (disable snap mod)</source>
      <translation>Завжди привʼязувати (вимкнути режим привʼязки)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="66"/>
      <source>Constrain mod</source>
      <translation>Режим обмежування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="86"/>
      <source>The Constraining modifier key</source>
      <translation>Кнопка зміни режиму обмеження</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="96"/>
      <location filename="../ui/preferences-draftsnap.ui" line="151"/>
      <location filename="../ui/preferences-draftsnap.ui" line="206"/>
      <source>Shift</source>
      <translation>Shift</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="101"/>
      <location filename="../ui/preferences-draftsnap.ui" line="156"/>
      <location filename="../ui/preferences-draftsnap.ui" line="211"/>
      <source>Ctrl</source>
      <translation>Ctrl</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="106"/>
      <location filename="../ui/preferences-draftsnap.ui" line="161"/>
      <location filename="../ui/preferences-draftsnap.ui" line="216"/>
      <source>Alt</source>
      <translation>Alt</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="118"/>
      <source>Snap mod</source>
      <translation>Режим привʼязування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="138"/>
      <source>The snap modifier key</source>
      <translation>Клавіша привʼязки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="173"/>
      <source>Alt mod</source>
      <translation>Alt-режим</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="193"/>
      <source>The Alt modifier key</source>
      <translation>Клавіша Alt</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="228"/>
      <source>If checked, the Snap toolbar will be shown whenever you use snapping</source>
      <translation>Якщо прапорець встановлений, панель інструментів привʼязки буде показувати, коли ви використовуєте режим привʼязки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="231"/>
      <source>Show Draft Snap toolbar</source>
      <translation>Показати панель інструментів привʼязки креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="251"/>
      <source>Hide Draft snap toolbar after use</source>
      <translation>Приховати панель інструментів привʼязки після використання</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="272"/>
      <source>Grid</source>
      <translation>Сітка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="278"/>
      <source>If checked, a grid will appear when drawing</source>
      <translation>Якщо встановлено, при виконанні креслення буде показуватися сітка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="281"/>
      <source>Use grid</source>
      <translation>Використовувати сітку</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="300"/>
      <source>If checked, the Draft grid will always be visible when the Draft workbench is active. Otherwise only when using a command</source>
      <translation>Якщо відмітити, сітка креслення завжди буде видима при використанні робочого столу креслення активного. В іншому випадку лише за допомогою команди</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="303"/>
      <source>Always show the grid</source>
      <translation>Завжди відображати сітку</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="319"/>
      <source>If checked, an additional border is displayed around the grid, showing the main square size in the bottom left border</source>
      <translation>Якщо вибрано, то буде показано додаткову межу навколо сітки, що показує основний квадратний розмір в нижньому лівому кордоні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="322"/>
      <source>Show grid border</source>
      <translation>Показати рамку сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="338"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the outline of a human figure is displayed at the bottom left corner of the grid. This option is only effective if the BIM workbench is installed and if &amp;quot;Show grid border&amp;quot; option is enabled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the outline of a human figure is displayed at the bottom left corner of the grid. This option is only effective if the BIM workbench is installed and if &amp;quot;Show grid border&amp;quot; option is enabled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="341"/>
      <source>Show human figure</source>
      <translation>Показати людську фігуру</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="357"/>
      <source>If set, the grid will have its two main axes colored in red, green or blue when they match global axes</source>
      <translation>Якщо встановити, то сітка матиме два основних осі, позначені червоним, зеленим або синім кольором, якщо відповідатиме глобальним осям</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="360"/>
      <source>Use colored axes</source>
      <translation>Використовувати кольорові осі</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="381"/>
      <source>Main lines every</source>
      <translation>Основні лінії кожні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="404"/>
      <source>Mainlines will be drawn thicker. Specify here how many squares between mainlines.</source>
      <translation>Основні лінії будуть намальовані товщі. Тут укажіть, скільки квадратів між основними лініями.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="430"/>
      <source>Grid spacing</source>
      <translation>Розмір сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="453"/>
      <source>The spacing between each grid line</source>
      <translation>Відстань між кожною лінією сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="485"/>
      <source>Grid size</source>
      <translation>Розмір сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="505"/>
      <source>The number of horizontal or vertical lines of the grid</source>
      <translation>Кількість горизонтальних або вертикальних ліній сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="511"/>
      <source> lines</source>
      <translation> ліній</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="534"/>
      <source>Grid color and transparency</source>
      <translation>Колір сітки та прозорість</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="554"/>
      <source>The color of the grid</source>
      <translation>Колір сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="574"/>
      <source>The overall transparency of the grid</source>
      <translation>Загальна прозорість сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="595"/>
      <source>Draft Edit preferences</source>
      <translation>Редагування налаштувань</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="598"/>
      <source>Edit</source>
      <translation>Правка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="621"/>
      <source>Maximum number of contemporary edited objects</source>
      <translation>Максимальна кількість одночасно редагованих об’єктів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="644"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sets the maximum number of objects Draft Edit&lt;/p&gt;&lt;p&gt;can process at the same time&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Встановлює максимальну кількість обʼєктів Draft Edit&lt;/p&gt;&lt;p&gt;може обробити одночасно&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="691"/>
      <source>Draft edit pick radius</source>
      <translation>Змінити радіус вибору чернетки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftsnap.ui" line="714"/>
      <source>Controls pick radius of edit nodes</source>
      <translation>Керування радіусом вибору вузлів редагування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="14"/>
      <source>Texts and dimensions</source>
      <translation>Тексти та розміри</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="26"/>
      <source>Text settings</source>
      <translation>Налаштування тексту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="34"/>
      <source>Font family</source>
      <translation>Гарнітура</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="47"/>
      <source>This is the default font name for all Draft texts and dimensions.
It can be a font name such as &quot;Arial&quot;, a default style such as &quot;sans&quot;, &quot;serif&quot;
or &quot;mono&quot;, or a family such as &quot;Arial,Helvetica,sans&quot; or a name with a style
such as &quot;Arial:Bold&quot;</source>
      <translation type="unfinished">This is the default font name for all Draft texts and dimensions.
It can be a font name such as &quot;Arial&quot;, a default style such as &quot;sans&quot;, &quot;serif&quot;
or &quot;mono&quot;, or a family such as &quot;Arial,Helvetica,sans&quot; or a name with a style
such as &quot;Arial:Bold&quot;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="59"/>
      <source>Internal font</source>
      <translation>Вбудований шрифт</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="76"/>
      <source>Font size</source>
      <translation>Розмір шрифту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="89"/>
      <source>Default height for texts and dimensions</source>
      <translation>Стандартна висота для тексту та розмірів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="209"/>
      <location filename="../ui/preferences-dxf.ui" line="535"/>
      <location filename="../ui/preferences-drafttexts.ui" line="92"/>
      <location filename="../ui/preferences-drafttexts.ui" line="211"/>
      <location filename="../ui/preferences-drafttexts.ui" line="247"/>
      <location filename="../ui/preferences-drafttexts.ui" line="283"/>
      <location filename="../ui/preferences-drafttexts.ui" line="365"/>
      <location filename="../ui/preferences-drafttexts.ui" line="432"/>
      <source>mm</source>
      <translation>мм</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="116"/>
      <source>Dimension settings</source>
      <translation>Налаштування розмірів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="124"/>
      <source>Display mode</source>
      <translation>Режим відображення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="144"/>
      <source>text above (2D)</source>
      <translation>текст зверху (2D)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="149"/>
      <source> text inside (3D)</source>
      <translation> текст всередині (3D)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="161"/>
      <source>Number of decimals</source>
      <translation>Кількість десяткових знаків</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="201"/>
      <source>Extension lines size</source>
      <translation>Розмір виносних ліній</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="208"/>
      <source>The default size of dimensions extension lines</source>
      <translation>Розмір виносних ліній за замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="237"/>
      <source>Extension line overshoot</source>
      <translation>Розширення виносної лінії </translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="244"/>
      <source>The default length of extension line above dimension line</source>
      <translation>Довжина за замовчуванням подовжувальної лінії вище розмірної лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="273"/>
      <source>Dimension line overshoot</source>
      <translation>Перевищення розмірної лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="280"/>
      <source>The default distance the dimension line is extended past extension lines</source>
      <translation>Відстань за замовчуванням, розмірна лінія продовжена за виносні лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="309"/>
      <source>Arrows style</source>
      <translation>Стиль стрілок</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="323"/>
      <source>Dot</source>
      <translation>Крапка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="328"/>
      <source>Circle</source>
      <translation>Коло</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="333"/>
      <source>Arrow</source>
      <translation>Стрілка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="338"/>
      <source>Tick</source>
      <translation>Позначка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="343"/>
      <source>Tick-2</source>
      <translation>Тік-2</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="355"/>
      <source>Arrows size</source>
      <translation>Розмір стрілок</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="362"/>
      <source>The default size of arrows</source>
      <translation>Розмір стрілок за замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="388"/>
      <source>Text orientation</source>
      <translation>Орієнтація тексту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="395"/>
      <source>This is the orientation of the dimension texts when those dimensions are vertical. Default is left, which is the ISO standard.</source>
      <translation>Це орієнтація текстів розмірів, коли ці розміри вертикальні. За замовчуванням - стандарт ISO.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="405"/>
      <source>Left (ISO standard)</source>
      <translation>Наліво (стандарт ISO)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="410"/>
      <source>Right</source>
      <translation>Направо</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="422"/>
      <source>Text spacing</source>
      <translation>Інтервал тексту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="429"/>
      <source>The space between the dimension line and the dimension text</source>
      <translation>Відстань між розмірною лінією та текстом</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="455"/>
      <source>Show the unit suffix in dimensions</source>
      <translation>Показати суфікс одиниці у розмірах</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="475"/>
      <source>Override unit</source>
      <translation>Перевизначити одиницю</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="495"/>
      <source>By leaving this field blank, the dimension measurements will be shown in the current unit defined in FreeCAD. By indicating a unit here such as m or cm, you can force new dimensions to be shown in that unit.</source>
      <translation>Якщо залишити це поле порожнім, розміри буде показано в поточній одиниці, визначеній у FreeCAD. Додаючи сюди одиниці вимірювання, наприклад m або см, ви можете змусити нові розміри показуватися в цій одиниці.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="513"/>
      <source>ShapeString settings</source>
      <translation>Налаштування Контуру рядка</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="521"/>
      <source>Default ShapeString font file</source>
      <translation>Файл шрифту ShapeString за замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-drafttexts.ui" line="534"/>
      <source>Select a font file</source>
      <translation>Обрати файл шрифту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="14"/>
      <source>Visual settings</source>
      <translation>Налаштування відображення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="26"/>
      <source>Visual Settings</source>
      <translation>Налаштування відображення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="34"/>
      <source>Snap symbols style</source>
      <translation>Стиль символів привʼязки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="54"/>
      <source>Draft classic style</source>
      <translation>Класичний стиль креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="59"/>
      <source>Bitsnpieces style</source>
      <translation>Стиль Bitsnpieces</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="80"/>
      <source>Color</source>
      <translation>Колір</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="87"/>
      <source>The default color for snap symbols</source>
      <translation>Встановлює колір за замовчуванням для символів привʼязки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="111"/>
      <source>Check this if you want to use the color/linewidth from the toolbar as default</source>
      <translation>Відмітьте цей пункт, якщо хочете використовувати колір/лінійність з панелі інструментів за замовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="114"/>
      <source>Save current color and linewidth across sessions</source>
      <translation>Зберігати поточний колір та ширину лінії між сеансами</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="131"/>
      <source>If checked, a widget indicating the current working plane orientation appears during drawing operations</source>
      <translation>Якщо прапорець встановлено, під час створення операції з малюванням віджет, який показуватиме поточну орієнтацію площини</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="134"/>
      <source>Show Working Plane tracker</source>
      <translation>Показати робочу область стеження</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="151"/>
      <source>Default template sheet</source>
      <translation>Стандартний шаблон аркуша</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="177"/>
      <source>The default template to use when creating a new drawing sheet</source>
      <translation>Стандартний шаблон для використання при створенні нового аркуша креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="194"/>
      <source>Alternate SVG patterns location</source>
      <translation>Додаткове розташування шаблонів SVG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="220"/>
      <source>Here you can specify a directory with custom SVG files containing &lt;pattern&gt; definitions to be added to the standard patterns</source>
      <translation>Тут можна вказати каталог з користувацькими SVG файлами, що містять &lt;pattern&gt; визначення, які будуть додані до стандартних шаблонів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="237"/>
      <source>SVG pattern resolution</source>
      <translation>Роздільна здатність SVG шаблонів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="257"/>
      <source>The resolution to draw the patterns in. Default value is 128. Higher values give better resolutions, lower values make drawing faster</source>
      <translation>Роздільна здатність малювання шаблонів. За замовчуванням значення 128. Більше значень дають кращі роздільні здатності, менші значення роблять креслення швидше</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="280"/>
      <source>SVG pattern default size</source>
      <translation>Розмір SVG шаблону за змовчуванням</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="300"/>
      <source>The default size for SVG patterns</source>
      <translation>Розмір за замовчуванням для шаблонів SVG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="326"/>
      <source>Check this if you want to preserve colors of faces while doing downgrade and upgrade (splitFaces and makeShell only)</source>
      <translation>Відмітьте цей пункт, якщо ви хочете зберегти кольори поверхонь при зменшенні та оновленні (тільки розділення Поверхонь та зробити Каркас)

make Shell</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="329"/>
      <source>Preserve colors of faces during downgrade/upgrade</source>
      <translation>Зберегти кольори обличчя під час зниження / оновлення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="346"/>
      <source>Check this if you want the face names to derive from the originating object name and vice versa while doing downgrade/upgrade (splitFaces and makeShell only)</source>
      <translation>Відмітьте цей пункт, якщо ви хочете, щоб імена поверхонь виходили з назви об’єкта і навпаки під час виконання понизити ( тільки розділення Поверхонь та зробити Каркас)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="349"/>
      <source>Preserve names of faces during downgrade/upgrade</source>
      <translation>Зберегти назви поверхонь під час пониження/оновлення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="367"/>
      <source>Drawing view line definitions</source>
      <translation>Визначення ліній на кресленні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="375"/>
      <source>Dashed line definition</source>
      <translation>Визначення пунктирної лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="395"/>
      <location filename="../ui/preferences-draftvisual.ui" line="438"/>
      <location filename="../ui/preferences-draftvisual.ui" line="481"/>
      <source>An SVG linestyle definition</source>
      <translation>Визначення стилів ліній SVG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="398"/>
      <source>0.09,0.05</source>
      <translation>0.09,0.05</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="418"/>
      <source>Dashdot line definition</source>
      <translation>Визначення штрихпунктирної лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="441"/>
      <source>0.09,0.05,0.02,0.05</source>
      <translation>0.09,0.05,0.02,0.05</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="461"/>
      <source>Dotted line definition</source>
      <translation>Визначення для пунктирної лінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-draftvisual.ui" line="484"/>
      <source>0.02,0.02</source>
      <translation>0.02,0.02</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="14"/>
      <source>DWG</source>
      <translation>DWG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="26"/>
      <source>DWG conversion</source>
      <translation>Перетворення DWG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="34"/>
      <source>Conversion method:</source>
      <translation>Метод перетворення:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="41"/>
      <source>This is the method FreeCAD will use to convert DWG files to DXF. If &quot;Automatic&quot; is chosen, FreeCAD will try to find one of the following converters in the same order as they are shown here. If FreeCAD is unable to find any, you might need to choose a specific converter and indicate its path here under. Choose the &quot;dwg2dxf&quot; utility if using LibreDWG, &quot;ODAFileConverter&quot; if using the ODA file converter, or the &quot;dwg2dwg&quot; utility if using the pro version of QCAD.</source>
      <translation type="unfinished">This is the method FreeCAD will use to convert DWG files to DXF. If &quot;Automatic&quot; is chosen, FreeCAD will try to find one of the following converters in the same order as they are shown here. If FreeCAD is unable to find any, you might need to choose a specific converter and indicate its path here under. Choose the &quot;dwg2dxf&quot; utility if using LibreDWG, &quot;ODAFileConverter&quot; if using the ODA file converter, or the &quot;dwg2dwg&quot; utility if using the pro version of QCAD.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="51"/>
      <source>Automatic</source>
      <translation>Автоматичний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="56"/>
      <source>LibreDWG</source>
      <translation>LibreDWG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="61"/>
      <source>ODA Converter</source>
      <translation>ODA Converter</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="66"/>
      <source>QCAD pro</source>
      <translation>QCAD pro</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="78"/>
      <source>Path to file converter</source>
      <translation>Шлях до конвертера файлів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="85"/>
      <source>The path to your DWG file converter executable</source>
      <translation>Встановлює шлях до вашого конвертора DWG файлів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dwg.ui" line="100"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note:&lt;/span&gt; DXF options apply to DWG files as well.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Примітка:&lt;/span&gt; Опції для DXF та DWG файлів аналогічні.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="14"/>
      <source>DXF</source>
      <translation>DXF</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="35"/>
      <source>This preferences dialog will be shown when importing/ exporting DXF files</source>
      <translation>При імпорті/експорті DXF файлів показує цей діалог налаштувань</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="38"/>
      <source>Show this dialog when importing and exporting</source>
      <translation>Показувати цей діалог під час імпорту та експорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="51"/>
      <source>Python importer is used, otherwise the newer C++ is used.
Note: C++ importer is faster, but is not as featureful yet</source>
      <translation>Використовує імпортер Python, в іншому випадку - новіший C++.
Примітка: імпортер C++ швидший, але ще не настільки функціональний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="55"/>
      <source>Use legacy python importer</source>
      <translation>Використовувати застарілий імпортер Python</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="71"/>
      <source>Python exporter is used, otherwise the newer C++ is used.
Note: C++ exporter is faster, but is not as featureful yet</source>
      <translation>Використовує експортер Python, в іншому випадку - новіший C++.
Примітка: експортер C++ швидший, але ще не настільки функціональний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="75"/>
      <source>Use legacy python exporter</source>
      <translation>Використовувати застарілий експортер Python</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="88"/>
      <source>Automatic update (legacy importer only)</source>
      <translation>Автоматичне оновлення (тільки попередній імпорт)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="96"/>
      <source>Allow FreeCAD to download the Python converter for DXF import and export.
You can also do this manually by installing the &quot;dxf_library&quot; workbench
from the Addon Manager.</source>
      <translation>Дозволяє FreeCAD завантажувати конвертер Python для імпорту та експорту DXF. 
Ви також можете зробити це вручну, встановивши &quot;dxf_library&quot; в Менеджері Додатків.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="101"/>
      <source>Allow FreeCAD to automatically download and update the DXF libraries</source>
      <translation>Дозволити FreeCAD автоматично завантажувати та оновлювати DXF-бібліотеки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="26"/>
      <location filename="../ui/preferences-dxf.ui" line="119"/>
      <location filename="../ui/preferences-oca.ui" line="26"/>
      <source>Import options</source>
      <translation>Налаштування імпорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="140"/>
      <source>Note: Not all the options below are used by the new importer yet</source>
      <translation>Примітка: Ще не всі параметри використовуються в новому імпортері</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="149"/>
      <source>Import</source>
      <translation>Імпортувати</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="156"/>
      <source>If unchecked, texts and mtexts won&apos;t be imported</source>
      <translation>Якщо вимкнено, не імпортує тексти/мультитексти</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="159"/>
      <source>texts and dimensions</source>
      <translation>тексти та розміри</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="172"/>
      <source>If unchecked, points won&apos;t be imported</source>
      <translation>Якщо вимкнено, не імпортує точки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="175"/>
      <source>points</source>
      <translation>точок</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="188"/>
      <source>If checked, paper space objects will be imported too</source>
      <translation>Якщо позначено, також імпортує обʼєкти простору паперу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="191"/>
      <source>layouts</source>
      <translation>шари</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="204"/>
      <source>If you want the non-named blocks (beginning with a *) to be imported too</source>
      <translation>Якщо позначено, також імпортує не іменовані блоки (починаються з *)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="207"/>
      <source>*blocks</source>
      <translation>* блоки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="224"/>
      <source>Create</source>
      <translation>Створити</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="231"/>
      <source>Only standard Part objects will be created (fastest)</source>
      <translation>Створює тільки стандартні обʼєкти Деталі (найшвидший метод)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="234"/>
      <source>simple Part shapes</source>
      <translation>прості форми</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="250"/>
      <source>Parametric Draft objects will be created whenever possible</source>
      <translation>Створює за можливості параметричні обʼєкти Draft</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="253"/>
      <source>Draft objects</source>
      <translation>Обʼєкти Draft</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="266"/>
      <source>Sketches will be created whenever possible</source>
      <translation>Ескізи будуть створюватися щоразу, коли їх буде можливо</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="269"/>
      <source>Sketches</source>
      <translation>Ескізи</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="289"/>
      <source>Scale factor to apply to imported files</source>
      <translation>Коефіцієнт масштабування для імпортованих файлів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="309"/>
      <source>Scale factor to apply to DXF files on import.
The factor is the conversion between the unit of your DXF file and millimeters.
Example: for files in millimeters: 1, in centimeters: 10,
                             in meters: 1000, in inches: 25.4, in feet: 304.8</source>
      <translation>Коефіцієнт масштабування для імпортованих DXF-файлів.
Коефіцієнт - це відношення між одиницею файлу DXF та міліметрами.
Приклад: для файлів в міліметрах: 1, в сантиметрах: 10,
                             в метрах: 1000, в дюймах: 25.4, у футах: 304.8</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="338"/>
      <source>Colors will be retrieved from the DXF objects whenever possible.
Otherwise default colors will be applied. </source>
      <translation>Отримує кольори з обʼєктів DXF за можливості.
В іншому випадку використовує кольори за замовчуванням. </translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="342"/>
      <source>Get original colors from the DXF file</source>
      <translation>Отримати оригінальні кольори з DXF файлу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="359"/>
      <source>FreeCAD will try to join coincident objects into wires.
Note that this can take a while!</source>
      <translation>FreeCAD спробує об’єднати об’єкти, що збігаються, у полілінії.
Зверніть увагу, що це може зайняти деякий час!</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="363"/>
      <source>Join geometry</source>
      <translation>Обʼєднати геометрію</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="380"/>
      <source>Objects from the same layers will be joined into Draft Blocks,
turning the display faster, but making them less easily editable </source>
      <translation>Обʼєкти з тих самих шарів будуть обʼєднані в Блоки Чернетки,
показуючи швидше, але роблячи їх менш зручними для редагування</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="384"/>
      <source>Group layers into blocks</source>
      <translation>Групувати шари у блоки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="401"/>
      <source>Imported texts will get the standard Draft Text size,
instead of the size they have in the DXF document</source>
      <translation>Імпортований текст отримає стандартний розмір тексту креслення
замість розміру, який вони мають у DXF документі</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="405"/>
      <source>Use standard font size for texts</source>
      <translation>Використовувати стандартний розмір шрифту для текстів</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="422"/>
      <source>If this is checked, DXF layers will be imported as Draft Layers</source>
      <translation>Якщо позначено, імпортує DXF шари як Шари Креслення</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="425"/>
      <source>Use Layers</source>
      <translation>Використати Шари</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="445"/>
      <source>Hatches will be converted into simple wires</source>
      <translation>Конвертує штрихування в прості полілінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="448"/>
      <source>Import hatch boundaries as wires</source>
      <translation>Імпортувати границі штрихування як полілінії</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="465"/>
      <source>If polylines have a width defined, they will be rendered
as closed wires with correct width</source>
      <translation>Якщо полілінії мають визначену ширину, візуалізувати 
їх як замкнені полілінії з правильною шириною</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="469"/>
      <source>Render polylines with width</source>
      <translation>Візуалізація полілінії за її шириною</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="486"/>
      <source>Ellipse export is poorly supported. Use this to export them as polylines instead.</source>
      <translation>Експорт еліпсів погано підтримується. Це дозволяє експортувати їх як полілінії.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="489"/>
      <source>Treat ellipses and splines as polylines</source>
      <translation>Вважати еліпси та сплайни полілініями</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="518"/>
      <source>Max Spline Segment:</source>
      <translation>Максимальний сегмент сплайну:</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="528"/>
      <source>Maximum length of each of the polyline segments.
If it is set to &apos;0&apos; the whole spline is treated as a straight segment.</source>
      <translation type="unfinished">Maximum length of each of the polyline segments.
If it is set to &apos;0&apos; the whole spline is treated as a straight segment.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="107"/>
      <location filename="../ui/preferences-dxf.ui" line="559"/>
      <source>Export options</source>
      <translation>Налаштування експорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="567"/>
      <source>All objects containing faces will be exported as 3D polyfaces</source>
      <translation>Всі обʼєкти, що містять грані, будуть експортовані як 3D багатогранники</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="570"/>
      <source>Export 3D objects as polyface meshes</source>
      <translation>Експортувати 3D-обʼєкти як багатогранні сітки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="587"/>
      <source>Drawing Views will be exported as blocks.
This might fail for post DXF R12 templates.</source>
      <translation>Експортує Види Креслень як блоки.
Це може не спрацювати для шаблонів після DXF R12.</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="591"/>
      <source>Export Drawing Views as blocks</source>
      <translation>Експорт Виду Креслення як блоки</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="611"/>
      <source>Exported objects will be projected to reflect the current view direction</source>
      <translation>Проектування експортованих об'єктів з урахуванням поточного напрямку перегляду</translation>
    </message>
    <message>
      <location filename="../ui/preferences-dxf.ui" line="614"/>
      <source>Project exported objects along current view direction</source>
      <translation>Показ експортованих об'єктів вздовж поточного напрямку перегляду</translation>
    </message>
    <message>
      <location filename="../ui/preferences-oca.ui" line="14"/>
      <source>OCA</source>
      <translation>OCA</translation>
    </message>
    <message>
      <location filename="../ui/preferences-oca.ui" line="46"/>
      <source>Check this if you want the areas (3D faces) to be imported too.</source>
      <translation>Позначте цей пункт, якщо ви хочете також імпортувати області (3D поверх).</translation>
    </message>
    <message>
      <location filename="../ui/preferences-oca.ui" line="49"/>
      <source>Import OCA areas</source>
      <translation>Імпорт OCA областей</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="14"/>
      <source>SVG</source>
      <translation>SVG</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="40"/>
      <source>Import style</source>
      <translation>Стиль імпорту кольору</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="47"/>
      <source>Method chosen for importing SVG object color to FreeCAD</source>
      <translation>Вибирає метод для імпорту кольору SVG об’єктів у FreeCAD</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="60"/>
      <source>None (fastest)</source>
      <translation>Не імпортувати (швидко)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="65"/>
      <source>Use default color and linewidth</source>
      <translation>Стандартний колір та товщина ліній</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="70"/>
      <source>Original color and linewidth</source>
      <translation>Оригінальний колір та товщина ліній</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="82"/>
      <source>If checked, no units conversion will occur.
One unit in the SVG file will translate as one millimeter. </source>
      <translation>Якщо позначено, не перетворювати одиниці вимірювання.
Одна одиниця у файлі SVG буде відповідати одному міліметру. </translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="86"/>
      <source>Disable units scaling</source>
      <translation>Відключити масштабування одиниць вимірювання</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="121"/>
      <source>Export style</source>
      <translation>Стиль експорту</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="128"/>
      <source>Style of SVG file to write when exporting a sketch</source>
      <translation>Встановлює стиль для запису SVG файлу при експорті ескізу</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="141"/>
      <source>Translated (for print &amp; display)</source>
      <translation>Перетворений (для друку та показу)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="146"/>
      <source>Raw (for CAM)</source>
      <translation>Необроблений (для CAM)</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="158"/>
      <source>All white lines will appear in black in the SVG for better readability against white backgrounds</source>
      <translation>Перетворює у SVG всі білі лінії в чорні для кращої читабельності на білому фоні</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="161"/>
      <source>Translate white line color to black</source>
      <translation>Перетворити білий колір лінії у чорний</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="181"/>
      <source>Max segment length for discretized arcs</source>
      <translation>Максимальна довжина сегмента для дискретизованих дуг</translation>
    </message>
    <message>
      <location filename="../ui/preferences-svg.ui" line="204"/>
      <source>Versions of Open CASCADE older than version 6.8 don&apos;t support arc projection.
In this case arcs will be discretized into small line segments.
This value is the maximum segment length. </source>
      <translation>Open CASCADE нижче версії 6.8 не підтримує проєкцію дуг.
Тому дуги розбивають на невеликі сегменти. Це значення є максимальною довжиною сегмента. </translation>
    </message>
  </context>
  <context>
    <name>ImportAirfoilDAT</name>
    <message>
      <location filename="../../importAirfoilDAT.py" line="193"/>
      <source>Did not find enough coordinates</source>
      <translation>Не знайдено достатньо координат</translation>
    </message>
  </context>
  <context>
    <name>ImportSVG</name>
    <message>
      <location filename="../../importSVG.py" line="1810"/>
      <source>Unknown SVG export style, switching to Translated</source>
      <translation>Невідомий стиль експорту SVG, переключіться на Переклад</translation>
    </message>
    <message>
      <location filename="../../importSVG.py" line="1830"/>
      <source>The export list contains no object with a valid bounding box</source>
      <translation>Список експорту не містить жодного об’єкта з дійсною обмежувальною рамкою</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../InitGui.py" line="116"/>
      <source>Draft creation tools</source>
      <translation>Інструменти створення ескізу</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="121"/>
      <source>Draft annotation tools</source>
      <translation>Інструменти анотації креслення</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="126"/>
      <source>Draft modification tools</source>
      <translation>Інструменти редагування ескізу</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="131"/>
      <source>Draft utility tools</source>
      <translation>Інструменти утиліту креслення</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="137"/>
      <source>&amp;Drafting</source>
      <translation>&amp;Креслення</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="141"/>
      <source>&amp;Annotation</source>
      <translation>&amp; Анотації</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="146"/>
      <source>&amp;Modification</source>
      <translation>&amp;Зміна</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="151"/>
      <source>&amp;Utilities</source>
      <translation>&amp;Утиліти</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="53"/>
      <source>Arc tools</source>
      <translation>Інструменти дуги</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="63"/>
      <source>Bézier tools</source>
      <translation>Інструменти для побудови кривих Безьє</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="101"/>
      <source>Array tools</source>
      <translation>Інструменти масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snapper.py" line="1598"/>
      <source>Draft Snap</source>
      <translation>Привʼязка креслення</translation>
    </message>
  </context>
  <context>
    <name>draft</name>
    <message>
      <location filename="../../InitGui.py" line="46"/>
      <source>Draft</source>
      <translation>Креслення</translation>
    </message>
    <message>
      <location filename="../../draftobjects/shapestring.py" line="73"/>
      <source>ShapeString: string has no wires</source>
      <translation>ShapeString: рядок не має каркасів</translation>
    </message>
    <message>
      <location filename="../../draftobjects/pointarray.py" line="149"/>
      <location filename="../../draftobjects/pointarray.py" line="170"/>
      <source>added property &apos;ExtraPlacement&apos;</source>
      <translation>додав властивість &apos;ExtraPlacement&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="290"/>
      <source>, path object doesn&apos;t have &apos;Edges&apos;.</source>
      <translation type="unfinished">, path object doesn&apos;t have &apos;Edges&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="414"/>
      <location filename="../../draftobjects/patharray.py" line="427"/>
      <location filename="../../draftobjects/patharray.py" line="440"/>
      <source>&apos;PathObj&apos; property will be migrated to &apos;PathObject&apos;</source>
      <translation type="unfinished">&apos;PathObj&apos; property will be migrated to &apos;PathObject&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="587"/>
      <source>Cannot calculate path tangent. Copy not aligned.</source>
      <translation>Не вдалося обчислити дотичний контур. Копія не вирівнюється.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="605"/>
      <source>Tangent and normal are parallel. Copy not aligned.</source>
      <translation>Дотична і нормаль паралельні. Копія не вирівняна.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="622"/>
      <source>Cannot calculate path normal, using default.</source>
      <translation>Неможливо обчислити шлях нормалі, за замовчуванням.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="631"/>
      <source>Cannot calculate path binormal. Copy not aligned.</source>
      <translation>Не вдалося обчислити бінарний шлях. Копія не вирівнюється.</translation>
    </message>
    <message>
      <location filename="../../draftobjects/patharray.py" line="637"/>
      <source>AlignMode {} is not implemented</source>
      <translation>AlignMode {} не реалізовано</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="95"/>
      <location filename="../../draftobjects/draft_annotation.py" line="118"/>
      <source>added view property &apos;ScaleMultiplier&apos;</source>
      <translation type="unfinished">added view property &apos;ScaleMultiplier&apos;</translation>
    </message>
    <message>
      <location filename="../../draftobjects/draft_annotation.py" line="141"/>
      <location filename="../../draftobjects/draft_annotation.py" line="149"/>
      <source>migrated &apos;DraftText&apos; type to &apos;Text&apos;</source>
      <translation type="unfinished">migrated &apos;DraftText&apos; type to &apos;Text&apos;</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="190"/>
      <source>Snap Lock</source>
      <translation>Привʼязування встановити</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="191"/>
      <source>Snap Endpoint</source>
      <translation>Привʼязка кінцевої точки</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="192"/>
      <source>Snap Midpoint</source>
      <translation>Привʼязка середньої точки</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="193"/>
      <source>Snap Center</source>
      <translation>Привʼязка центру</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="194"/>
      <source>Snap Angle</source>
      <translation>Кут привʼязки:</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="195"/>
      <source>Snap Intersection</source>
      <translation>Перетин привʼязки</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="196"/>
      <source>Snap Perpendicular</source>
      <translation>Привʼязка перпендикулярна</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="197"/>
      <source>Snap Extension</source>
      <translation>Розширення привʼязки</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="198"/>
      <source>Snap Parallel</source>
      <translation>Паралельна прив’язка</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="199"/>
      <source>Snap Special</source>
      <translation>Особливі привʼязки</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="200"/>
      <source>Snap Near</source>
      <translation>Привʼязка поруч</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="201"/>
      <source>Snap Ortho</source>
      <translation>Притягування перпендикулярних</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="202"/>
      <source>Snap Grid</source>
      <translation>Привʼязка до сітки</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="203"/>
      <source>Snap WorkingPlane</source>
      <translation>Привʼязувати робочу площину</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="204"/>
      <source>Snap Dimensions</source>
      <translation>Привʼязка розмірів</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_tools.py" line="205"/>
      <source>Toggle Draft Grid</source>
      <translation>Увімкнути / вимкнути сітку чернетки</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="83"/>
      <source>No graphical interface</source>
      <translation>Немає графічного інтерфейсу</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="165"/>
      <source>Unable to insert new object into a scaled part</source>
      <translation>Не вдалося вставити новий обʼєкт в масштабовану частину</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="270"/>
      <source>Symbol not implemented. Using a default symbol.</source>
      <translation>Символ не реалізується. Використання символу за замовчуванням.</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="338"/>
      <source>Visibility off; removed from list: </source>
      <translation>Видимість вимкнуто; видалено зі списку: </translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="613"/>
      <source>image is Null</source>
      <translation>зображення невидиме</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="623"/>
      <source>filename does not exist on the system or in the resource file</source>
      <translation>ім'я файлу не існує в системі або у файлі ресурсу</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="682"/>
      <source>unable to load texture</source>
      <translation>неможливо завантажити текстуру</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1094"/>
      <location filename="../../draftutils/groups.py" line="94"/>
      <location filename="../../draftutils/gui_utils.py" line="734"/>
      <location filename="../../draftfunctions/cut.py" line="57"/>
      <location filename="../../draftmake/make_array.py" line="84"/>
      <location filename="../../draftmake/make_pointarray.py" line="108"/>
      <location filename="../../draftmake/make_label.py" line="200"/>
      <location filename="../../draftmake/make_patharray.py" line="167"/>
      <location filename="../../draftmake/make_patharray.py" line="344"/>
      <location filename="../../draftmake/make_text.py" line="84"/>
      <location filename="../../draftmake/make_text.py" line="182"/>
      <location filename="../../draftmake/make_dimension.py" line="223"/>
      <location filename="../../draftmake/make_dimension.py" line="316"/>
      <location filename="../../draftmake/make_dimension.py" line="461"/>
      <location filename="../../draftmake/make_dimension.py" line="613"/>
      <location filename="../../draftmake/make_layer.py" line="59"/>
      <location filename="../../draftmake/make_layer.py" line="153"/>
      <source>No active document. Aborting.</source>
      <translation>Немає активного документа. Скасування операції.</translation>
    </message>
    <message>
      <location filename="../../draftutils/groups.py" line="131"/>
      <location filename="../../draftutils/gui_utils.py" line="743"/>
      <location filename="../../draftmake/make_pointarray.py" line="117"/>
      <location filename="../../draftmake/make_pointarray.py" line="128"/>
      <location filename="../../draftmake/make_label.py" line="245"/>
      <location filename="../../draftmake/make_orthoarray.py" line="190"/>
      <location filename="../../draftmake/make_patharray.py" line="176"/>
      <location filename="../../draftmake/make_patharray.py" line="187"/>
      <location filename="../../draftmake/make_patharray.py" line="353"/>
      <location filename="../../draftmake/make_patharray.py" line="364"/>
      <location filename="../../draftmake/make_circulararray.py" line="135"/>
      <location filename="../../draftmake/make_polararray.py" line="102"/>
      <location filename="../../draftmake/make_dimension.py" line="330"/>
      <location filename="../../draftmake/make_dimension.py" line="470"/>
      <source>Wrong input: object not in document.</source>
      <translation>Неправильне введення: обʼєкт не в документі.</translation>
    </message>
    <message>
      <location filename="../../draftutils/gui_utils.py" line="754"/>
      <source>Does not have &apos;ViewObject.RootNode&apos;.</source>
      <translation type="unfinished">Does not have &apos;ViewObject.RootNode&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="64"/>
      <location filename="../../draftutils/init_draft_statusbar.py" line="79"/>
      <location filename="../../draftutils/init_draft_statusbar.py" line="93"/>
      <location filename="../../draftutils/init_draft_statusbar.py" line="184"/>
      <source>custom</source>
      <translation>На замовлення</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="169"/>
      <source>Unable to convert input into a  scale factor</source>
      <translation>Неможливо перетворити вхідні дані в коефіцієнт масштабування</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="185"/>
      <source>Set custom scale</source>
      <translation>Встановити довільний масштаб</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="188"/>
      <source>Set custom annotation scale in format x:x, x=x</source>
      <translation>Встановити спеціальний масштаб анотації у форматі x:x, x=x</translation>
    </message>
    <message>
      <location filename="../../draftutils/init_draft_statusbar.py" line="247"/>
      <source>Set the scale used by draft annotation tools</source>
      <translation>Встановіть масштаб, який використовується в проекті анотації</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="701"/>
      <source>Solids:</source>
      <translation>Суцільні тіла:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="702"/>
      <source>Faces:</source>
      <translation>Грані:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="703"/>
      <source>Wires:</source>
      <translation>Каркас:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="704"/>
      <source>Edges:</source>
      <translation>Ребра:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="705"/>
      <source>Vertices:</source>
      <translation>Вершини:</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="709"/>
      <source>Face</source>
      <translation>Грань</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="714"/>
      <source>Wire</source>
      <translation>Каркас</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="749"/>
      <location filename="../../draftutils/utils.py" line="757"/>
      <source>different types</source>
      <translation>різні типи</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="771"/>
      <source>Objects have different placements. Distance between the two base points: </source>
      <translation>Об'єкти мають різні розташування. Відстань між двома базовими точками: </translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="777"/>
      <source>has a different value</source>
      <translation>має інше значення</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="782"/>
      <source>doesn&apos;t exist in one of the objects</source>
      <translation type="unfinished">doesn&apos;t exist in one of the objects</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="898"/>
      <source>%s shares a base with %d other objects. Please check if you want to modify this.</source>
      <translation>%s поділяє базу з %d іншими об’єктами. Будь ласка, перевірте, чи ви хочете це змінити.</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="912"/>
      <source>%s cannot be modified because its placement is readonly.</source>
      <translation>%s не можна змінювати, оскільки його розміщення тільки для читання.</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1057"/>
      <source>Wrong input: unknown document.</source>
      <translation>Неправильне введення: невідомий документ.</translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1136"/>
      <source>This function will be deprecated in </source>
      <translation>Ця функція буде застаріла в </translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1138"/>
      <location filename="../../draftutils/utils.py" line="1144"/>
      <source>Please use </source>
      <translation>Будь ласка, використовуйте </translation>
    </message>
    <message>
      <location filename="../../draftutils/utils.py" line="1143"/>
      <source>This function will be deprecated. </source>
      <translation>Ця функція буде застаріла. </translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="59"/>
      <location filename="../../draftmake/make_fillet.py" line="150"/>
      <location filename="../../draftmake/make_fillet.py" line="156"/>
      <location filename="../../draftmake/make_fillet.py" line="162"/>
      <source>length:</source>
      <translation>довжина:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="135"/>
      <source>Two elements are needed.</source>
      <translation>Потрібно дві елементи.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="142"/>
      <source>Radius is too large</source>
      <translation>Радіус завеликий</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="148"/>
      <location filename="../../draftmake/make_fillet.py" line="154"/>
      <location filename="../../draftmake/make_fillet.py" line="160"/>
      <source>Segment</source>
      <translation>Відрізок</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_fillet.py" line="182"/>
      <source>Removed original objects.</source>
      <translation>Видалено оригінальні об’єкти.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_pointarray.py" line="142"/>
      <source>Wrong input: point object doesn&apos;t have &apos;Geometry&apos;, &apos;Links&apos;, or &apos;Components&apos;.</source>
      <translation type="unfinished">Wrong input: point object doesn&apos;t have &apos;Geometry&apos;, &apos;Links&apos;, or &apos;Components&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_pointarray.py" line="157"/>
      <location filename="../../draftmake/make_label.py" line="224"/>
      <location filename="../../draftmake/make_text.py" line="118"/>
      <source>Wrong input: must be a placement, a vector, or a rotation.</source>
      <translation>Невірне введення: має бути розміщення, вектор або обертання.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="98"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="306"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="504"/>
      <location filename="../../draftmake/make_circulararray.py" line="127"/>
      <source>Circular array</source>
      <translation>Круговий масив</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_circulararray.py" line="152"/>
      <source>Wrong input: must be a number or quantity.</source>
      <translation>Неправильне введення: повинно бути числом або кількістю.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="131"/>
      <location filename="../../draftmake/make_circulararray.py" line="161"/>
      <location filename="../../draftmake/make_polararray.py" line="111"/>
      <source>Wrong input: must be an integer number.</source>
      <translation>Введено неправильні дані: це ціле число.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="209"/>
      <location filename="../../draftmake/make_patharray.py" line="204"/>
      <location filename="../../draftmake/make_patharray.py" line="264"/>
      <location filename="../../draftmake/make_patharray.py" line="274"/>
      <location filename="../../draftmake/make_circulararray.py" line="170"/>
      <location filename="../../draftmake/make_polararray.py" line="125"/>
      <location filename="../../draftmake/make_dimension.py" line="230"/>
      <location filename="../../draftmake/make_dimension.py" line="237"/>
      <location filename="../../draftmake/make_dimension.py" line="245"/>
      <location filename="../../draftmake/make_dimension.py" line="373"/>
      <location filename="../../draftmake/make_dimension.py" line="395"/>
      <location filename="../../draftmake/make_dimension.py" line="541"/>
      <location filename="../../draftmake/make_dimension.py" line="620"/>
      <location filename="../../draftmake/make_dimension.py" line="647"/>
      <location filename="../../draftmake/make_dimension.py" line="655"/>
      <source>Wrong input: must be a vector.</source>
      <translation>Неправильне введення: має бути вектор.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="176"/>
      <source>This function is deprecated. Do not use this function directly.</source>
      <translation>Ця функція застаріла. Не використовуйте цю функцію безпосередньо.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="182"/>
      <source>Use one of &apos;make_linear_dimension&apos;, or &apos;make_linear_dimension_obj&apos;.</source>
      <translation type="unfinished">Use one of &apos;make_linear_dimension&apos;, or &apos;make_linear_dimension_obj&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="239"/>
      <location filename="../../draftmake/make_dimension.py" line="324"/>
      <source>Wrong input: object must not be a list.</source>
      <translation>Неправильне введення: обʼєкт не повинен бути списком.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="337"/>
      <location filename="../../draftmake/make_dimension.py" line="477"/>
      <source>Wrong input: object doesn&apos;t have a &apos;Shape&apos; to measure.</source>
      <translation type="unfinished">Wrong input: object doesn&apos;t have a &apos;Shape&apos; to measure.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="348"/>
      <source>Wrong input: object doesn&apos;t have at least one element in &apos;Vertexes&apos; to use for measuring.</source>
      <translation type="unfinished">Wrong input: object doesn&apos;t have at least one element in &apos;Vertexes&apos; to use for measuring.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="355"/>
      <location filename="../../draftmake/make_dimension.py" line="492"/>
      <source>Wrong input: must be an integer.</source>
      <translation>Введене значення має бути цілим числом.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="362"/>
      <source>i1: values below 1 are not allowed; will be set to 1.</source>
      <translation>i1: значення нижче 1 не допускаються; значення буде встановлено в 1.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="366"/>
      <location filename="../../draftmake/make_dimension.py" line="387"/>
      <source>Wrong input: vertex not in object.</source>
      <translation>Неправильне введення: вершина не в обʼєкті.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="383"/>
      <source>i2: values below 1 are not allowed; will be set to the last vertex in the object.</source>
      <translation>i2: значення нижче 1 не допускаються; буде встановлено до останньої вершини об'єкта.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="485"/>
      <source>Wrong input: object doesn&apos;t have at least one element in &apos;Edges&apos; to use for measuring.</source>
      <translation type="unfinished">Wrong input: object doesn&apos;t have at least one element in &apos;Edges&apos; to use for measuring.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="501"/>
      <source>index: values below 1 are not allowed; will be set to 1.</source>
      <translation>індекс: значення нижче 1 заборонені; буде встановлено в 1.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="510"/>
      <source>Wrong input: index doesn&apos;t correspond to an edge in the object.</source>
      <translation type="unfinished">Wrong input: index doesn&apos;t correspond to an edge in the object.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="518"/>
      <source>Wrong input: index doesn&apos;t correspond to a circular edge.</source>
      <translation type="unfinished">Wrong input: index doesn&apos;t correspond to a circular edge.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="527"/>
      <location filename="../../draftmake/make_dimension.py" line="533"/>
      <source>Wrong input: must be a string, &apos;radius&apos; or &apos;diameter&apos;.</source>
      <translation type="unfinished">Wrong input: must be a string, &apos;radius&apos; or &apos;diameter&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_dimension.py" line="628"/>
      <location filename="../../draftmake/make_dimension.py" line="634"/>
      <source>Wrong input: must be a list with two angles.</source>
      <translation>Введено неправильні дані: має бути списком з двома кутами.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_sketch.py" line="110"/>
      <source>No shape found</source>
      <translation>Форму не знайдено</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_sketch.py" line="117"/>
      <source>All Shapes must be planar</source>
      <translation>Усі Фігури мають бути пласкими</translation>
    </message>
    <message>
      <location filename="../../WorkingPlane.py" line="739"/>
      <location filename="../../draftmake/make_sketch.py" line="133"/>
      <location filename="../../draftmake/make_sketch.py" line="146"/>
      <source>All Shapes must be coplanar</source>
      <translation>Усі Фігури мають бути компланарними</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="69"/>
      <source>Layers</source>
      <translation>Шари</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_layers.py" line="47"/>
      <location filename="../../draftmake/make_layer.py" line="149"/>
      <location filename="../../draftmake/make_layer.py" line="166"/>
      <source>Layer</source>
      <translation>Шар</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="161"/>
      <source>Wrong input: it must be a string.</source>
      <translation>Неправильне введення: це має бути рядком.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="175"/>
      <location filename="../../draftmake/make_layer.py" line="183"/>
      <location filename="../../draftmake/make_layer.py" line="202"/>
      <location filename="../../draftmake/make_layer.py" line="210"/>
      <source>Wrong input: must be a tuple of three floats 0.0 to 1.0.</source>
      <translation>Неправильний вхідний параметр: має бути рядок з трьох плаваючих 0.0 до 1.0.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="348"/>
      <location filename="../../draftmake/make_orthoarray.py" line="175"/>
      <location filename="../../draftmake/make_patharray.py" line="196"/>
      <location filename="../../draftmake/make_patharray.py" line="373"/>
      <location filename="../../draftmake/make_polararray.py" line="118"/>
      <location filename="../../draftmake/make_layer.py" line="225"/>
      <source>Wrong input: must be a number.</source>
      <translation>Неправильне введення: має бути число.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="237"/>
      <location filename="../../draftmake/make_layer.py" line="246"/>
      <source>Wrong input: must be &apos;Solid&apos;, &apos;Dashed&apos;, &apos;Dotted&apos;, or &apos;Dashdot&apos;.</source>
      <translation type="unfinished">Wrong input: must be &apos;Solid&apos;, &apos;Dashed&apos;, &apos;Dotted&apos;, or &apos;Dashdot&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_layer.py" line="254"/>
      <source>Wrong input: must be a number between 0 and 100.</source>
      <translation>Невірне введення: має бути число від 0 до 100.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="264"/>
      <location filename="../../draftmake/make_patharray.py" line="221"/>
      <source>Wrong input: must be a list or tuple of strings, or a single string.</source>
      <translation>Неправильне введення: має бути списком або кортежем рядків, або єдиним рядком.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_patharray.py" line="257"/>
      <source>Wrong input: must be &apos;Original&apos;, &apos;Frenet&apos;, or &apos;Tangent&apos;.</source>
      <translation type="unfinished">Wrong input: must be &apos;Original&apos;, &apos;Frenet&apos;, or &apos;Tangent&apos;.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="95"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="266"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="446"/>
      <location filename="../../draftmake/make_polararray.py" line="94"/>
      <source>Polar array</source>
      <translation>Полярний масив</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="276"/>
      <source>Wrong input: subelement not in object.</source>
      <translation>Невірне введення: піделемент  не в об’єкті.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="285"/>
      <source>Wrong input: label_type must be a string.</source>
      <translation>Неправильне введення: тип етикетки має бути рядком.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="292"/>
      <source>Wrong input: label_type must be one of the following: </source>
      <translation>Неправильне введення: тип етикетки повинен бути одним з наступних: </translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="306"/>
      <location filename="../../draftmake/make_label.py" line="316"/>
      <location filename="../../draftmake/make_text.py" line="95"/>
      <location filename="../../draftmake/make_text.py" line="103"/>
      <source>Wrong input: must be a list of strings or a single string.</source>
      <translation>Неправильне введення: повинен бути списком рядків або одного рядка.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="330"/>
      <location filename="../../draftmake/make_label.py" line="339"/>
      <source>Wrong input: must be a string, &apos;Horizontal&apos;, &apos;Vertical&apos;, or &apos;Custom&apos;.</source>
      <translation type="unfinished">Wrong input: must be a string, &apos;Horizontal&apos;, &apos;Vertical&apos;, or &apos;Custom&apos;.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="357"/>
      <source>Wrong input: must be a list of at least two vectors.</source>
      <translation>Неправильний вхідний параметр: має бути списком щонайменше двох векторів.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="389"/>
      <source>Direction is not &apos;Custom&apos;; points won&apos;t be used.</source>
      <translation type="unfinished">Direction is not &apos;Custom&apos;; points won&apos;t be used.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_label.py" line="426"/>
      <source>Wrong input: must be a list of two elements. For example, [object, &apos;Edge1&apos;].</source>
      <translation type="unfinished">Wrong input: must be a list of two elements. For example, [object, &apos;Edge1&apos;].</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="63"/>
      <source>Internal orthogonal array</source>
      <translation>Внутрішній ортогональний масив</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="102"/>
      <source>Wrong input: must be a number or vector.</source>
      <translation>Введено неправильні дані: має бути число або вектор.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="107"/>
      <location filename="../../draftmake/make_orthoarray.py" line="110"/>
      <location filename="../../draftmake/make_orthoarray.py" line="113"/>
      <source>Input: single value expanded to vector.</source>
      <translation>Введення: одне значення розширене до вектора.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="139"/>
      <location filename="../../draftmake/make_orthoarray.py" line="146"/>
      <location filename="../../draftmake/make_orthoarray.py" line="153"/>
      <source>Input: number of elements must be at least 1. It is set to 1.</source>
      <translation>Введення: кількість елементів має бути принаймні 1.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="98"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="270"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="407"/>
      <location filename="../../draftmake/make_orthoarray.py" line="300"/>
      <source>Orthogonal array</source>
      <translation>Ортогональний масив</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="382"/>
      <source>Orthogonal array 2D</source>
      <translation>Ортогональний масив 2D</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="449"/>
      <source>Rectangular array</source>
      <translation>Прямокутний масив</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_orthoarray.py" line="522"/>
      <source>Rectangular array 2D</source>
      <translation>Прямокутний масив 2D</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="130"/>
      <location filename="../../draftmake/make_arc_3points.py" line="139"/>
      <source>Points:</source>
      <translation>Точки</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="135"/>
      <location filename="../../draftmake/make_arc_3points.py" line="144"/>
      <source>Wrong input: must be list or tuple of three points exactly.</source>
      <translation>Неправильне введення: точно має бути список або ряд з трьох точок.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="151"/>
      <source>Placement:</source>
      <translation>Розміщення:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="152"/>
      <source>Wrong input: incorrect type of placement.</source>
      <translation>Неправильне введення: неправильний тип розміщення.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="166"/>
      <source>Wrong input: incorrect type of points.</source>
      <translation>Неправильне введення: неправильний тип точок.</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="172"/>
      <source>Cannot generate shape:</source>
      <translation>Не вдається згенерувати фігуру:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="179"/>
      <source>Radius:</source>
      <translation>Радіус:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="180"/>
      <source>Center:</source>
      <translation>Центр:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="183"/>
      <source>Create primitive object</source>
      <translation>Створити примітивний обʼєкт</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="208"/>
      <location filename="../../draftmake/make_arc_3points.py" line="221"/>
      <source>Final placement:</source>
      <translation>Остаточне розміщення:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="210"/>
      <source>Face: True</source>
      <translation>Поверхня: Обрано</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="212"/>
      <source>Support:</source>
      <translation>Підтримка:</translation>
    </message>
    <message>
      <location filename="../../draftmake/make_arc_3points.py" line="213"/>
      <source>Map mode:</source>
      <translation>Режим карти:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="85"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="83"/>
      <source>Task panel:</source>
      <translation>Панель завдань:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="207"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="197"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="183"/>
      <source>At least one element must be selected.</source>
      <translation>Потрібно вибрати хоча б один елемент.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="201"/>
      <source>Number of elements must be at least 1.</source>
      <translation>Кількість елементів має бути принаймні 1.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="218"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="208"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="190"/>
      <source>Selection is not suitable for array.</source>
      <translation>Виділення не підходить для масиву.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="219"/>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="389"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="211"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="384"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="191"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="337"/>
      <source>Object:</source>
      <translation>Обʼєкт:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="326"/>
      <source>Interval X reset:</source>
      <translation>Інтервал X скинуто:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="335"/>
      <source>Interval Y reset:</source>
      <translation>Інтервал X скинуто:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="344"/>
      <source>Interval Z reset:</source>
      <translation>Інтервал X скинуто:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="358"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="353"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="306"/>
      <source>Fuse:</source>
      <translation>Злиття:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="372"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="367"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="320"/>
      <source>Create Link array:</source>
      <translation>Створити масив посилання:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="385"/>
      <source>Number of X elements:</source>
      <translation>Кількість елементів X:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="388"/>
      <source>Interval X:</source>
      <translation>Інтервал X:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="390"/>
      <source>Number of Y elements:</source>
      <translation>Кількість елементів Y:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="393"/>
      <source>Interval Y:</source>
      <translation>Інтервал Y:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="395"/>
      <source>Number of Z elements:</source>
      <translation>Кількість Z елементів:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="398"/>
      <source>Interval Z:</source>
      <translation>Інтервал Z:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="504"/>
      <location filename="../../drafttaskpanels/task_orthoarray.py" line="407"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="446"/>
      <source>Aborted:</source>
      <translation>Перервано:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="211"/>
      <source>Number of layers must be at least 2.</source>
      <translation>Кількість шарів має бути не менше 2.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="228"/>
      <source>Radial distance is zero. Resulting array may not look correct.</source>
      <translation>Радіальна відстань дорівнює нулю. Результат може виглядати неправильно.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="235"/>
      <source>Radial distance is negative. It is made positive to proceed.</source>
      <translation>Радикальна відстань негативна. Достатньо позитивно продовжити.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="239"/>
      <source>Tangential distance cannot be zero.</source>
      <translation>Дотична відстань не може бути нульовою.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="247"/>
      <source>Tangential distance is negative. It is made positive to proceed.</source>
      <translation>Тангенціальна відстань від'ємна. Це робиться позитивним для продовження.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="349"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="297"/>
      <source>Center reset:</source>
      <translation>Скинути центр:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="390"/>
      <source>Radial distance:</source>
      <translation>Радіальна відстань:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="392"/>
      <source>Tangential distance:</source>
      <translation>Дотична відстань:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="395"/>
      <source>Number of circular layers:</source>
      <translation>Кількість циклічних шарів:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="397"/>
      <source>Symmetry parameter:</source>
      <translation>Параметр симетрії:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_circulararray.py" line="400"/>
      <location filename="../../drafttaskpanels/task_polararray.py" line="342"/>
      <source>Center of rotation:</source>
      <translation>Центр обертання:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_shapestring.py" line="56"/>
      <source>ShapeString</source>
      <translation>РядФорми</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_shapestring.py" line="70"/>
      <source>Default</source>
      <translation>За замовчуванням</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="141"/>
      <location filename="../../drafttaskpanels/task_shapestring.py" line="184"/>
      <source>Create ShapeString</source>
      <translation>Створити ShapeString</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="195"/>
      <source>Number of elements must be at least 2.</source>
      <translation>Кількість елементів має бути принаймні 2.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="204"/>
      <source>The angle is above 360 degrees. It is set to this value to proceed.</source>
      <translation>Кут вище 360 градусів. Для продовження встановлено це значення.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="212"/>
      <source>The angle is below -360 degrees. It is set to this value to proceed.</source>
      <translation>Кут вище 360 градусів. Для продовження встановлено це значення.</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="338"/>
      <source>Number of elements:</source>
      <translation>Кількість елементів:</translation>
    </message>
    <message>
      <location filename="../../drafttaskpanels/task_polararray.py" line="339"/>
      <source>Polar angle:</source>
      <translation>Полярний кут:</translation>
    </message>
    <message>
      <location filename="../../importDXF.py" line="164"/>
      <source>The DXF import/export libraries needed by FreeCAD to handle
the DXF format were not found on this system.
Please either enable FreeCAD to download these libraries:
  1 - Load Draft workbench
  2 - Menu Edit &gt; Preferences &gt; Import-Export &gt; DXF &gt; Enable downloads
Or download these libraries manually, as explained on
https://github.com/yorikvanhavre/Draft-dxf-importer
To enabled FreeCAD to download these libraries, answer Yes.</source>
      <translation>DXF імпорт/експорт бібліотек, необхідних FreeCAD для обробки
Формат DXF не знайдено в цій системі.
Будь ласка, або увімкніть FreeCAD, щоб завантажити ці бібліотеки:
  1 - Завантажити робочий стіл Draft
  2 - Меню Редагування &gt; Налаштування &gt; Імпорт &gt; DXF &gt; Увімкнути завантаження
Або завантажити ці бібліотеки вручну, завантажити як описано на
https://github. om/yorikvanhavre/Draft-dxf-імпортер
Щоб увімкнути FreeCAD завантажити ці бібліотеки, відповісти на "Так".</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lineslope.py" line="64"/>
      <source>Change slope</source>
      <translation>Змінити нахил</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_sketcher_objects.py" line="70"/>
      <source>Sketch is too complex to edit: it is suggested to use sketcher default editor</source>
      <translation>Ескіз занадто складний для редагування: рекомендується використовувати редактор ескізів за замовчуванням</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="81"/>
      <source>Select an object to join</source>
      <translation>Оберіть обʼєкт для зʼєднання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="103"/>
      <source>Join lines</source>
      <translation>Обʼєднати рядки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_join.py" line="113"/>
      <source>Selection:</source>
      <translation>Вибір:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="316"/>
      <source>Select a Draft object to edit</source>
      <translation>Оберіть креслення для редагування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="584"/>
      <source>No edit point found for selected object</source>
      <translation>Не знайдено точки редагування для вибраного обʼєкту</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="842"/>
      <source>Too many objects selected, max number set to:</source>
      <translation>Забагато обʼєктів обрано. Максимальне число встановлено на:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit.py" line="850"/>
      <source>: this object is not editable</source>
      <translation>: цей обʼєкт не редагується</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_grid.py" line="51"/>
      <source>Toggle grid</source>
      <translation>Показати або приховати сітку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_styles.py" line="80"/>
      <source>Change Style</source>
      <translation>Змінити стиль</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="72"/>
      <location filename="../../draftguitools/gui_downgrade.py" line="72"/>
      <source>Select an object to upgrade</source>
      <translation>Оберіть обʼєкт для покращення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_downgrade.py" line="89"/>
      <source>Downgrade</source>
      <translation>Зниження</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="160"/>
      <source>Pick a face, 3 vertices or a WP Proxy to define the drawing plane</source>
      <translation>Виберіть поверхню, 3 вершини або WP проксі для визначення рисуючої площини</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1112"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="292"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="360"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="565"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="576"/>
      <source>Current working plane</source>
      <translation>Поточна робоча площина</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="295"/>
      <source>Working plane aligned to global placement of</source>
      <translation>Робоча площина вирівнений до глобального розташування</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="832"/>
      <location filename="../../WorkingPlane.py" line="854"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="408"/>
      <source>Top</source>
      <translation>Згори</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="834"/>
      <location filename="../../WorkingPlane.py" line="868"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="420"/>
      <source>Front</source>
      <translation>Фронт</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="836"/>
      <location filename="../../WorkingPlane.py" line="882"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="432"/>
      <source>Side</source>
      <translation>Сторона</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="558"/>
      <source>Dir</source>
      <translation>Директорія</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1353"/>
      <location filename="../../draftguitools/gui_selectplane.py" line="562"/>
      <location filename="../../draftguitools/gui_offset.py" line="260"/>
      <location filename="../../draftguitools/gui_offset.py" line="278"/>
      <location filename="../../draftguitools/gui_offset.py" line="340"/>
      <source>Offset</source>
      <translation>Зсув</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_selectplane.py" line="574"/>
      <source>Custom</source>
      <translation>Підлаштувати</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snapper.py" line="1663"/>
      <source>(ON)</source>
      <translation>Вкл</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snapper.py" line="1669"/>
      <source>(OFF)</source>
      <translation>Викл</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="92"/>
      <source>Select an object to scale</source>
      <translation>Оберіть обʼєкт для масштабування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="111"/>
      <source>Pick base point</source>
      <translation>Вкажіть базову точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="138"/>
      <source>Pick reference distance from base point</source>
      <translation>Вкажіть відносну відстань від базової точки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="77"/>
      <location filename="../../DraftGui.py" line="1053"/>
      <location filename="../../draftguitools/gui_scale.py" line="208"/>
      <location filename="../../draftguitools/gui_scale.py" line="233"/>
      <location filename="../../draftguitools/gui_scale.py" line="371"/>
      <location filename="../../draftguitools/gui_move.py" line="215"/>
      <location filename="../../draftguitools/gui_rotate.py" line="296"/>
      <source>Copy</source>
      <translation>Копіювати</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="212"/>
      <location filename="../../draftguitools/gui_scale.py" line="242"/>
      <location filename="../../draftguitools/gui_scale.py" line="374"/>
      <source>Scale</source>
      <translation>Масштабування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="215"/>
      <source>Some subelements could not be scaled.</source>
      <translation>Деякі піделементи не можуть масштабуватися.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="351"/>
      <source>Unable to scale object:</source>
      <translation>Неможливо масштабувати обʼєкт:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="355"/>
      <source>Unable to scale objects:</source>
      <translation>Неможливо масштабувати обʼєкт:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="361"/>
      <source>This object type cannot be scaled directly. Please use the clone method.</source>
      <translation>Цей тип об'єкта не можна масштабувати безпосередньо. Будь ласка, використовуйте метод клонування.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_scale.py" line="421"/>
      <source>Pick new distance from base point</source>
      <translation>Вкажіть нову відстань від базовоої точки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="69"/>
      <source>Click anywhere on a line to split it.</source>
      <translation>Клацніть будь-де на лінії, щоб розділити її.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_split.py" line="112"/>
      <source>Split line</source>
      <translation>Розділити лінію</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_points.py" line="148"/>
      <location filename="../../draftguitools/gui_points.py" line="162"/>
      <source>Create Point</source>
      <translation>Створити точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="80"/>
      <source>Select an object to stretch</source>
      <translation>Оберіть обʼєкт для розтягування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="141"/>
      <source>Pick first point of selection rectangle</source>
      <translation>Вкажіть першу точку прямокутника вибору</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="177"/>
      <source>Pick opposite point of selection rectangle</source>
      <translation>Вкажіть протилежну точку прямокутника вибору</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="186"/>
      <source>Pick start point of displacement</source>
      <translation>Вкажіть початкову точку зсуву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="249"/>
      <source>Pick end point of displacement</source>
      <translation>Вкажіть кінцеву точку зсуву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="529"/>
      <source>Turning one Rectangle into a Wire</source>
      <translation>Перетворення  одного Прямокутника в Сітку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_stretch.py" line="558"/>
      <source>Stretch</source>
      <translation>Розтягнути</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="112"/>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="91"/>
      <source>Please select exactly two objects, the base object and the path object, before calling this command.</source>
      <translation>Будь ласка, виберіть саме два об’єкти, основний об'єкт і шлях до запуску цієї команди.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pathtwistedarray.py" line="115"/>
      <source>Path twisted array</source>
      <translation>Траєкторія скручування масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="70"/>
      <source>Annotation style editor</source>
      <translation>Редактор стилів анотацій</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="306"/>
      <source>Open styles file</source>
      <translation>Відкрити файл стилів</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="308"/>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="330"/>
      <source>JSON file (*.json)</source>
      <translation>JSON файл (*.json)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_annotationstyleeditor.py" line="328"/>
      <source>Save styles file</source>
      <translation>Зберегти файл стилів</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="83"/>
      <source>Pick location point</source>
      <translation>Вкажіть точку розташування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_texts.py" line="129"/>
      <source>Create Text</source>
      <translation>Створити текст</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimension_ops.py" line="57"/>
      <source>Flip dimension</source>
      <translation>Перевернути розмірність</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="131"/>
      <source>Main toggle snap</source>
      <translation>Основний перемикач привʼязки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="170"/>
      <source>Midpoint snap</source>
      <translation>середина приʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="206"/>
      <source>Perpendicular snap</source>
      <translation>Перпендикулярне привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="241"/>
      <source>Grid snap</source>
      <translation>Привʼязка до сітки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="277"/>
      <source>Intersection snap</source>
      <translation>Привʼязування перетину</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="313"/>
      <source>Parallel snap</source>
      <translation>Паралельне привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="350"/>
      <source>Endpoint snap</source>
      <translation>Кінцеве привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="387"/>
      <source>Angle snap (30 and 45 degrees)</source>
      <translation>Кути привʼязування (30 і 45 градусів)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="424"/>
      <source>Arc center snap</source>
      <translation>Центр дуги привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="460"/>
      <source>Edge extension snap</source>
      <translation>Ребро розширення привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="494"/>
      <source>Near snap</source>
      <translation>близьке привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="531"/>
      <source>Orthogonal snap</source>
      <translation>Ортогональне привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="568"/>
      <source>Special point snap</source>
      <translation>Спеціальна точка привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="606"/>
      <source>Dimension display</source>
      <translation>Відображення розмірів</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="648"/>
      <source>Working plane snap</source>
      <translation>Робоча плода привʼязування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_snaps.py" line="683"/>
      <source>Show snap toolbar</source>
      <translation>Показувати панель інструментів привʼязки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="85"/>
      <location filename="../../draftguitools/gui_arcs.py" line="93"/>
      <source>Pick center point</source>
      <translation>Вкажіть точку центру</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="206"/>
      <location filename="../../draftguitools/gui_polygons.py" line="217"/>
      <location filename="../../draftguitools/gui_polygons.py" line="279"/>
      <location filename="../../draftguitools/gui_arcs.py" line="279"/>
      <location filename="../../draftguitools/gui_arcs.py" line="298"/>
      <location filename="../../draftguitools/gui_arcs.py" line="452"/>
      <source>Pick radius</source>
      <translation>Вибрати радіус</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="243"/>
      <source>Create Polygon (Part)</source>
      <translation>Створити багатокутник (Частина)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_polygons.py" line="263"/>
      <source>Create Polygon</source>
      <translation>Створити новий полігон</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="114"/>
      <source>Please select exactly two objects, the base object and the point object, before calling this command.</source>
      <translation>Будь ласка, оберіть лише два об’єкти, базовий об'єкт та об'єкт точки перед виконанням цієї команди.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_pointarray.py" line="134"/>
      <source>Point array</source>
      <translation>Масив точок</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_clone.py" line="84"/>
      <source>Select an object to clone</source>
      <translation>Оберіть обʼєкт для клонування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shapestrings.py" line="90"/>
      <location filename="../../draftguitools/gui_shapestrings.py" line="104"/>
      <source>Pick ShapeString location point</source>
      <translation>Вкажіть точку розташування ShapeString</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_drawing.py" line="87"/>
      <source>The Drawing Workbench is obsolete since 0.17, consider using the TechDraw Workbench instead.</source>
      <translation>Малювання Workbench є застарілим з 0.17, натомість розгляньте можливість використання Workbench TechDraw</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="73"/>
      <location filename="../../draftguitools/gui_drawing.py" line="91"/>
      <source>Select an object to project</source>
      <translation>Оберіть обʼєкт для проектування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="81"/>
      <source>Select an object to offset</source>
      <translation>Виділіть обʼєкт для зсуву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="88"/>
      <source>Offset only works on one object at a time.</source>
      <translation>Зсув працює лише для одного обʼєкта за раз.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="98"/>
      <source>Cannot offset this object type</source>
      <translation>Неможливо змістити цей тип обʼєкта</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_offset.py" line="134"/>
      <source>Offset of Bezier curves is currently not supported</source>
      <translation>Зсув кривих Безьє зараз не підтримується</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="177"/>
      <location filename="../../draftguitools/gui_offset.py" line="154"/>
      <source>Pick distance</source>
      <translation>Вибрати відстань</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="87"/>
      <source>Pick target point</source>
      <translation>Вкажіть цільову точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="167"/>
      <source>Create Label</source>
      <translation>Створити мітку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="199"/>
      <location filename="../../draftguitools/gui_labels.py" line="226"/>
      <source>Pick endpoint of leader line</source>
      <translation>Вкажіть кінцеву точку виносної лінії </translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_labels.py" line="209"/>
      <location filename="../../draftguitools/gui_labels.py" line="236"/>
      <source>Pick text position</source>
      <translation>Вказати положення тексту</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="71"/>
      <source>Select an object to convert.</source>
      <translation>Оберіть обʼєкт для перетворення.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="104"/>
      <source>Convert to Sketch</source>
      <translation>Перетворити на ескіз</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="118"/>
      <source>Convert to Draft</source>
      <translation>Перетворити на чернетки</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_draft2sketch.py" line="147"/>
      <source>Convert Draft/Sketch</source>
      <translation>Перетворення Чернетки/ескіз</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_subelements.py" line="115"/>
      <source>Select an object to edit</source>
      <translation>Оберіть обʼєкт для редагування</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_heal.py" line="51"/>
      <source>Heal</source>
      <translation>Зцілення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="98"/>
      <source>Select objects to trim or extend</source>
      <translation>Оберіть обʼєкти, щоб обрізати або розширити</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1359"/>
      <location filename="../../DraftGui.py" line="1418"/>
      <location filename="../../draftguitools/gui_trimex.py" line="217"/>
      <source>Distance</source>
      <translation>Відстань</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1360"/>
      <location filename="../../DraftGui.py" line="1419"/>
      <location filename="../../draftguitools/gui_trimex.py" line="218"/>
      <source>Offset distance</source>
      <translation>Відстань зсуву</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="933"/>
      <location filename="../../draftguitools/gui_trimex.py" line="221"/>
      <source>Angle</source>
      <translation>Кут</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="222"/>
      <source>Offset angle</source>
      <translation>Кут зсуву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="491"/>
      <source>Unable to trim these objects, only Draft wires and arcs are supported.</source>
      <translation>Підрізати можливо лише каркаси креслення та дуги.
</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="498"/>
      <source>Unable to trim these objects, too many wires</source>
      <translation>Неможливо обрізати ці обʼєкти, занадто багато ліній</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="515"/>
      <source>These objects don&apos;t intersect.</source>
      <translation type="unfinished">These objects don&apos;t intersect.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_trimex.py" line="518"/>
      <source>Too many intersection points.</source>
      <translation>Забагато точок перетину.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_shape2dview.py" line="114"/>
      <source>Create 2D view</source>
      <translation>Створити 2D вигляд</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_facebinders.py" line="75"/>
      <source>Select faces from existing objects</source>
      <translation>Оберіть грані серед наявних обʼєктів</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="73"/>
      <source>No active Draft Toolbar.</source>
      <translation>Не активний інструмент креслення.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="94"/>
      <source>Construction mode</source>
      <translation>Режим Будівництва</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="135"/>
      <source>Continue mode</source>
      <translation>Режим Продовження</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_togglemodes.py" line="177"/>
      <source>Toggle display mode</source>
      <translation>Перемкнути режим відображення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="129"/>
      <location filename="../../draftguitools/gui_ellipses.py" line="77"/>
      <location filename="../../draftguitools/gui_lines.py" line="88"/>
      <location filename="../../draftguitools/gui_rectangles.py" line="75"/>
      <source>Pick first point</source>
      <translation>Вкажіть першу точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="170"/>
      <source>Create Line</source>
      <translation>Створити лінію</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="193"/>
      <source>Create Wire</source>
      <translation>Створити каркас</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="152"/>
      <location filename="../../draftguitools/gui_lines.py" line="226"/>
      <location filename="../../draftguitools/gui_lines.py" line="235"/>
      <location filename="../../draftguitools/gui_lines.py" line="242"/>
      <location filename="../../draftguitools/gui_lines.py" line="250"/>
      <location filename="../../draftguitools/gui_lines.py" line="260"/>
      <location filename="../../draftguitools/gui_splines.py" line="148"/>
      <source>Pick next point</source>
      <translation>Вкажіть наступну точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="347"/>
      <source>Unable to create a Wire from selected objects</source>
      <translation>Не вдалося створити Сітку із вибраних обʼєктів</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_lines.py" line="370"/>
      <source>Convert to Wire</source>
      <translation>Конвертувати в Сітку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="133"/>
      <source>Create Plane</source>
      <translation>Створити площину</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rectangles.py" line="151"/>
      <source>Create Rectangle</source>
      <translation>Створити прямокутник</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="202"/>
      <location filename="../../draftguitools/gui_rectangles.py" line="210"/>
      <source>Pick opposite point</source>
      <translation>Вкажіть протилежну точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_ellipses.py" line="129"/>
      <location filename="../../draftguitools/gui_ellipses.py" line="147"/>
      <source>Create Ellipse</source>
      <translation>Створити еліпс</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_upgrade.py" line="89"/>
      <source>Upgrade</source>
      <translation>Оновити</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="221"/>
      <location filename="../../draftguitools/gui_dimensions.py" line="257"/>
      <location filename="../../draftguitools/gui_dimensions.py" line="272"/>
      <location filename="../../draftguitools/gui_dimensions.py" line="303"/>
      <source>Create Dimension</source>
      <translation>Створити розмір</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="319"/>
      <source>Create Dimension (radial)</source>
      <translation>Створити розмірність (радіал)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="526"/>
      <source>Edge too short!</source>
      <translation>Ребро занадто коротке!</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_dimensions.py" line="546"/>
      <source>Edges don&apos;t intersect!</source>
      <translation type="unfinished">Edges don&apos;t intersect!</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="95"/>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="589"/>
      <source>This object does not support possible coincident points, please try again.</source>
      <translation>Цей об'єкт не підтримує можливі випадкові точки, будь ласка, спробуйте ще раз.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="168"/>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="633"/>
      <source>Active object must have more than two points/nodes</source>
      <translation>Активний обʼєкт повинен мати більше двох точок/вузлів</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="753"/>
      <source>Selection is not a Knot</source>
      <translation>Виділений елемент не є вузлом</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_edit_draft_objects.py" line="784"/>
      <source>Endpoint of BezCurve can&apos;t be smoothed</source>
      <translation type="unfinished">Endpoint of BezCurve can&apos;t be smoothed</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="80"/>
      <source>Fillet radius</source>
      <translation>Скруглення радіус</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="83"/>
      <source>Radius of fillet</source>
      <translation>Радіус скруглення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="120"/>
      <source>Enter radius.</source>
      <translation>Введіть радіус.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="139"/>
      <source>Delete original objects:</source>
      <translation>Видалити оригінальні об’єкти:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="144"/>
      <source>Chamfer mode:</source>
      <translation>Режим Фаски:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="161"/>
      <source>Two elements needed.</source>
      <translation>Потрібно два елемента.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="168"/>
      <source>Test object</source>
      <translation>Тестовий обʼєкт</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="169"/>
      <source>Test object removed</source>
      <translation>Тестовий обʼєкт видалено</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="171"/>
      <source>Fillet cannot be created</source>
      <translation>Скруглення не може бути створено</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_fillets.py" line="203"/>
      <source>Create fillet</source>
      <translation>Створити заокруглення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="82"/>
      <source>Select an object to array</source>
      <translation>Оберіть обʼєкт для створення масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_array_simple.py" line="108"/>
      <source>Array</source>
      <translation>Масив</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="136"/>
      <location filename="../../draftguitools/gui_beziers.py" line="341"/>
      <source>Bézier curve has been closed</source>
      <translation>Крива Безьє була замкнена</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="144"/>
      <location filename="../../draftguitools/gui_beziers.py" line="377"/>
      <location filename="../../draftguitools/gui_splines.py" line="138"/>
      <source>Last point has been removed</source>
      <translation>Останню точку було видалено</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="155"/>
      <location filename="../../draftguitools/gui_splines.py" line="153"/>
      <source>Pick next point, or finish (A) or close (O)</source>
      <translation>Виберіть наступну точку або завершити (А) або закрити (O)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="219"/>
      <location filename="../../draftguitools/gui_beziers.py" line="469"/>
      <source>Create BezCurve</source>
      <translation>Створити криву Безьє</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="385"/>
      <source>Click and drag to define next knot</source>
      <translation>Клацніть та перетягніть, щоб визначити наступне вузол</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_beziers.py" line="394"/>
      <source>Click and drag to define next knot, or finish (A) or close (O)</source>
      <translation>Клік та перетягніть, щоб визначити наступне кно, або завершити (А) або закрити (O)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="84"/>
      <source>Select an object to rotate</source>
      <translation>Оберіть обʼєкт для обертання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="101"/>
      <source>Pick rotation center</source>
      <translation>Вибрати центр обертання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="196"/>
      <location filename="../../draftguitools/gui_rotate.py" line="411"/>
      <source>Base angle</source>
      <translation>Базовий кут</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="199"/>
      <location filename="../../draftguitools/gui_rotate.py" line="414"/>
      <source>The base angle you wish to start the rotation from</source>
      <translation>Базовий кут, з якого ви хочете розпочати обертання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="204"/>
      <location filename="../../draftguitools/gui_rotate.py" line="417"/>
      <source>Pick base angle</source>
      <translation>Оберіть базовий кут</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="210"/>
      <location filename="../../draftguitools/gui_rotate.py" line="426"/>
      <source>Rotation</source>
      <translation>Поворот</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="216"/>
      <location filename="../../draftguitools/gui_rotate.py" line="432"/>
      <source>The amount of rotation you wish to perform.
The final angle will be the base angle plus this amount.</source>
      <translation>Кількість обертання, яке бажаєте виконати.
Остаточний кут буде базовим кутом плюс це сума.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_rotate.py" line="223"/>
      <location filename="../../draftguitools/gui_rotate.py" line="440"/>
      <source>Pick rotation angle</source>
      <translation>Кут повороту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1290"/>
      <location filename="../../draftguitools/gui_rotate.py" line="300"/>
      <source>Rotate</source>
      <translation>Обертання</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="222"/>
      <location filename="../../draftguitools/gui_rotate.py" line="304"/>
      <source>Some subelements could not be moved.</source>
      <translation>Деякі піделементи не можуть рухатися.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="307"/>
      <location filename="../../draftguitools/gui_arcs.py" line="310"/>
      <location filename="../../draftguitools/gui_arcs.py" line="489"/>
      <location filename="../../draftguitools/gui_arcs.py" line="490"/>
      <source>Start angle</source>
      <translation>Початковий кут:</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="317"/>
      <location filename="../../draftguitools/gui_arcs.py" line="495"/>
      <source>Pick start angle</source>
      <translation>Вибрати початковий кут</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="321"/>
      <location filename="../../draftguitools/gui_arcs.py" line="324"/>
      <location filename="../../draftguitools/gui_arcs.py" line="497"/>
      <location filename="../../draftguitools/gui_arcs.py" line="498"/>
      <source>Aperture angle</source>
      <translation>Кут щілини</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="329"/>
      <source>Pick aperture</source>
      <translation>Обрати шпарину</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="357"/>
      <source>Create Circle (Part)</source>
      <translation>Створити коло (частина)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="376"/>
      <source>Create Circle</source>
      <translation>Створити коло</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="411"/>
      <source>Create Arc (Part)</source>
      <translation>Створити дугу (частина)</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="432"/>
      <source>Create Arc</source>
      <translation>Створити дугу</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="511"/>
      <source>Pick aperture angle</source>
      <translation>Виберіть кут апертури</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_arcs.py" line="559"/>
      <location filename="../../draftguitools/gui_arcs.py" line="602"/>
      <source>Arc by 3 points</source>
      <translation>Дуга за 3 точками</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="65"/>
      <source>Add to group</source>
      <translation>Додати до групи</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="68"/>
      <source>Ungroup</source>
      <translation>Розгрупувати</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="70"/>
      <source>Add new group</source>
      <translation>Додати нову групу</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="163"/>
      <source>Select group</source>
      <translation>Вибрати групу</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="205"/>
      <source>No new selection. You must select non-empty groups or objects inside groups.</source>
      <translation>Немає нового вибору. Ви повинні вибрати не порожні групи або об'єкти всередині груп.</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="215"/>
      <source>Autogroup</source>
      <translation>Автогрупування</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="876"/>
      <location filename="../../DraftGui.py" line="920"/>
      <location filename="../../DraftGui.py" line="1377"/>
      <location filename="../../DraftGui.py" line="2438"/>
      <location filename="../../DraftGui.py" line="2458"/>
      <location filename="../../draftguitools/gui_groups.py" line="256"/>
      <location filename="../../draftguitools/gui_groups.py" line="261"/>
      <source>None</source>
      <translation>Немає</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="267"/>
      <source>Add new Layer</source>
      <translation>Додати новий шар</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="325"/>
      <source>Add to construction group</source>
      <translation>Додати до групи конструкцій</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="384"/>
      <source>Add a new group with a given name</source>
      <translation>Додати нову групу із заданим імʼям</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="417"/>
      <source>Add group</source>
      <translation>Додати групу</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="419"/>
      <source>Group name</source>
      <translation>Назва групи</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_groups.py" line="427"/>
      <source>Group</source>
      <translation>Група</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="87"/>
      <source>Select an object to move</source>
      <translation>Оберіть обʼєкт для переміщення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="106"/>
      <source>Pick start point</source>
      <translation>Вкажіть початкову точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="167"/>
      <location filename="../../draftguitools/gui_move.py" line="315"/>
      <source>Pick end point</source>
      <translation>Вкажіть кінцеву точку</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_move.py" line="219"/>
      <source>Move</source>
      <translation>Переміщення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_patharray.py" line="155"/>
      <source>Path array</source>
      <translation>Шлях масиву</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="126"/>
      <source>Spline has been closed</source>
      <translation>Сплайн було замкнено</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_splines.py" line="191"/>
      <source>Create B-spline</source>
      <translation>Створити B-сплайн</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="78"/>
      <source>Select an object to mirror</source>
      <translation>Оберіть обʼєкт для створення дзеркального зображення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="97"/>
      <source>Pick start point of mirror line</source>
      <translation>Вкажіть початкову точку лінії віддзеркалення</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="126"/>
      <source>Mirror</source>
      <translation>Віддзеркалити</translation>
    </message>
    <message>
      <location filename="../../draftguitools/gui_mirror.py" line="177"/>
      <location filename="../../draftguitools/gui_mirror.py" line="204"/>
      <source>Pick end point of mirror line</source>
      <translation>Вкажіть кінцеву точку лінії віддзеркалення</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="57"/>
      <location filename="../../DraftGui.py" line="947"/>
      <source>Relative</source>
      <translation>Відносний</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="62"/>
      <location filename="../../DraftGui.py" line="959"/>
      <source>Global</source>
      <translation>Глобальна</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="67"/>
      <location filename="../../DraftGui.py" line="994"/>
      <location filename="../../DraftGui.py" line="1448"/>
      <source>Continue</source>
      <translation>Продовжити</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="72"/>
      <location filename="../../DraftGui.py" line="1018"/>
      <source>Close</source>
      <translation>Закрити</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="82"/>
      <source>Subelement mode</source>
      <translation>Режим піделементу</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="87"/>
      <source>Fill</source>
      <translation>Заповнити</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="92"/>
      <source>Exit</source>
      <translation>Вихід</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="97"/>
      <source>Snap On/Off</source>
      <translation>Привʼязка вкл/викл</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="102"/>
      <source>Increase snap radius</source>
      <translation>Збільшити радіус привʼязки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="107"/>
      <source>Decrease snap radius</source>
      <translation>Зменшити радіус привʼязки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="112"/>
      <source>Restrict X</source>
      <translation>Обмежити X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="117"/>
      <source>Restrict Y</source>
      <translation>Обмежити Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="122"/>
      <source>Restrict Z</source>
      <translation>Обмежити Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="127"/>
      <location filename="../../DraftGui.py" line="1040"/>
      <source>Select edge</source>
      <translation>Оберіть ребро</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="132"/>
      <source>Add custom snap point</source>
      <translation>Додати користувацьку точку привʼязки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="137"/>
      <source>Length mode</source>
      <translation>Режим довжини</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="142"/>
      <location filename="../../DraftGui.py" line="1024"/>
      <source>Wipe</source>
      <translation>Витерти</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="147"/>
      <source>Set Working Plane</source>
      <translation>Встановити робочу площину</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="152"/>
      <source>Cycle snap object</source>
      <translation>Циклічний об’єкт привʼязки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="157"/>
      <source>Toggle near snap on/off</source>
      <translation>Вкл./викл поряд з приєднанням</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="351"/>
      <source>Draft Command Bar</source>
      <translation>Панель команд креслення</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="838"/>
      <source>Auto</source>
      <translation>Автоматично</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="919"/>
      <source>active command:</source>
      <translation>активна команда:</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="921"/>
      <source>Active Draft command</source>
      <translation>Активна команда креслення</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="922"/>
      <source>X coordinate of next point</source>
      <translation>X координати наступної точки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="923"/>
      <location filename="../../DraftGui.py" line="1378"/>
      <source>X</source>
      <translation>X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="924"/>
      <source>Y</source>
      <translation>Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="925"/>
      <source>Z</source>
      <translation>Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="926"/>
      <source>Y coordinate of next point</source>
      <translation>Y координати наступної точки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="927"/>
      <source>Z coordinate of next point</source>
      <translation>Z координати наступної точки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="928"/>
      <source>Enter point</source>
      <translation>Введіть точку</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="931"/>
      <source>Enter a new point with the given coordinates</source>
      <translation>Задати нову точку з вказаними координатами</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="932"/>
      <source>Length</source>
      <translation>Довжина</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="934"/>
      <source>Length of current segment</source>
      <translation>Довжина поточного сегменту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="935"/>
      <source>Angle of current segment</source>
      <translation>Кут поточного сегменту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="939"/>
      <source>Check this to lock the current angle</source>
      <translation>Поставте прапорець, щоб зафіксувати поточний кут</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="943"/>
      <location filename="../../DraftGui.py" line="1428"/>
      <source>Radius</source>
      <translation>Радіус</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="944"/>
      <location filename="../../DraftGui.py" line="1429"/>
      <source>Radius of Circle</source>
      <translation>Радіус кола</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="957"/>
      <source>Coordinates relative to last point or to coordinate system origin
if is the first point to set</source>
      <translation>Координати відносно останньої точки або початку  системи координат
якщо це перша точка для встановлення</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="967"/>
      <source>Coordinates relative to global coordinate system.
Uncheck to use working plane coordinate system</source>
      <translation>Координати відносно глобальної системи координат.
Зніміть прапорець, щоб використовувати систему координат робочої площини</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="969"/>
      <source>Filled</source>
      <translation>Заповнено</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="978"/>
      <source>Check this if the object should appear as filled, otherwise it will appear as wireframe.
Not available if Draft preference option &apos;Use Part Primitives&apos; is enabled</source>
      <translation type="unfinished">Check this if the object should appear as filled, otherwise it will appear as wireframe.
Not available if Draft preference option &apos;Use Part Primitives&apos; is enabled</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="980"/>
      <source>Finish</source>
      <translation>Завершити</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="984"/>
      <source>Finishes the current drawing or editing operation</source>
      <translation>Завершити дію поточного креслення або операцію редагування</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="991"/>
      <source>If checked, command will not finish until you press the command button again</source>
      <translation>Якщо обрано, команда не завершиться, доки ви не натиснете командну кнопку знову</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1004"/>
      <source>If checked, an OCC-style offset will be performedinstead of the classic offset</source>
      <translation>Якщо вибрано, то замість класичного зсуву буде виконаний зсув OCC-типу</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1005"/>
      <source>&amp;OCC-style offset</source>
      <translation>Зсув у &amp;OCC стилі</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1015"/>
      <source>&amp;Undo (CTRL+Z)</source>
      <translation>&amp;Скасувати (CTRL+Z)</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1016"/>
      <source>Undo the last segment</source>
      <translation>Скасувати останній відрізок</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1022"/>
      <source>Finishes and closes the current line</source>
      <translation>Завершення та закриття поточної лінії</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1031"/>
      <source>Wipes the existing segments of this line and starts again from the last point</source>
      <translation>Стирає наявні сегменти цієї лінії та починає знову з останньої точки</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1033"/>
      <source>Set WP</source>
      <translation>Встановити WP</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1037"/>
      <source>Reorients the working plane on the last segment</source>
      <translation>Переорієнтує робочу площину на останній сегмент</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1048"/>
      <source>Selects an existing edge to be measured by this dimension</source>
      <translation>Вибирає існуюче ребро для вимірювання цього розміру</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1049"/>
      <source>Sides</source>
      <translation>Сторони</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1050"/>
      <source>Number of sides</source>
      <translation>Кількість сторін</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1060"/>
      <source>If checked, objects will be copied instead of moved. Preferences -&gt; Draft -&gt; Global copy mode to keep this mode in next commands</source>
      <translation>Якщо прапорець встановлено, об'єкти будуть скопійовані замість переміщення. Налаштування -&gt; Чернетка -&gt; Глобальна копія зробить цей режим в наступних командах</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1063"/>
      <source>Modify subelements</source>
      <translation>Змінити піделементи</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1072"/>
      <source>If checked, subelements will be modified instead of entire objects</source>
      <translation>Якщо прапорець встановлено, субелементи будуть змінені замість усіх об'єктів</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1073"/>
      <source>Text string to draw</source>
      <translation>Текстовий рядок для малювання</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1074"/>
      <source>String</source>
      <translation>Рядок</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1075"/>
      <source>Height of text</source>
      <translation>Висота тексту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1076"/>
      <source>Height</source>
      <translation>Висота</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1077"/>
      <source>Intercharacter spacing</source>
      <translation>Міжсимвольна відстань</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1078"/>
      <source>Tracking</source>
      <translation>Відстеження</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1079"/>
      <source>Full path to font file:</source>
      <translation>Повний шлях до файлу шрифта:</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1082"/>
      <source>Open a FileChooser for font file</source>
      <translation>Відкрити FileChooser для файла шрифту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1083"/>
      <source>Create text</source>
      <translation>Створення тексту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1089"/>
      <source>Press this button to create the text object, or finish your text with two blank lines</source>
      <translation>Натисніть цю кнопку, щоб створити текстовий об’єкт, або закінчити текст двома пустими рядками</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1116"/>
      <source>Change default style for new objects</source>
      <translation>Змінити стиль за замовчуванням для нових обʼєктів</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1117"/>
      <source>Toggle construction mode</source>
      <translation>Змінити режим конструювання</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1118"/>
      <location filename="../../DraftGui.py" line="2444"/>
      <location filename="../../DraftGui.py" line="2464"/>
      <source>Autogroup off</source>
      <translation>Автогрупування вимкнено</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1242"/>
      <source>Line</source>
      <translation>Лінія</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1257"/>
      <source>DWire</source>
      <translation>DWire</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1280"/>
      <source>Circle</source>
      <translation>Коло</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1285"/>
      <source>Arc</source>
      <translation>Дуга</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1296"/>
      <source>Point</source>
      <translation>Точка</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1332"/>
      <source>Label</source>
      <translation>Позначка</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1415"/>
      <source>Trimex</source>
      <translation>Тримекс</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1529"/>
      <source>Pick Object</source>
      <translation>Вкажіть обʼєкт</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1535"/>
      <source>Edit</source>
      <translation>Правка</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1585"/>
      <source>Local u0394X</source>
      <translation>Локальний u0394X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1586"/>
      <source>Local u0394Y</source>
      <translation>Локальний u0394Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1587"/>
      <source>Local u0394Z</source>
      <translation>Локальний u0394X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1589"/>
      <source>Local X</source>
      <translation>Локальна X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1590"/>
      <source>Local Y</source>
      <translation>Локальна Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1591"/>
      <source>Local Z</source>
      <translation>Локальна Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1593"/>
      <source>Global u0394X</source>
      <translation>Глобальний u0394X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1594"/>
      <source>Global u0394Y</source>
      <translation>Глобальний u0394Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1595"/>
      <source>Global u0394Z</source>
      <translation>Глобальний u0394Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1597"/>
      <source>Global X</source>
      <translation>Глобальна X</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1598"/>
      <source>Global Y</source>
      <translation>Глобальна Y</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1599"/>
      <source>Global Z</source>
      <translation>Глобальна Z</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1846"/>
      <source>Invalid Size value. Using 200.0.</source>
      <translation>Неприпустиме значення розміру. Буде використано 200.0.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1856"/>
      <source>Invalid Tracking value. Using 0.</source>
      <translation>Неприпустиме значення для відслідковування. Використовуючи 0.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1874"/>
      <source>Please enter a text string.</source>
      <translation>Введіть рядок тексту, будь ласка.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1882"/>
      <source>Select a Font file</source>
      <translation>Обрати файл шрифту</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="1924"/>
      <source>Please enter a font file.</source>
      <translation>Введіть файл шрифта, будь ласка.</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2453"/>
      <source>Autogroup:</source>
      <translation>Автогрупа:</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2819"/>
      <source>Faces</source>
      <translation>Поверхні</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2820"/>
      <source>Remove</source>
      <translation>Видалити</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2821"/>
      <source>Add</source>
      <translation>Додати</translation>
    </message>
    <message>
      <location filename="../../DraftGui.py" line="2824"/>
      <source>Facebinder elements</source>
      <translation>Елементи поверхонь</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_base.py" line="430"/>
      <source>Please load the Draft Workbench to enable editing this object</source>
      <translation>Будь ласка, завантажте робочий стіл Draft щоб увімкнути редагування цього об'єкту</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="370"/>
      <source>Activate this layer</source>
      <translation>Активувати цей шар</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="378"/>
      <source>Select layer contents</source>
      <translation>Оберіть вміст шару</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="418"/>
      <location filename="../../draftviewproviders/view_layer.py" line="437"/>
      <source>Merge layer duplicates</source>
      <translation>Обʼєднати дублікати шарів</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="425"/>
      <location filename="../../draftviewproviders/view_layer.py" line="491"/>
      <source>Add new layer</source>
      <translation>Додати новий шар</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="475"/>
      <source>Relabeling layer:</source>
      <translation>Перемаркування шару:</translation>
    </message>
    <message>
      <location filename="../../draftviewproviders/view_layer.py" line="480"/>
      <source>Merging layer:</source>
      <translation>Обʼєднання шару:</translation>
    </message>
    <message>
      <location filename="../../WorkingPlane.py" line="748"/>
      <source>Selected Shapes must define a plane</source>
      <translation>Обрані форми повинні визначити площину</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/mirror.py" line="90"/>
      <source>No object given</source>
      <translation>Обʼєкт не задано</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/mirror.py" line="94"/>
      <source>The two points are coincident</source>
      <translation>Дві точки збігаються</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/mirror.py" line="113"/>
      <source>mirrored</source>
      <translation>відображено</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="151"/>
      <source>Object must be a closed shape</source>
      <translation>Обʼєкт повинен бути закритою формою</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="153"/>
      <source>No solid object created</source>
      <translation>Не створено суцільний об’єкт</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="277"/>
      <source>Faces must be coplanar to be refined</source>
      <translation>Сторони повинні бути копланарними для переробки</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="439"/>
      <location filename="../../draftfunctions/downgrade.py" line="237"/>
      <source>Upgrade: Unknown force method:</source>
      <translation>Оновлення: невідомий метод сили:</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="459"/>
      <source>Found groups: closing each open object inside</source>
      <translation>Знайдені групи: закриття кожного відкритого обʼєкта всередині</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="465"/>
      <source>Found meshes: turning into Part shapes</source>
      <translation>Знайдені сітки: перетворення по частини форм</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="477"/>
      <source>Found 1 solidifiable object: solidifying it</source>
      <translation>Знайдено 1 цільовий обʼєкт: зміцнити його</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="482"/>
      <source>Found 2 objects: fusing them</source>
      <translation>Знайдені 2 об’єкти: обʼєднуючи їх</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="503"/>
      <source>Found object with several coplanar faces: refine them</source>
      <translation>Знайдений об'єкт з кількома лінійними обличчями: уточнити їх</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="514"/>
      <source>Found 1 non-parametric objects: draftifying it</source>
      <translation>Знайдений 1 непараметричний обʼєкт: конвертація його</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="531"/>
      <source>Found 1 closed sketch object: creating a face from it</source>
      <translation>Знайдено 1 закритий ескізний об'єкт: створити поверхню з нього</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="536"/>
      <source>Found closed wires: creating faces</source>
      <translation>Знайдені закриті каркаси: створюються поверхні</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="546"/>
      <source>Found several wires or edges: wiring them</source>
      <translation>Знайдено декілька каркасів або ребер: під’єднайте їх</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="553"/>
      <location filename="../../draftfunctions/upgrade.py" line="595"/>
      <source>Found several non-treatable objects: creating compound</source>
      <translation>Знайти кілька непридатних об’єктів: створення сполучення</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="558"/>
      <source>trying: closing it</source>
      <translation>намагається: закрити</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="560"/>
      <source>Found 1 open wire: closing it</source>
      <translation>Знайдений 1 відкритий каркас: закривається</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="580"/>
      <source>Found 1 object: draftifying it</source>
      <translation>Знайдено 1 обʼєкт: конвертувати у креслення</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="585"/>
      <source>Found points: creating compound</source>
      <translation>Знайдені точки: створення сполучення</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/upgrade.py" line="598"/>
      <source>Unable to upgrade these objects.</source>
      <translation>Оновити ці обʼєкти не вдалося.</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="245"/>
      <source>Found 1 block: exploding it</source>
      <translation>Знайдений 1 блок: його використання</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="252"/>
      <source>Found 1 multi-solids compound: exploding it</source>
      <translation>Знайдений 1 багатотверді сполучення: використовуйте його</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="266"/>
      <source>Found 1 parametric object: breaking its dependencies</source>
      <translation>Знайдено 1 параметричний обʼєкт: розірвання залежностей</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="274"/>
      <source>Found 2 objects: subtracting them</source>
      <translation>Знайдено 2 обʼєкти: відніміть їх</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="281"/>
      <source>Found several faces: splitting them</source>
      <translation>Знайдено декілька поверхонь: розділіть їх</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="291"/>
      <source>Found several objects: subtracting them from the first one</source>
      <translation>Знайти кілька об’єктів: віднімаючи їх від першого</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="296"/>
      <source>Found 1 face: extracting its wires</source>
      <translation>Знайдена 1 поверхня: вирізати її сітку</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="302"/>
      <source>Found only wires: extracting their edges</source>
      <translation>Знайти тільки сітку: вирізати їх ребра</translation>
    </message>
    <message>
      <location filename="../../draftfunctions/downgrade.py" line="306"/>
      <source>No more downgrade possible</source>
      <translation>Більше неможливе зниження рівня</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="215"/>
      <location filename="../../importDWG.py" line="297"/>
      <source>LibreDWG error</source>
      <translation>Помилка LibreDWG</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="225"/>
      <location filename="../../importDWG.py" line="307"/>
      <source>Converting:</source>
      <translation>Перетворення:</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="231"/>
      <source>Conversion successful</source>
      <translation>Перетворення виконано успішно</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="239"/>
      <source>Error during DWG conversion. Try moving the DWG file to a directory path without spaces and non-english characters, or try saving to a lower DWG version.</source>
      <translation>Помилка при перетворенні DWG. Спробуйте перемістити DWG файл в шлях до каталогу без пробілів і неанглійських символів, або спробуйте зберегти до більш низької версії DWG.</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="244"/>
      <location filename="../../importDWG.py" line="315"/>
      <source>ODA File Converter not found</source>
      <translation>Конвертер файлу ODA не знайдено</translation>
    </message>
    <message>
      <location filename="../../importDWG.py" line="258"/>
      <location filename="../../importDWG.py" line="326"/>
      <source>QCAD error</source>
      <translation>Помилка QCAD</translation>
    </message>
  </context>
  <context>
    <name>importOCA</name>
    <message>
      <location filename="../../importOCA.py" line="358"/>
      <source>OCA error: couldn&apos;t determine character encoding</source>
      <translation type="unfinished">OCA error: couldn&apos;t determine character encoding</translation>
    </message>
    <message>
      <location filename="../../importOCA.py" line="442"/>
      <source>OCA: found no data to export</source>
      <translation>OCA: не знайдено даних для експорту</translation>
    </message>
    <message>
      <location filename="../../importOCA.py" line="486"/>
      <source>successfully exported</source>
      <translation>успішно експортовано</translation>
    </message>
  </context>
</TS>
