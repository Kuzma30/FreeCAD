<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
  <context>
    <name>CmdDrawingAnnotation</name>
    <message>
      <location filename="../../Command.cpp" line="478"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="479"/>
      <source>&amp;Annotation</source>
      <translation type="unfinished">&amp;Annotation</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="480"/>
      <location filename="../../Command.cpp" line="482"/>
      <source>Inserts an Annotation view in the active drawing</source>
      <translation type="unfinished">Inserts an Annotation view in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingClip</name>
    <message>
      <location filename="../../Command.cpp" line="526"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="527"/>
      <source>&amp;Clip</source>
      <translation type="unfinished">&amp;Clip</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="528"/>
      <location filename="../../Command.cpp" line="530"/>
      <source>Inserts a clip group in the active drawing</source>
      <translation type="unfinished">Inserts a clip group in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingDraftView</name>
    <message>
      <location filename="../../Command.cpp" line="717"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="718"/>
      <source>&amp;Draft View</source>
      <translation type="unfinished">&amp;Draft View</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="719"/>
      <location filename="../../Command.cpp" line="721"/>
      <source>Inserts a Draft view of the selected object(s) in the active drawing</source>
      <translation type="unfinished">Inserts a Draft view of the selected object(s) in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingExportPage</name>
    <message>
      <location filename="../../Command.cpp" line="628"/>
      <source>File</source>
      <translation type="unfinished">File</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="629"/>
      <source>&amp;Export page...</source>
      <translation type="unfinished">&amp;Export page...</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="630"/>
      <location filename="../../Command.cpp" line="632"/>
      <source>Export a page to an SVG file</source>
      <translation type="unfinished">Export a page to an SVG file</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingNewA3Landscape</name>
    <message>
      <location filename="../../Command.cpp" line="272"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="273"/>
      <location filename="../../Command.cpp" line="274"/>
      <source>Insert new A3 landscape drawing</source>
      <translation type="unfinished">Insert new A3 landscape drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingNewPage</name>
    <message>
      <location filename="../../Command.cpp" line="94"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="95"/>
      <location filename="../../Command.cpp" line="96"/>
      <source>Insert new drawing</source>
      <translation type="unfinished">Insert new drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingNewView</name>
    <message>
      <location filename="../../Command.cpp" line="311"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="312"/>
      <source>Insert view in drawing</source>
      <translation type="unfinished">Insert view in drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="313"/>
      <source>Insert a new View of a Part in the active drawing</source>
      <translation type="unfinished">Insert a new View of a Part in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingOpen</name>
    <message>
      <location filename="../../Command.cpp" line="61"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="62"/>
      <source>Open SVG...</source>
      <translation type="unfinished">Open SVG...</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="63"/>
      <source>Open a scalable vector graphic</source>
      <translation type="unfinished">Open a scalable vector graphic</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingOpenBrowserView</name>
    <message>
      <location filename="../../Command.cpp" line="440"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="441"/>
      <source>Open &amp;browser view</source>
      <translation type="unfinished">Open &amp;browser view</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="442"/>
      <location filename="../../Command.cpp" line="444"/>
      <source>Opens the selected page in a browser view</source>
      <translation type="unfinished">Opens the selected page in a browser view</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingOrthoViews</name>
    <message>
      <location filename="../../Command.cpp" line="390"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="391"/>
      <source>Insert orthographic views</source>
      <translation type="unfinished">Insert orthographic views</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="392"/>
      <source>Insert an orthographic projection of a part in the active drawing</source>
      <translation type="unfinished">Insert an orthographic projection of a part in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingProjectShape</name>
    <message>
      <location filename="../../Command.cpp" line="681"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="682"/>
      <source>Project shape...</source>
      <translation type="unfinished">Project shape...</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="683"/>
      <location filename="../../Command.cpp" line="684"/>
      <source>Project shape onto a user-defined plane</source>
      <translation type="unfinished">Project shape onto a user-defined plane</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingSpreadsheetView</name>
    <message>
      <location filename="../../Command.cpp" line="748"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="749"/>
      <source>&amp;Spreadsheet View</source>
      <translation type="unfinished">&amp;Spreadsheet View</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="750"/>
      <location filename="../../Command.cpp" line="752"/>
      <source>Inserts a view of a selected spreadsheet in the active drawing</source>
      <translation type="unfinished">Inserts a view of a selected spreadsheet in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>CmdDrawingSymbol</name>
    <message>
      <location filename="../../Command.cpp" line="571"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="572"/>
      <source>&amp;Symbol</source>
      <translation type="unfinished">&amp;Symbol</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="573"/>
      <location filename="../../Command.cpp" line="575"/>
      <source>Inserts a symbol from a svg file in the active drawing</source>
      <translation type="unfinished">Inserts a symbol from a svg file in the active drawing</translation>
    </message>
  </context>
  <context>
    <name>DrawingGui::DrawingView</name>
    <message>
      <location filename="../../DrawingView.cpp" line="217"/>
      <source>&amp;Background</source>
      <translation type="unfinished">&amp;Background</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="223"/>
      <source>&amp;Outline</source>
      <translation type="unfinished">&amp;Outline</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="229"/>
      <source>&amp;Native</source>
      <translation type="unfinished">&amp;Native</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="233"/>
      <source>&amp;OpenGL</source>
      <translation type="unfinished">&amp;OpenGL</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="236"/>
      <source>&amp;Image</source>
      <translation type="unfinished">&amp;Image</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="240"/>
      <source>&amp;High Quality Antialiasing</source>
      <translation type="unfinished">&amp;High Quality Antialiasing</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="277"/>
      <source>Open SVG File</source>
      <translation type="unfinished">Open SVG File</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="278"/>
      <source>Could not open file &apos;%1&apos;.</source>
      <translation type="unfinished">Could not open file &apos;%1&apos;.</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="365"/>
      <source>&amp;Renderer</source>
      <translation type="unfinished">&amp;Renderer</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="470"/>
      <source>Export PDF</source>
      <translation type="unfinished">Export PDF</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="471"/>
      <source>PDF file</source>
      <translation type="unfinished">PDF file</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="487"/>
      <source>Page sizes</source>
      <translation type="unfinished">Page sizes</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="488"/>
      <source>A0</source>
      <translation type="unfinished">A0</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="490"/>
      <source>A1</source>
      <translation type="unfinished">A1</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="492"/>
      <source>A2</source>
      <translation type="unfinished">A2</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="494"/>
      <source>A3</source>
      <translation type="unfinished">A3</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="496"/>
      <source>A4</source>
      <translation type="unfinished">A4</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="498"/>
      <source>A5</source>
      <translation type="unfinished">A5</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="581"/>
      <source>Different orientation</source>
      <translation type="unfinished">Different orientation</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="582"/>
      <source>The printer uses a different orientation than the drawing.
Do you want to continue?</source>
      <translation type="unfinished">The printer uses a different orientation than the drawing.
Do you want to continue?</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="589"/>
      <location filename="../../DrawingView.cpp" line="597"/>
      <source>Different paper size</source>
      <translation type="unfinished">Different paper size</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="590"/>
      <location filename="../../DrawingView.cpp" line="598"/>
      <source>The printer uses a different paper size than the drawing.
Do you want to continue?</source>
      <translation type="unfinished">The printer uses a different paper size than the drawing.
Do you want to continue?</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="609"/>
      <source>Opening file failed</source>
      <translation type="unfinished">Opening file failed</translation>
    </message>
    <message>
      <location filename="../../DrawingView.cpp" line="610"/>
      <source>Can&apos;t open file &apos;%1&apos; for writing.</source>
      <translation type="unfinished">Can&apos;t open file &apos;%1&apos; for writing.</translation>
    </message>
  </context>
  <context>
    <name>DrawingGui::TaskOrthoViews</name>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="26"/>
      <source>Orthographic Projection</source>
      <translation type="unfinished">Orthographic Projection</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="34"/>
      <source>Projection</source>
      <translation type="unfinished">Projection</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="45"/>
      <source>Third Angle</source>
      <translation type="unfinished">Third Angle</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="50"/>
      <source>First Angle</source>
      <translation type="unfinished">First Angle</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="62"/>
      <source>View from:</source>
      <translation type="unfinished">View from:</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="76"/>
      <location filename="../../TaskOrthoViews.ui" line="747"/>
      <source>X +ve</source>
      <translation type="unfinished">X +ve</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="81"/>
      <location filename="../../TaskOrthoViews.ui" line="752"/>
      <location filename="../../TaskOrthoViews.ui" line="791"/>
      <source>Y +ve</source>
      <translation type="unfinished">Y +ve</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="86"/>
      <location filename="../../TaskOrthoViews.ui" line="757"/>
      <location filename="../../TaskOrthoViews.ui" line="796"/>
      <source>Z +ve</source>
      <translation type="unfinished">Z +ve</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="91"/>
      <location filename="../../TaskOrthoViews.ui" line="762"/>
      <source>X -ve</source>
      <translation type="unfinished">X -ve</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="96"/>
      <location filename="../../TaskOrthoViews.ui" line="767"/>
      <location filename="../../TaskOrthoViews.ui" line="801"/>
      <source>Y -ve</source>
      <translation type="unfinished">Y -ve</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="101"/>
      <location filename="../../TaskOrthoViews.ui" line="772"/>
      <location filename="../../TaskOrthoViews.ui" line="806"/>
      <source>Z -ve</source>
      <translation type="unfinished">Z -ve</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="109"/>
      <source>Axis aligned right:</source>
      <translation type="unfinished">Axis aligned right:</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="154"/>
      <source>Secondary Views</source>
      <translation type="unfinished">Secondary Views</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="176"/>
      <location filename="../../TaskOrthoViews.ui" line="201"/>
      <location filename="../../TaskOrthoViews.ui" line="232"/>
      <location filename="../../TaskOrthoViews.ui" line="257"/>
      <location filename="../../TaskOrthoViews.ui" line="282"/>
      <location filename="../../TaskOrthoViews.ui" line="307"/>
      <location filename="../../TaskOrthoViews.ui" line="357"/>
      <location filename="../../TaskOrthoViews.ui" line="382"/>
      <location filename="../../TaskOrthoViews.ui" line="407"/>
      <location filename="../../TaskOrthoViews.ui" line="432"/>
      <location filename="../../TaskOrthoViews.ui" line="457"/>
      <location filename="../../TaskOrthoViews.ui" line="482"/>
      <source>Right click for axonometric settings</source>
      <translation type="unfinished">Right click for axonometric settings</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="329"/>
      <source>Primary view</source>
      <translation type="unfinished">Primary view</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="532"/>
      <source>General</source>
      <translation type="unfinished">General</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="541"/>
      <source>Auto scale / position</source>
      <translation type="unfinished">Auto scale / position</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="556"/>
      <source>Scale</source>
      <translation type="unfinished">Scale</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="588"/>
      <source>Top left x / y</source>
      <translation type="unfinished">Top left x / y</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="636"/>
      <source>Spacing dx / dy</source>
      <translation type="unfinished">Spacing dx / dy</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="681"/>
      <source>Show hidden lines</source>
      <translation type="unfinished">Show hidden lines</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="688"/>
      <source>Show smooth lines</source>
      <translation type="unfinished">Show smooth lines</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="699"/>
      <source>Axonometric</source>
      <translation type="unfinished">Axonometric</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="707"/>
      <source>View projection</source>
      <translation type="unfinished">View projection</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="718"/>
      <source>Isometric</source>
      <translation type="unfinished">Isometric</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="723"/>
      <source>Dimetric</source>
      <translation type="unfinished">Dimetric</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="728"/>
      <source>Trimetric</source>
      <translation type="unfinished">Trimetric</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="736"/>
      <source> Axis aligned up</source>
      <translation type="unfinished"> Axis aligned up</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="780"/>
      <source>Axis out and right</source>
      <translation type="unfinished">Axis out and right</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="814"/>
      <source>Vertical tilt</source>
      <translation type="unfinished">Vertical tilt</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="821"/>
      <location filename="../../TaskOrthoViews.ui" line="858"/>
      <source>Flip</source>
      <translation type="unfinished">Flip</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="834"/>
      <source> Scale</source>
      <translation type="unfinished"> Scale</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.ui" line="848"/>
      <source> Trimetric</source>
      <translation type="unfinished"> Trimetric</translation>
    </message>
  </context>
  <context>
    <name>DrawingGui::TaskProjection</name>
    <message>
      <location filename="../../TaskDialog.cpp" line="51"/>
      <source>Visible sharp edges</source>
      <translation type="unfinished">Visible sharp edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="52"/>
      <source>Visible smooth edges</source>
      <translation type="unfinished">Visible smooth edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="53"/>
      <source>Visible sewn edges</source>
      <translation type="unfinished">Visible sewn edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="54"/>
      <source>Visible outline edges</source>
      <translation type="unfinished">Visible outline edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="55"/>
      <source>Visible isoparameters</source>
      <translation type="unfinished">Visible isoparameters</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="56"/>
      <source>Hidden sharp edges</source>
      <translation type="unfinished">Hidden sharp edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="57"/>
      <source>Hidden smooth edges</source>
      <translation type="unfinished">Hidden smooth edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="58"/>
      <source>Hidden sewn edges</source>
      <translation type="unfinished">Hidden sewn edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="59"/>
      <source>Hidden outline edges</source>
      <translation type="unfinished">Hidden outline edges</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="60"/>
      <source>Hidden isoparameters</source>
      <translation type="unfinished">Hidden isoparameters</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="77"/>
      <source>Project shapes</source>
      <translation type="unfinished">Project shapes</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="91"/>
      <source>No active document</source>
      <translation type="unfinished">No active document</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="92"/>
      <source>There is currently no active document to complete the operation</source>
      <translation type="unfinished">There is currently no active document to complete the operation</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="97"/>
      <source>No active view</source>
      <translation type="unfinished">No active view</translation>
    </message>
    <message>
      <location filename="../../TaskDialog.cpp" line="98"/>
      <source>There is currently no active view to complete the operation</source>
      <translation type="unfinished">There is currently no active view to complete the operation</translation>
    </message>
  </context>
  <context>
    <name>Drawing_NewPage</name>
    <message>
      <location filename="../../Command.cpp" line="220"/>
      <source>Landscape</source>
      <translation type="unfinished">Landscape</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="222"/>
      <source>Portrait</source>
      <translation type="unfinished">Portrait</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="226"/>
      <source>%1%2 %3</source>
      <translation type="unfinished">%1%2 %3</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="231"/>
      <source>Insert new %1%2 %3 drawing</source>
      <translation type="unfinished">Insert new %1%2 %3 drawing</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="238"/>
      <source>%1%2 %3 (%4)</source>
      <translation type="unfinished">%1%2 %3 (%4)</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="244"/>
      <source>Insert new %1%2 %3 (%4) drawing</source>
      <translation type="unfinished">Insert new %1%2 %3 (%4) drawing</translation>
    </message>
  </context>
  <context>
    <name>QObject</name>
    <message>
      <location filename="../../Command.cpp" line="73"/>
      <location filename="../../Command.cpp" line="592"/>
      <source>Choose an SVG file to open</source>
      <translation type="unfinished">Choose an SVG file to open</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="74"/>
      <location filename="../../Command.cpp" line="593"/>
      <location filename="../../Command.cpp" line="647"/>
      <source>Scalable Vector Graphic</source>
      <translation type="unfinished">Scalable Vector Graphic</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="324"/>
      <location filename="../../Command.cpp" line="403"/>
      <location filename="../../Command.cpp" line="453"/>
      <location filename="../../Command.cpp" line="641"/>
      <location filename="../../Command.cpp" line="761"/>
      <source>Wrong selection</source>
      <translation type="unfinished">Wrong selection</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="325"/>
      <source>Select a Part object.</source>
      <translation type="unfinished">Select a Part object.</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="333"/>
      <location filename="../../Command.cpp" line="412"/>
      <location filename="../../Command.cpp" line="493"/>
      <location filename="../../Command.cpp" line="541"/>
      <location filename="../../Command.cpp" line="586"/>
      <location filename="../../Command.cpp" line="767"/>
      <source>No page found</source>
      <translation type="unfinished">No page found</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="334"/>
      <location filename="../../Command.cpp" line="413"/>
      <location filename="../../Command.cpp" line="494"/>
      <location filename="../../Command.cpp" line="542"/>
      <location filename="../../Command.cpp" line="587"/>
      <location filename="../../Command.cpp" line="768"/>
      <source>Create a page first.</source>
      <translation type="unfinished">Create a page first.</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="404"/>
      <source>Select exactly one Part object.</source>
      <translation type="unfinished">Select exactly one Part object.</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="454"/>
      <location filename="../../Command.cpp" line="642"/>
      <source>Select one Page object.</source>
      <translation type="unfinished">Select one Page object.</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="648"/>
      <source>All Files</source>
      <translation type="unfinished">All Files</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="650"/>
      <source>Export page</source>
      <translation type="unfinished">Export page</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="762"/>
      <source>Select exactly one Spreadsheet object.</source>
      <translation type="unfinished">Select exactly one Spreadsheet object.</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.cpp" line="50"/>
      <location filename="../../TaskOrthoViews.cpp" line="978"/>
      <source>Make axonometric...</source>
      <translation type="unfinished">Make axonometric...</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.cpp" line="51"/>
      <location filename="../../TaskOrthoViews.cpp" line="979"/>
      <source>Edit axonometric settings...</source>
      <translation type="unfinished">Edit axonometric settings...</translation>
    </message>
    <message>
      <location filename="../../TaskOrthoViews.cpp" line="52"/>
      <location filename="../../TaskOrthoViews.cpp" line="980"/>
      <source>Make orthographic</source>
      <translation type="unfinished">Make orthographic</translation>
    </message>
    <message>
      <location filename="../../ViewProviderPage.cpp" line="152"/>
      <source>Show drawing</source>
      <translation type="unfinished">Show drawing</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../Workbench.cpp" line="37"/>
      <source>Drawing</source>
      <translation type="unfinished">Drawing</translation>
    </message>
  </context>
</TS>
