<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
  <context>
    <name>CmdCreateImagePlane</name>
    <message>
      <location filename="../../Command.cpp" line="100"/>
      <source>Image</source>
      <translation type="unfinished">Image</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="101"/>
      <source>Create image plane...</source>
      <translation type="unfinished">Create image plane...</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="102"/>
      <source>Create a planar image in the 3D space</source>
      <translation type="unfinished">Create a planar image in the 3D space</translation>
    </message>
  </context>
  <context>
    <name>CmdImageOpen</name>
    <message>
      <location filename="../../Command.cpp" line="55"/>
      <source>Image</source>
      <translation type="unfinished">Image</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="56"/>
      <source>Open...</source>
      <translation type="unfinished">Open...</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="57"/>
      <source>Open image view</source>
      <translation type="unfinished">Open image view</translation>
    </message>
  </context>
  <context>
    <name>CmdImageScaling</name>
    <message>
      <location filename="../../Command.cpp" line="175"/>
      <source>Image</source>
      <translation type="unfinished">Image</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="176"/>
      <source>Scale...</source>
      <translation type="unfinished">Scale...</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="177"/>
      <source>Image Scaling</source>
      <translation type="unfinished">Image Scaling</translation>
    </message>
  </context>
  <context>
    <name>Command</name>
    <message>
      <location filename="../../Command.cpp" line="150"/>
      <source>Create ImagePlane</source>
      <translation type="unfinished">Create ImagePlane</translation>
    </message>
  </context>
  <context>
    <name>Dialog</name>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="168"/>
      <source>Scale image plane</source>
      <translation type="unfinished">Scale image plane</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="169"/>
      <source>Distance [mm]</source>
      <translation type="unfinished">Distance [mm]</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="170"/>
      <source>Select first point</source>
      <translation type="unfinished">Select first point</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="199"/>
      <source>&lt;font color=&apos;red&apos;&gt;Enter distance&lt;/font&gt;</source>
      <translation type="unfinished">&lt;font color=&apos;red&apos;&gt;Enter distance&lt;/font&gt;</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="206"/>
      <source>&lt;font color=&apos;red&apos;&gt;Select ImagePlane&lt;/font&gt;</source>
      <translation type="unfinished">&lt;font color=&apos;red&apos;&gt;Select ImagePlane&lt;/font&gt;</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="235"/>
      <source>Select second point</source>
      <translation type="unfinished">Select second point</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="252"/>
      <source>Select Image Plane and type distance</source>
      <translation type="unfinished">Select Image Plane and type distance</translation>
    </message>
  </context>
  <context>
    <name>ImageGui::GLImageBox</name>
    <message>
      <location filename="../../OpenGLImageBox.cpp" line="392"/>
      <source>Image pixel format</source>
      <translation type="unfinished">Image pixel format</translation>
    </message>
    <message>
      <location filename="../../OpenGLImageBox.cpp" line="393"/>
      <source>Undefined type of colour space for image viewing</source>
      <translation type="unfinished">Undefined type of colour space for image viewing</translation>
    </message>
  </context>
  <context>
    <name>ImageGui::ImageOrientationDialog</name>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="14"/>
      <source>Choose orientation</source>
      <translation type="unfinished">Choose orientation</translation>
    </message>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="20"/>
      <source>Image plane</source>
      <translation type="unfinished">Image plane</translation>
    </message>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="26"/>
      <source>XY-Plane</source>
      <translation type="unfinished">XY-Plane</translation>
    </message>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="36"/>
      <source>XZ-Plane</source>
      <translation type="unfinished">XZ-Plane</translation>
    </message>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="43"/>
      <source>YZ-Plane</source>
      <translation type="unfinished">YZ-Plane</translation>
    </message>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="72"/>
      <source>Reverse direction</source>
      <translation type="unfinished">Reverse direction</translation>
    </message>
    <message>
      <location filename="../../ImageOrientationDialog.ui" line="81"/>
      <source>Offset:</source>
      <translation type="unfinished">Offset:</translation>
    </message>
  </context>
  <context>
    <name>ImageGui::ImageView</name>
    <message>
      <location filename="../../ImageView.cpp" line="107"/>
      <source>&amp;Fit image</source>
      <translation type="unfinished">&amp;Fit image</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="109"/>
      <source>Stretch the image to fit the view</source>
      <translation type="unfinished">Stretch the image to fit the view</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="113"/>
      <source>&amp;1:1 scale</source>
      <translation type="unfinished">&amp;1:1 scale</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="115"/>
      <source>Display the image at a 1:1 scale</source>
      <translation type="unfinished">Display the image at a 1:1 scale</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="124"/>
      <source>Standard</source>
      <translation>Стандартно</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="142"/>
      <source>Ready...</source>
      <translation type="unfinished">Ready...</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="543"/>
      <source>grey</source>
      <translation type="unfinished">grey</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="544"/>
      <location filename="../../ImageView.cpp" line="547"/>
      <location filename="../../ImageView.cpp" line="557"/>
      <location filename="../../ImageView.cpp" line="562"/>
      <location filename="../../ImageView.cpp" line="572"/>
      <location filename="../../ImageView.cpp" line="577"/>
      <location filename="../../ImageView.cpp" line="588"/>
      <location filename="../../ImageView.cpp" line="593"/>
      <location filename="../../ImageView.cpp" line="604"/>
      <location filename="../../ImageView.cpp" line="609"/>
      <source>zoom</source>
      <translation type="unfinished">zoom</translation>
    </message>
    <message>
      <location filename="../../ImageView.cpp" line="547"/>
      <location filename="../../ImageView.cpp" line="557"/>
      <location filename="../../ImageView.cpp" line="572"/>
      <location filename="../../ImageView.cpp" line="588"/>
      <location filename="../../ImageView.cpp" line="604"/>
      <source>outside image</source>
      <translation type="unfinished">outside image</translation>
    </message>
  </context>
  <context>
    <name>Image_Scaling</name>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="69"/>
      <source>Scale image plane</source>
      <translation type="unfinished">Scale image plane</translation>
    </message>
    <message>
      <location filename="../../../ImageTools/_CommandImageScaling.py" line="74"/>
      <source>Scales an image plane by defining a distance between two points</source>
      <translation type="unfinished">Scales an image plane by defining a distance between two points</translation>
    </message>
  </context>
  <context>
    <name>QObject</name>
    <message>
      <location filename="../../Command.cpp" line="70"/>
      <location filename="../../Command.cpp" line="114"/>
      <source>Images</source>
      <translation type="unfinished">Images</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="75"/>
      <location filename="../../Command.cpp" line="119"/>
      <source>All files</source>
      <translation type="unfinished">All files</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="77"/>
      <location filename="../../Command.cpp" line="121"/>
      <source>Choose an image file to open</source>
      <translation type="unfinished">Choose an image file to open</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="127"/>
      <source>Error opening image</source>
      <translation type="unfinished">Error opening image</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="128"/>
      <source>Could not load the chosen image</source>
      <translation type="unfinished">Could not load the chosen image</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../Workbench.cpp" line="32"/>
      <source>Image</source>
      <translation type="unfinished">Image</translation>
    </message>
  </context>
</TS>
