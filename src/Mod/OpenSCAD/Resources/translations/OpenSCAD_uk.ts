<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk" sourcelanguage="en">
  <context>
    <name>Gui::Dialog::DlgSettingsOpenSCAD</name>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="14"/>
      <source>General settings</source>
      <translation>Загальні параметри</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="35"/>
      <source>General OpenSCAD Settings</source>
      <translation>Загальні налаштування OpenSCAD</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="43"/>
      <source>OpenSCAD executable</source>
      <translation>Виконуваний файл OpenSCAD</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="56"/>
      <source>The path to the OpenSCAD executable</source>
      <translation>Шлях до виконуваного файлу OpenSCAD</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="74"/>
      <source>OpenSCAD import</source>
      <translation>OpenSCAD імпорт</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="82"/>
      <source>Print debug information in the Console</source>
      <translation>Вивести налагоджувальну інформацію в консолі</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="99"/>
      <source>If this is checked, Features will claim their children in the tree view</source>
      <translation>Якщо встановити, то властивості будуть застосовані до дочірніх обʼєктів у дереві перегляду</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="102"/>
      <source>Use ViewProvider in Tree View</source>
      <translation>Використовувати ViewProvider у дереві перегляду</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="119"/>
      <source>If this is checked, Multmatrix Object will be Parametric</source>
      <translation>Якщо буде позначено цей пункт, то обʼєкт Мультиматриця буде параметричним</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="122"/>
      <source>Use Multmatrix Feature</source>
      <translation>Використовувати функцію Мультиматриці</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="139"/>
      <location filename="../ui/openscadprefs-base.ui" line="162"/>
      <source>The maximum number of faces of a polygon, prism or frustum. If fn is greater than this value the object is considered to be a circular. Set to 0 for no limit</source>
      <translation>Максимальна кількість граней багатокутника, призми або зрізаний конус. Якщо fn більше, ніж це значення, то обʼєкт вважається круговим. Значення 0 для без обмежень</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="142"/>
      <source>Maximum number of faces for polygons (fn)</source>
      <translation>Максимальна кількість граней многокутника (fn)</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="182"/>
      <source>Send to OpenSCAD via:</source>
      <translation type="unfinished">Send to OpenSCAD via:</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="195"/>
      <source>The transfer mechanism for getting data to and from OpenSCAD</source>
      <translation type="unfinished">The transfer mechanism for getting data to and from OpenSCAD</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="205"/>
      <source>Standard temp directory</source>
      <translation type="unfinished">Standard temp directory</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="210"/>
      <source>User-specified directory</source>
      <translation type="unfinished">User-specified directory</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="215"/>
      <source>stdout pipe (requires OpenSCAD &gt;= 2021.1)</source>
      <translation type="unfinished">stdout pipe (requires OpenSCAD &gt;= 2021.1)</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="227"/>
      <source>Transfer directory</source>
      <translation type="unfinished">Transfer directory</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="240"/>
      <source>The path to the directory for transferring files to and from OpenSCAD</source>
      <translation type="unfinished">The path to the directory for transferring files to and from OpenSCAD</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="258"/>
      <source>OpenSCAD export</source>
      <translation>OpenSCAD експорт</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="266"/>
      <source>maximum fragment size</source>
      <translation>максимальний розмір фрагменту</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="286"/>
      <location filename="../ui/openscadprefs-base.ui" line="296"/>
      <source>Minimum angle for a fragment</source>
      <translation>Мінімальний кут для фрагменту</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="289"/>
      <source>angular (fa)</source>
      <translation>кутові (фа)</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="299"/>
      <source>°</source>
      <translation>°</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="328"/>
      <location filename="../ui/openscadprefs-base.ui" line="353"/>
      <source>Minimum size of a fragment</source>
      <translation>Мінімальний розмір фрагменту</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="331"/>
      <source>size (fs)</source>
      <translation>Розмір (fs)</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="359"/>
      <source>mm</source>
      <translation>мм</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="385"/>
      <source>convexity</source>
      <translation>опуклість</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="422"/>
      <source>Mesh fallback</source>
      <translation>Скасувати зміни сітки</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="442"/>
      <location filename="../ui/openscadprefs-base.ui" line="459"/>
      <source>Deflection</source>
      <translation>Відхилення</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="445"/>
      <source>deflection</source>
      <translation>відхилення</translation>
    </message>
    <message>
      <location filename="../ui/openscadprefs-base.ui" line="452"/>
      <source>Triangulation settings</source>
      <translation>Налаштування тріангуляції</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD</name>
    <message>
      <location filename="../../InitGui.py" line="131"/>
      <source>It looks like you may be using a Snap version of OpenSCAD.</source>
      <translation type="unfinished">It looks like you may be using a Snap version of OpenSCAD.</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="136"/>
      <location filename="../../InitGui.py" line="149"/>
      <source>If OpenSCAD execution fails to load the temporary file, use FreeCAD&apos;s OpenSCAD Workbench Preferences to change the transfer mechanism.</source>
      <translation type="unfinished">If OpenSCAD execution fails to load the temporary file, use FreeCAD&apos;s OpenSCAD Workbench Preferences to change the transfer mechanism.</translation>
    </message>
    <message>
      <location filename="../../InitGui.py" line="144"/>
      <source>It looks like you may be using a sandboxed version of FreeCAD.</source>
      <translation type="unfinished">It looks like you may be using a sandboxed version of FreeCAD.</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="128"/>
      <source>Unable to explode %s</source>
      <translation>Неможливо розбити %s</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="196"/>
      <source>Convert Edges to Faces</source>
      <translation>Перетворити ребра в грані</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="445"/>
      <source>Please select 3 objects first</source>
      <translation>Будь ласка, оберіть спочатку 3 обʼєкти</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="497"/>
      <location filename="../../OpenSCADCommands.py" line="520"/>
      <source>Add</source>
      <translation>Додати</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="498"/>
      <location filename="../../OpenSCADCommands.py" line="524"/>
      <source>Clear</source>
      <translation>Очистити</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="499"/>
      <location filename="../../OpenSCADCommands.py" line="521"/>
      <source>Load</source>
      <translation>Завантажити</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="500"/>
      <location filename="../../OpenSCADCommands.py" line="522"/>
      <source>Save</source>
      <translation>Зберегти</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="501"/>
      <location filename="../../OpenSCADCommands.py" line="523"/>
      <source>Refresh</source>
      <translation>Оновити</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="502"/>
      <location filename="../../OpenSCADCommands.py" line="525"/>
      <source>as Mesh</source>
      <translation>як сітку</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="515"/>
      <location filename="../../OpenSCADCommands.py" line="526"/>
      <source>Add OpenSCAD Element</source>
      <translation>Додати елемент OpenSCAD</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="619"/>
      <location filename="../../OpenSCADCommands.py" line="648"/>
      <source>Perform</source>
      <translation>Виконати</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="645"/>
      <location filename="../../OpenSCADCommands.py" line="649"/>
      <source>Mesh Boolean</source>
      <translation>Логічна сітка</translation>
    </message>
    <message>
      <location filename="../../importCSG.py" line="604"/>
      <location filename="../../importCSG.py" line="1625"/>
      <source>Unsupported Function</source>
      <translation>Непідтримувані функції</translation>
    </message>
    <message>
      <location filename="../../importCSG.py" line="605"/>
      <location filename="../../importCSG.py" line="1626"/>
      <source>Press OK</source>
      <translation>Натисніть OK</translation>
    </message>
    <message>
      <location filename="../../OpenSCADUtils.py" line="1221"/>
      <source>OpenSCAD file contains both 2D and 3D shapes. That is not supported in this importer, all shapes must have the same dimensionality.</source>
      <translation type="unfinished">OpenSCAD file contains both 2D and 3D shapes. That is not supported in this importer, all shapes must have the same dimensionality.</translation>
    </message>
    <message>
      <location filename="../../OpenSCADUtils.py" line="1237"/>
      <source>Error all shapes must be either 2D or both must be 3D</source>
      <translation>Помилка: усі обʼєкти повинні бути або дво-, або тривимірними</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_AddOpenSCADElement</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="705"/>
      <source>Add OpenSCAD Element...</source>
      <translation>Додати елемент OpenSCAD...</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="709"/>
      <source>Add an OpenSCAD element by entering OpenSCAD code and executing the OpenSCAD binary</source>
      <translation>Додати елемент OpenSCAD, ввівши код OpenSCAD і запустивши OpenSCAD</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_ColorCodeShape</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="167"/>
      <source>Color Shapes</source>
      <translation>Колір фігур</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="170"/>
      <source>Color Shapes by validity and type</source>
      <translation>Колір фігури залежно від терміну дії та типу</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_Edgestofaces</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="195"/>
      <source>Convert Edges To Faces</source>
      <translation>Перетворити ребра в грані</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_ExpandPlacements</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="415"/>
      <source>Expand Placements</source>
      <translation>Розгорнути місця розташування</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="419"/>
      <source>Expand all placements downwards the FeatureTree</source>
      <translation>Розгорнути всі місця розташування вниз на FeatureTree</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_ExplodeGroup</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="140"/>
      <source>Explode Group</source>
      <translation>Розбити групу</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="144"/>
      <source>Remove fusion, apply placement to children, and color randomly</source>
      <translation>Видалити злиття та випадково застосувати розташування та колір до дочірніх обʼєктів</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_Hull</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="756"/>
      <source>Hull</source>
      <translation>Корпус</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="757"/>
      <source>Perform Hull</source>
      <translation>Сформувати корпус</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_IncreaseToleranceFeature</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="387"/>
      <source>Increase Tolerance Feature</source>
      <translation>Збільшити точність</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="391"/>
      <source>Create Feature that allows to increase the tolerance</source>
      <translation>Створити властивість, яка дозволить збільшити точність</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_MeshBoolean</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="729"/>
      <source>Mesh Boolean...</source>
      <translation>Логічна сітка...</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="733"/>
      <source>Export objects as meshes and use OpenSCAD to perform a boolean operation</source>
      <translation>Експортувати об'єкти у вигляді сітки та задіяти OpenSCAD для виконання логічних операцій</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_Minkowski</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="782"/>
      <source>Minkowski</source>
      <translation>метрика Мінковського</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="785"/>
      <source>Perform Minkowski</source>
      <translation>Задіяти метрику Мінковського</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_MirrorMeshFeature</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="273"/>
      <source>Mirror Mesh Feature...</source>
      <translation>Функція дзеркальної сітки...</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="276"/>
      <source>Create Mirror Mesh Feature</source>
      <translation>Створити функцію дзеркальної сітки</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_RefineShapeFeature</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="221"/>
      <source>Refine Shape Feature</source>
      <translation>Уточнити властивості фігури</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="224"/>
      <source>Create Refine Shape Feature</source>
      <translation>Створити елемент, що уточнює форму</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_RemoveSubtree</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="475"/>
      <source>Remove Objects and their Children</source>
      <translation>Видалити обʼєкти і їхніх нащадків</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="479"/>
      <source>Removes the selected objects and all children that are not referenced from other objects</source>
      <translation>Видалити обраний та всі дочірні об'єкти, на які відсутні посилання з інших об'єктів</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_ReplaceObject</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="453"/>
      <source>Replace Object</source>
      <translation>Замінити обʼєкт</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="457"/>
      <source>Replace an object in the Feature Tree. Please select old, new, and parent object</source>
      <translation>Замінити об'єкт у дереві властивостей. Оберіть, будь ласка, старий, новий та породжуючий об'єкт</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_ResizeMeshFeature</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="359"/>
      <source>Resize Mesh Feature...</source>
      <translation>Змінити розмір функції сітки...</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="362"/>
      <source>Create Resize Mesh Feature</source>
      <translation>Створити функцію зміни розміру сітки</translation>
    </message>
  </context>
  <context>
    <name>OpenSCAD_ScaleMeshFeature</name>
    <message>
      <location filename="../../OpenSCADCommands.py" line="316"/>
      <source>Scale Mesh Feature...</source>
      <translation>Функція масштабування сітки...</translation>
    </message>
    <message>
      <location filename="../../OpenSCADCommands.py" line="319"/>
      <source>Create Scale Mesh Feature</source>
      <translation>Створіть функцію масштабування сітки</translation>
    </message>
  </context>
  <context>
    <name>Workbech</name>
    <message>
      <location filename="../../InitGui.py" line="157"/>
      <source>OpenSCAD Part tools</source>
      <translation>Інструменти OpenSCAD для деталі</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../InitGui.py" line="153"/>
      <source>OpenSCADTools</source>
      <translation>Інструменти OpenSCAD</translation>
    </message>
  </context>
</TS>
