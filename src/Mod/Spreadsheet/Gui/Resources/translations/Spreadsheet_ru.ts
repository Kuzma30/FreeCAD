<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
  <context>
    <name>CmdCreateSpreadsheet</name>
    <message>
      <location filename="../../Command.cpp" line="892"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="893"/>
      <source>Create spreadsheet</source>
      <translation type="unfinished">Create spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="894"/>
      <source>Create a new spreadsheet</source>
      <translation type="unfinished">Create a new spreadsheet</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetAlignBottom</name>
    <message>
      <location filename="../../Command.cpp" line="503"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="504"/>
      <source>Align bottom</source>
      <translation type="unfinished">Align bottom</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="505"/>
      <source>Bottom-align contents of selected cells</source>
      <translation type="unfinished">Bottom-align contents of selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetAlignCenter</name>
    <message>
      <location filename="../../Command.cpp" line="347"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="348"/>
      <source>Align center</source>
      <translation type="unfinished">Align center</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="349"/>
      <source>Center-align contents of selected cells</source>
      <translation type="unfinished">Center-align contents of selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetAlignLeft</name>
    <message>
      <location filename="../../Command.cpp" line="295"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="296"/>
      <source>Align left</source>
      <translation type="unfinished">Align left</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="297"/>
      <source>Left-align contents of selected cells</source>
      <translation type="unfinished">Left-align contents of selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetAlignRight</name>
    <message>
      <location filename="../../Command.cpp" line="399"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="400"/>
      <source>Align right</source>
      <translation type="unfinished">Align right</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="401"/>
      <source>Right-align contents of selected cells</source>
      <translation type="unfinished">Right-align contents of selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetAlignTop</name>
    <message>
      <location filename="../../Command.cpp" line="451"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="452"/>
      <source>Align top</source>
      <translation type="unfinished">Align top</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="453"/>
      <source>Top-align contents of selected cells</source>
      <translation type="unfinished">Top-align contents of selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetAlignVCenter</name>
    <message>
      <location filename="../../Command.cpp" line="555"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="556"/>
      <source>Vertically center-align</source>
      <translation type="unfinished">Vertically center-align</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="557"/>
      <source>Vertically center-align contents of selected cells</source>
      <translation type="unfinished">Vertically center-align contents of selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetExport</name>
    <message>
      <location filename="../../Command.cpp" line="234"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="235"/>
      <source>Export spreadsheet</source>
      <translation type="unfinished">Export spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="236"/>
      <source>Export spreadsheet to CSV file</source>
      <translation type="unfinished">Export spreadsheet to CSV file</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetImport</name>
    <message>
      <location filename="../../Command.cpp" line="184"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="185"/>
      <source>Import spreadsheet</source>
      <translation type="unfinished">Import spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="186"/>
      <source>Import CSV file into spreadsheet</source>
      <translation type="unfinished">Import CSV file into spreadsheet</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetMergeCells</name>
    <message>
      <location filename="../../Command.cpp" line="70"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="71"/>
      <source>Merge cells</source>
      <translation type="unfinished">Merge cells</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="72"/>
      <source>Merge selected cells</source>
      <translation type="unfinished">Merge selected cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetSetAlias</name>
    <message>
      <location filename="../../Command.cpp" line="828"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="829"/>
      <source>Set alias</source>
      <translation type="unfinished">Set alias</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="830"/>
      <source>Set alias for selected cell</source>
      <translation type="unfinished">Set alias for selected cell</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetSplitCell</name>
    <message>
      <location filename="../../Command.cpp" line="125"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="126"/>
      <source>Split cell</source>
      <translation type="unfinished">Split cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="127"/>
      <source>Split previously merged cells</source>
      <translation type="unfinished">Split previously merged cells</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetStyleBold</name>
    <message>
      <location filename="../../Command.cpp" line="607"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="608"/>
      <source>Bold text</source>
      <translation type="unfinished">Bold text</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="609"/>
      <source>Set text in selected cells bold</source>
      <translation type="unfinished">Set text in selected cells bold</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetStyleItalic</name>
    <message>
      <location filename="../../Command.cpp" line="681"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="682"/>
      <source>Italic text</source>
      <translation type="unfinished">Italic text</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="683"/>
      <source>Set text in selected cells italic</source>
      <translation type="unfinished">Set text in selected cells italic</translation>
    </message>
  </context>
  <context>
    <name>CmdSpreadsheetStyleUnderline</name>
    <message>
      <location filename="../../Command.cpp" line="755"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="756"/>
      <source>Underline text</source>
      <translation type="unfinished">Underline text</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="757"/>
      <source>Underline text in selected cells</source>
      <translation type="unfinished">Underline text in selected cells</translation>
    </message>
  </context>
  <context>
    <name>ColorPickerPopup</name>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="915"/>
      <source>Custom Color</source>
      <translation type="unfinished">Custom Color</translation>
    </message>
  </context>
  <context>
    <name>Command</name>
    <message>
      <location filename="../../Command.cpp" line="91"/>
      <source>Merge cells</source>
      <translation type="unfinished">Merge cells</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="146"/>
      <source>Split cell</source>
      <translation type="unfinished">Split cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="317"/>
      <source>Left-align cell</source>
      <translation type="unfinished">Left-align cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="369"/>
      <source>Center cell</source>
      <translation type="unfinished">Center cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="421"/>
      <source>Right-align cell</source>
      <translation type="unfinished">Right-align cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="473"/>
      <source>Top-align cell</source>
      <translation type="unfinished">Top-align cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="525"/>
      <source>Bottom-align cell</source>
      <translation type="unfinished">Bottom-align cell</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="577"/>
      <source>Vertically center cells</source>
      <translation type="unfinished">Vertically center cells</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="646"/>
      <source>Set bold text</source>
      <translation type="unfinished">Set bold text</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="720"/>
      <source>Set italic text</source>
      <translation type="unfinished">Set italic text</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="794"/>
      <source>Set underline text</source>
      <translation type="unfinished">Set underline text</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="905"/>
      <source>Create Spreadsheet</source>
      <translation type="unfinished">Create Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.cpp" line="228"/>
      <source>Set cell properties</source>
      <translation type="unfinished">Set cell properties</translation>
    </message>
    <message>
      <location filename="../../SheetModel.cpp" line="487"/>
      <source>Edit cell</source>
      <translation type="unfinished">Edit cell</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="295"/>
      <location filename="../../SheetTableView.cpp" line="327"/>
      <source>Insert rows</source>
      <translation type="unfinished">Insert rows</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="346"/>
      <location filename="../../SheetTableView.cpp" line="419"/>
      <source>Remove rows</source>
      <translation type="unfinished">Remove rows</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="367"/>
      <location filename="../../SheetTableView.cpp" line="400"/>
      <source>Insert columns</source>
      <translation type="unfinished">Insert columns</translation>
    </message>
    <message>
      <location filename="../../SpreadsheetView.cpp" line="167"/>
      <location filename="../../SheetTableView.cpp" line="576"/>
      <source>Clear cell(s)</source>
      <translation type="unfinished">Clear cell(s)</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="131"/>
      <source>Set foreground color</source>
      <translation type="unfinished">Set foreground color</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="158"/>
      <source>Set background color</source>
      <translation type="unfinished">Set background color</translation>
    </message>
  </context>
  <context>
    <name>DlgBindSheet</name>
    <message>
      <location filename="../../DlgBindSheet.ui" line="14"/>
      <source>Bind Spreadsheet Cells</source>
      <translation type="unfinished">Bind Spreadsheet Cells</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="20"/>
      <source>From cells:</source>
      <translation type="unfinished">From cells:</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="27"/>
      <source>Binding cell range start</source>
      <translation type="unfinished">Binding cell range start</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="34"/>
      <source>Binding cell range end
</source>
      <translation type="unfinished">Binding cell range end
</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="42"/>
      <source>To cells:</source>
      <translation type="unfinished">To cells:</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="49"/>
      <source>Starting cell address to bind to. Type &apos;=&apos; if you want to use expression.
The expression must evaluates to a string of some cell address.</source>
      <translation type="unfinished">Starting cell address to bind to. Type &apos;=&apos; if you want to use expression.
The expression must evaluates to a string of some cell address.</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="57"/>
      <source>Ending cell address to bind to. Type &apos;=&apos; if you want to use expression.
The expression must evaluates to a string of some cell address.</source>
      <translation type="unfinished">Ending cell address to bind to. Type &apos;=&apos; if you want to use expression.
The expression must evaluates to a string of some cell address.</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="65"/>
      <source>Sheet:</source>
      <translation type="unfinished">Sheet:</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="72"/>
      <source>Select which spread sheet to bind to.</source>
      <translation type="unfinished">Select which spread sheet to bind to.</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="79"/>
      <source>Use hidden reference not avoid creating a depdenecy with the referenced object. Use with caution!</source>
      <translation type="unfinished">Use hidden reference not avoid creating a depdenecy with the referenced object. Use with caution!</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="82"/>
      <source>Use hidden reference</source>
      <translation type="unfinished">Use hidden reference</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="91"/>
      <source>Unbind</source>
      <translation type="unfinished">Unbind</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="98"/>
      <source>Cancel</source>
      <translation type="unfinished">Cancel</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.ui" line="105"/>
      <source>OK</source>
      <translation type="unfinished">OK</translation>
    </message>
  </context>
  <context>
    <name>DlgSheetConf</name>
    <message>
      <location filename="../../DlgSheetConf.ui" line="14"/>
      <source>Setup Configuration Table</source>
      <translation type="unfinished">Setup Configuration Table</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="20"/>
      <source>Cell range:</source>
      <translation type="unfinished">Cell range:</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="27"/>
      <source>Starting cell address.

The first column of the range is assumed to contain a list of configuration
names, which will be used to generate a string list and bind to the given
property for user to dynamically switch configuration.

The first row of the range will be bound to whatever row (indirectly) selected
by that property.
</source>
      <translation type="unfinished">Starting cell address.

The first column of the range is assumed to contain a list of configuration
names, which will be used to generate a string list and bind to the given
property for user to dynamically switch configuration.

The first row of the range will be bound to whatever row (indirectly) selected
by that property.
</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="42"/>
      <source>Ending cell address.

The first column of the range is assumed to contain a list of configuration
names, which will be used to generate a string list and bind to the given
property for user to dynamically switch configuration.

The first row of the range will be bound to whatever row (indirectly) selected
by that property.
</source>
      <translation type="unfinished">Ending cell address.

The first column of the range is assumed to contain a list of configuration
names, which will be used to generate a string list and bind to the given
property for user to dynamically switch configuration.

The first row of the range will be bound to whatever row (indirectly) selected
by that property.
</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="57"/>
      <source>Property:</source>
      <translation type="unfinished">Property:</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="64"/>
      <source>Type in an expression to specify the object and property name to dynamically
switch the design configuration. The property will be created if not exist.</source>
      <translation type="unfinished">Type in an expression to specify the object and property name to dynamically
switch the design configuration. The property will be created if not exist.</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="72"/>
      <source>Group:</source>
      <translation type="unfinished">Group:</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="79"/>
      <source>Optional property group name.</source>
      <translation type="unfinished">Optional property group name.</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="88"/>
      <source>Unsetup</source>
      <translation type="unfinished">Unsetup</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="95"/>
      <source>Cancel</source>
      <translation type="unfinished">Cancel</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.ui" line="102"/>
      <source>OK</source>
      <translation type="unfinished">OK</translation>
    </message>
  </context>
  <context>
    <name>PropertiesDialog</name>
    <message>
      <location filename="../../PropertiesDialog.ui" line="14"/>
      <source>Cell properties</source>
      <translation type="unfinished">Cell properties</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="27"/>
      <source>&amp;Color</source>
      <translation type="unfinished">&amp;Color</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="33"/>
      <source>Text</source>
      <translation type="unfinished">Text</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="43"/>
      <source>Background</source>
      <translation type="unfinished">Background</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="80"/>
      <source>&amp;Alignment</source>
      <translation type="unfinished">&amp;Alignment</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="88"/>
      <source>Horizontal</source>
      <translation type="unfinished">Horizontal</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="95"/>
      <source>Left</source>
      <translation type="unfinished">Left</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="102"/>
      <location filename="../../PropertiesDialog.ui" line="155"/>
      <source>Center</source>
      <translation type="unfinished">Center</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="109"/>
      <source>Right</source>
      <translation type="unfinished">Right</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="141"/>
      <source>Vertical</source>
      <translation type="unfinished">Vertical</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="148"/>
      <source>Top</source>
      <translation type="unfinished">Top</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="162"/>
      <source>Bottom</source>
      <translation type="unfinished">Bottom</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="188"/>
      <source>&amp;Style</source>
      <translation type="unfinished">&amp;Style</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="196"/>
      <source>Bold</source>
      <translation type="unfinished">Bold</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="203"/>
      <source>Italic</source>
      <translation type="unfinished">Italic</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="210"/>
      <source>Underline</source>
      <translation type="unfinished">Underline</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="233"/>
      <source>&amp;Display unit</source>
      <translation type="unfinished">&amp;Display unit</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="239"/>
      <source>Unit string</source>
      <translation type="unfinished">Unit string</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="263"/>
      <source>A&amp;lias</source>
      <translation type="unfinished">A&amp;lias</translation>
    </message>
    <message>
      <location filename="../../PropertiesDialog.ui" line="269"/>
      <source>Alias for this cell</source>
      <translation type="unfinished">Alias for this cell</translation>
    </message>
  </context>
  <context>
    <name>QObject</name>
    <message>
      <location filename="../../Command.cpp" line="196"/>
      <location filename="../../Command.cpp" line="252"/>
      <source>All (*)</source>
      <translation type="unfinished">All (*)</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="198"/>
      <source>Import file</source>
      <translation type="unfinished">Import file</translation>
    </message>
    <message>
      <location filename="../../Command.cpp" line="254"/>
      <source>Export file</source>
      <translation type="unfinished">Export file</translation>
    </message>
    <message>
      <location filename="../../ViewProviderSpreadsheet.cpp" line="141"/>
      <source>Show spreadsheet</source>
      <translation type="unfinished">Show spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="90"/>
      <location filename="../../Workbench.cpp" line="92"/>
      <source>Set cell(s) foreground color</source>
      <translation type="unfinished">Set cell(s) foreground color</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="91"/>
      <source>Sets the Spreadsheet cell(s) foreground color</source>
      <translation type="unfinished">Sets the Spreadsheet cell(s) foreground color</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="105"/>
      <location filename="../../Workbench.cpp" line="107"/>
      <source>Set cell(s) background color</source>
      <translation type="unfinished">Set cell(s) background color</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="106"/>
      <source>Sets the Spreadsheet cell(s) background color</source>
      <translation type="unfinished">Sets the Spreadsheet cell(s) background color</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="696"/>
      <source>Copy &amp; Paste failed</source>
      <translation type="unfinished">Copy &amp; Paste failed</translation>
    </message>
    <message>
      <location filename="../../SpreadsheetView.cpp" line="434"/>
      <source>Alias contains invalid characters!</source>
      <translation type="unfinished">Alias contains invalid characters!</translation>
    </message>
    <message>
      <location filename="../../SpreadsheetView.cpp" line="440"/>
      <source>Refer to cell by alias, for example
Spreadsheet.my_alias_name instead of Spreadsheet.B1</source>
      <translation type="unfinished">Refer to cell by alias, for example
Spreadsheet.my_alias_name instead of Spreadsheet.B1</translation>
    </message>
  </context>
  <context>
    <name>QtColorPicker</name>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="283"/>
      <location filename="../../qtcolorpicker.cpp" line="410"/>
      <location filename="../../qtcolorpicker.cpp" line="520"/>
      <source>Black</source>
      <translation type="unfinished">Black</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="411"/>
      <location filename="../../qtcolorpicker.cpp" line="521"/>
      <source>White</source>
      <translation type="unfinished">White</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="412"/>
      <location filename="../../qtcolorpicker.cpp" line="522"/>
      <source>Red</source>
      <translation type="unfinished">Red</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="413"/>
      <location filename="../../qtcolorpicker.cpp" line="523"/>
      <source>Dark red</source>
      <translation type="unfinished">Dark red</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="414"/>
      <location filename="../../qtcolorpicker.cpp" line="524"/>
      <source>Green</source>
      <translation type="unfinished">Green</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="415"/>
      <location filename="../../qtcolorpicker.cpp" line="525"/>
      <source>Dark green</source>
      <translation type="unfinished">Dark green</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="416"/>
      <location filename="../../qtcolorpicker.cpp" line="526"/>
      <source>Blue</source>
      <translation type="unfinished">Blue</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="417"/>
      <location filename="../../qtcolorpicker.cpp" line="527"/>
      <source>Dark blue</source>
      <translation type="unfinished">Dark blue</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="418"/>
      <location filename="../../qtcolorpicker.cpp" line="528"/>
      <source>Cyan</source>
      <translation type="unfinished">Cyan</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="419"/>
      <location filename="../../qtcolorpicker.cpp" line="529"/>
      <source>Dark cyan</source>
      <translation type="unfinished">Dark cyan</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="420"/>
      <location filename="../../qtcolorpicker.cpp" line="530"/>
      <source>Magenta</source>
      <translation type="unfinished">Magenta</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="421"/>
      <location filename="../../qtcolorpicker.cpp" line="531"/>
      <source>Dark magenta</source>
      <translation type="unfinished">Dark magenta</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="422"/>
      <location filename="../../qtcolorpicker.cpp" line="532"/>
      <source>Yellow</source>
      <translation type="unfinished">Yellow</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="423"/>
      <location filename="../../qtcolorpicker.cpp" line="533"/>
      <source>Dark yellow</source>
      <translation type="unfinished">Dark yellow</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="424"/>
      <location filename="../../qtcolorpicker.cpp" line="534"/>
      <source>Gray</source>
      <translation type="unfinished">Gray</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="425"/>
      <location filename="../../qtcolorpicker.cpp" line="535"/>
      <source>Dark gray</source>
      <translation type="unfinished">Dark gray</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="426"/>
      <location filename="../../qtcolorpicker.cpp" line="536"/>
      <source>Light gray</source>
      <translation type="unfinished">Light gray</translation>
    </message>
    <message>
      <location filename="../../qtcolorpicker.cpp" line="448"/>
      <source>Custom Color</source>
      <translation type="unfinished">Custom Color</translation>
    </message>
  </context>
  <context>
    <name>Sheet</name>
    <message>
      <location filename="../../Sheet.ui" line="14"/>
      <source>Form</source>
      <translation type="unfinished">Form</translation>
    </message>
    <message>
      <location filename="../../Sheet.ui" line="22"/>
      <source>&amp;Content:</source>
      <translation type="unfinished">&amp;Content:</translation>
    </message>
    <message>
      <location filename="../../Sheet.ui" line="39"/>
      <source>&amp;Alias:</source>
      <translation type="unfinished">&amp;Alias:</translation>
    </message>
    <message>
      <location filename="../../Sheet.ui" line="52"/>
      <source>Refer to cell by alias, for example
Spreadsheet.my_alias_name instead of Spreadsheet.B1</source>
      <translation type="unfinished">Refer to cell by alias, for example
Spreadsheet.my_alias_name instead of Spreadsheet.B1</translation>
    </message>
  </context>
  <context>
    <name>Spreadsheet</name>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="870"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="879"/>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1032"/>
      <source>Cell</source>
      <translation type="unfinished">Cell</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="882"/>
      <source>Apply</source>
      <translation type="unfinished">Apply</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="886"/>
      <source>Apply the changes to the current cell</source>
      <translation type="unfinished">Apply the changes to the current cell</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="888"/>
      <source>Delete</source>
      <translation type="unfinished">Delete</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="892"/>
      <source>Deletes the contents of the current cell</source>
      <translation type="unfinished">Deletes the contents of the current cell</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="894"/>
      <source>Compute</source>
      <translation type="unfinished">Compute</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="898"/>
      <source>Updates the values handled by controllers</source>
      <translation type="unfinished">Updates the values handled by controllers</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1076"/>
      <source>Create Spreadsheet</source>
      <translation type="unfinished">Create Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1114"/>
      <source>Add controller</source>
      <translation type="unfinished">Add controller</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1155"/>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1179"/>
      <source>Add property controller</source>
      <translation type="unfinished">Add property controller</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetGui::DlgBindSheet</name>
    <message>
      <location filename="../../DlgBindSheet.cpp" line="180"/>
      <source>Bind cells</source>
      <translation type="unfinished">Bind cells</translation>
    </message>
    <message>
      <location filename="../../DlgBindSheet.cpp" line="197"/>
      <source>Unbind cells</source>
      <translation type="unfinished">Unbind cells</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetGui::DlgSettings</name>
    <message>
      <location filename="../../DlgSettings.ui" line="20"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="26"/>
      <source>Import/Export Settings</source>
      <translation type="unfinished">Import/Export Settings</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="38"/>
      <source>Delimiter Character: </source>
      <translation type="unfinished">Delimiter Character: </translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="58"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Character to use as field delimiter.  Default is tab, but also commonly used are commas (,) and semicolons (;). Select from the list or enter your own in the field. Must be a single character or the words &lt;span style=&quot; font-style:italic;&quot;&gt;tab&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;comma&lt;/span&gt;, or &lt;span style=&quot; font-style:italic;&quot;&gt;semicolon&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Character to use as field delimiter.  Default is tab, but also commonly used are commas (,) and semicolons (;). Select from the list or enter your own in the field. Must be a single character or the words &lt;span style=&quot; font-style:italic;&quot;&gt;tab&lt;/span&gt;, &lt;span style=&quot; font-style:italic;&quot;&gt;comma&lt;/span&gt;, or &lt;span style=&quot; font-style:italic;&quot;&gt;semicolon&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="74"/>
      <source>tab</source>
      <translation type="unfinished">tab</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="79"/>
      <source>;</source>
      <translation type="unfinished">;</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="84"/>
      <source>,</source>
      <translation type="unfinished">,</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="98"/>
      <source>Quote Character: </source>
      <translation type="unfinished">Quote Character: </translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="105"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Character used to delimit strings, typically is single quote (&apos;) or double quote (&amp;quot;). Must be a single character.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Character used to delimit strings, typically is single quote (&apos;) or double quote (&amp;quot;). Must be a single character.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="111"/>
      <source>&quot;</source>
      <translation type="unfinished">&quot;</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="130"/>
      <source>Escape Character: </source>
      <translation type="unfinished">Escape Character: </translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="137"/>
      <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Escape character, typically the backslash (\), used to indicate special unprintable characters, e.g. \t = tab. Must be a single character.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
      <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Escape character, typically the backslash (\), used to indicate special unprintable characters, e.g. \t = tab. Must be a single character.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
      <location filename="../../DlgSettings.ui" line="140"/>
      <source>\</source>
      <translation type="unfinished">\</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetGui::DlgSheetConf</name>
    <message>
      <location filename="../../DlgSheetConf.cpp" line="245"/>
      <source>Setup configuration table</source>
      <translation type="unfinished">Setup configuration table</translation>
    </message>
    <message>
      <location filename="../../DlgSheetConf.cpp" line="292"/>
      <source>Unsetup configuration table</source>
      <translation type="unfinished">Unsetup configuration table</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetGui::Module</name>
    <message>
      <location filename="../../AppSpreadsheetGui.cpp" line="88"/>
      <source>Unnamed</source>
      <translation type="unfinished">Unnamed</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetGui::SheetTableView</name>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="129"/>
      <source>Insert %n row(s) above</source>
      <translation type="unfinished">
        <numerusform>Insert %n row(s) above</numerusform>
        <numerusform>Insert %n row(s) above</numerusform>
        <numerusform>Insert %n row(s) above</numerusform>
        <numerusform>Insert %n row(s) above</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="133"/>
      <source>Insert %n row(s) below</source>
      <translation type="unfinished">
        <numerusform>Insert %n row(s) below</numerusform>
        <numerusform>Insert %n row(s) below</numerusform>
        <numerusform>Insert %n row(s) below</numerusform>
        <numerusform>Insert %n row(s) below</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="137"/>
      <source>Insert %n non-contiguous rows</source>
      <translation type="unfinished">
        <numerusform>Insert %n non-contiguous rows</numerusform>
        <numerusform>Insert %n non-contiguous rows</numerusform>
        <numerusform>Insert %n non-contiguous rows</numerusform>
        <numerusform>Insert %n non-contiguous rows</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="140"/>
      <source>Remove row(s)</source>
      <translation type="unfinished">
        <numerusform>Remove row(s)</numerusform>
        <numerusform>Remove row(s)</numerusform>
        <numerusform>Remove row(s)</numerusform>
        <numerusform>Remove row(s)</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="154"/>
      <source>Insert %n column(s) left</source>
      <translation type="unfinished">
        <numerusform>Insert %n column(s) left</numerusform>
        <numerusform>Insert %n column(s) left</numerusform>
        <numerusform>Insert %n column(s) left</numerusform>
        <numerusform>Insert %n column(s) left</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="158"/>
      <source>Insert %n column(s) right</source>
      <translation type="unfinished">
        <numerusform>Insert %n column(s) right</numerusform>
        <numerusform>Insert %n column(s) right</numerusform>
        <numerusform>Insert %n column(s) right</numerusform>
        <numerusform>Insert %n column(s) right</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="162"/>
      <source>Insert %n non-contiguous columns</source>
      <translation type="unfinished">
        <numerusform>Insert %n non-contiguous columns</numerusform>
        <numerusform>Insert %n non-contiguous columns</numerusform>
        <numerusform>Insert %n non-contiguous columns</numerusform>
        <numerusform>Insert %n non-contiguous columns</numerusform>
      </translation>
    </message>
    <message numerus="yes">
      <location filename="../../SheetTableView.cpp" line="165"/>
      <source>Remove column(s)</source>
      <translation type="unfinished">
        <numerusform>Remove column(s)</numerusform>
        <numerusform>Remove column(s)</numerusform>
        <numerusform>Remove column(s)</numerusform>
        <numerusform>Remove column(s)</numerusform>
      </translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="170"/>
      <source>Properties...</source>
      <translation type="unfinished">Properties...</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="182"/>
      <source>Recompute</source>
      <translation type="unfinished">Recompute</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="186"/>
      <source>Bind...</source>
      <translation type="unfinished">Bind...</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="190"/>
      <source>Configuration table...</source>
      <translation type="unfinished">Configuration table...</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="198"/>
      <source>Merge cells</source>
      <translation type="unfinished">Merge cells</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="200"/>
      <source>Split cells</source>
      <translation type="unfinished">Split cells</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="204"/>
      <source>Cut</source>
      <translation type="unfinished">Cut</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="206"/>
      <source>Copy</source>
      <translation type="unfinished">Copy</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="208"/>
      <source>Paste</source>
      <translation type="unfinished">Paste</translation>
    </message>
    <message>
      <location filename="../../SheetTableView.cpp" line="210"/>
      <source>Delete</source>
      <translation type="unfinished">Delete</translation>
    </message>
  </context>
  <context>
    <name>SpreadsheetGui::SheetView</name>
    <message>
      <location filename="../../SpreadsheetView.cpp" line="262"/>
      <source>Export PDF</source>
      <translation type="unfinished">Export PDF</translation>
    </message>
    <message>
      <location filename="../../SpreadsheetView.cpp" line="263"/>
      <source>PDF file</source>
      <translation type="unfinished">PDF file</translation>
    </message>
  </context>
  <context>
    <name>Spreadsheet_Controller</name>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1093"/>
      <source>Add controller</source>
      <translation type="unfinished">Add controller</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1097"/>
      <source>Adds a cell controller to a selected spreadsheet</source>
      <translation type="unfinished">Adds a cell controller to a selected spreadsheet</translation>
    </message>
  </context>
  <context>
    <name>Spreadsheet_Create</name>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1066"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1069"/>
      <source>Adds a spreadsheet object to the active document</source>
      <translation type="unfinished">Adds a spreadsheet object to the active document</translation>
    </message>
  </context>
  <context>
    <name>Spreadsheet_PropertyController</name>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1134"/>
      <source>Add property controller</source>
      <translation type="unfinished">Add property controller</translation>
    </message>
    <message>
      <location filename="../../../App/Spreadsheet_legacy.py" line="1138"/>
      <source>Adds a property controller to a selected spreadsheet</source>
      <translation type="unfinished">Adds a property controller to a selected spreadsheet</translation>
    </message>
  </context>
  <context>
    <name>Workbench</name>
    <message>
      <location filename="../../Workbench.cpp" line="49"/>
      <source>Spreadsheet</source>
      <translation type="unfinished">Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="50"/>
      <source>&amp;Spreadsheet</source>
      <translation type="unfinished">&amp;Spreadsheet</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="51"/>
      <source>&amp;Alignment</source>
      <translation type="unfinished">&amp;Alignment</translation>
    </message>
    <message>
      <location filename="../../Workbench.cpp" line="52"/>
      <source>&amp;Styles</source>
      <translation type="unfinished">&amp;Styles</translation>
    </message>
  </context>
</TS>
