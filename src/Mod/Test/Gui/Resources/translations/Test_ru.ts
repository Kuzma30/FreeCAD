<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
  <context>
    <name>TestGui::UnitTest</name>
    <message>
      <location filename="../../UnitTest.ui" line="17"/>
      <source>FreeCAD UnitTest</source>
      <translation type="unfinished">FreeCAD UnitTest</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="32"/>
      <source>Test</source>
      <translation type="unfinished">Test</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="44"/>
      <source>Select test name:</source>
      <translation type="unfinished">Select test name:</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="96"/>
      <source>&amp;Start</source>
      <translation type="unfinished">&amp;Start</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="99"/>
      <source>Alt+S</source>
      <translation type="unfinished">Alt+S</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="128"/>
      <source>&amp;Help</source>
      <translation type="unfinished">&amp;Help</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="131"/>
      <source>F1</source>
      <translation type="unfinished">F1</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="141"/>
      <source>&amp;About</source>
      <translation type="unfinished">&amp;About</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="144"/>
      <source>Alt+A</source>
      <translation type="unfinished">Alt+A</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="154"/>
      <source>&amp;Close</source>
      <translation type="unfinished">&amp;Close</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="157"/>
      <source>Alt+C</source>
      <translation type="unfinished">Alt+C</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="169"/>
      <source>Progress</source>
      <translation type="unfinished">Progress</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="188"/>
      <source>Run:</source>
      <translation type="unfinished">Run:</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="205"/>
      <source>Failures:</source>
      <translation type="unfinished">Failures:</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="222"/>
      <source>Errors:</source>
      <translation type="unfinished">Errors:</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="239"/>
      <source>Remaining:</source>
      <translation type="unfinished">Remaining:</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="259"/>
      <source>Failures and errors</source>
      <translation type="unfinished">Failures and errors</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="275"/>
      <source>Description</source>
      <translation type="unfinished">Description</translation>
    </message>
    <message>
      <location filename="../../UnitTest.ui" line="292"/>
      <source>Idle</source>
      <translation type="unfinished">Idle</translation>
    </message>
  </context>
  <context>
    <name>TestGui::UnitTestDialog</name>
    <message>
      <location filename="../../UnitTestImp.cpp" line="155"/>
      <source>Help</source>
      <translation type="unfinished">Help</translation>
    </message>
    <message>
      <location filename="../../UnitTestImp.cpp" line="155"/>
      <source>Enter the name of a callable object which, when called, will return a TestCase.
Click &apos;start&apos;, and the test thus produced will be run.

Double click on an error in the tree view to see more information about it, including the stack trace.</source>
      <translation type="unfinished">Enter the name of a callable object which, when called, will return a TestCase.
Click &apos;start&apos;, and the test thus produced will be run.

Double click on an error in the tree view to see more information about it, including the stack trace.</translation>
    </message>
    <message>
      <location filename="../../UnitTestImp.cpp" line="167"/>
      <source>About FreeCAD UnitTest</source>
      <translation type="unfinished">About FreeCAD UnitTest</translation>
    </message>
    <message>
      <location filename="../../UnitTestImp.cpp" line="167"/>
      <source>Copyright (c) Werner Mayer

FreeCAD UnitTest is part of FreeCAD and supports writing Unit Tests for ones own modules.</source>
      <translation type="unfinished">Copyright (c) Werner Mayer

FreeCAD UnitTest is part of FreeCAD and supports writing Unit Tests for ones own modules.</translation>
    </message>
  </context>
</TS>
